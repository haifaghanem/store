
function scrollToTop() {
    $('html, body').animate({
        scrollTop: 0
    }, 800);
    return false;
}

$(document).on('click', '.ui-dialog-mask', function(e) {

    idModal = this.id;
    console.log(idModal);
    idModal = idModal.replace("_modal","");
    getWidgetVarById(idModal).hide();
});

function getWidgetVarById(id) {
    for (var propertyName in PrimeFaces.widgets) {
        var widget = PrimeFaces.widgets[propertyName];
        if (widget && widget.id === id) {
            return widget;
        }
    }
}

PrimeFaces.vb = function (cfg) {

    //Call Method From Primefaces Validation
    var valid = this.validate(cfg);
    // if Validation is Error and enable scroll Then scrollTop
    if (!valid && !$('body').hasClass('disableScroll')) {
        scrollToTop();
    }

    return valid;
};

PrimeFaces.widget.CommandButton = PrimeFaces.widget.CommandButton.extend({

    init: function (cfg) {
        this._super(cfg);
        if (this.jq.hasClass('NO-PF-CLASS')) {
            this.jq.removeClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-left');
        }
        if (this.jq.hasClass('custom-disabled')) {
            var elem = "<i class='custom-disabled'></i>";
            $(this.jq).append(elem);
        }
    },


});


$.fn.wrapInTag = function (opts) {
    if (opts.words == null || opts.words == '' || !opts.words) {
        return;
    }
    var tag = opts.tag || 'strong'
        , words = opts.words || []
        , regex = RegExp(words.join('|'), 'gi') // case insensitive
        ,
        replacement = '<' + tag + ' style="color: #dc3545;font-weight: bold;">$&</' + tag + '>';

    return this.html(function () {
        return $(this).text().replace(regex, replacement);
    });
};



$(document).ready(function () {
    localStorage.setItem('storage-event-refresh', $.cookie('refresh'));
});

function addEvent(to, type, fn) {
    if (document.addEventListener) {
        to.addEventListener(type, fn, false);
    } else if (document.attachEvent) {
        to.attachEvent('on' + type, fn);
    } else {
        to['on' + type] = fn;
    }
}

addEvent(window, 'storage', function (event) {
    if (event.key == 'storage-event-refresh') {
       // location.reload();
    }
});


$(function () {
    try {

        $('.niceSelect').niceSelect();
    } catch (e) {

    }
})
$(document).on('pfAjaxComplete', function () {
    try {

        $('.niceSelect').niceSelect();
    } catch (e) {

    }
});