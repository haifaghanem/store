package com.ellepharma.web.component;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LocalCache {
    @Bean
    public CacheManager createSimpleCacheManager() {
        SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
        List<Cache> caches = new ArrayList<>(2);
        caches.add(new ConcurrentMapCache("task"));
        caches.add(new ConcurrentMapCache("item"));
        caches.add(new ConcurrentMapCache("menu"));
        caches.add(new ConcurrentMapCache("MenuString"));
        simpleCacheManager.setCaches(caches);
        return simpleCacheManager;
    }
}