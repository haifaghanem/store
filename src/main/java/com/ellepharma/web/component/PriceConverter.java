package com.ellepharma.web.component;

import com.ellepharma.utils.Utils;
import org.apache.commons.lang3.StringUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.text.DecimalFormat;

@FacesConverter("priceConverter")
@ManagedBean
@RequestScoped
public class PriceConverter implements Converter {


    private boolean leftPosition = false;

	private String pattern = "###,###,###.###";

    /**
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return Utils.convertPrice(new Double(value));
    }

    /**
     * @param context
     * @param component
     * @param object
     * @return
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        String code = Utils.getCurrentCurrency().getCurrencyCode();
        if (object==null|| StringUtils.isEmpty(object.toString().trim()))
            object="0.0";

        return (leftPosition ? code : "") + " " + priceWithDecimal(new Double(object.toString()),component) +     " " + (!leftPosition ? code : "");
    }

    /**
     * @param price
     * @param component
     * @return
     */
    public String priceWithDecimal(Double price, UIComponent component) {
        DecimalFormat formatter = new DecimalFormat(pattern);
        Long     qty = (Long) component.getAttributes().get("qty");
        if (qty==null){
        return formatter.format(Utils.convertPrice(price));

        }
        return formatter.format(Utils.convertPrice(price)*qty);

    }




}