package com.ellepharma.web.component;

import org.primefaces.extensions.component.inputphone.InputPhone;
import org.primefaces.extensions.util.MessageFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

public class PhoneValidators {

    public PhoneValidators() {
    }

    public void validate(FacesContext context, UIComponent component, Object object) throws ValidatorException {
        InputPhone inputPhone = (InputPhone) component;
        String country = context.getExternalContext().getRequestParameterMap().get(inputPhone.getClientId() + "_iso2");
        if (country == null || "auto".equals(country)) {
            country = "";
        }

       /* try {
            PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse((String) object, country);
            if (!phoneNumberUtil.isValidNumber(phoneNumber)) {
                throw new ValidatorException(this.getMessage());
            }
        } catch (NumberParseException var8) {
            throw new ValidatorException(this.getMessage());
        }*/
    }

    protected FacesMessage getMessage() {
        return MessageFactory.getMessage("primefaces.extensions.inputphone.INVALID", FacesMessage.SEVERITY_ERROR);
    }
}