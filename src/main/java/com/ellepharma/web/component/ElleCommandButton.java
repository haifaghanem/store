package com.ellepharma.web.component;

import org.primefaces.component.commandbutton.CommandButton;

import javax.faces.context.FacesContext;
import java.io.IOException;

public class ElleCommandButton extends CommandButton {


    @Override
    public void encodeBegin(FacesContext context) throws IOException {

        setValidateClient(true);
        super.encodeBegin(context);
    }

    @Override
    public String getStyleClass() {

        return super.getStyleClass();
    }

    private boolean notUsePFStyle() {
        Object notUsePFStyle = getAttributes().get("notUsePFStyle");
        return notUsePFStyle != null && (new Boolean(notUsePFStyle.toString()));
    }
}
