package com.ellepharma.web.component;

import com.ellepharma.data.model.Category;
import com.ellepharma.service.CategoryService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("categoryConverter")
@ManagedBean
@RequestScoped
public class CategoryConverter implements Converter {


    @ManagedProperty("#{categoryService}")
    private CategoryService categoryService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String newValue) {
        return categoryService.findById(Long.valueOf(newValue));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object == null) {
            return "";
        }
        if (object instanceof Category) {
            Category category = (Category) object;

            return category.getId().toString();
        } else {
            throw new ConverterException(new FacesMessage(object + " is not a valid car"));
        }
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
}