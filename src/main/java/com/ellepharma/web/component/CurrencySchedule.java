package com.ellepharma.web.component;

import com.ellepharma.data.model.CurrencyResponse;
import com.ellepharma.service.CurrencyService;
import com.ellepharma.service.GlobalConfigProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Category.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 09, 2019
 */

@Component
public class CurrencySchedule {


    @Autowired
    private CurrencyService currencyService;

    public CurrencySchedule() {
    }

    @Scheduled(cron = "0 0 23 * * *")
    public void currencyScheduleTaskWithCronExpression() {


        if (!update(GlobalConfigProperties.getInstance().getProperty("apilayer.key1"))) {
            update(GlobalConfigProperties.getInstance().getProperty("apilayer.key2"));
        }
    }

    public static void main(String[] args) {
        CurrencySchedule v = new CurrencySchedule();

        if (!v.update("dfa2013eeb8d13ea65b3a8d3fda52805")) {
            if (!v.update("e978e372595dfb3177a22c794a6dcadd")) {
                v.update("2705790097a3a11f9b23c92610cbbf36");

            }
        }

    }

    public boolean update(String key) {
        try {
            HttpResponse<String> response = Unirest.get("http://apilayer.net/api/live").queryString("access_key", key)
                    .asString();
            ObjectMapper mapper = new ObjectMapper();
            CurrencyResponse currencyResponse = mapper.readValue(response.getBody(), CurrencyResponse.class);
            currencyService.update(currencyResponse);

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        System.out.println("*********************** currency is updated successfully ***********");
        return true;
    }

}
