package com.ellepharma.web.component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;

public class ShowImage extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowImage() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sourceFile = request.getParameter("Path");

        try {
            sourceFile = sourceFile.substring(1);
            InputStream in = new BufferedInputStream(
                    new FileInputStream("/" + sourceFile));

            String s = URLConnection.guessContentTypeFromStream(in);
            response.setContentType(s);

            byte[] pic = new byte[in.available()];
            in.read(pic);

            OutputStream out = response.getOutputStream();
            out.write(pic);
            in.close();
            out.close();
        } catch (Exception e) {

        }
    }

}
