package com.ellepharma.web.component;

import com.ellepharma.data.repository.OrderRepo;
import com.ellepharma.service.OrderService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Category.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 09, 2019
 */

@Component
public class OrderSchedule {

    @Autowired
    private OrderRepo orderRepo;
    @Autowired
    private OrderService orderService;

    public OrderSchedule() {


    }

    @Scheduled(cron = "0 0/10 * * * ?")
    public void currencyScheduleTaskWithCronExpression() {

        List<String> orders = orderRepo.findPaymentIDByPaymentStatusAndPaymentType(DateUtils.addMinutes(new Date(), -10));
        if (CollectionUtils.isNotEmpty(orders)) {
            orders.forEach(paymentId -> {
                try {
                    orderService.checkOrderByPaymentId(paymentId);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });

        }
    }


}
