package com.ellepharma.web.api;


public class CreatePayPageRequest {
    private String result;
    private String response_code;
    private String payment_url;
    private String p_id;


    // Getter Methods

    public String getResult() {
        return result;
    }

    public String getResponse_code() {
        return response_code;
    }

    public String getPayment_url() {
        return payment_url;
    }

    public String getP_id() {
        return p_id;
    }

    // Setter Methods

    public void setResult(String result) {
        this.result = result;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    public void setPayment_url(String payment_url) {
        this.payment_url = payment_url;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }
}