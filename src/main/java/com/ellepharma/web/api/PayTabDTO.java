package com.ellepharma.web.api;

public class PayTabDTO {

    private String title;
    private String ccFirstName;
    private String ccLastName;
    private String ccPhoneNumber;
    private String phoneNumber;
    private String email;
    private String productsPerTitle;
    private String unitPrice;
    private String quantity;
    private Float otherCharges;
    private Float amount;
    private Float discount;
    private String referenceNo;
    private String currency;
    private String ipCustomer;
    private String ipMerchant;
    private String billingAddress;
    private String state;
    private String city;
    private String postalCode;
    private String country;
    private String addressShipping;
    private String stateShipping;
    private String cityShipping;
    private String postalCodeShipping;
    private String countryShipping;
    private String msgLang;
    private String cmsWithVersion;
    private String paypageInfo;
    private String shippingFirstName;
    private String shippingLastName;


    public PayTabDTO() {
        quantity = "1 || 1 || 1";
        unitPrice = "2 || 2 || 6";
        productsPerTitle = "Product1 || Product 2 || Product 4";
        paypageInfo = "12331";
        cmsWithVersion = "JavaCode1.0";
        ipCustomer = "192.168.11.1";
        ipMerchant = "192.168.11.1";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCcFirstName() {
        return ccFirstName;
    }

    public void setCcFirstName(String ccFirstName) {
        this.ccFirstName = ccFirstName;
    }

    public String getCcLastName() {
        return ccLastName;
    }

    public void setCcLastName(String ccLastName) {
        this.ccLastName = ccLastName;
    }

    public String getCcPhoneNumber() {
        return ccPhoneNumber;
    }

    public void setCcPhoneNumber(String ccPhoneNumber) {
        this.ccPhoneNumber = ccPhoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProductsPerTitle() {
        return productsPerTitle;
    }

    public void setProductsPerTitle(String productsPerTitle) {
        this.productsPerTitle = productsPerTitle;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Float getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(Float otherCharges) {
        this.otherCharges = otherCharges;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIpCustomer() {
        return ipCustomer;
    }

    public void setIpCustomer(String ipCustomer) {
        this.ipCustomer = ipCustomer;
    }

    public String getIpMerchant() {
        return ipMerchant;
    }

    public void setIpMerchant(String ipMerchant) {
        this.ipMerchant = ipMerchant;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddressShipping() {
        return addressShipping;
    }

    public void setAddressShipping(String addressShipping) {
        this.addressShipping = addressShipping;
    }

    public String getStateShipping() {
        return stateShipping;
    }

    public void setStateShipping(String stateShipping) {
        this.stateShipping = stateShipping;
    }

    public String getCityShipping() {
        return cityShipping;
    }

    public void setCityShipping(String cityShipping) {
        this.cityShipping = cityShipping;
    }

    public String getPostalCodeShipping() {
        return postalCodeShipping;
    }

    public void setPostalCodeShipping(String postalCodeShipping) {
        this.postalCodeShipping = postalCodeShipping;
    }

    public String getCountryShipping() {
        return countryShipping;
    }

    public void setCountryShipping(String countryShipping) {
        this.countryShipping = countryShipping;
    }

    public String getMsgLang() {
        return msgLang;
    }

    public void setMsgLang(String msgLang) {
        this.msgLang = msgLang;
    }

    public String getCmsWithVersion() {
        return cmsWithVersion;
    }

    public void setCmsWithVersion(String cmsWithVersion) {
        this.cmsWithVersion = cmsWithVersion;
    }

    public String getPaypageInfo() {
        return paypageInfo;
    }

    public void setPaypageInfo(String paypageInfo) {
        this.paypageInfo = paypageInfo;
    }

    public String getShippingFirstName() {
        return shippingFirstName;
    }

    public void setShippingFirstName(String shippingFirstName) {
        this.shippingFirstName = shippingFirstName;
    }

    public String getShippingLastName() {
        return shippingLastName;
    }

    public void setShippingLastName(String shippingLastName) {
        this.shippingLastName = shippingLastName;
    }
}
