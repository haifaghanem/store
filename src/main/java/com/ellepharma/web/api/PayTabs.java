package com.ellepharma.web.api;

import com.ellepharma.constant.ConfigEnum;
import com.ellepharma.data.dto.VerifyRequest;
import com.ellepharma.service.GlobalConfigProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;


public class PayTabs {
    private String payTabsUrl;//= "https://www.paytabs.com/apiv2/";
    private String merchantEmail;//= "nameeras@ellepharma.com";
    private String merchantSecretKey;// = "YihON2RBKChAgHv40vt5p8BPtyas38q2qIReKIQvl75BOAGP5aVuixkM5ue9DFb4o3i8L9E7gJI5kpVqzQVSnfEsJTCnyOrWRK5D";
    private String siteUrl;//= "https://www.ellebpharmacy.com/";
    private String siteReturnUrl;// = "https://www.ellebpharmacy.com/";

    public PayTabs() {
        payTabsUrl =GlobalConfigProperties.getInstance().getProperty("payTabsUrl");
        merchantEmail =GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MERCHANT_EMAIL.name());
        merchantSecretKey =GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MERCHANT_SECRET_KEY.name());
        siteUrl =GlobalConfigProperties.getInstance().getProperty(ConfigEnum.SITE_URL.name());
        // siteReturnUrl =GlobalConfigProperties.getInstance().getProperty("siteReturnUrl");
    }

    public String authentication_request(String urlParameters) {
        try {

            urlParameters = "merchant_email=" + URLEncoder.encode(merchantEmail, "UTF-8") + "&secret_key="
                    + URLEncoder.encode(merchantSecretKey, "UTF-8");
            return connection_request("validate_secret_key/", urlParameters);
        } catch (Exception e) {
            return null;
        }
    }

    public CreatePayPageRequest createPayPageRequest(PayTabDTO payTabDTO) throws IOException, ParseException {

//        String response = authentication_request("");
        String response;

        String urlParameters;
        urlParameters =
                "&cc_first_name=" + URLEncoder.encode(payTabDTO.getCcFirstName(), "UTF-8")
                        + "&cc_last_name=" + URLEncoder.encode(payTabDTO.getCcLastName(), "UTF-8")
                        + "&cc_phone_number=" + URLEncoder.encode(payTabDTO.getCcPhoneNumber(), "UTF-8")
                        + "&phone_number=" + URLEncoder.encode(payTabDTO.getPhoneNumber().replaceAll(" ",""), "UTF-8")
                        + "&billing_address=" + URLEncoder.encode(payTabDTO.getBillingAddress(), "UTF-8")
                        + "&city=" + URLEncoder.encode(payTabDTO.getCity(), "UTF-8")
                        + "&state=" + URLEncoder.encode(payTabDTO.getState(), "UTF-8")
                        + "&postal_code=" + URLEncoder.encode(payTabDTO.getPostalCode(), "UTF-8")
                        + "&country=" + URLEncoder.encode(payTabDTO.getCountry(), "UTF-8")
                        + "&address_shipping=" + URLEncoder.encode(payTabDTO.getAddressShipping(), "UTF-8")
                        + "&city_shipping=" + URLEncoder.encode(payTabDTO.getCityShipping(), "UTF-8")
                        + "&state_shipping=" + URLEncoder.encode(payTabDTO.getStateShipping(), "UTF-8")
                        + "&postal_code_shipping=" + URLEncoder.encode(payTabDTO.getPostalCode(), "UTF-8")
                        + "&country_shipping=" + URLEncoder.encode(payTabDTO.getCountryShipping(), "UTF-8")
                        + "&email=" + URLEncoder.encode(payTabDTO.getEmail(), "UTF-8")
                        + "&amount=" + URLEncoder.encode(payTabDTO.getAmount().toString(), "UTF-8")
                        + "&currency=" + URLEncoder.encode(payTabDTO.getCurrency(), "UTF-8")
                        + "&title=" + URLEncoder.encode(payTabDTO.getTitle(), "UTF-8")
                        + "&discount=" + URLEncoder.encode(payTabDTO.getDiscount().toString(), "UTF-8")
                        + "&msg_lang=" + URLEncoder.encode(payTabDTO.getMsgLang(), "UTF-8")
                        + "&quantity=" + URLEncoder.encode(payTabDTO.getQuantity(), "UTF-8")
                        + "&unit_price=" + URLEncoder.encode(payTabDTO.getUnitPrice(), "UTF-8")
                        + "&products_per_title=" + URLEncoder.encode(payTabDTO.getProductsPerTitle(), "UTF-8")
                        // + "&paypage_info=" + URLEncoder.encode(payTabDTO.getPaypageInfo(), "UTF-8")
                        + "&reference_no=" + URLEncoder.encode(payTabDTO.getReferenceNo(), "UTF-8")
                        + "&site_url=" + URLEncoder.encode(siteUrl, "UTF-8")
                        + "&return_url=" + URLEncoder.encode(siteReturnUrl, "UTF-8")
                        + "&cms_with_version=" + URLEncoder.encode(payTabDTO.getCmsWithVersion(), "UTF-8")
                        + "&other_charges=" + URLEncoder.encode(payTabDTO.getOtherCharges().toString(), "UTF-8")
                        + "&shipping_first_name=" + URLEncoder.encode(payTabDTO.getShippingFirstName(), "UTF-8")
                        + "&shipping_last_name=" + URLEncoder.encode(payTabDTO.getShippingLastName(), "UTF-8")
                        + "&ip_customer=" + URLEncoder.encode(payTabDTO.getIpCustomer(), "UTF-8")
                        + "&ip_merchant=" + URLEncoder.encode(payTabDTO.getIpMerchant(), "UTF-8");
        response = connection_request("create_pay_page/", urlParameters);

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(response, CreatePayPageRequest.class);

    }

    public VerifyRequest verifyPayment(String id) {
        try {
            String urlParameters = "&payment_reference=" + URLEncoder.encode(id, "UTF-8");
            String response = connection_request("verify_payment/", urlParameters);
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(response, VerifyRequest.class);
        } catch (Exception e) {
            return null;
        }
    }
    public VerifyRequest verifyPayment2(String id) throws UnirestException {

        HttpResponse<VerifyRequest> j= Unirest.post(GlobalConfigProperties.getInstance().getProperty("CreateShipments.url"))
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .body("")
                .asObject(VerifyRequest.class);

        try {
            String urlParameters = "&payment_reference=" + URLEncoder.encode(id, "UTF-8");
            String response = connection_request("", urlParameters);
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(response, VerifyRequest.class);
        } catch (Exception e) {
            return null;
        }
    }

    private String connection_request(String targetURL, String urlParameters) throws ParseException {
        URL url;
        HttpURLConnection connection = null;
        try {
            url = new URL(payTabsUrl + targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            String mandatoryParameters = "merchant_email=" + URLEncoder.encode(merchantEmail, "UTF-8") + "&secret_key=" + URLEncoder.encode(merchantSecretKey, "UTF-8");
            urlParameters = mandatoryParameters + urlParameters;
          //  System.out.println(urlParameters + " \n");
            connection.setRequestProperty("Content-Length", "" + urlParameters.getBytes().length);
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.out.println("Output from Server .... \n" + connection.getResponseCode());
            }

            //Get Response	
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
           // System.out.println("Output from Server .... \n");
            while ((line = rd.readLine()) != null) {
                return line;
            }
            rd.close();

        } catch (IOException ex) {
            System.err.println("An IOException was caught!");
            ex.printStackTrace();
        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
        return "";
    }

    public String getPayTabsUrl() {
        return payTabsUrl;
    }

    public void setPayTabsUrl(String payTabsUrl) {
        this.payTabsUrl = payTabsUrl;
    }

    public String getMerchantEmail() {
        return merchantEmail;
    }

    public void setMerchantEmail(String merchantEmail) {
        this.merchantEmail = merchantEmail;
    }

    public String getMerchantSecretKey() {
        return merchantSecretKey;
    }

    public void setMerchantSecretKey(String merchantSecretKey) {
        this.merchantSecretKey = merchantSecretKey;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public String getSiteReturnUrl() {
        return siteReturnUrl;
    }

    public void setSiteReturnUrl(String siteReturnUrl) {
        this.siteReturnUrl = siteReturnUrl;
    }
}
