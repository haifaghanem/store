package com.ellepharma.web.api;

import com.ellepharma.data.dto.*;
import com.ellepharma.service.GlobalConfigProperties;
import com.ellepharma.web.aramex.dto.*;
import com.ellepharma.web.aramex.wsdl.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.xml.bind.JAXBElement;

@Component
public class AramexApi {

    @Autowired
    WebServiceTemplate webServiceTemplate;

    @Autowired
    Jaxb2Marshaller jaxb2Marshaller;

    @Autowired
    ObjectFactory objectFactory;

    @Autowired
    private RestTemplate restTemplate;

    public ImportSKUResponse importSKU(ImportSKUDTO importSKUDTO) {
        ImportSKU importSKU = createImportSKUObject(importSKUDTO);
        ImportSKUResponse resp = (ImportSKUResponse) getResponse("ImportSKU", importSKU);
        return resp;
    }

    public InquiryStockResponse inquiryStock(InquiryStockDTO inquiryStockDTO) {
        InquiryStock inquiryStock = createInquiryStockObject(inquiryStockDTO);
        InquiryStockResponse inquiryStockResponse = (InquiryStockResponse) getResponse("InquiryStock", inquiryStock);
        return inquiryStockResponse;
    }

    public ImportASNResponse importASN(ImportASNDTO importASNDTO) {
        ImportASN importASN = createImportASNObject(importASNDTO);
        ImportASNResponse response = (ImportASNResponse) getResponse("ImportASN", importASN);
        return response;
    }

    public ImportSOResponse importSO(ImportSODTO importSODTO) {
        ImportSO importSO = createImportSOObject(importSODTO);
        ImportSOResponse response = (ImportSOResponse) getResponse("ImportSO", importSO);
        return response;
    }

    public ImportSKUsResponse importSKUs(ImportSKUsDTO importSKUsDTO) {
        ImportSKUs importSKUs = createImportSKUsObject(importSKUsDTO);
        ImportSKUsResponse response = (ImportSKUsResponse) getResponse("ImportSKUs", importSKUs);
        return response;
    }

    public InquirySOResponse inquirySO(InquirySODTO inquirySODTO) {
        InquirySO inquirySO = createInquirySOObject(inquirySODTO);
        InquirySOResponse response = (InquirySOResponse) getResponse("InquirySO", inquirySO);
        return response;
    }

    private Object getResponse(String url, Object requestData) {
        Object responseObj = null;
        try {
            webServiceTemplate.setMarshaller(jaxb2Marshaller);
            webServiceTemplate.afterPropertiesSet();
            webServiceTemplate.setUnmarshaller(jaxb2Marshaller);
            webServiceTemplate.afterPropertiesSet();

            responseObj = webServiceTemplate.marshalSendAndReceive(
                    "https://portal.infor.aramex.com/WS_EDI_TEST_V02/Service_EDI.svc?singleWsdl", requestData,
                    new SoapActionCallback("http://tempuri.org/IService_EDI/" + url));

        } catch (SoapFaultClientException e) {
            e.printStackTrace();
        } catch (WebServiceClientException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return responseObj;
    }

    private ImportSKU createImportSKUObject(ImportSKUDTO importSKUDTO) {
        ImportSKU importSKU = objectFactory.createImportSKU();
        ARXEDISKU SKU = objectFactory.createARXEDISKU();
        // application header
        ARXEDIApplicationHeader applicationHeader = objectFactory.createARXEDIApplicationHeader();
        applicationHeader.setRequestedSystem(objectFactory.createARXEDIApplicationHeaderRequestedDate(importSKUDTO.getRequestedDate()));
        applicationHeader.setTransactionID(objectFactory.createARXEDIApplicationHeaderTransactionID(importSKUDTO.getTransactionID()));

        //date header
        ARXEDIDataHeaderSKU dataHeader = objectFactory.createARXEDIDataHeaderSKU();
        dataHeader.setDescription(objectFactory.createARXEDIDataHeaderSKUDescription(importSKUDTO.getDescription()));
        dataHeader.setFacility(objectFactory.createARXEDIDataHeaderSKUFacility(importSKUDTO.getFacility()));
        dataHeader.setHSCode(objectFactory.createARXEDIDataHeaderSKUHSCode(importSKUDTO.getHsCode()));
        dataHeader.setSKU(objectFactory.createARXEDIDataHeaderSKUSKU(importSKUDTO.getSku()));
        dataHeader.setSerialCount(importSKUDTO.getSerialCount());
        dataHeader.setStorerKey(objectFactory.createARXEDIDataHeaderSKUStorerKey(importSKUDTO.getStorerKey()));
        dataHeader.setUPC(objectFactory.createARXEDIDataHeaderSKUUPC(importSKUDTO.getUpc()));

        // ssa
        ARXEDISSAServer ssa = objectFactory.createARXEDISSAServer();
        ssa.setSSALogin(objectFactory.createARXEDISSAServerSSALogin(importSKUDTO.getSsaLogin()));
        ssa.setSSAPassword(objectFactory.createARXEDISSAServerSSAPassword(importSKUDTO.getSsaPassword()));

        SKU.setApplicationHeader(applicationHeader);
        SKU.setDataHeader(dataHeader);
        SKU.setSSA(ssa);
        importSKU.setSKU(SKU);
        return importSKU;
    }

    private InquiryStock createInquiryStockObject(InquiryStockDTO inquiryStockDTO) {
        InquiryStock inquiryStock = objectFactory.createInquiryStock();
        ARXEDIStock stock = objectFactory.createARXEDIStock();

        ARXEDIDataHeaderStock dataHeader = objectFactory.createARXEDIDataHeaderStock();
        dataHeader.setFacility(objectFactory.createARXEDIDataHeaderStockFacility(inquiryStockDTO.getFacility()));
        dataHeader.setGroupBySKU(inquiryStockDTO.isGroupBySKU());
        dataHeader.setStorerKey(objectFactory.createARXEDIDataHeaderStockStorerKey(inquiryStockDTO.getStorerKey()));

        ARXEDISSAServer ssa = objectFactory.createARXEDISSAServer();
        ssa.setSSALogin(objectFactory.createARXEDISSAServerSSALogin(inquiryStockDTO.getSsaLogin()));
        ssa.setSSAPassword(objectFactory.createARXEDISSAServerSSAPassword(inquiryStockDTO.getSsaPassword()));

        stock.setDataHeader(dataHeader);
        stock.setSSA(ssa);
        inquiryStock.setStock(stock);

        return inquiryStock;
    }

    private ImportASN createImportASNObject(ImportASNDTO importASNDTO) {
        ImportASN importASN = objectFactory.createImportASN();
        ARXEDIASN asn = objectFactory.createARXEDIASN();

        ARXEDIDataHeaderASN dataHeader = objectFactory.createARXEDIDataHeaderASN();
        dataHeader.setClinetSystemRef(objectFactory.createARXEDIDataHeaderASNClinetSystemRef(importASNDTO.getClinetSystemRef()));
        dataHeader.setCurrency(objectFactory.createARXEDIDataHeaderASNCurrency(importASNDTO.getCurrency()));
        dataHeader.setFacility(objectFactory.createARXEDIDataHeaderASNFacility(importASNDTO.getFacility()));
        dataHeader.setStorerKey(objectFactory.createARXEDIDataHeaderASNStorerKey(importASNDTO.getStorerKey()));

        JAXBElement<ArrayOfARXEDIDataLineASN> dataLines;
        ArrayOfARXEDIDataLineASN dateLineArray = objectFactory.createArrayOfARXEDIDataLineASN();
        ARXEDIDataLineASN dataLineASN;

        for (ImportAsnDateLines importAsnDateLines : importASNDTO.getDataLines()) {
            dataLineASN = objectFactory.createARXEDIDataLineASN();
            dataLineASN.setExternLineNo(objectFactory.createARXEDIDataLineASNExternLineNo(importAsnDateLines.getExternLineNo()));
            dataLineASN.setLinePO(objectFactory.createARXEDIDataLineASNLinePO(importAsnDateLines.getLinePO()));
            dataLineASN.setQty(importAsnDateLines.getQty());
            dataLineASN.setSKU(objectFactory.createARXEDIDataLineASNSKU(importAsnDateLines.getSku()));
            dataLineASN.setUnitCost(importAsnDateLines.getUnitCost());
            dateLineArray.getARXEDIDataLineASN().add(dataLineASN);
        }
        dataLines = objectFactory.createArrayOfARXEDIDataLineASN(dateLineArray);

        ARXEDISSAServer ssa = objectFactory.createARXEDISSAServer();
        ssa.setSSALogin(objectFactory.createARXEDISSAServerSSALogin(importASNDTO.getSsaLogin()));
        ssa.setSSAPassword(objectFactory.createARXEDISSAServerSSAPassword(importASNDTO.getSsaPassword()));

        asn.setDataHeader(dataHeader);
        asn.setDataLines(dataLines);
        asn.setSSA(ssa);

        importASN.setASN(asn);

        return importASN;
    }

    private ImportSO createImportSOObject(ImportSODTO importSODTO) {
        ImportSO importSO = objectFactory.createImportSO();
        ARXEDISO so = objectFactory.createARXEDISO();

        ARXEDIDataHeaderSO dataHeader = objectFactory.createARXEDIDataHeaderSO();

        ARXEDIAddress bAddress = objectFactory.createARXEDIAddress();
        bAddress.setAddress1(objectFactory.createARXEDIAddressAddress1(importSODTO.getbAddress().getAddress1()));
        bAddress.setAddress2(objectFactory.createARXEDIAddressAddress2(importSODTO.getbAddress().getAddress2()));
        bAddress.setAddress3(objectFactory.createARXEDIAddressAddress3(importSODTO.getbAddress().getAddress3()));
        bAddress.setCity(objectFactory.createARXEDIAddressCity(importSODTO.getbAddress().getCity()));
        bAddress.setCompany(objectFactory.createARXEDIAddressCompany(importSODTO.getbAddress().getCompany()));
        bAddress.setContact(objectFactory.createARXEDIAddressContact(importSODTO.getbAddress().getContact()));
        bAddress.setCountry(objectFactory.createARXEDIAddressCountry(importSODTO.getbAddress().getCountry()));
        bAddress.setEmail(objectFactory.createARXEDIAddressEmail(importSODTO.getbAddress().getEmail()));
        bAddress.setFax(objectFactory.createARXEDIAddressFax(importSODTO.getbAddress().getFax()));
        bAddress.setPhone1(objectFactory.createARXEDIAddressPhone1(importSODTO.getbAddress().getPhone1()));
        bAddress.setPhone2(objectFactory.createARXEDIAddressPhone2(importSODTO.getbAddress().getPhone2()));
        bAddress.setPhone2(objectFactory.createARXEDIAddressPhone2(importSODTO.getbAddress().getPhone2()));
        dataHeader.setBAddress(bAddress);

        ARXEDIAddress cAddress = objectFactory.createARXEDIAddress();
        cAddress.setAddress1(objectFactory.createARXEDIAddressAddress1(importSODTO.getcAddress().getAddress1()));
        cAddress.setAddress2(objectFactory.createARXEDIAddressAddress2(importSODTO.getcAddress().getAddress2()));
        cAddress.setAddress3(objectFactory.createARXEDIAddressAddress3(importSODTO.getcAddress().getAddress3()));
        cAddress.setCity(objectFactory.createARXEDIAddressCity(importSODTO.getcAddress().getCity()));
        cAddress.setCompany(objectFactory.createARXEDIAddressCompany(importSODTO.getcAddress().getCompany()));
        cAddress.setContact(objectFactory.createARXEDIAddressContact(importSODTO.getcAddress().getContact()));
        cAddress.setCountry(objectFactory.createARXEDIAddressCountry(importSODTO.getcAddress().getCountry()));
        cAddress.setEmail(objectFactory.createARXEDIAddressEmail(importSODTO.getcAddress().getEmail()));
        cAddress.setFax(objectFactory.createARXEDIAddressFax(importSODTO.getcAddress().getFax()));
        cAddress.setPhone1(objectFactory.createARXEDIAddressPhone1(importSODTO.getcAddress().getPhone1()));
        cAddress.setPhone2(objectFactory.createARXEDIAddressPhone2(importSODTO.getcAddress().getPhone2()));
        cAddress.setPhone2(objectFactory.createARXEDIAddressPhone2(importSODTO.getcAddress().getPhone2()));
        dataHeader.setCAddress(cAddress);

        dataHeader.setClinetSystemRef(objectFactory.createARXEDIDataHeaderSOClinetSystemRef(importSODTO.getClinetSystemRef()));
        dataHeader.setCurrency(objectFactory.createARXEDIDataHeaderSOCurrency(importSODTO.getCurrency()));

        ARXEDIAddress dAddress = objectFactory.createARXEDIAddress();
        dAddress.setAddress1(objectFactory.createARXEDIAddressAddress1(importSODTO.getdAddress().getAddress1()));
        dAddress.setAddress2(objectFactory.createARXEDIAddressAddress2(importSODTO.getdAddress().getAddress2()));
        dAddress.setAddress3(objectFactory.createARXEDIAddressAddress3(importSODTO.getdAddress().getAddress3()));
        dAddress.setCity(objectFactory.createARXEDIAddressCity(importSODTO.getdAddress().getCity()));
        dAddress.setCompany(objectFactory.createARXEDIAddressCompany(importSODTO.getdAddress().getCompany()));
        dAddress.setContact(objectFactory.createARXEDIAddressContact(importSODTO.getdAddress().getContact()));
        dAddress.setCountry(objectFactory.createARXEDIAddressCountry(importSODTO.getdAddress().getCountry()));
        dAddress.setEmail(objectFactory.createARXEDIAddressEmail(importSODTO.getdAddress().getEmail()));
        dAddress.setFax(objectFactory.createARXEDIAddressFax(importSODTO.getdAddress().getFax()));
        dAddress.setPhone1(objectFactory.createARXEDIAddressPhone1(importSODTO.getdAddress().getPhone1()));
        dAddress.setPhone2(objectFactory.createARXEDIAddressPhone2(importSODTO.getdAddress().getPhone2()));
        dAddress.setPhone2(objectFactory.createARXEDIAddressPhone2(importSODTO.getdAddress().getPhone2()));
        dataHeader.setDAddress(dAddress);

        dataHeader.setFacility(objectFactory.createARXEDIDataHeaderSOFacility(importSODTO.getFacility()));
        dataHeader.setForwarder(objectFactory.createARXEDIDataHeaderSOForwarder(importSODTO.getForwarder()));
        dataHeader.setStorerKey(objectFactory.createARXEDIDataHeaderSOStorerKey(importSODTO.getStorerKey()));
        dataHeader.setTransportationMode(objectFactory.createARXEDIDataHeaderSOTransportationMode(importSODTO.getTransportationMode()));

        JAXBElement<ArrayOfARXEDIDataLineSO> dataLines;
        ArrayOfARXEDIDataLineSO dateLineArray = objectFactory.createArrayOfARXEDIDataLineSO();
        ARXEDIDataLineSO dataLineSO;

        for (ImportSODateLines importSODateLines : importSODTO.getDataLines()) {
            dataLineSO = objectFactory.createARXEDIDataLineSO();
            dataLineSO.setExternLineNo(objectFactory.createARXEDIDataLineSOExternLineNo(importSODateLines.getExternLineNo()));
            dataLineSO.setQty(importSODateLines.getQty());
            dataLineSO.setSKU(objectFactory.createARXEDIDataLineASNSKU(importSODateLines.getSku()));
            dataLineSO.setUnitPrice(importSODateLines.getUnitPrice());
            dateLineArray.getARXEDIDataLineSO().add(dataLineSO);
        }
        dataLines = objectFactory.createArrayOfARXEDIDataLineSO(dateLineArray);

        ARXEDISSAServer ssa = objectFactory.createARXEDISSAServer();
        ssa.setSSALogin(objectFactory.createARXEDISSAServerSSALogin(importSODTO.getSsaLogin()));
        ssa.setSSAPassword(objectFactory.createARXEDISSAServerSSAPassword(importSODTO.getSsaPassword()));

        so.setDataHeader(dataHeader);
        so.setDataLines(dataLines);
        so.setSSA(ssa);

        importSO.setSO(so);
        return importSO;
    }

    private ImportSKUs createImportSKUsObject(ImportSKUsDTO importSKUsDTO) {
        ImportSKUs importSKUs = objectFactory.createImportSKUs();
        ARXEDISKUs skUs = objectFactory.createARXEDISKUs();

        ARXEDIApplicationHeader applicationHeader = objectFactory.createARXEDIApplicationHeader();
        applicationHeader.setTransactionID(objectFactory.createARXEDIApplicationHeaderTransactionID(importSKUsDTO.getTransactionID()));

        JAXBElement<ArrayOfARXEDIDataHeaderSKU> dataHeader;
        ArrayOfARXEDIDataHeaderSKU arrayDataHeaderSKU = objectFactory.createArrayOfARXEDIDataHeaderSKU();
        ARXEDIDataHeaderSKU dataHeaderSKU;

        for (ImportSKUsDateHeaderDTO importSKUsDateHeaderDTO : importSKUsDTO.getDataHeader()) {
            dataHeaderSKU = objectFactory.createARXEDIDataHeaderSKU();
            dataHeaderSKU.setDescription(objectFactory.createARXEDIDataHeaderSKUDescription(importSKUsDateHeaderDTO.getDescription()));
            dataHeaderSKU.setFacility(objectFactory.createARXEDIDataHeaderSKUFacility(importSKUsDateHeaderDTO.getFacility()));
            dataHeaderSKU.setHSCode(objectFactory.createARXEDIDataHeaderSKUHSCode(importSKUsDateHeaderDTO.getHsCode()));
            dataHeaderSKU.setSKU(objectFactory.createARXEDIDataHeaderSKUSKU(importSKUsDateHeaderDTO.getSku()));
            dataHeaderSKU.setSerialCount(importSKUsDateHeaderDTO.getSerialCount());
            dataHeaderSKU.setStorerKey(objectFactory.createARXEDIDataHeaderSKUStorerKey(importSKUsDateHeaderDTO.getStorerKey()));
            dataHeaderSKU.setUPC(objectFactory.createARXEDIDataHeaderSKUUPC(importSKUsDateHeaderDTO.getUpc()));
            arrayDataHeaderSKU.getARXEDIDataHeaderSKU().add(dataHeaderSKU);
        }
        dataHeader = objectFactory.createARXEDISKUsDataHeader(arrayDataHeaderSKU);

        ARXEDISSAServer ssa = objectFactory.createARXEDISSAServer();
        ssa.setSSALogin(objectFactory.createARXEDISSAServerSSALogin(importSKUsDTO.getSsaLogin()));
        ssa.setSSAPassword(objectFactory.createARXEDISSAServerSSAPassword(importSKUsDTO.getSsaPassword()));

        skUs.setApplicationHeader(applicationHeader);
        skUs.setDataHeader(dataHeader);
        skUs.setSSA(ssa);

        importSKUs.setSKUs(skUs);

        return importSKUs;
    }

    private InquirySO createInquirySOObject(InquirySODTO inquirySODTO) {
        InquirySO inquirySO = objectFactory.createInquirySO();
        ARXEDISOInquiry so = objectFactory.createARXEDISOInquiry();

        ARXEDIDataHeaderSOInquiry dataHeader = objectFactory.createARXEDIDataHeaderSOInquiry();
        dataHeader.setClinetSystemRef(objectFactory.createARXEDIDataHeaderSOInquiryClinetSystemRef(inquirySODTO.getClinetSystemRef()));
        dataHeader.setFacility(objectFactory.createARXEDIDataHeaderSOInquiryFacility(inquirySODTO.getFacility()));
        dataHeader.setLatestOnly(inquirySODTO.getLatestOnly());
        dataHeader.setStorerKey(objectFactory.createARXEDIDataHeaderSOInquiryStorerKey(inquirySODTO.getStorerKey()));

        ARXEDISSAServer ssa = objectFactory.createARXEDISSAServer();
        ssa.setSSALogin(objectFactory.createARXEDISSAServerSSALogin(inquirySODTO.getSsaLogin()));
        ssa.setSSAPassword(objectFactory.createARXEDISSAServerSSAPassword(inquirySODTO.getSsaPassword()));

        so.setDataHeader(dataHeader);
        so.setSSA(ssa);

        inquirySO.setSO(so);
        return inquirySO;
    }

    /////////////////////////////////////////////////////////////////////

    public JSONObject trackPickup(TrackPickupDto dto) {
        Gson gson = new Gson();
        GsonBuilder builder = new GsonBuilder();
        gson = builder.serializeNulls().create();

        return getResponse("https://ws.aramex.net/ShippingAPI.V2/Tracking/Service_1_0.svc/json/TrackShipments",
                gson.toJson(dto));
    }

    public JSONObject trackShipments(TrackShipmentsDto dto) {
        Gson gson = new Gson();
        GsonBuilder builder = new GsonBuilder();
        gson = builder.serializeNulls().create();

        return getResponse("https://ws.aramex.net/ShippingAPI.V2/Tracking/Service_1_0.svc/json/TrackShipments",
                gson.toJson(dto));
    }

    public String createShipments(CreateShipmentsDto dto) throws UnirestException {

        Gson gson = new GsonBuilder().serializeNulls().create();
       HttpResponse<String> j= Unirest.post(GlobalConfigProperties.getInstance().getProperty("CreateShipments.url"))
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .body(gson.toJson(dto))
                .asString();



        return j.getBody();
    }

    public JSONObject createPickup(CreatePickupDto dto) {
        Gson gson = new Gson();
        GsonBuilder builder = new GsonBuilder();
        gson = builder.serializeNulls().create();
        // System.err.println(gson.toJson(dto));
        return getResponse("https://ws.dev.aramex.net/ShippingAPI.V2/Shipping/Service_1_0.svc/json/CreatePickup",
                gson.toJson(dto));
    }

    public JSONObject cancelPickup(CancelPickupDto dto) {
        Gson gson = new Gson();
        GsonBuilder builder = new GsonBuilder();
        gson = builder.serializeNulls().create();
        return getResponse("https://ws.dev.aramex.net/ShippingAPI.V2/Shipping/Service_1_0.svc/json/CancelPickup",
                gson.toJson(dto));
    }

    public JSONObject fetchCountries(FetchCountriesDto dto) {
        Gson gson = new Gson();
        GsonBuilder builder = new GsonBuilder();
        gson = builder.serializeNulls().create();
        return getResponse("https://ws.dev.aramex.net/ShippingAPI.V2/Location/Service_1_0.svc/json/FetchCountries",
                gson.toJson(dto));
    }

    public JSONObject fetchCities(FetchCitiesDto dto) {
        Gson gson = new Gson();
        GsonBuilder builder = new GsonBuilder();
        gson = builder.serializeNulls().create();
        return getResponse(GlobalConfigProperties.getInstance().getProperty("FetchCitiesUrl"),
                gson.toJson(dto));
    }

    public JSONObject getResponse(String url, String params) {

        String responseBody = null;
        JSONObject json = new JSONObject();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> request = new HttpEntity<>(params, headers);
            String response = restTemplate.postForObject(url, request, String.class);
            responseBody = response;
            JSONParser parser = new JSONParser();
            json = (JSONObject) parser.parse(responseBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

}
