package com.ellepharma.web.translator;

import com.ellepharma.constant.TransField;
import com.ellepharma.data.model.SystemLanguage;
import com.ellepharma.utils.Utils;
import org.primefaces.component.dialog.Dialog;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.extensions.component.ckeditor.CKEditor;

import javax.faces.application.Resource;
import javax.faces.component.FacesComponent;
import javax.faces.component.NamingContainer;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * BaseEntity.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 18, 2019
 */

@FacesComponent("translator")
public class TranslatorComponentBB extends UIInput implements NamingContainer {

    private static final String ATT_LABEL = "label";

    private static final String UI_LABEL_1 = "lang1Lbl";

    private static final String UI_LABEL_2 = "lang2Lbl";

    private static final String UI_INPUTL_1 = "lang1Input";

    private static final String UI_INPUTL_2 = "lang2Input";

    private static final String UI_BUTTON_MORE = "moreBtn";

    private static final String UI_DILOG_EXTRA_LANG = "extraLangsDlg";

    private static final String UI_GRID_EXTRA_LANG = "extraLangsDlgPnlGrd";

    private static final String ID_EXTRA_LANG = "extraLang_";

    // need it in translatorTextArea to use in InputTextArea
    private static final String COMPOSITE_NAME_TEXT_AREA = "translatorTextArea";

    private static final String ROWS_KEW = "rows";

    private static final String COLS_KEY = "cols";

    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        //Get the TransField
        Object valueObject = getValue();
        TransField transField = new TransField();

        if (valueObject != null) {
            if (valueObject instanceof TransField) {
                transField = (TransField) valueObject;
            } else {
                //object is not a translation field object
                transField = new TransField();
            }
        }

        List<SystemLanguage> systemLangs = Utils.getSystemLangsSorted();
        int numberOfLangs = systemLangs.size();

        //get component other parameters
        Map<String, Object> componentAttributes = getAttributes();
        String labelValue = componentAttributes.get(ATT_LABEL) == null ? "No Label!!!" : componentAttributes.get(ATT_LABEL).toString();

        if (numberOfLangs > 0) {
            SystemLanguage firstLang = systemLangs.get(0);
            prepareFieldValues(UI_LABEL_1, UI_INPUTL_1, firstLang, labelValue, transField);

            if (numberOfLangs > 1) {
                SystemLanguage secondLang = systemLangs.get(1);
                //get second language label/field references
                prepareFieldValues(UI_LABEL_2, UI_INPUTL_2, secondLang, labelValue, transField);
            }
        }

        //if there are only 2 languages, then hide the more button
        if (numberOfLangs < 3) {
            UIComponent moreBtn = findComponent(UI_BUTTON_MORE);
            Dialog extraLangsDlg = (Dialog) findComponent(UI_DILOG_EXTRA_LANG);
            moreBtn.setRendered(false);
            extraLangsDlg.setRendered(false);
        } else {
            //add all extra languages label/field to panelGrid inside the dialog
            PanelGrid panelGrid = (PanelGrid) findComponent(UI_GRID_EXTRA_LANG);
            panelGrid.setColumns(1);

            for (int i = 2; i < numberOfLangs; i++) {
                SystemLanguage extraLanguage = systemLangs.get(i);
                String id = ID_EXTRA_LANG + extraLanguage.getId();
                String value = transField.get(extraLanguage.getLocale());

                componentAttributes.put("tabindex", i);
                //Get Input component(inputText or InputTextArea) depend use composite component

                UIInput inputText = getInputComponent(componentAttributes, extraLanguage, id, value);

                if (findComponent(id) == null) {
                    OutputLabel label = new OutputLabel();
                    label.setFor(id);
                    label.setValue(labelValue + " (" + extraLanguage.getShortcut() + ")");

                    panelGrid.getChildren().add(i - 2, label);
                    panelGrid.getChildren().add(i - 1, inputText);
                }
            }
        }

        super.encodeBegin(context);
    }

    /**
     * @param componentAttributes
     * @param extraLanguage
     * @param id
     * @param value
     * @return
     */
    private UIInput getInputComponent(Map<String, Object> componentAttributes, SystemLanguage extraLanguage, String id, String value) {

        //if use translatorArea Composite component return input Type as inputTextArea
        if (componentAttributes.get(Resource.COMPONENT_RESOURCE_KEY).toString().contains(COMPOSITE_NAME_TEXT_AREA)) {
            InputTextarea inputTextarea = findComponent(id) == null ? new InputTextarea() : (InputTextarea) findComponent(id);
            inputTextarea.setId(id);
            inputTextarea.setValue(value == null ? "" : value);
            inputTextarea.setRequired(extraLanguage.getIsMandatory() && (Boolean) getAttributes().get("required"));
            inputTextarea.setDir(extraLanguage.getDirection());
            inputTextarea.setAutoResize(false);// to prevent auto resize width and height
            inputTextarea.setStyle("resize: none;");// to prevent user resize width and height
            inputTextarea.setRows(Integer.parseInt(componentAttributes.get(ROWS_KEW).toString()));// read from composite attribute
            inputTextarea.setCols(Integer.parseInt(componentAttributes.get(COLS_KEY).toString()));// read from composite attribute
            inputTextarea.setTabindex(componentAttributes.get("tabindex").toString());

            if (componentAttributes.get("disabled") != null) {
                inputTextarea.setReadonly(Boolean.valueOf(componentAttributes.get("disabled").toString()));
            }
            return inputTextarea;
        } else {
            InputText inputText = findComponent(id) == null ? new InputText() : (InputText) findComponent(id);
            inputText.setId(id);
            inputText.setValue(value == null ? "" : value);
            inputText.setRequired(extraLanguage.getIsMandatory() && (Boolean) getAttributes().get("required"));
            inputText.setDir(extraLanguage.getDirection());
            inputText.setTabindex(componentAttributes.get("tabindex").toString());
            if (componentAttributes.get("disabled") != null) {
                inputText.setReadonly(Boolean.valueOf(componentAttributes.get("disabled").toString()));
            }
            return inputText;

        }
    }

    //prepare the values
    private void prepareFieldValues(String labelId, String inputId, SystemLanguage lang, String labelValue,
                                    TransField transField) {
        //get language label/field references
        OutputLabel langLabel = (OutputLabel) findComponent(labelId);
        UIComponent component = findComponent(inputId);
        UIInput langInput = (UIInput) component;
        langLabel.setValue(labelValue + " (" + lang.getShortcut() + ")");
        String value = transField.get(lang.getLocale());
        langInput.setValue(value == null ? "" : value);
        langInput.setRequired(lang.getIsMandatory() && (Boolean) getAttributes().get("required"));
        if (component instanceof CKEditor) {
            ((CKEditor) langInput).setStyleClass(lang.getDirection());

        } else if (component instanceof InputText) {
            ((InputText) langInput).setStyleClass(lang.getDirection());
        } else {
            ((InputTextarea) langInput).setStyleClass(lang.getDirection());
        }
    }

    @Override
    protected Object getConvertedValue(FacesContext context, Object newSubmittedValue) throws ConverterException {

        List<SystemLanguage> systemLangs = getSystemLangsSorted();
        int numberOfLangs = systemLangs.size();

        UIInput lang1Input = (UIInput) findComponent(UI_INPUTL_1);
        UIInput lang2Input = (UIInput) findComponent(UI_INPUTL_2);

        //if UIInput is Disabled get current value else return SubmittedValue
        Object valInput1 = isDisabled(lang1Input) ? lang1Input.getValue() : lang1Input.getSubmittedValue();
        Object valInput2 = isDisabled(lang2Input) ? lang2Input.getValue() : lang2Input.getSubmittedValue();

        TransField transField = new TransField();

        if (numberOfLangs > 0) {
            SystemLanguage lang = systemLangs.get(0);
            transField.put(lang.getLocale(), valInput1 == null ? "" : valInput1.toString());

            if (numberOfLangs > 1) {
                lang = systemLangs.get(1);
                transField.put(lang.getLocale(), valInput2 == null ? "" : valInput2.toString());
            }

            if (numberOfLangs > 2) {

                PanelGrid extraLangsGrid = (PanelGrid) findComponent(UI_GRID_EXTRA_LANG);
                List<UIComponent> extraInputs = extraLangsGrid.getChildren();

                for (int i = 2; i < systemLangs.size(); i++) {
                    lang = systemLangs.get(i);
                    for (UIComponent extraInput : extraInputs) {
                        if (extraInput instanceof UIInput && extraInput.getId().endsWith("_" + lang.getId())) {
                            Object extraInputText = ((UIInput) extraInput).getValue();
                            transField.put(lang.getLocale(), extraInputText == null ? "" : extraInputText.toString());
                            break;
                        }
                    }

                }
            }
        }

        return transField;
    }

    private List<SystemLanguage> getSystemLangsSorted() {
        return Utils.getSystemLangsSorted();
    }

    @Override
    public String getFamily() {
        return ("javax.faces.NamingContainer");
    }

    @Override
    public Object getSubmittedValue() {
        return (this);
    }

    /**
     * @param component
     * @return
     */
    public boolean isDisabled(UIComponent component) {
        boolean disabled = false;

        if (component instanceof InputText) {
            disabled = ((InputText) component).isDisabled();
        } else if (component instanceof InputTextarea) {
            disabled = ((InputTextarea) component).isDisabled();
        } else if (component instanceof CKEditor) {
            disabled = ((CKEditor) component).isDisabled();
        }
        return disabled;
    }

}
