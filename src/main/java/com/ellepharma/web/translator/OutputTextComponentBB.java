package com.ellepharma.web.translator;

import com.ellepharma.constant.TransField;
import com.ellepharma.utils.TransUtil;

import javax.faces.component.FacesComponent;
import javax.faces.component.NamingContainer;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import java.io.IOException;

/**
 * OutputTextComponentBB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */

@FacesComponent("cust.outputText")
public class OutputTextComponentBB extends UIOutput implements NamingContainer {

	private static final String UI_OUTPUT_1 = "lang1Output";

	@Override
	public void encodeBegin(FacesContext context) throws IOException {
		//Get the TransField
		Object valueObject = getValue();
		String value = null;

		if (valueObject != null) {
			if (valueObject instanceof TransField) {
				TransField transField = (TransField) valueObject;
				value = transField.get(TransUtil.getCurrentLanguage());

			} else {
				//object is not a translation field object
				value = valueObject.toString();
			}
		}

		//get language label/field references
		UIOutput langoutput = (UIOutput) findComponent(UI_OUTPUT_1);
		langoutput.setValue(value);

		super.encodeBegin(context);
	}

	@Override
	public String getFamily() {
		return ("javax.faces.NamingContainer");
	}

}
