package com.ellepharma.web.MB;

import com.ellepharma.data.model.Category;
import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.Image;
import com.ellepharma.service.CategoryService;
import com.ellepharma.service.ImageService;
import com.ellepharma.utils.ExceptionCodes;
import com.ellepharma.utils.Utils;
import org.primefaces.event.FileUploadEvent;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.List;

import static com.ellepharma.utils.Constants.CATEGORY;

/**
 * CategoryMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */
@ManagedBean
@ViewScoped
public class CategoryMB extends BaseMB {

    @ManagedProperty("#{categoryService}")
    private CategoryService categoryService;

    @ManagedProperty("#{imageService}")
    private ImageService imageService;

    private Category selectedCategory;

    private CustomLazyDataModel<Category> categoryCustomLazyDataModel;

    private List<Category> categoryList;

    @PostConstruct
    public void init() {
        selectedCategory = new Category();
        categoryList = categoryService.findAll();
        categoryCustomLazyDataModel = new CustomLazyDataModel<>(categoryService);
    }

    /**
     *
     */
    public void addNewCategory() {
        selectedCategory = new Category();
        categoryList = categoryService.findAll();
        setEditMode(false);
        showDialog("dlgCategory");
        updates("catFrm");

    }

    /**
     * @param category
     */
    public void editCategory(Category category) {
        selectedCategory = category;
        setEditMode(true);
        categoryList = categoryService.findAll();
        categoryList.remove(category);
        showDialog("dlgCategory");
        updates("catFrm", "catDtl");
    }

    /**
     * @param category
     */
    public void deleteCategory(Category category) {
        try {
            categoryService.delete(category);
            categoryList.remove(category);

        } catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
            addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
            return;
        }
        updates("catDtl");
        addSuccessMessage();

    }

    /**
     *
     */
    public boolean saveCategory() {
        try {
            selectedCategory = categoryService.save(selectedCategory);
            categoryList.add(selectedCategory);
            selectedCategory = new Category();
        } catch (DataIntegrityViolationException ex) {
            addErrorMessage(ex);
            return false;
        }
        updates("catDtl", "catFrm");
        addSuccessMessage();
        return true;
    }

    public void upload(FileUploadEvent event) throws Exception {
        String name = Utils.uploud(event, CATEGORY);
        Image image = new Image();
        if (selectedCategory.getImage() != null) {
            image = selectedCategory.getImage();
        }
        image.setName(name);
        image.setPriorityOrder(1L);
        //image.setPic(event.getFile().getContents());
        image = imageService.save(image);

        selectedCategory.setImage(image);
    }

    public void saveCategoryAndHide() {
        if (saveCategory()) {
            hideDialog("dlgCategory");

        }


    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public Category getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public CustomLazyDataModel<Category> getCategoryCustomLazyDataModel() {
        return categoryCustomLazyDataModel;
    }

    public void setCategoryCustomLazyDataModel(
            CustomLazyDataModel<Category> categoryCustomLazyDataModel) {
        this.categoryCustomLazyDataModel = categoryCustomLazyDataModel;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public ImageService getImageService() {
        return imageService;
    }

    public void setImageService(ImageService imageService) {
        this.imageService = imageService;
    }
}
