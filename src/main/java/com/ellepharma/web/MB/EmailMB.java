package com.ellepharma.web.MB;

import com.ellepharma.service.AccountService;
import com.ellepharma.service.EmailUtil;
import com.ellepharma.utils.JsfUtils;
import org.apache.commons.collections4.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.mail.MessagingException;
import java.util.List;
import java.util.stream.Collectors;

@ManagedBean
@ViewScoped
public class EmailMB extends BaseMB {

    @ManagedProperty("#{accountService}")
    private AccountService accountService;

    @ManagedProperty("#{emailUtil}")
    private EmailUtil emailUtil;
    private List<String> allEmail;
    private List<String> selectEmail;
    private String text;
    private String subject;

    @PostConstruct
    public void init() {
        allEmail = accountService.findAllEmail();

    }

    public void send() {

        if (CollectionUtils.isEmpty(allEmail)){

            JsfUtils.addErrorMessageDirect("TO is required");
            return;
        }
        try {
            emailUtil.sendMailCC(String.join(",", selectEmail), subject, text);
            addSuccessMessage();
        } catch (MessagingException e) {
            e.printStackTrace();
        }


    }

    public List<String> getAllEmail() {
        return allEmail;
    }

    public void setAllEmail(List<String> allEmail) {
        this.allEmail = allEmail;
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    public List<String> getSelectEmail() {
        return selectEmail;
    }

    public void setSelectEmail(List<String> selectEmail) {
        this.selectEmail = selectEmail;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setEmailUtil(EmailUtil emailUtil) {
        this.emailUtil = emailUtil;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
