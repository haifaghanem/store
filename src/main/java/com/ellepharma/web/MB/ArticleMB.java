package com.ellepharma.web.MB;

import com.ellepharma.data.model.Article;
import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.Image;
import com.ellepharma.service.ArticleService;
import com.ellepharma.service.ImageService;
import com.ellepharma.utils.ExceptionCodes;
import com.ellepharma.utils.SecurityUtils;
import com.ellepharma.utils.Utils;
import org.primefaces.event.FileUploadEvent;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.Date;

import static com.ellepharma.utils.Constants.ARTICLE;

/**
 * ArticleMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Jun 16, 2019
 */
@ManagedBean
@ViewScoped
public class ArticleMB extends BaseMB {

    @ManagedProperty("#{articleService}")
    private ArticleService articleService;

    @ManagedProperty("#{imageService}")
    private ImageService imageService;

    private Article selectedArticle;

    private CustomLazyDataModel<Article> articleCustomLazyDataModel;

    @PostConstruct
    public void init() {
        articleCustomLazyDataModel = new CustomLazyDataModel<>(articleService);
    }
    public void init( Long id) {
        selectedArticle=articleService.findById(id);
    }

    /**
     *
     */
    public void addNewArticle() {
        selectedArticle = new Article();
        selectedArticle.setPublishDate(new Date());
        selectedArticle.setAccount(SecurityUtils.getCurrentUser());
        setEditMode(false);
        showDialog("dlgArticle");
        updates("articleFrm");

    }

    /**
     * @param article
     */
    public void editArticle(Article article) {
        selectedArticle = article;
        setEditMode(true);
        showDialog("dlgArticle");
        updates("articleFrm", "articleDtl");
    }

    /**
     * @param article
     */
    public void deleteArticle(Article article) {
        try {
            articleService.delete(article);
            updates("articleDtl");
            addSuccessMessage();

        } catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
            addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
        }

    }

    /**
     *
     */
    public boolean saveArticle() {
        try {
            selectedArticle = articleService.save(selectedArticle);

        } catch (DataIntegrityViolationException ex) {
            addErrorMessage(ex);

            return false;
        }
        selectedArticle = new Article();
        updates("articleFrm", "articleDtl");
        addSuccessMessage();
        return true;

    }

    public void uploadImages(FileUploadEvent event) throws Exception {
        String name = Utils.uploud(event, ARTICLE);
        Image image = new Image();
        image.setName(name);
        image.setPriorityOrder(1L);
        //image.setPic(event.getFile().getContents());
        image = imageService.save(image);
        selectedArticle.getImages().add(image);
    }

    public void upload(FileUploadEvent event) throws Exception {
        String name = Utils.uploud(event, ARTICLE);
        Image image = new Image();
        image.setName(name);
        image.setPriorityOrder(1L);
        selectedArticle.setCover(image);
    }

    public void saveArticleAndHide() {
        if (saveArticle()) {
            hideDialog("dlgArticle");
        }
    }

    public Article getSelectedArticle() {
        return selectedArticle;
    }

    public void setSelectedArticle(Article selectedArticle) {
        this.selectedArticle = selectedArticle;
    }

    public CustomLazyDataModel<Article> getArticleCustomLazyDataModel() {
        return articleCustomLazyDataModel;
    }

    public void setArticleCustomLazyDataModel(CustomLazyDataModel<Article> articleCustomLazyDataModel) {
        this.articleCustomLazyDataModel = articleCustomLazyDataModel;
    }

    public ArticleService getArticleService() {
        return articleService;
    }

    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    public void setImageService(ImageService imageService) {
        this.imageService = imageService;
    }
}
