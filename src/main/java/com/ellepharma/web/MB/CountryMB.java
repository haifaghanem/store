package com.ellepharma.web.MB;

import com.ellepharma.data.model.Country;
import com.ellepharma.data.model.Currency;
import com.ellepharma.service.CountryService;
import com.ellepharma.service.CurrencyService;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * CountryMB.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 10, 2019
 */
@ManagedBean
@ViewScoped
public class CountryMB extends BaseMB {

    @ManagedProperty("#{countryService}")
    private CountryService countryService;

    @ManagedProperty("#{currencyService}")
    private CurrencyService currencyService;

    private List<Country> countryList;
    private List<Currency> currencyList;

    @PostConstruct
    public void init() {
        countryList = countryService.findAll();
        currencyList = currencyService.findAllByStatusIsTrue();
    }

    /**
     *
     */
    public void saveCountry() {
        try {
            countryService.save(countryList);
            addSuccessMessage();
        } catch (DataIntegrityViolationException ex) {
            addErrorMessage(ex);
        }
    }

    public CountryService getCountryService() {
        return countryService;
    }

    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }

    public List<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
    }

//    public CustomLazyDataModel<Country> getCountryCustomLazyDataModel() {
//        return countryCustomLazyDataModel;
//    }
//
//    public void setCountryCustomLazyDataModel(CustomLazyDataModel<Country> countryCustomLazyDataModel) {
//        this.countryCustomLazyDataModel = countryCustomLazyDataModel;
//    }

    public CurrencyService getCurrencyService() {
        return currencyService;
    }

    public void setCurrencyService(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    public List<Currency> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<Currency> currencyList) {
        this.currencyList = currencyList;
    }

}
