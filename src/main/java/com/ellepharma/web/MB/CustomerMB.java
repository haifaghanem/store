package com.ellepharma.web.MB;

import com.ellepharma.data.model.Account;
import com.ellepharma.data.model.AccountType;
import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.Image;
import com.ellepharma.service.AccountService;
import com.ellepharma.service.ImageService;
import com.ellepharma.utils.ExceptionCodes;
import com.ellepharma.utils.Utils;
import org.primefaces.event.FileUploadEvent;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.List;

import static com.ellepharma.utils.Constants.ACCOUNT;

/**
 * CategoryMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */
@ManagedBean
@ViewScoped
public class CustomerMB extends BaseMB {

    @ManagedProperty("#{accountService}")
    private AccountService accountService;

    @ManagedProperty("#{imageService}")
    private ImageService imageService;

    private Account selectedAccount;

    private CustomLazyDataModel<Account> userAccountCustomLazyDataModel;
    private List<Account> accounts;
    private String emails;

    @PostConstruct
    public void init() {
        selectedAccount = new Account();
        userAccountCustomLazyDataModel = new CustomLazyDataModel(accountService, "findCustomer", null);
        List<String> allEmail = accountService.findAllEmail();

        emails = String.join(",", allEmail);
    }

    /**
     * @param account
     */
    public void editCelebrity(Account account) {
        selectedAccount = account;
        setEditMode(true);
        showDialog("dlgCelebrity");
        updates("catFrm", "catDtl");
    }

    /**
     *
     */
    public void addNewCelebrity() {
        selectedAccount = new Account();
        setEditMode(false);
        showDialog("dlgCelebrity");
        updates("catFrm");

    }

    /**
     * @param account
     */
    public void deleteCelebrity(Account account) {
        try {
            accountService.delete(account);
        } catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
            addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
            return;
        }
        updates("catDtl");
        addSuccessMessage();

    }

    /**
     *
     */
    public void active(Account account) {
        account.setActive(true);
        account = accountService.update(account);
    }

    public void deactivate(Account account) {
        account.setActive(false);
        account = accountService.update(account);
    }

    public boolean saveCelebrity() {
        try {
            selectedAccount.setAccountType(AccountType.CELEBRATED);
            selectedAccount = accountService.save(selectedAccount);
            selectedAccount = new Account();
        } catch (DataIntegrityViolationException ex) {
            addErrorMessage(ex);
            return false;
        }
        updates("catDtl", "catFrm");
        addSuccessMessage();
        return true;
    }

    public void upload(FileUploadEvent event) throws Exception {
        String name = Utils.uploud(event, ACCOUNT);
        Image image = new Image();
        if (selectedAccount.getImage() != null) {
            image = selectedAccount.getImage();
        }
        image.setName(name);
        image.setPriorityOrder(1L);
        //image.setPic(event.getFile().getContents());
        image = imageService.save(image);
        selectedAccount.setImage(image);
    }

    public void saveCelebrityAndHide() {
        if (saveCelebrity()) {
            hideDialog("dlgCelebrity");

        }

    }

    public AccountService getAccountService() {
        return accountService;
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    public ImageService getImageService() {
        return imageService;
    }

    public void setImageService(ImageService imageService) {
        this.imageService = imageService;
    }

    public Account getSelectedAccount() {
        return selectedAccount;
    }

    public void setSelectedAccount(Account selectedAccount) {
        this.selectedAccount = selectedAccount;
    }


    public CustomLazyDataModel<Account> getUserAccountCustomLazyDataModel() {
        return userAccountCustomLazyDataModel;
    }

    public void setUserAccountCustomLazyDataModel(CustomLazyDataModel<Account> userAccountCustomLazyDataModel) {
        this.userAccountCustomLazyDataModel = userAccountCustomLazyDataModel;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }
}
