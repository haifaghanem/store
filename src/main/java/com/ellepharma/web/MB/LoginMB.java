package com.ellepharma.web.MB;

import com.ellepharma.data.model.Account;
import com.ellepharma.data.model.AccountType;
import com.ellepharma.utils.JsfUtils;
import com.ellepharma.utils.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;

@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginMB extends BaseMB {

    private static final Logger LOG = LoggerFactory.getLogger(LoginMB.class);

    private String username;

    private String password;

    private boolean loggedIn;
    private boolean accountLoggedIn;

    @ManagedProperty(value = "#{authenticationManager}")
    private AuthenticationManager authManager;

    @ManagedProperty(value = "#{countryDetectMB}")
    private CountryDetectMB countryDetectMB;
    private Account account;

    public String login(boolean redirect) {
        String page = login();
        if (loggedIn) {
            if (redirect) {
                return page;
            } else {
                JsfUtils.executeScript("location.reload()");
            }
        }
        return null;
    }

    public String login() {
        loggedIn = false;
        LOG.info("Starting login from LoginManagedBean");
        try {

            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(username,
                    password);
            Authentication authenticate = authManager.authenticate(usernamePasswordAuthenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authenticate);
            loggedIn = authenticate.isAuthenticated();
            account = SecurityUtils.getAccount(authenticate.getPrincipal());
            countryDetectMB.prepareCCS(account.getCountry());


            return goToAccountPage();
        } catch (final Exception e) {
            e.printStackTrace();
            LOG.error("Error log in " + e);
            addErrorMessage("Invalid.login", "form_messages");

        }

        return null;
    }

    public String goToAccountPage() {
        if (account == null || account.getAccountType() == null)
            return "/";

        if (account.getAccountType().equals(AccountType.ADMIN)) {

            return "admin/index.xhtml?faces-redirect=true";
        } else {
            accountLoggedIn = true;
            return "/customer/account.xhtml?faces-redirect=true";


        }
    }

    public void logout() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        SecurityContextHolder.clearContext();
        JsfUtils.redirect("logout");
        JsfUtils.executeScript("location.reload()");

    }

    public String getUsername() {
        return username;
    }

    public Account getCurrentAccount() {
        return SecurityUtils.getCurrentAccount();
    }

    public Account getCurrentUser() {
        return SecurityUtils.getCurrentUser();
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AuthenticationManager getAuthManager() {
        return authManager;
    }

    public void setAuthManager(AuthenticationManager authManager) {
        this.authManager = authManager;
    }

    public boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public boolean isAccountLoggedIn() {
        return accountLoggedIn;
    }

    public void setAccountLoggedIn(boolean accountLoggedIn) {
        this.accountLoggedIn = accountLoggedIn;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setCountryDetectMB(CountryDetectMB countryDetectMB) {
        this.countryDetectMB = countryDetectMB;
    }

}