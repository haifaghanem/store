package com.ellepharma.web.MB;

import com.ellepharma.data.dto.ItemDTO;
import com.ellepharma.data.dto.SortTypeEnum;
import com.ellepharma.data.model.Category;
import com.ellepharma.data.model.Item;
import com.ellepharma.data.model.Manufacturer;
import com.ellepharma.service.CategoryService;
import com.ellepharma.service.ItemService;
import com.ellepharma.service.ManufacturerService;
import com.ellepharma.utils.JsfUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.TreeNode;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ellepharma.utils.Constants.*;

/**
 * ShopMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 31, 2019
 */
@ManagedBean
@ViewScoped
public class ShopMB extends BaseMB {

    @ManagedProperty("#{itemService}")
    private ItemService itemService;

    @ManagedProperty("#{categoryService}")
    private CategoryService categoryService;

    @ManagedProperty("#{manufacturerService}")
    private ManufacturerService manufacturerService;

    private List<SortTypeEnum> sortWays;

    private int count;

    private int currentPage;

    private int fromPage;

    private int toPage;

    private int fromItem;

    private int toItem;

    private int sizePage = 12;

    private String view = "grid-view";

    private String searchByName;

    private List<Item> itemList = new ArrayList<>();

    private List<Category> categoryList = new ArrayList<>();

    private List<Manufacturer> brandList = new ArrayList<>();

    private Pageable pageable;

    private ItemDTO itemDTO;

    private TreeNode root3;

    private TreeNode[] selectedNodes2;

    @PostConstruct
    public void init() {
        sortWays = Arrays.asList(SortTypeEnum.values());
        pageable = new PageRequest(0, sizePage);
        categoryList = categoryService.findAllActive();
        root3 = categoryService.createCheckboxDocuments();
        brandList = manufacturerService.findAllActive();
        itemDTO = new ItemDTO();
        itemDTO.setFromPrice(0d);
        itemDTO.setToPrice(10000d);
        itemDTO.setSortWay(SortTypeEnum.ASC);

    }

    public void goToShop(String catId) {
        init();
        JsfUtils.redirect("/shop.xhtml", CAT_IDS + "=" + catId);

    }

    public void chanePageSize() {
        pageable = new PageRequest(0, sizePage);
        search();
    }

    public void changeView() {
        view = JsfUtils.getRequestParameterMap("view");

    }

    public void search() {
        if (StringUtils.isEmpty(itemDTO.getSearchByName())) {
            itemDTO.setSearchByName(null);
        }
        itemList = itemService.findItems(itemDTO, pageable);
        count = itemService.findItemsCount(itemDTO).intValue();
        currentPage = pageable.getPageNumber();
        toPage = (int) Math.ceil((double) count / (double) pageable.getPageSize());
        fromPage = pageable.getPageNumber();
        fromItem = count != 0 ? (currentPage) * sizePage + 1 : 0;
        toItem = (currentPage + 1) * sizePage;
        if (toItem >= count) {
            toItem = count;
        }
    }

    public void next() {
        pageable = pageable.next();
        currentPage = pageable.getPageNumber();
        prepareUrl(false);
    }

    public void previous() {
        pageable = pageable.previousOrFirst();
        currentPage = pageable.getPageNumber();
        prepareUrl(false);
    }

    public void changePage() {
        pageable = new PageRequest(currentPage, sizePage);
        currentPage = pageable.getPageNumber();
        prepareUrl(false);
    }

    public void prepareForSearch() {
        sizePage = 12;
        String cat = JsfUtils.getRequestParameterMap(CAT_IDS);
        String brand = JsfUtils.getRequestParameterMap(BRAND_IDS);
        String sortType = JsfUtils.getRequestParameterMap(SORT_TYPE);
        String currentPage = JsfUtils.getRequestParameterMap(CURRENT_PAGE);
        searchByName = JsfUtils.getRequestParameterMap(SEARCH_BY_NAME);
        this.currentPage = StringUtils.isNotEmpty(currentPage) ? getInteger(currentPage) : 0;
        pageable = new PageRequest(this.currentPage, sizePage);
        itemDTO.setCatIds(cat != null ? getListLogFromUrl(cat) : null);
        itemDTO.setBrandIds(brand != null ? getListLogFromUrl(brand) : null);
        itemDTO.setSortWay(sortType != null ? SortTypeEnum.valueOf(sortType) : SortTypeEnum.ASC);
        itemDTO.setSearchByName(searchByName);
        prepareSelected(root3);
//        if (CollectionUtils.isNotEmpty(itemDTO.getCatIds())) {
//            selectedNodes2 = null;
//
//        }
        search();
    }

    private Integer getInteger(String currentPage) {
        try {
            int p = new Integer(currentPage);
            return p > 0 ? p - 1 : 0;

        } catch (Exception e) {
            return 0;

        }
    }

    private void prepareSelected(TreeNode nood) {
        nood.setSelected(false);
        Object i = nood.getData();
        if (i != null) {
            Category category = (Category) i;
            if (itemDTO.getCatIds() != null) {
                if (itemDTO.getCatIds().contains(category.getId()) || (category.getParent() != null &&
                        itemDTO.getCatIds().contains(category.getParent().getId()))) {
                    nood.setSelected(true);
                    selectedNodes2 = ArrayUtils.add(selectedNodes2, nood);
                }
            }
        }

        if (CollectionUtils.isNotEmpty(nood.getChildren())) {
            nood.getChildren().forEach(treeNode -> {
                prepareSelected(treeNode);
            });
        }
    }

    private List<Long> getListLogFromUrl(String cat) {
        List<Long> longs = new ArrayList<>();
        for (String i : cat.split(",")) {
            try {
                longs.add(Long.parseLong(i));
            } catch (Exception e) {

            }

        }
        return longs;

    }

    public void filterPrice() {
        try {

            Double fromPrice = new Double(JsfUtils.getRequestParameterMap("fromPrice"));
            Double toPrice = new Double(JsfUtils.getRequestParameterMap("toPrice"));
            itemDTO.setFromPrice(fromPrice);
            itemDTO.setToPrice(toPrice);
            filter();
        } catch (Exception e) {

        }

    }

    public void filterCat() {
        List<Long> ids = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(selectedNodes2)) {

            for (int i = 0; i < selectedNodes2.length; i++) {
                Category c = (Category) selectedNodes2[i].getData();
                if (c != null) {
                    ids.add(c.getId());
                }

            }

        }
        itemDTO.setCatIds(ids);
        filter();
    }

    public void filter() {
        currentPage = 0;
        sizePage = 12;
        prepareUrl(true);

    }

    private void prepareUrl(boolean resetPage) {
        if (resetPage) {

            pageable = new PageRequest(0, sizePage);
        }
        search();

        String filter = JsfUtils.getContextPath() + "/shop.xhtml?";

        if (StringUtils.isNotEmpty(itemDTO.getSearchByName())) {
            if (!filter.endsWith("?")) {
                filter = filter + "&";
            }
            filter = filter + SEARCH_BY_NAME + "=" + itemDTO.getSearchByName();

        }
        if (CollectionUtils.isNotEmpty(itemDTO.getCatIds())) {
            if (!filter.endsWith("?")) {
                filter = filter + "&";
            }
            filter = filter + CAT_IDS + "=" + StringUtils.join(itemDTO.getCatIds(), ",");
        }
        if (CollectionUtils.isNotEmpty(itemDTO.getBrandIds())) {
            if (!filter.endsWith("?")) {
                filter = filter + "&";
            }
            filter = filter + BRAND_IDS + "=" + StringUtils.join(itemDTO.getBrandIds(), ",");
        }

        if (itemDTO.getSortWay() != null) {
            if (!filter.endsWith("?")) {
                filter = filter + "&";
            }
            filter = filter + SORT_TYPE + "=" + itemDTO.getSortWay();
        }

        if (pageable.getPageNumber() >= 0) {
            if (!filter.endsWith("?")) {
                filter = filter + "&";
            }
            filter = filter + CURRENT_PAGE + "=" + (this.currentPage + 1);
        }

        JsfUtils.executeScript("window.history.replaceState({},'','" + filter + "');");
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public ItemService getItemService() {
        return itemService;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getFromPage() {
        return fromPage;
    }

    public void setFromPage(int fromPage) {
        this.fromPage = fromPage;
    }

    public int getToPage() {
        return toPage;
    }

    public void setToPage(int toPage) {
        this.toPage = toPage;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public int getFromItem() {
        return fromItem;
    }

    public int getToItem() {
        return toItem;
    }

    public void setFromItem(int fromItem) {
        this.fromItem = fromItem;
    }

    public void setToItem(int toItem) {
        this.toItem = toItem;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public List<Manufacturer> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<Manufacturer> brandList) {
        this.brandList = brandList;
    }

    public void setManufacturerService(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    public ItemDTO getItemDTO() {
        return itemDTO;
    }

    public void setItemDTO(ItemDTO itemDTO) {
        this.itemDTO = itemDTO;
    }

    public List<SortTypeEnum> getSortWays() {
        return sortWays;
    }

    public void setSortWays(List<SortTypeEnum> sortWays) {
        this.sortWays = sortWays;
    }

    public TreeNode getRoot3() {
        return root3;
    }

    public void setRoot3(TreeNode root3) {
        this.root3 = root3;
    }

    public TreeNode[] getSelectedNodes2() {
        return selectedNodes2;
    }

    public void setSelectedNodes2(TreeNode[] selectedNodes2) {
        this.selectedNodes2 = selectedNodes2;
    }

    public String getSearchByName() {
        return searchByName;
    }

    public void setSearchByName(String searchByName) {
        this.searchByName = searchByName;
    }

    public int getSizePage() {
        return sizePage;
    }

    public void setSizePage(int sizePage) {
        this.sizePage = sizePage;
    }
}