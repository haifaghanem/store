package com.ellepharma.web.MB;

import com.ellepharma.utils.JsfUtils;
import com.ellepharma.utils.Utils;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.Map;

/**
 * BaseMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */
public class BaseMB implements Serializable {

    private static final long serialVersionUID = 1L;
    private boolean editMode;
    private boolean viewMode;

	public void addSuccessMessage() {
		JsfUtils.addSuccessMessage();

	}

	public void addSuccessMessage(String key) {
		JsfUtils.addSuccessMessage(key);

	}

	public void addErrorMessage(Exception e) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, Utils.getMsg("common.addErrorMessage") + " " + e.getMessage(),
				null);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage("growl", message);
		JsfUtils.update("form_messages");

	}

	public void addErrorMessage(String key) {
		JsfUtils.addErrorMessage(key);

	}

	public void addWarningMessage(String key) {
		JsfUtils.addWarningMessage(key);

	}

    public void addErrorMessage(String key, UIComponent component) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, Utils.getMsg(key), null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(component.getClientId(), message);
    }

    public void addErrorMessage(String key, String id) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, Utils.getMsg(key), null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(id, message);
    }

    public void addWarningMessage(String key, UIComponent component) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, Utils.getMsg(key), null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(component.getClientId(), message);
    }

    public void addInfoMessage(String key) {
        keepMessage();
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, Utils.getMsg(key), null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growl", message);
    }
    public void addDirectInfoMessage(String msg) {
        keepMessage();
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growl", message);
    }

    public void addInfoMessage(String key, String id) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, Utils.getMsg(key), null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(id, message);
    }

    public void showDialog(String dialogVar) {
        JsfUtils.openDialog(dialogVar);
    }


    public void updates(String... componentIds) {
            JsfUtils.update(componentIds);
    }

    public void hideDialog(String dialogVar) {
        JsfUtils.hideDialog(dialogVar);
    }

    public void keepMessage() {
        JsfUtils.keepMessage();
    }

    protected HttpSession getSession(boolean create) {
        return (HttpSession) getExternalContext().getSession(create);
    }

    protected FacesContext getContext() {
        return FacesContext.getCurrentInstance();
    }

    protected ExternalContext getExternalContext() {
        return FacesContext.getCurrentInstance().getExternalContext();
    }

    protected HttpServletRequest getHttpServletRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }

    protected HttpServletResponse getHttpServletResponse() {
        return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
    }

    protected ServletContext getServletContext() {
        return (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
    }

    protected String getRequestParam(String name) {
        return getContext().getExternalContext().getRequestParameterMap().get(name);
    }

    protected Map<String, Object> getSessionMap(String name) {

        Map<String, Object> sessionMap = getExternalContext().getSessionMap();

        return (Map<String, Object>) sessionMap.get(name);
    }

    protected ELContext getELContext() {
        return getContext().getELContext();
    }

    protected Application getApplication() {
        return getContext().getApplication();
    }

    protected ExpressionFactory getExpressionFactory() {
        return getApplication().getExpressionFactory();
    }

    protected Object getELValue(Object base, Object property) {
        return getELContext().getELResolver().getValue(getELContext(), base, property);
    }

    public static void setExpressionValue(final Object value, final String expression) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ELContext elContext = facesContext.getELContext();

        ValueExpression targetExpression = facesContext.getApplication().getExpressionFactory()
                .createValueExpression(elContext, expression, Object.class);
        targetExpression.setValue(elContext, value);
    }

    public static Object getExpressionValue(final String expression) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ELContext elContext = facesContext.getELContext();

        ValueExpression targetExpression = facesContext.getApplication().getExpressionFactory()
                .createValueExpression(elContext, expression, Object.class);
        return targetExpression.getValue(elContext);
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public boolean isViewMode() {
        return viewMode;
    }

    public void setViewMode(boolean viewMode) {
        this.viewMode = viewMode;
    }

    public String getCurrentLanguage() {
        return Utils.getCurrentLang().getLocale();
    }
}
