package com.ellepharma.web.MB;

import com.ellepharma.data.model.Image;
import com.ellepharma.data.model.Item;
import com.ellepharma.data.repository.ImageRepo;
import com.ellepharma.data.repository.ItemRepo;
import com.ellepharma.utils.Utils;
import com.google.common.io.Files;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * CategoryMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 01, 2019
 */
@ManagedBean
@ViewScoped
public class LinkImageMB extends BaseMB {

    @ManagedProperty("#{itemRepo}")
    private ItemRepo itemRepo;

    @ManagedProperty("#{imageRepo}")
    private ImageRepo imageRepo;

    private List<String> imageDone = new ArrayList<>();
    private List<String> imageFailed = new ArrayList<>();

    private String dir;

    @PostConstruct
    public void init() {

        imageDone = new ArrayList<>();
        imageFailed = new ArrayList<>();
    }

    public void upload(FileUploadEvent event) throws Exception {
        File tempFile = File.createTempFile("zip", "." + FilenameUtils.getExtension(event.getFile().getFileName()));

        FileUtils.writeByteArrayToFile(tempFile, event.getFile().getContents());
        File destDir = Files.createTempDir();
        dir = destDir.getAbsolutePath();

        try {
            ZipFile zipFile = new ZipFile(tempFile);

            zipFile.extractAll(dir);
        } catch (ZipException e) {
            e.printStackTrace();
        }

        System.out.println("Done!!!!!!!!");

        prepare();
    }

    public void prepare() {
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        linkImage(listOfFiles);
    }

    public void linkImage(File[] listOfFiles) {
        if (listOfFiles != null) {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    File file = listOfFiles[i];
                    String fileName = file.getName();
                    int last = fileName.lastIndexOf(".");
                    String barcode = last >= 1 ? fileName.substring(0, last) : fileName;
                    List<Item> items = itemRepo.findAllByBarcodeList(barcode);
                    if (CollectionUtils.isNotEmpty(items)) {
                        Image image = imageRepo.findByName(file.getName());
                        if (image == null) {
                            image = new Image();
                        }
                        image.setName(file.getName());
                        image.setPriorityOrder(1L);
                        try {

                            Utils.getContentService().putContent("store", file.getName(), new FileInputStream(file), file.length(), "image/jpeg");
                            image = imageRepo.save(image);
                            imageDone.add(file.getName());
                            for (Item item : items) {
                                if (item.getImage() == null) {
                                    item.setImage(new ArrayList<>());
                                }
                                Image finalImage = image;
                                item.getImage().removeIf(image1 -> Objects.equals(image1.getId(), finalImage.getId()));
                                item.getImage().add(image);
                            }
                            itemRepo.save(items);
                        } catch (Exception e) {
                            imageFailed.add(file.getName());
                            e.printStackTrace();
                        }
                    } else {
                        imageFailed.add(file.getName());

                    }
                    System.out.println("File " + listOfFiles[i].getName());
                } else if (listOfFiles[i].isDirectory()) {
                    System.out.println("Directory " + listOfFiles[i].getName());
                    linkImage(listOfFiles[i].listFiles());
                }
            }
        }
    }

    public void setItemRepo(ItemRepo itemRepo) {
        this.itemRepo = itemRepo;
    }

    public void setImageRepo(ImageRepo imageRepo) {
        this.imageRepo = imageRepo;
    }

    public List<String> getImageDone() {
        return imageDone;
    }

    public void setImageDone(List<String> imageDone) {
        this.imageDone = imageDone;
    }

    public List<String> getImageFailed() {
        return imageFailed;
    }

    public void setImageFailed(List<String> imageFailed) {
        this.imageFailed = imageFailed;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }
}
