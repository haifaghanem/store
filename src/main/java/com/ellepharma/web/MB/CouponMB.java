package com.ellepharma.web.MB;

import com.ellepharma.data.model.Coupon;
import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.Item;
import com.ellepharma.data.model.Order;
import com.ellepharma.service.CouponService;
import com.ellepharma.service.OrderService;
import com.ellepharma.utils.ExceptionCodes;
import com.ellepharma.utils.SecurityUtils;
import com.ellepharma.utils.Utils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

/**
 * CouponMB.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 28, 2019
 */
@ManagedBean
@ViewScoped
public class CouponMB extends BaseMB {

	@ManagedProperty("#{couponService}")
	private CouponService couponService;

	@ManagedProperty("#{orderService}")
	private OrderService orderService;

	private Coupon selectedCoupon;

	private CustomLazyDataModel<Coupon> couponCustomLazyDataModel;

	private List<Coupon> coupons;

	private CustomLazyDataModel<Coupon> myCouponCustomLazyDataModel;

	private CustomLazyDataModel<Order> orderCustomLazyDataModel;

	private CustomLazyDataModel<Item> itemCustomLazyDataModel;

	private String code = "";

	private Double totalAll;

	private Double totalAllItem;

	private Double totalCommissions;

	private Date from;

	private Date to;

	@PostConstruct
	public void init() {
		from = Date.from(LocalDateTime.now().minusMonths(1).atZone(ZoneId.of(Utils.getDefaultZoneId())).toInstant());
		to = Date.from(LocalDateTime.now().atZone(ZoneId.of(Utils.getDefaultZoneId())).toInstant());
		selectedCoupon = new Coupon();
		couponCustomLazyDataModel = new CustomLazyDataModel<>(couponService);
		myCouponCustomLazyDataModel = new CustomLazyDataModel(couponService, "findByCelebrity",
				Arrays.asList(SecurityUtils.getCurrentAccount()));

	}

	/**
	 *
	 */
	public void addNewCoupon() {
		selectedCoupon = new Coupon();
		setEditMode(false);
		showDialog("dlgCoupon");
		updates("couponFrm");

	}

	/**
	 * @param coupon
	 */
	public void editCoupon(Coupon coupon) {
		selectedCoupon = coupon;
		setEditMode(true);
		showDialog("dlgCoupon");
		updates("couponFrm", "couponDtl");
	}

	/**
	 * @param coupon
	 */
	public void deleteCoupon(Coupon coupon) {
		try {
			couponService.delete(coupon);
		} catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
			addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
			return;
		}
		updates("couponDtl");
		addSuccessMessage();

	}

	/**
	 *
	 */
	public boolean saveCoupon() {
		try {
			selectedCoupon = couponService.save(selectedCoupon);
			selectedCoupon = new Coupon();
		} catch (Exception ex) {
			addErrorMessage(ex);
			return false;
		}
		updates("couponDtl", "couponFrm");
		addSuccessMessage();
		return true;
	}

	public void defaultFind() {
		if (myCouponCustomLazyDataModel != null && CollectionUtils.isNotEmpty(myCouponCustomLazyDataModel.getWrappedData())) {
			List<Coupon> i = myCouponCustomLazyDataModel.getWrappedData();
			code = i.get(0).getCouponNumber();
			findOrderByCoupon();
		}
	}

	public void findOrderByCoupon() {

		//totalAll = orderService.sumOrdersByCouponCode(code);
		//        totalAllItem = orderService.sumItemOrdersByCouponCode(code);
		Map<String, Object> map = new HashMap();
		map.put("code", code);
		map.put("from", from);
		map.put("to", to);
		totalCommissions = orderService.totalCommissions(code, from, to);
		orderCustomLazyDataModel = new CustomLazyDataModel<>(orderService, "findByCoupon", Arrays.asList(map));
		itemCustomLazyDataModel = new CustomLazyDataModel<>(orderService, "findItemsINOrderByCoupon", Arrays.asList(map));
	}

	public void saveCouponeAndHide() {
		if (saveCoupon()) {
			hideDialog("dlgCoupon");
		}
	}

	public CouponService getCouponService() {
		return couponService;
	}

	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}

	public Coupon getSelectedCoupon() {
		return selectedCoupon;
	}

	public void setSelectedCoupon(Coupon selectedCoupon) {
		this.selectedCoupon = selectedCoupon;
	}

	public CustomLazyDataModel<Coupon> getCouponCustomLazyDataModel() {
		return couponCustomLazyDataModel;
	}

	public void setCouponCustomLazyDataModel(CustomLazyDataModel<Coupon> couponCustomLazyDataModel) {
		this.couponCustomLazyDataModel = couponCustomLazyDataModel;
	}

	public CustomLazyDataModel<Order> getOrderCustomLazyDataModel() {
		return orderCustomLazyDataModel;
	}

	public void setOrderCustomLazyDataModel(CustomLazyDataModel<Order> orderCustomLazyDataModel) {
		this.orderCustomLazyDataModel = orderCustomLazyDataModel;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getTotalAll() {
		return totalAll;
	}

	public void setTotalAll(Double totalAll) {
		this.totalAll = totalAll;
	}

	public CustomLazyDataModel<Coupon> getMyCouponCustomLazyDataModel() {
		return myCouponCustomLazyDataModel;
	}

	public void setMyCouponCustomLazyDataModel(CustomLazyDataModel<Coupon> myCouponCustomLazyDataModel) {
		this.myCouponCustomLazyDataModel = myCouponCustomLazyDataModel;
	}

	public Double getTotalAllItem() {
		return totalAllItem;
	}

	public void setTotalAllItem(Double totalAllItem) {
		this.totalAllItem = totalAllItem;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public OrderService getOrderService() {
		return orderService;
	}

	public Double getTotalCommissions() {
		return totalCommissions;
	}

	public void setTotalCommissions(Double totalCommissions) {
		this.totalCommissions = totalCommissions;
	}

	public CustomLazyDataModel<Item> getItemCustomLazyDataModel() {
		return itemCustomLazyDataModel;
	}

	public void setItemCustomLazyDataModel(CustomLazyDataModel<Item> itemCustomLazyDataModel) {
		this.itemCustomLazyDataModel = itemCustomLazyDataModel;
	}

	public List<Coupon> getCoupons() {
		if (coupons == null) {
			coupons = couponService.findAll();
		}
		return coupons;
	}

	public void setCoupons(List<Coupon> coupons) {
		this.coupons = coupons;
	}
}
