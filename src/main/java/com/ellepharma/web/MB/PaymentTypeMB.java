package com.ellepharma.web.MB;

import com.ellepharma.data.model.CountryCashOnDelivery;
import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.PaymentType;
import com.ellepharma.service.PaymentTypeService;
import com.ellepharma.utils.ExceptionCodes;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;

/**
 * PaymentTypeMB.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since Nov 17, 2020
 */
@ManagedBean
@ViewScoped
public class PaymentTypeMB extends BaseMB {

	@ManagedProperty("#{paymentTypeService}")
	private PaymentTypeService paymentTypeService;

	private PaymentType selectedPaymentType;

	private CountryCashOnDelivery countryCashOnDelivery;

	private boolean editCountryMode;

	private CustomLazyDataModel<PaymentType> paymentTypeCustomLazyDataModel;

	@PostConstruct
	public void init() {
		selectedPaymentType = new PaymentType();
		paymentTypeCustomLazyDataModel = new CustomLazyDataModel<>(paymentTypeService);
	}

	/**
	 *
	 */
	public void addNewPaymentType() {
		selectedPaymentType = new PaymentType();
		setEditMode(false);
		showDialog("dlgPaymentType");
		updates("payFrm");

	}

	/**
	 * @param paymentType
	 */
	public void editPaymentType(PaymentType paymentType) {
		selectedPaymentType = paymentType;
		setEditMode(true);
		showDialog("dlgPaymentType");
		updates("payFrm", "payDtl");
	}

	/**
	 * @param paymentType
	 */
	public void deletePaymentType(PaymentType paymentType) {
		try {
			paymentTypeService.delete(paymentType);

		} catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
			addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
			return;
		}
		updates("payDtl");
		addSuccessMessage();

	}

	/**
	 *
	 */
	public boolean savePaymentType() {
		try {
			selectedPaymentType = paymentTypeService.save(selectedPaymentType);
			selectedPaymentType = new PaymentType();
		} catch (DataIntegrityViolationException ex) {
			addErrorMessage(ex);
			return false;
		}
		updates("payDtl", "payFrm");
		addSuccessMessage();
		return true;
	}

	public void savePaymentTypeAndHide() {
		if (savePaymentType()) {
			hideDialog("dlgPaymentType");

		}
	}

	public void addNewCountry() {
		countryCashOnDelivery = new CountryCashOnDelivery();
		editCountryMode = false;
		showDialog("dlgCountry");
		updates("countryFrm");

	}

	/**
	 * @param countryCashOnDelivery
	 */
	public void editCountry(CountryCashOnDelivery countryCashOnDelivery) {
		this.countryCashOnDelivery = countryCashOnDelivery;
		editCountryMode = true;
		showDialog("dlgCountry");
		updates("countryFrm", "countryDtl");
	}

	/**
	 * @param countryCashOnDelivery
	 */
	public void deleteCountry(CountryCashOnDelivery countryCashOnDelivery) {
		selectedPaymentType.getCountryCashOnDeliveries().remove(countryCashOnDelivery);
		updates("countryDtl");
		addSuccessMessage();

	}

	public void editCountryAndHide() {
		int index = selectedPaymentType.getCountryCashOnDeliveries().indexOf(countryCashOnDelivery);
		selectedPaymentType.getCountryCashOnDeliveries().remove(countryCashOnDelivery);
		selectedPaymentType.getCountryCashOnDeliveries().add(index, countryCashOnDelivery);
		countryCashOnDelivery = new CountryCashOnDelivery();
		updates("countryDtl", "countryFrm");
		hideDialog("dlgCountry");
	}

	public boolean saveCountry() {
		selectedPaymentType.getCountryCashOnDeliveries().add(countryCashOnDelivery);
		countryCashOnDelivery = new CountryCashOnDelivery();
		updates("countryDtl", "countryFrm");
		return true;
	}

	public void saveCountryAndHide() {
		selectedPaymentType.getCountryCashOnDeliveries().add(countryCashOnDelivery);
		countryCashOnDelivery = new CountryCashOnDelivery();
		updates("countryDtl", "countryFrm");
		hideDialog("dlgCountry");

	}

	public PaymentTypeService getPaymentTypeService() {
		return paymentTypeService;
	}

	public void setPaymentTypeService(PaymentTypeService paymentTypeService) {
		this.paymentTypeService = paymentTypeService;
	}

	public PaymentType getSelectedPaymentType() {
		return selectedPaymentType;
	}

	public void setSelectedPaymentType(PaymentType selectedPaymentType) {
		this.selectedPaymentType = selectedPaymentType;
	}

	public CustomLazyDataModel<PaymentType> getPaymentTypeCustomLazyDataModel() {
		return paymentTypeCustomLazyDataModel;
	}

	public void setPaymentTypeCustomLazyDataModel(CustomLazyDataModel<PaymentType> paymentTypeCustomLazyDataModel) {
		this.paymentTypeCustomLazyDataModel = paymentTypeCustomLazyDataModel;
	}

	public CountryCashOnDelivery getCountryCashOnDelivery() {
		return countryCashOnDelivery;
	}

	public void setCountryCashOnDelivery(CountryCashOnDelivery countryCashOnDelivery) {
		this.countryCashOnDelivery = countryCashOnDelivery;
	}

	public boolean getEditCountryMode() {
		return editCountryMode;
	}

	public void setEditCountryMode(boolean editCountryMode) {
		this.editCountryMode = editCountryMode;
	}
}
