package com.ellepharma.web.MB;

import com.ellepharma.constant.ConfigEnum;
import com.ellepharma.constant.TransField;
import com.ellepharma.data.model.*;
import com.ellepharma.data.repository.CityRepo;
import com.ellepharma.service.*;
import com.ellepharma.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class DataInitializer implements Serializable {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ShipmentTypeService shipmentTypeService;

    @Autowired
    private PaymentTypeService paymentTypeService;

    @Autowired
    private CountryShipmentService countryShipmentService;

    @Autowired
    private AdsService adsService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private CityRepo cityRepo;

    public void init() {
        System.out.println("*****************************************************************************************");
        System.out.println("Start Prepare Data");

        try {

       /*     if (cityRepo.count() == 0) {
                List<String> codecountry = Arrays.asList("BH", "EG", "FI", "IQ", "JO",
                        "KW", "LB", "OM", "SA", "SS", "SE", "AE");
                codecountry.forEach(country -> {
                    List<String> ci = countryService.cities(country);
                    ci.forEach(a -> {
                        City city = new City();
                        city.setName(a);
                        city.setCountryCode(country);
                        cityRepo.save(city);
                    });

                });
            }*/
            if (countryService.count() == 0) {
                countryService.save(new Country(1L, "أفغانستان", "Afghanistan", "AF", "AFG"));
                countryService.save(new Country(2L, "جزر آلاند", "Åland Islands", "AX", "ALA"));
                countryService.save(new Country(3L, "ألبانيا", "Albania", "AL", "ALB"));
                countryService.save(new Country(4L, "الجزائر", "Algeria", "DZ", "DZA"));
                countryService.save(new Country(5L, "ساموا الأمريكية", "American Samoa", "AS", "ASM"));
                countryService.save(new Country(6L, "أندورا", "Andorra", "AD", "AND"));
                countryService.save(new Country(7L, "أنغولا", "Angola", "AO", "AGO"));
                countryService.save(new Country(8L, "أنغيلا", "Anguilla", "AI", "AIA"));
                countryService.save(new Country(9L, "أنتيغوا وبربودا", "Antigua and Barbuda", "AG", "ATG"));
                countryService.save(new Country(10L, "الأرجنتين", "Argentina", "AR", "ARG"));
                countryService.save(new Country(11L, "أرمينيا", "Armenia", "AM", "ARM"));
                countryService.save(new Country(12L, "أروبا", "Aruba", "AW", "ABW"));
                countryService.save(new Country(13L, "أستراليا", "Australia", "AU", "AUS"));
                countryService.save(new Country(14L, "النمسا", "Austria", "AT", "AUT"));
                countryService.save(new Country(15L, "أذربيجان", "Azerbaijan", "AZ", "AZE"));
                countryService.save(new Country(16L, "الباهاما", "Bahamas", "BS", "BHS"));
                countryService.save(new Country(17L, "البحرين", "Bahrain", "BH", "BHR"));
                countryService.save(new Country(18L, "بنغلاديش", "Bangladesh", "BD", "BGD"));
                countryService.save(new Country(19L, "بربادوس", "Barbados", "BB", "BRB"));
                countryService.save(new Country(20L, "روسيا البيضاء", "Belarus", "BY", "BLR"));
                countryService.save(new Country(21L, "بلجيكا", "Belgium", "BE", "BEL"));
                countryService.save(new Country(22L, "بليز", "Belize", "BZ", "BLZ"));
                countryService.save(new Country(23L, "بنين", "Benin", "BJ", "BEN"));
                countryService.save(new Country(24L, "برمودا", "Bermuda", "BM", "BMU"));
                countryService.save(new Country(25L, "بوتان", "Bhutan", "BT", "BTN"));
                countryService.save(new Country(26L, "بوليفيا (دولة متعددة القوميات)", "Bolivia (Plurinational State of)", "BO", "BOL"));
                countryService.save(new Country(27L, "بونير، سينت أوستاتيوس وسابا", "Bonaire, Sint Eustatius and Saba", "BQ", "BES"));
                countryService.save(new Country(28L, "البوسنة والهرسك", "Bosnia and Herzegovina", "BA", "BIH"));
                countryService.save(new Country(29L, "بوتسوانا", "Botswana", "BW", "BWA"));
                countryService.save(new Country(30L, "البرازيل", "Brazil", "BR", "BRA"));
                countryService.save(new Country(31L, "جزر فيرجن البريطانية", "British Virgin Islands", "VG", "VGB"));
                countryService.save(new Country(32L, "بروناي دار السلام", "Brunei Darussalam", "BN", "BRN"));
                countryService.save(new Country(33L, "بلغاريا", "Bulgaria", "BG", "BGR"));
                countryService.save(new Country(34L, "بوركينا فاسو", "Burkina Faso", "BF", "BFA"));
                countryService.save(new Country(35L, "بوروندي", "Burundi", "BI", "BDI"));
                countryService.save(new Country(36L, "كابو فيردي", "Cabo Verde", "CV", "CPV"));
                countryService.save(new Country(37L, "كمبوديا", "Cambodia", "KH", "KHM"));
                countryService.save(new Country(38L, "الكاميرون", "Cameroon", "CM", "CMR"));
                countryService.save(new Country(39L, "كندا", "Canada", "CA", "CAN"));
                countryService.save(new Country(40L, "جزر كايمان", "Cayman Islands", "KY", "CYM"));
                countryService.save(new Country(41L, "جمهورية افريقيا الوسطى", "Central African Republic", "CF", "CAF"));
                countryService.save(new Country(42L, "تشاد", "Chad", "TD", "TCD"));
                countryService.save(new Country(43L, "جزر القناة", "Channel Islands", "", ""));
                countryService.save(new Country(44L, "تشيلي", "Chile", "CL", "CHL"));
                countryService.save(new Country(45L, "الصين", "China", "CN", "CHN"));
                countryService
                        .save(new Country(46L, "الصين ومنطقة هونغ كونغ الإدارية الخاصة", "China, Hong Kong Special Administrative Region",
                                "HK", "HKG"));
                countryService
                        .save(new Country(47L, "الصين ومنطقة ماكاو الإدارية الخاصة", "China, Macao Special Administrative Region", "MO",
                                "MAC"));
                countryService.save(new Country(48L, "كولومبيا", "Colombia", "CO", "COL"));
                countryService.save(new Country(49L, "جزر القمر", "Comoros", "KM", "COM"));
                countryService.save(new Country(50L, "الكونغو", "Congo", "CG", "COG"));
                countryService.save(new Country(51L, "جزر كوك", "Cook Islands", "CK", "COK"));
                countryService.save(new Country(52L, "كوستا ريكا", "Costa Rica", "CR", "CRI"));
                countryService.save(new Country(53L, "كوت ديفوار", "Côte d'Ivoire", "CI", "CIV"));
                countryService.save(new Country(54L, "كرواتيا", "Croatia", "HR", "HRV"));
                countryService.save(new Country(55L, "كوبا", "Cuba", "CU", "CUB"));
                countryService.save(new Country(56L, "كوراساو", "Curaçao", "CW", "CUW"));
                countryService.save(new Country(57L, "قبرص", "Cyprus", "CY", "CYP"));
                countryService.save(new Country(58L, "جمهورية التشيك", "Czech Republic", "CZ", "CZE"));
                countryService
                        .save(new Country(59L, "جمهورية كوريا الديمقراطية االشعبية", "Democratic People's Republic of Korea", "KP", "PRK"));
                countryService.save(new Country(60L, "جمهورية الكونغو الديموقراطية", "Democratic Republic of the Congo", "CD", "COD"));
                countryService.save(new Country(61L, "الدنمارك", "Denmark", "DK", "DNK"));
                countryService.save(new Country(62L, "جيبوتي", "Djibouti", "DJ", "DJI"));
                countryService.save(new Country(63L, "دومينيكا", "Dominica", "DM", "DMA"));
                countryService.save(new Country(64L, "جمهورية الدومنيكان", "Dominican Republic", "DO", "DOM"));
                countryService.save(new Country(65L, "الإكوادور", "Ecuador", "EC", "ECU"));
                countryService.save(new Country(66L, "مصر", "Egypt", "EG", "EGY"));
                countryService.save(new Country(67L, "السلفادور", "El Salvador", "SV", "SLV"));
                countryService.save(new Country(68L, "غينيا الإستوائية", "Equatorial Guinea", "GQ", "GNQ"));
                countryService.save(new Country(69L, "إريتريا", "Eritrea", "ER", "ERI"));
                countryService.save(new Country(70L, "استونيا", "Estonia", "EE", "EST"));
                countryService.save(new Country(71L, "أثيوبيا", "Ethiopia", "ET", "ETH"));
                countryService.save(new Country(72L, "جزر فارو", "Faeroe Islands", "FO", "FRO"));
                countryService.save(new Country(73L, "جزر فوكلاند (مالفيناس)", "Falkland Islands (Malvinas)", "FK", "FLK"));
                countryService.save(new Country(74L, "فيجي", "Fiji", "FJ", "FJI"));
                countryService.save(new Country(75L, "فنلندا", "Finland", "FI", "FIN"));
                countryService.save(new Country(76L, "فرنسا", "France", "FR", "FRA"));
                countryService.save(new Country(77L, "غيانا الفرنسية", "French Guiana", "GF", "GUF"));
                countryService.save(new Country(78L, "بولينيزيا الفرنسية", "French Polynesia", "PF", "PYF"));
                countryService.save(new Country(79L, "الغابون", "Gabon", "GA", "GAB"));
                countryService.save(new Country(80L, "غامبيا", "Gambia", "GM", "GMB"));
                countryService.save(new Country(81L, "جورجيا", "Georgia", "GE", "GEO"));
                countryService.save(new Country(82L, "ألمانيا", "Germany", "DE", "DEU"));
                countryService.save(new Country(83L, "غانا", "Ghana", "GH", "GHA"));
                countryService.save(new Country(84L, "جبل طارق", "Gibraltar", "GI", "GIB"));
                countryService.save(new Country(85L, "اليونان", "Greece", "GR", "GRC"));
                countryService.save(new Country(86L, "الأرض الخضراء", "Greenland", "GL", "GRL"));
                countryService.save(new Country(87L, "غرينادا", "Grenada", "GD", "GRD"));
                countryService.save(new Country(88L, "جوادلوب", "Guadeloupe", "GP", "GLP"));
                countryService.save(new Country(89L, "غوام", "Guam", "GU", "GUM"));
                countryService.save(new Country(90L, "غواتيمالا", "Guatemala", "GT", "GTM"));
                countryService.save(new Country(91L, "غيرنسي", "Guernsey", "GG", "GGY"));
                countryService.save(new Country(92L, "غينيا", "Guinea", "GN", "GIN"));
                countryService.save(new Country(93L, "غينيا بيساو", "Guinea-Bissau", "GW", "GNB"));
                countryService.save(new Country(94L, "غيانا", "Guyana", "GY", "GUY"));
                countryService.save(new Country(95L, "هايتي", "Haiti", "HT", "HTI"));
                countryService.save(new Country(96L, "الكرسي الرسولي", "Holy See", "VA", "VAT"));
                countryService.save(new Country(97L, "هندوراس", "Honduras", "HN", "HND"));
                countryService.save(new Country(98L, "اليونان", "Hungary", "HU", "HUN"));
                countryService.save(new Country(99L, "أيسلندا", "Iceland", "IS", "ISL"));
                countryService.save(new Country(100L, "الهند", "India", "IN", "IND"));
                countryService.save(new Country(101L, "أندونيسيا", "Indonesia", "ID", "IDN"));
                countryService.save(new Country(102L, "إيران (الجمهورية الإسلامية)", "Iran (Islamic Republic of)", "IR", "IRN"));
                countryService.save(new Country(103L, "العراق", "Iraq", "IQ", "IRQ"));
                countryService.save(new Country(104L, "أيرلندا", "Ireland", "IE", "IRL"));
                countryService.save(new Country(105L, "جزيرة آيل أوف مان", "Isle of Man", "IM", "IMN"));
                countryService.save(new Country(106L, "دولة فلسطين", "State of Palestine", "IL", "ISR"));
                countryService.save(new Country(107L, "إيطاليا", "Italy", "IT", "ITA"));
                countryService.save(new Country(108L, "جامايكا", "Jamaica", "JM", "JAM"));
                countryService.save(new Country(109L, "اليابان", "Japan", "JP", "JPN"));
                countryService.save(new Country(110L, "جيرسي", "Jersey", "JE", "JEY"));
                countryService.save(new Country(111L, "الأردن", "Jordan", "JO", "JOR"));
                countryService.save(new Country(112L, "كازاخستان", "Kazakhstan", "KZ", "KAZ"));
                countryService.save(new Country(113L, "كينيا", "Kenya", "KE", "KEN"));
                countryService.save(new Country(114L, "كيريباس", "Kiribati", "KI", "KIR"));
                countryService.save(new Country(115L, "الكويت", "Kuwait", "KW", "KWT"));
                countryService.save(new Country(116L, "قرغيزستان", "Kyrgyzstan", "KG", "KGZ"));
                countryService.save(new Country(117L, "جمهورية لاو الديمقراطية الشعبية", "Lao People's Democratic Republic", "LA", "LAO"));
                countryService.save(new Country(118L, "لاتفيا", "Latvia", "LV", "LVA"));
                countryService.save(new Country(119L, "لبنان", "Lebanon", "LB", "LBN"));
                countryService.save(new Country(120L, "ليسوتو", "Lesotho", "LS", "LSO"));
                countryService.save(new Country(121L, "ليبيريا", "Liberia", "LR", "LBR"));
                countryService.save(new Country(122L, "ليبيا", "Libya", "LY", "LBY"));
                countryService.save(new Country(123L, "ليختنشتاين", "Liechtenstein", "LI", "LIE"));
                countryService.save(new Country(124L, "ليتوانيا", "Lithuania", "LT", "LTU"));
                countryService.save(new Country(125L, "لوكسمبورغ", "Luxembourg", "LU", "LUX"));
                countryService.save(new Country(126L, "مدغشقر", "Madagascar", "MG", "MDG"));
                countryService.save(new Country(127L, "مالاوي", "Malawi", "MW", "MWI"));
                countryService.save(new Country(128L, "ماليزيا", "Malaysia", "MY", "MYS"));
                countryService.save(new Country(129L, "جزر المالديف", "Maldives", "MV", "MDV"));
                countryService.save(new Country(130L, "مالي", "Mali", "ML", "MLI"));
                countryService.save(new Country(131L, "مالطا", "Malta", "MT", "MLT"));
                countryService.save(new Country(132L, "جزر مارشال", "Marshall Islands", "MH", "MHL"));
                countryService.save(new Country(133L, "مارتينيك", "Martinique", "MQ", "MTQ"));
                countryService.save(new Country(134L, "موريتانيا", "Mauritania", "MR", "MRT"));
                countryService.save(new Country(135L, "موريشيوس", "Mauritius", "MU", "MUS"));
                countryService.save(new Country(136L, "مايوت", "Mayotte", "YT", "MYT"));
                countryService.save(new Country(137L, "المكسيك", "Mexico", "MX", "MEX"));
                countryService.save(new Country(138L, "ميكرونيزيا (ولايات)", "Micronesia (Federated States of)", "FM", "FSM"));
                countryService.save(new Country(139L, "موناكو", "Monaco", "MC", "MCO"));
                countryService.save(new Country(140L, "منغوليا", "Mongolia", "MN", "MNG"));
                countryService.save(new Country(141L, "الجبل الأسود", "Montenegro", "ME", "MNE"));
                countryService.save(new Country(142L, "مونتسيرات", "Montserrat", "MS", "MSR"));
                countryService.save(new Country(143L, "المغرب", "Morocco", "MA", "MAR"));
                countryService.save(new Country(144L, "موزمبيق", "Mozambique", "MZ", "MOZ"));
                countryService.save(new Country(145L, "ميانمار", "Myanmar", "MM", "MMR"));
                countryService.save(new Country(146L, "ناميبيا", "Namibia", "NA", "NAM"));
                countryService.save(new Country(147L, "ناورو", "Nauru", "NR", "NRU"));
                countryService.save(new Country(148L, "نيبال", "Nepal", "NP", "NPL"));
                countryService.save(new Country(149L, "هولندا", "Netherlands", "NL", "NLD"));
                countryService.save(new Country(150L, "كاليدونيا الجديدة", "New Caledonia", "NC", "NCL"));
                countryService.save(new Country(151L, "نيوزيلندا", "New Zealand", "NZ", "NZL"));
                countryService.save(new Country(152L, "نيكاراغوا", "Nicaragua", "NI", "NIC"));
                countryService.save(new Country(153L, "النيجر", "Niger", "NE", "NER"));
                countryService.save(new Country(154L, "نيجيريا", "Nigeria", "NG", "NGA"));
                countryService.save(new Country(155L, "نيوي", "Niue", "NU", "NIU"));
                countryService.save(new Country(156L, "جزيرة نورفولك", "Norfolk Island", "NF", "NFK"));
                countryService.save(new Country(157L, "جزر مريانا الشمالية", "Northern Mariana Islands", "MP", "MNP"));
                countryService.save(new Country(158L, "النرويج", "Norway", "NO", "NOR"));
                countryService.save(new Country(159L, "سلطنة عمان", "Oman", "OM", "OMN"));
                countryService.save(new Country(160L, "باكستان", "Pakistan", "PK", "PAK"));
                countryService.save(new Country(161L, "بالاو", "Palau", "PW", "PLW"));
                countryService.save(new Country(162L, "بناما", "Panama", "PA", "PAN"));
                countryService.save(new Country(163L, "بابوا غينيا الجديدة", "Papua New Guinea", "PG", "PNG"));
                countryService.save(new Country(164L, "باراغواي", "Paraguay", "PY", "PRY"));
                countryService.save(new Country(165L, "بيرو", "Peru", "PE", "PER"));
                countryService.save(new Country(166L, "الفلبين", "Philippines", "PH", "PHL"));
                countryService.save(new Country(167L, "بيتكيرن", "Pitcairn", "PN", "PCN"));
                countryService.save(new Country(168L, "بولندا", "Poland", "PL", "POL"));
                countryService.save(new Country(169L, "البرتغال", "Portugal", "PT", "PRT"));
                countryService.save(new Country(170L, "بورتوريكو", "Puerto Rico", "PR", "PRI"));
                countryService.save(new Country(171L, "دولة قطر", "Qatar", "QA", "QAT"));
                countryService.save(new Country(172L, "جمهورية كوريا", "Republic of Korea", "KR", "KOR"));
                countryService.save(new Country(173L, "جمهورية مولدوفا", "Republic of Moldova", "MD", "MDA"));
                countryService.save(new Country(174L, "R؟ الاتحاد", "R?union", "RE", "REU"));
                countryService.save(new Country(175L, "رومانيا", "Romania", "RO", "ROU"));
                countryService.save(new Country(176L, "الاتحاد الروسي", "Russian Federation", "RU", "RUS"));
                countryService.save(new Country(177L, "رواندا", "Rwanda", "RW", "RWA"));
                countryService.save(new Country(178L, "سانت بارتيليمي", "Saint Barthélemy", "BL", "BLM"));
                countryService.save(new Country(179L, "سانت هيلانة", "Saint Helena", "SH", "SHN"));
                countryService.save(new Country(180L, "سانت كيتس ونيفيس", "Saint Kitts and Nevis", "KN", "KNA"));
                countryService.save(new Country(181L, "القديسة لوسيا", "Saint Lucia", "LC", "LCA"));
                countryService.save(new Country(182L, "سانت مارتن (الجزء الفرنسي)", "Saint Martin (French part)", "MF", "MAF"));
                countryService.save(new Country(183L, "سانت بيير وميكلون", "Saint Pierre and Miquelon", "PM", "SPM"));
                countryService.save(new Country(184L, "سانت فنسنت وجزر غرينادين", "Saint Vincent and the Grenadines", "VC", "VCT"));
                countryService.save(new Country(185L, "ساموا", "Samoa", "WS", "WSM"));
                countryService.save(new Country(186L, "سان مارينو", "San Marino", "SM", "SMR"));
                countryService.save(new Country(187L, "ساو تومي وبرينسيبي", "Sao Tome and Principe", "ST", "STP"));
                countryService.save(new Country(188L, "سارك", "Sark", "", ""));
                countryService.save(new Country(189L, "المملكة العربية السعودية", "Saudi Arabia", "SA", "SAU"));
                countryService.save(new Country(190L, "السنغال", "Senegal", "SN", "SEN"));
                countryService.save(new Country(191L, "صربيا", "Serbia", "RS", "SRB"));
                countryService.save(new Country(192L, "سيشيل", "Seychelles", "SC", "SYC"));
                countryService.save(new Country(193L, "سيرا ليون", "Sierra Leone", "SL", "SLE"));
                countryService.save(new Country(194L, "سنغافورة", "Singapore", "SG", "SGP"));
                countryService.save(new Country(195L, "سانت مارتن (الجزء الهولندي)", "Sint Maarten (Dutch part)", "SX", "SXM"));
                countryService.save(new Country(196L, "سلوفاكيا", "Slovakia", "SK", "SVK"));
                countryService.save(new Country(197L, "سلوفينيا", "Slovenia", "SI", "SVN"));
                countryService.save(new Country(198L, "جزر سليمان", "Solomon Islands", "SB", "SLB"));
                countryService.save(new Country(199L, "الصومال", "Somalia", "SO", "SOM"));
                countryService.save(new Country(200L, "جنوب أفريقيا", "South Africa", "ZA", "ZAF"));
                countryService.save(new Country(201L, "جنوب السودان", "South Sudan", "SS", "SSD"));
                countryService.save(new Country(202L, "إسبانيا", "Spain", "ES", "ESP"));
                countryService.save(new Country(203L, "سيريلانكا", "Sri Lanka", "LK", "LKA"));
                countryService.save(new Country(204L, "دولة فلسطين", "State of Palestine", "PS", "PSE"));
                countryService.save(new Country(205L, "سودان", "Sudan", "SD", "SDN"));
                countryService.save(new Country(206L, "سورينام", "Suriname", "SR", "SUR"));
                countryService.save(new Country(207L, "جزر سفالبارد وجان ماين", "Svalbard and Jan Mayen Islands", "SJ", "SJM"));
                countryService.save(new Country(208L, "سوازيلاند", "Swaziland", "SZ", "SWZ"));
                countryService.save(new Country(209L, "السويد", "Sweden", "SE", "SWE"));
                countryService.save(new Country(210L, "سويسرا", "Switzerland", "CH", "CHE"));
                countryService.save(new Country(211L, "الجمهورية العربية السورية", "Syrian Arab Republic", "SY", "SYR"));
                countryService.save(new Country(212L, "طاجيكستان", "Tajikistan", "TJ", "TJK"));
                countryService.save(new Country(213L, "تايلاند", "Thailand", "TH", "THA"));
                countryService
                        .save(new Country(214L, "جمهورية مقدونيا اليوغوسلافية السابقة", "The former Yugoslav Republic of Macedonia", "MK",
                                "MKD"));
                countryService.save(new Country(215L, "تيمور الشرقية", "Timor-Leste", "TL", "TLS"));
                countryService.save(new Country(216L, "ليذهب", "Togo", "TG", "TGO"));
                countryService.save(new Country(217L, "توكيلاو", "Tokelau", "TK", "TKL"));
                countryService.save(new Country(218L, "تونغا", "Tonga", "TO", "TON"));
                countryService.save(new Country(219L, "ترينداد وتوباغو", "Trinidad and Tobago", "TT", "TTO"));
                countryService.save(new Country(220L, "تونس", "Tunisia", "TN", "TUN"));
                countryService.save(new Country(221L, "ديك رومي", "Turkey", "TR", "TUR"));
                countryService.save(new Country(222L, "تركمانستان", "Turkmenistan", "TM", "TKM"));
                countryService.save(new Country(223L, "جزر تركس وكايكوس", "Turks and Caicos Islands", "TC", "TCA"));
                countryService.save(new Country(224L, "توفالو", "Tuvalu", "TV", "TUV"));
                countryService.save(new Country(225L, "أوغندا", "Uganda", "UG", "UGA"));
                countryService.save(new Country(226L, "أوكرانيا", "Ukraine", "UA", "UKR"));
                countryService.save(new Country(227L, "الإمارات العربية المتحدة", "United Arab Emirates", "AE", "ARE"));
                countryService.save(new Country(228L, "المملكة المتحدة لبريطانيا العظمى وأيرلندا الشمالية",
                        "United Kingdom of Great Britain and Northern Ireland", "GB", "GBR"));
                countryService.save(new Country(229L, "جمهورية تنزانيا المتحدة", "United Republic of Tanzania", "TZ", "TZA"));
                countryService.save(new Country(230L, "الولايات المتحدة الامريكية", "United States of America", "US", "USA"));
                countryService.save(new Country(231L, "الجزر العذراء الأمريكية", "United States Virgin Islands", "VI", "VIR"));
                countryService.save(new Country(232L, "أوروغواي", "Uruguay", "UY", "URY"));
                countryService.save(new Country(233L, "أوزبكستان", "Uzbekistan", "UZ", "UZB"));
                countryService.save(new Country(234L, "فانواتو", "Vanuatu", "VU", "VUT"));
                countryService
                        .save(new Country(235L, "فنزويلا (الجمهورية البوليفارية)", "Venezuela (Bolivarian Republic of)", "VE", "VEN"));
                countryService.save(new Country(236L, "فيتنام", "Viet Nam", "VN", "VNM"));
                countryService.save(new Country(237L, "واليس وفوتونا", "Wallis and Futuna Islands", "WF", "WLF"));
                countryService.save(new Country(238L, "الصحراء الغربية", "Western Sahara", "EH", "ESH"));
                countryService.save(new Country(239L, "اليمن", "Yemen", "YE", "YEM"));
                countryService.save(new Country(240L, "زامبيا", "Zambia", "ZM", "ZMB"));
                countryService.save(new Country(241L, "زيمبابوي", "Zimbabwe", "ZW", "ZWE"));

            }
            if (currencyService.count() == 0) {
                Currency currency = new Currency();
                currency.setId(1L);
                currency.setValue(1D);
                currency.setCurrencyCode("USD");
                currency.setStatus(true);
                currencyService.save(currency);

                Currency currency2 = new Currency();
                currency2.setValue(0.71D);
                currency2.setId(2L);
                currency2.setCurrencyCode("JOD");
                currency2.setStatus(true);
                currency2 = currencyService.save(currency2);
                countryService.updateCurrencyForAll(currency2);
            }
            if (roleService.count() == 0) {
                Role role = new Role();
                role.setId(1L);
                role.setName("ADMIN");
                roleService.save(role);

                Role role1 = new Role();
                role1.setId(2L);
                role1.setName("CUSTOMER");
                roleService.save(role1);

                Role role3 = new Role();
                role3.setId(3L);
                role3.setName("CELEBRITY");
                roleService.save(role3);

                Role role4 = new Role();
                role4.setId(4L);
                role4.setName("VENDOR");
                roleService.save(role4);
            }

            if (accountService.count() == 0) {
                Account account = new Account();
                account.setAccountType(AccountType.ADMIN);
                account.setActive(true);
                account.setUsername("admin");
                account.setPassword("admin");
                account.setEmail("admin@admin.com");
                account.setPhone("0796106702");
                account.setPostcode("11973");
                account.setAddress("Amman abu nsair");
                account.setCity("Amman");
                account.setFirstName("Admin");
                account.setLastName("Admin");
                account.setNewsletter(false);
                account.setPhoneCode("962");
                account.setReceiveOffers(false);
                account.setState("abu Nsair");
                account.setCountry(countryService.findByIsoCode2("JO"));
                account.setGenderEnum(GenderEnum.MALE);
                account.setRoles(roleService.findAll());

                accountService.save(account);

            }
            if (articleService.count() == 0) {

                Article article1 = new Article();
                article1.setId(1L);
                article1.setCode("ABOUT_US");
                article1.setEnableDelete(false);
                article1.setPublishDate(new Date());
                articleService.save(article1);

                Article article2 = new Article();
                article2.setId(2L);
                article2.setCode("TERMS_AND_CONDITIONS");
                article2.setEnableDelete(false);
                article2.setPublishDate(new Date());
                articleService.save(article2);

            }

            if (adsService.count() == 0) {
                Ads ads1 = new Ads();
                ads1.setCode("1");
                ads1.setStatus(true);
                adsService.save(ads1);

                Ads ads2 = new Ads();
                ads2.setCode("2");
                ads2.setStatus(true);
                adsService.save(ads2);

                Ads ads3 = new Ads();
                ads3.setCode("3");
                ads3.setStatus(true);
                adsService.save(ads3);

                Ads ads4 = new Ads();
                ads4.setCode("4");
                ads4.setStatus(true);
                adsService.save(ads4);

            }

            if (paymentTypeService.count() == 0) {
                PaymentType paymentType = new PaymentType();
                paymentType.setId(1L);
                paymentType.setDefaults(true);
                paymentType.setStatus(true);

                TransField desc = new TransField();
                desc.put("ar", "الدفع عند الإستلام");
                desc.put("en", "Cash on Delivery");
                paymentType.setName(desc);
                paymentType.setDesc(desc);
                // paymentType.setPaymentRate(8d);
                PaymentType paymentType1 = new PaymentType();
                paymentType1.setId(1L);
                paymentType1.setDefaults(true);
                paymentType1.setStatus(true);
                TransField desc1 = new TransField();
                desc1.put("ar", "بيتابس");
                desc1.put("en", "paytabs");
                paymentType1.setName(desc1);
                paymentType1.setDesc(desc1);
                //  paymentType1.setPaymentRate(0d);
                paymentTypeService.save(paymentType);
                paymentTypeService.save(paymentType1);
            }
            if (shipmentTypeService.count() == 0) {
                ShipmentType shipmentType = new ShipmentType();
                shipmentType.setId(1L);
                TransField n = new TransField();
                n.put("ar", "أرامكس");
                n.put("en", "Aramex");
                shipmentType.setName(n);
                shipmentType.setDesc(n);
                shipmentType.setShipmentRate(0d);
                shipmentType.setStatus(true);
                CountryShipment countryShipment = new CountryShipment();
                countryShipment.setId(1L);
                countryShipment.setCountry(countryService.findByIsoCode2("SA"));
                countryShipment.setShipmentFree(false);
                countryShipment.setShipmentRate(5D);
                countryShipment.setShipmentRateExtra(10D);
                countryShipment.setPercentage(false);

                CountryShipment countryShipment1 = new CountryShipment();
                countryShipment1.setId(2L);
                countryShipment1.setCountry(countryService.findByIsoCode2("JO"));
                countryShipment1.setShipmentFree(false);
                countryShipment1.setShipmentRate(5D);
                countryShipment1.setShipmentRateExtra(10D);
                countryShipment1.setPercentage(false);
                countryShipment = countryShipmentService.save(countryShipment);
                countryShipment1 = countryShipmentService.save(countryShipment1);
                shipmentType.setCountryShipmentList(Arrays.asList(countryShipment, countryShipment1));

                shipmentTypeService.save(shipmentType);
            }

            if (configService.count() == 0) {
                List<SystemLanguage> allLanguages = Utils.getSystemLangsSorted();

                Arrays.stream(ConfigEnum.values()).forEach(co -> {

                    Config config = new Config();
                    TransField transField = new TransField();
                    allLanguages.forEach(lang -> {
                        transField.put(lang.getLocale(), "");
                    });
                    config.setName(co.name());
                    config.setOriginal(true);

                    configService.save(config);

                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("End Prepare Data");

    }

    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    public void setShipmentTypeService(ShipmentTypeService shipmentTypeService) {
        this.shipmentTypeService = shipmentTypeService;
    }

    public void setAdsService(AdsService adsService) {
        this.adsService = adsService;
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }
}
