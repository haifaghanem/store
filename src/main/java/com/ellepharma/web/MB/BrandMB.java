package com.ellepharma.web.MB;

import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.Image;
import com.ellepharma.data.model.Manufacturer;
import com.ellepharma.service.ImageService;
import com.ellepharma.service.ManufacturerService;
import com.ellepharma.utils.ExceptionCodes;
import com.ellepharma.utils.Utils;
import org.primefaces.event.FileUploadEvent;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;

import static com.ellepharma.utils.Constants.BRAND;

/**
 * CategoryMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 01, 2019
 */
@ManagedBean
@ViewScoped
public class BrandMB extends BaseMB {

    @ManagedProperty("#{manufacturerService}")
    private ManufacturerService manufacturerService;

    @ManagedProperty("#{imageService}")
    private ImageService imageService;

    private Manufacturer selectedManufacturer;

    private CustomLazyDataModel<Manufacturer> manufacturerCustomLazyDataModel;

    @PostConstruct
    public void init() {
        manufacturerCustomLazyDataModel = new CustomLazyDataModel<>(manufacturerService);
    }

    /**
     *
     */
    public void addNewManufacturer() {
        selectedManufacturer = new Manufacturer();
        setEditMode(false);
        showDialog("dlgBrand");
        updates("brandFrm");

    }

    /**
     * @param manufacturer
     */
    public void editManufacturer(Manufacturer manufacturer) {
        selectedManufacturer = manufacturer;
        setEditMode(true);
        showDialog("dlgBrand");
        updates("brandFrm", "brandDtl");
    }

    /**
     * @param manufacturer
     */
    public void deleteManufacturer(Manufacturer manufacturer) {
        try {
            manufacturerService.delete(manufacturer);
            updates("brandDtl");
            addSuccessMessage();

        } catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
            addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
        }

    }

    /**
     *
     */
    public boolean saveManufacturer() {
        try {
            selectedManufacturer = manufacturerService.save(selectedManufacturer);

        } catch (DataIntegrityViolationException ex) {
            addErrorMessage(ex);

            return false;
        }
        selectedManufacturer = new Manufacturer();
        updates("brandFrm", "brandDtl");
        addSuccessMessage();
        return true;

    }

    public void upload(FileUploadEvent event) throws Exception {
        String name = Utils.uploud(event, BRAND);
        Image image = new Image();
        if (selectedManufacturer.getImage() != null) {
            image = selectedManufacturer.getImage();
        }
        image.setName(name);
        image.setPriorityOrder(1L);
        //image.setPic(event.getFile().getContents());
        image= imageService.save(image);
        selectedManufacturer.setImage(image);
    }

    public void saveManufacturerAndHide() {
        if (saveManufacturer()) {
            hideDialog("dlgBrand");
        }
    }

    public ManufacturerService getManufacturerService() {
        return manufacturerService;
    }

    public void setManufacturerService(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    public Manufacturer getSelectedManufacturer() {
        return selectedManufacturer;
    }

    public void setSelectedManufacturer(Manufacturer selectedManufacturer) {
        this.selectedManufacturer = selectedManufacturer;
    }

    public CustomLazyDataModel<Manufacturer> getManufacturerCustomLazyDataModel() {
        return manufacturerCustomLazyDataModel;
    }

    public void setManufacturerCustomLazyDataModel(
            CustomLazyDataModel<Manufacturer> manufacturerCustomLazyDataModel) {
        this.manufacturerCustomLazyDataModel = manufacturerCustomLazyDataModel;
    }

    public ImageService getImageService() {
        return imageService;
    }

    public void setImageService(ImageService imageService) {
        this.imageService = imageService;
    }
}
