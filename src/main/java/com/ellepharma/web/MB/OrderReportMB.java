package com.ellepharma.web.MB;

import com.ellepharma.data.dto.OrderReportFilterDTO;
import com.ellepharma.data.dto.OrderReportResultDTO;
import com.ellepharma.data.model.*;
import com.ellepharma.service.CountryService;
import com.ellepharma.service.OrderService;
import com.ellepharma.utils.JsfUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import java.util.Arrays;
import java.util.List;

/**
 * OrderReportMB.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since August 18, 2019
 */
@ManagedBean
@SessionScoped
public class OrderReportMB extends BaseMB {

	@ManagedProperty("#{orderService}")
	private OrderService orderService;

	@ManagedProperty("#{countryService}")
	private CountryService countryService;

	private OrderReportFilterDTO orderReportFilterDTO;

	private List<OrderReportResultDTO> orderReportResultDTOList;

	private List<Country> countryList;

	private List<ShipmentStatusEnum> shipmentStatusList;

	private List<PaymentStatusEnum> paymentStatusList;

	private CustomLazyDataModel<Order> orderCustomLazyDataModel;

	private List<Order> orderShipmentList;

	private Order selectedOrder;

	private Double sumTotalShipmentOrders;

	@PostConstruct
	public void init() {
		orderReportFilterDTO = new OrderReportFilterDTO();
		orderReportFilterDTO.setPaymentStatus(PaymentStatusEnum.TNC_100);
		countryList = countryService.findAll();
		findOrders();
		shipmentStatusList = Arrays.asList(ShipmentStatusEnum.values());
		paymentStatusList = Arrays.asList(PaymentStatusEnum.values());

		orderCustomLazyDataModel = new CustomLazyDataModel<>(orderService, "findAllOrderReport", Arrays.asList(orderReportFilterDTO));
		orderShipmentList = orderService.findShipmentOrderReport(null, orderReportFilterDTO);
	}

	/**
	 * @param orderID
	 */
	public void changeStatus(Long orderID) {
		selectedOrder = orderService.findById(orderID);
		JsfUtils.openDialog("dlgOrderStatus");
		JsfUtils.update("frmOrderStatus", "headerdlgOrderStatus");

	}

	public void addTrackNo(Long orderID) {

		selectedOrder = orderService.findById(orderID);
		JsfUtils.openDialog("dlgProductArmexTRNO");
		JsfUtils.update("frmTacking", "headerdlgProductArmexTRNO");

	}

	public void saveChangeStatus() {
		selectedOrder = orderService.save(selectedOrder);
		JsfUtils.hideDialog("dlgOrderStatus");
		JsfUtils.update("orderDtl");
		addSuccessMessage();

	}

	public void saveTrackingNo() {
		selectedOrder.setAramexSent(true);
		selectedOrder = orderService.save(selectedOrder);
		JsfUtils.hideDialog("dlgProductArmexTRNO");
		JsfUtils.update("orderDtl");
		addSuccessMessage();

	}

	public void findOrders() {
		sumTotalShipmentOrders = orderService.sumShipmentOrderReport(orderReportFilterDTO);

		//        orderReportResultDTOList = orderService.findAllOrderReport(orderReportFilterDTO);
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	public OrderReportFilterDTO getOrderReportFilterDTO() {
		return orderReportFilterDTO;
	}

	public void setOrderReportFilterDTO(OrderReportFilterDTO orderReportFilterDTO) {
		this.orderReportFilterDTO = orderReportFilterDTO;
	}

	public List<OrderReportResultDTO> getOrderReportResultDTOList() {
		return orderReportResultDTOList;
	}

	public void setOrderReportResultDTOList(List<OrderReportResultDTO> orderReportResultDTOList) {
		this.orderReportResultDTOList = orderReportResultDTOList;
	}

	public void setCountryService(CountryService countryService) {
		this.countryService = countryService;
	}

	public void setCountryList(List<Country> countryList) {
		this.countryList = countryList;
	}

	public List<Country> getCountryList() {
		return countryList;
	}

	public List<ShipmentStatusEnum> getShipmentStatusList() {
		return shipmentStatusList;
	}

	public void setShipmentStatusList(List<ShipmentStatusEnum> shipmentStatusList) {
		this.shipmentStatusList = shipmentStatusList;
	}

	public List<PaymentStatusEnum> getPaymentStatusList() {
		return paymentStatusList;
	}

	public void setPaymentStatusList(List<PaymentStatusEnum> paymentStatusList) {
		this.paymentStatusList = paymentStatusList;
	}

	public CustomLazyDataModel<Order> getOrderCustomLazyDataModel() {
		return orderCustomLazyDataModel;
	}

	public void setOrderCustomLazyDataModel(CustomLazyDataModel<Order> orderCustomLazyDataModel) {
		this.orderCustomLazyDataModel = orderCustomLazyDataModel;
	}

	public Order getSelectedOrder() {
		return selectedOrder;
	}

	public void setSelectedOrder(Order selectedOrder) {
		this.selectedOrder = selectedOrder;
	}

	public List<Order> getOrderShipmentList() {
		return orderShipmentList;
	}

	public void setOrderShipmentList(List<Order> orderShipmentList) {
		this.orderShipmentList = orderShipmentList;
	}

	public Double getSumTotalShipmentOrders() {
		return sumTotalShipmentOrders;
	}

	public void setSumTotalShipmentOrders(Double sumTotalShipmentOrders) {
		this.sumTotalShipmentOrders = sumTotalShipmentOrders;
	}
}
