package com.ellepharma.web.MB;

import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.Image;
import com.ellepharma.data.model.Slider;
import com.ellepharma.service.SliderService;
import com.ellepharma.utils.ExceptionCodes;
import com.ellepharma.utils.Utils;
import org.primefaces.event.FileUploadEvent;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.List;

import static com.ellepharma.utils.Constants.SLIDER;

/**
 * CategoryMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 24, 2019
 */
@ManagedBean
@ViewScoped
public class SliderMB extends BaseMB {

	@ManagedProperty("#{sliderService}")
	private SliderService sliderService;

	private Slider selectedSlider;

	private CustomLazyDataModel<Slider> sliderCustomLazyDataModel;

	private List<Slider> sliderList;

	@PostConstruct
	public void init() {
		selectedSlider = new Slider();
		sliderList = sliderService.findAll();
		sliderCustomLazyDataModel = new CustomLazyDataModel<>(sliderService);
	}

	/**
	 *
	 */
	public void addNewSlider() {
		selectedSlider = new Slider();
		sliderList = sliderService.findAll();
		setEditMode(false);
		showDialog("dlgSlider");
		updates("slidFrm");

	}

	/**
	 * @param slider
	 */
	public void editSlider(Slider slider) {
 		selectedSlider = slider;
		setEditMode(true);
		sliderList = sliderService.findAll();
		sliderList.remove(slider);
		showDialog("dlgSlider");
		updates("slidFrm", "slidDtl");
	}

	/**
	 * @param slider
	 */
	public void deleteSlider(Slider slider) {
		try {
			sliderService.delete(slider);
			sliderList.remove(slider);
			updates("slidDtl");
			addSuccessMessage();

		} catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
			addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
		}

	}

	/**
	 *
	 */
	public void saveSlider() {
		selectedSlider = sliderService.save(selectedSlider);
		sliderList.add(selectedSlider);

		selectedSlider = new Slider();
		updates("slidFrm", "slidDtl");
		addSuccessMessage();

	}

	public void upload(FileUploadEvent event) throws Exception {
		String name = Utils.uploud(event, SLIDER);
		Image image = selectedSlider.getImage();

			image= new Image();
		image.setName(name);
		image.setPriorityOrder(1L);

		//image.setPic(event.getFile().getContents());

		selectedSlider.setImage(image);
	}

	public void saveSliderAndHide() {
		saveSlider();
		hideDialog("dlgSlider");

	}

	public SliderService getSliderService() {
		return sliderService;
	}

	public void setSliderService(SliderService sliderService) {
		this.sliderService = sliderService;
	}

	public Slider getSelectedSlider() {
		return selectedSlider;
	}

	public void setSelectedSlider(Slider selectedSlider) {
		this.selectedSlider = selectedSlider;
	}

	public CustomLazyDataModel<Slider> getSliderCustomLazyDataModel() {
		return sliderCustomLazyDataModel;
	}

	public void setSliderCustomLazyDataModel(CustomLazyDataModel<Slider> sliderCustomLazyDataModel) {
		this.sliderCustomLazyDataModel = sliderCustomLazyDataModel;
	}

	public List<Slider> getSliderList() {
		return sliderList;
	}

	public void setSliderList(List<Slider> sliderList) {
		this.sliderList = sliderList;
	}
}
