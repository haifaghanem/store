package com.ellepharma.web.MB;

import com.ellepharma.data.model.*;
import com.ellepharma.service.*;
import com.ellepharma.utils.JsfUtils;
import com.ellepharma.utils.Utils;
import com.ellepharma.web.api.AramexApi;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * CategoryMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 25, 2019
 */
@ManagedBean
@ViewScoped
public class HomeMB extends BaseMB {

    @ManagedProperty("#{sliderService}")
    private SliderService sliderService;

    @ManagedProperty("#{categoryService}")
    private CategoryService categoryService;

    @ManagedProperty("#{itemService}")
    private ItemService itemService;

    @ManagedProperty("#{menuService}")
    private MenuService menuService;

    @ManagedProperty("#{aramexApi}")
    private AramexApi aramexApi;

    public void setAramexApi(AramexApi aramexApi) {
        this.aramexApi = aramexApi;
    }

    @ManagedProperty("#{manufacturerService}")
    private ManufacturerService manufacturerService;

    private List<Slider> sliderList;
    private List<Category> categoryList;
    private List<Item> newItemList;
    private List<Item> bestPriceItem;
    private List<Item> bestSellerItem;
    private List<Menu> menuList;
    private List<Manufacturer> brandList;

    private Item selectedItem;

    private String menuHtml;
    private String menuHtml2;


    @PostConstruct
    public void init() {

//        JSONObject x = aramexApi.trackPickup(new TrackPickupDto());
/*
        System.out.println(x.toString());

        x = aramexApi.trackShipments(new TrackShipmentsDto());
        System.out.println(x.toString());

        x = aramexApi.createShipments(new CreateShipmentsDto());
        System.out.println(x.toString());

        x = aramexApi.createPickup(new CreatePickupDto());
        System.out.println(x.toString());

        x = aramexApi.cancelPickup(new CancelPickupDto());
        System.out.println(x.toString());
        */
//        x = aramexApi.fetchCountries(new FetchCountriesDto());
//        System.out.println(x.toString());

        categoryList = categoryService.findAllByStatusTrueAndShowInSliderTrue();
        sliderList = sliderService.findAllByStatusTrue();
        newItemList = itemService.findAllByNewItemTrue();
        bestPriceItem = itemService.findDistinctTop15ByStatusTrueOrderByPriceAsc();
        bestSellerItem = itemService.findTop15ByOrderBySeller();
        menuList = menuService.findAllByStatusTrueAndWitChild();
        brandList = manufacturerService.findAllActive();
        menuHtml = menuService.getMenuString(Utils.getCurrentLang().getLocale());
        menuHtml2 = menuService.getMenuString2(Utils.getCurrentLang().getLocale());

    }

    public void viewPdoduct() {
        String productId = JsfUtils.getRequestParameterMap("productId");
        this.selectedItem = itemService.findById(Long.parseLong(productId));
    }

    public void setSliderService(SliderService sliderService) {
        this.sliderService = sliderService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }

    public List<Slider> getSliderList() {
        return sliderList;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public String getMenuHtml() {
        return menuHtml;
    }

    public List<Item> getNewItemList() {
        return newItemList;
    }

    public void setNewItemList(List<Item> newItemList) {
        this.newItemList = newItemList;
    }

    public ItemService getItemService() {
        return itemService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public Item getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(Item selectedItem) {
        this.selectedItem = selectedItem;
    }

    public void setManufacturerService(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    public List<Manufacturer> getBrandList() {
        return brandList;
    }

    public String getMenuHtml2() {
        return menuHtml2;
    }

    public void setMenuHtml2(String menuHtml2) {
        this.menuHtml2 = menuHtml2;
    }

    public List<Item> getBestPriceItem() {
        return bestPriceItem;
    }

    public void setBestPriceItem(List<Item> bestPriceItem) {
        this.bestPriceItem = bestPriceItem;
    }

    public List<Item> getBestSellerItem() {
        return bestSellerItem;
    }

    public void setBestSellerItem(List<Item> bestSellerItem) {
        this.bestSellerItem = bestSellerItem;
    }
}
