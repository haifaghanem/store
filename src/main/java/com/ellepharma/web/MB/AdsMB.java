package com.ellepharma.web.MB;

import com.ellepharma.data.model.Ads;
import com.ellepharma.data.model.Image;
import com.ellepharma.service.AdsService;
import com.ellepharma.service.ImageService;
import com.ellepharma.utils.ExceptionCodes;
import com.ellepharma.utils.JsfUtils;
import com.ellepharma.utils.Utils;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.event.FileUploadEvent;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import static com.ellepharma.utils.Constants.ADS;

/**
 * CategoryMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Jun 19, 2019
 */
@ManagedBean
@ViewScoped
public class AdsMB extends BaseMB {

    @ManagedProperty("#{adsService}")
    private AdsService adsService;

    @ManagedProperty("#{imageService}")
    private ImageService imageService;

    private String url;

    private List<Ads> ads = new ArrayList<>();

    private Ads ads1;
    private Ads ads2;
    private Ads ads3;
    private Ads ads4;


    @PostConstruct
    public void init() {
        //ads = adsService.findAll();
        initAds();

    }

    private void initAds() {
        ads1 = adsService.findByCode("1");
        ads2 = adsService.findByCode("2");
        ads3 = adsService.findByCode("3");
        ads4 = adsService.findByCode("4");
    }


    public void deleteAds(String code) {
        try {
            adsService.deleteByCode(code);
            JsfUtils.update("main_from");

            addSuccessMessage();

        } catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
            addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
        }

    }


    public boolean checkSaveAds(String code) {
        Ads ads = adsService.findByCode(code);
        return ads != null && ads.getImage() != null;

    }

    public Ads findAds(String code) {
        return adsService.findByCode(code);


    }

    public void upload(FileUploadEvent event) throws Exception {
        String code = (String) event.getComponent().getAttributes().get("code");
        InputText inputText = JsfUtils.findComponent("url" + code);
        Ads ads = adsService.findByCode(code);
        if (ads == null) {
            ads = new Ads();
            ads.setImage(new Image());
        }
        ads.setUrl(inputText.getSubmittedValue() != null ? inputText.getSubmittedValue().toString() : "");
        ads.setCode(code);
        Image image = ads.getImage();
        String name = Utils.uploud(event, ADS);
        image.setName(name);
        image.setPriorityOrder(1L);
        // image.setPic(event.getFile().getContents());
        image = imageService.save(image);
        ads.setImage(image);

        adsService.save(ads);
        initAds();
    }


    public void save(String code) throws Exception {
        InputText inputText = JsfUtils.findComponent("url" + code);
        Ads ads = adsService.findByCode(code);
        if (ads == null) {
            ads = new Ads();
            ads.setImage(new Image());
        }
        ads.setUrl(inputText.getValue() != null ? inputText.getValue().toString() : "");
        ads.setCode(code);
        adsService.save(ads);
    }


    public AdsService getAdsService() {
        return adsService;
    }

    public void setAdsService(AdsService adsService) {
        this.adsService = adsService;
    }


    public ImageService getImageService() {
        return imageService;
    }

    public void setImageService(ImageService imageService) {
        this.imageService = imageService;
    }


    public List<Ads> getAds() {
        return ads;
    }

    public void setAds(List<Ads> ads) {
        this.ads = ads;
    }

    public Ads getAds1() {
        return ads1;
    }

    public void setAds1(Ads ads1) {
        this.ads1 = ads1;
    }

    public Ads getAds2() {
        return ads2;
    }

    public void setAds2(Ads ads2) {
        this.ads2 = ads2;
    }

    public Ads getAds3() {
        return ads3;
    }

    public void setAds3(Ads ads3) {
        this.ads3 = ads3;
    }

    public Ads getAds4() {
        return ads4;
    }

    public void setAds4(Ads ads4) {
        this.ads4 = ads4;
    }


}
