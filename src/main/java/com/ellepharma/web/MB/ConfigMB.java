package com.ellepharma.web.MB;

import com.ellepharma.constant.ConfigEnum;
import com.ellepharma.constant.TransField;
import com.ellepharma.data.model.Config;
import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.SystemLanguage;
import com.ellepharma.service.ConfigService;
import com.ellepharma.service.GlobalConfigProperties;
import com.ellepharma.utils.ExceptionCodes;
import com.ellepharma.utils.JsfUtils;
import com.ellepharma.utils.Utils;
import org.primefaces.event.FileUploadEvent;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.*;

/**
 * ConfigMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 09, 2019
 */
@ManagedBean
@ApplicationScoped
public class ConfigMB extends BaseMB {

    private Map<String, Object> mapConfig;

    @ManagedProperty("#{configService}")
    private ConfigService configService;

    private Config selectedConfig;

    private CustomLazyDataModel<Config> configCustomLazyDataModel;

    List<SystemLanguage> allLanguages;

    List<Config> allConf;

    @PostConstruct
    public void init() {
        allLanguages = Utils.getSystemLangsSorted();
        allConf = configService.findAll();

        Arrays.stream(ConfigEnum.values()).forEach(co -> {
            Config config = configService.findByName(co.name());
            if (Objects.isNull(config)) {
                config = new Config();
                TransField transField = new TransField();
                allLanguages.forEach(lang -> {
                    transField.put(lang.getLocale(), "");
                });
                config.setName(co.name());
                config.setOriginal(true);
            }

            if (config.getId() == null) {
                allConf.add(config);
                configService.save(allConf);
            }

        });

        mapConfig = new HashMap<>();
        List<SystemLanguage> languages = Utils.getSystemLangsSorted();
        for (Config config : allConf) {
            for (SystemLanguage language : languages) {
                mapConfig.put(config.getName() + "_" + language.getLocale(), config.getValue().get(language.getLocale()));
            }
            mapConfig.put("VERSION", config.getVersionId());
        }

        configCustomLazyDataModel = new CustomLazyDataModel<>(configService);

    }

    public Object getConf(String key) {
        return mapConfig.get(key.concat("_").concat(Utils.getCurrentLang().getLocale()));
    }

    public String getProp(String key) {
        return GlobalConfigProperties.getInstance().getProperty(key);
    }

    public void clearAllCache() {

        configService.evictAllCaches();
        init();
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        JsfUtils.executeScript("var cookies = $.cookie();\n" +
                "for(var cookie in cookies) {\n" +
                "   $.removeCookie(cookie);\n" +
                "}");

    }

    /**
     *
     */
    public void addNewConfig() {
        selectedConfig = new Config();
        setEditMode(false);
        showDialog("dlgConfig");
        updates("configFrm");

    }

    /**
     * @param config
     */
    public void deleteConfig(Config config) {
        try {
            configService.delete(config);
            init();
            updates("configDtl");
            addSuccessMessage();

        } catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
            addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
        }

    }

    /**
     * @param config
     */
    public void editConfig(Config config) {
        selectedConfig = config;
        setEditMode(true);
        showDialog("dlgConfig");
        updates("configFrm", "configDtl");

    }

    /**
     *
     */
    public boolean saveConfig() {
        try {
            selectedConfig = configService.save(selectedConfig);

        } catch (DataIntegrityViolationException ex) {
            addErrorMessage(ex);

            return false;
        }
        selectedConfig = new Config();
        updates("configFrm", "configDtl");
        addSuccessMessage();
        init();
        return true;

    }

    public void upload(FileUploadEvent event) throws Exception {
        String name = Utils.uploud(event, ConfigEnum.LOGO.name());
        TransField transField = new TransField();
        allLanguages.forEach(lang -> {
            transField.put(lang.getLocale(), name);
        });

        selectedConfig.setValue(transField);
    }

    public void saveConfigAndHide() {
        if (saveConfig()) {
            hideDialog("dlgConfig");
        }
    }

    public Map<String, Object> getMapConfig() {
        return mapConfig;
    }

    public Config getSelectedConfig() {
        return selectedConfig;
    }

    public CustomLazyDataModel<Config> getConfigCustomLazyDataModel() {
        return configCustomLazyDataModel;
    }

    public void setConfigService(ConfigService configService) {
        this.configService = configService;
    }

    public void setSelectedConfig(Config selectedConfig) {
        this.selectedConfig = selectedConfig;
    }

}
