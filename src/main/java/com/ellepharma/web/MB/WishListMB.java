package com.ellepharma.web.MB;

import com.ellepharma.data.model.Item;
import com.ellepharma.data.model.WishList;
import com.ellepharma.service.ItemService;
import com.ellepharma.service.WishListService;
import com.ellepharma.utils.JsfUtils;
import com.ellepharma.utils.SecurityUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;

@ManagedBean
@ViewScoped
public class WishListMB extends BaseMB {

    @ManagedProperty("#{wishListService}")
    private WishListService wishListService;

    @ManagedProperty("#{itemService}")
    private ItemService itemService;

    private WishList wishList;

    @PostConstruct
    public void init() {
        if (SecurityUtils.getCurrentAccount() != null) {
            wishList = wishListService.getForCurrentAccount();
        }
    }

    public void removeFromWishlist() {
        if (SecurityUtils.getCurrentAccount() == null) {
            addErrorMessage("common.pleaseSignIn");

            return;
        }
        Long productId = new Long(JsfUtils.getRequestParameterMap("productId"));
        Item item = itemService.findById(productId);

        wishList = wishListService.getForCurrentAccount();

        wishList.getItems().remove(item);
        wishListService.save(wishList);
        addSuccessMessage("common.operationSuccessful");
    }

    public void addToWishList() {
        if (SecurityUtils.getCurrentAccount() == null) {
            addErrorMessage("common.pleaseSignIn");

            return;
        }
        Long productId = new Long(JsfUtils.getRequestParameterMap("productId"));
        Item item = itemService.findById(productId);
        wishList = wishListService.getForCurrentAccount();
        if (wishList == null) {
            wishList = new WishList();
            wishList.setItems(new ArrayList<>());
            wishList.setAccount(SecurityUtils.getCurrentAccount());
        }

        if (wishList.getItems().stream().filter(item1 -> item1.getId().equals(item.getId())).findFirst().isPresent()) {
            addErrorMessage("common.addedPreviously");
            return;
        }

        wishList.getItems().add(item);
        try {
        wishList = wishListService.save(wishList);
        addSuccessMessage("common.added");

        }
        catch (Exception e){

        }

    }

    public void setWishListService(WishListService wishListService) {
        this.wishListService = wishListService;
    }

    public WishList getWishList() {
        return wishList;
    }

    public void setWishList(WishList wishList) {
        this.wishList = wishList;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }
}
