package com.ellepharma.web.MB;

import org.springframework.web.context.annotation.SessionScope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.util.Locale;

/**
 * LocalMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */

@ManagedBean
@SessionScope
public class localeMB extends BaseMB  {

	private Locale locale;

	private String dir;

	@PostConstruct
	public void init() {
		setLanguage("ar");
	}

	public Locale getLocale() {
		return locale;
	}

	public String getDir() {
		dir = locale.getLanguage().equalsIgnoreCase("ar") ? "RTL" : "LTR";
		return dir;
	}

	public String getAlign() {
		String align = locale.getLanguage().equalsIgnoreCase("ar") ? "right" : "left";
		return align;
	}
	public String getFloata() {
		String floata = locale.getLanguage().equalsIgnoreCase("ar") ? "right":"left";
		return floata;
	}
	public String getInvFloat() {
		String floata = locale.getLanguage().equalsIgnoreCase("ar") ? "left":"right";
		return floata;
	}

	public String getLanguage() {
		return locale.getLanguage();
	}

	public void setLanguage(String language) {
		locale = new Locale(language);
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
	}

	public void setDir(String dir) {
		this.dir = dir;
	}
}
