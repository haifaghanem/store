package com.ellepharma.web.MB;

import com.ellepharma.data.dto.VerifyRequest;
import com.ellepharma.data.model.*;
import com.ellepharma.data.repository.CountryCashOnDeliveryRepo;
import com.ellepharma.service.*;
import com.ellepharma.utils.Constants;
import com.ellepharma.utils.JsfUtils;
import com.ellepharma.utils.SecurityUtils;
import com.ellepharma.utils.Utils;
import com.ellepharma.web.api.AramexApi;
import com.ellepharma.web.api.CreatePayPageRequest;
import com.ellepharma.web.api.PayTabDTO;
import com.ellepharma.web.api.PayTabs;
import com.ellepharma.web.aramex.dto.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * CartMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 01, 2019
 */
@ManagedBean
@SessionScoped
public class CartMB extends BaseMB {

    @ManagedProperty("#{itemService}")
    private ItemService itemService;

    @ManagedProperty("#{couponService}")
    private CouponService couponService;

    @ManagedProperty("#{orderService}")
    private OrderService orderService;

    @ManagedProperty("#{loginBean}")
    private LoginMB loginMB;

    @ManagedProperty("#{aramexApi}")
    private AramexApi aramexApi;

    @ManagedProperty("#{orderProductService}")
    private OrderProductService orderProductService;

    @ManagedProperty("#{stockInService}")
    private StockInService stockInService;

    @ManagedProperty("#{paymentTypeService}")
    private PaymentTypeService paymentTypeService;

    @ManagedProperty("#{shipmentTypeService}")
    private ShipmentTypeService shipmentTypeService;

    @ManagedProperty("#{countryCashOnDeliveryRepo}")
    private CountryCashOnDeliveryRepo countryCashOnDeliveryRepo;

    private List<Cart> itemNotAvailable = new ArrayList<>();

    private List<PaymentType> paymentTypeList = new ArrayList<>();

    private List<Cart> cartList = new ArrayList<>();

    private double subtotal;

    private String sCoupon;

    private Coupon coupon;

    private long sumQty;

    private PaymentType paymentType;

    private Order selectedOrder;

    @PostConstruct
    public void init() {
        cartList = new ArrayList<>();
        preparePaymentType();

    }

    public void preparePaymentType() {
        Country cc = JsfUtils.getFromSessionMap(Constants.CURRENT_COUNTRY);
        paymentTypeList = paymentTypeService.findAllByCountry(cc);
        if (CollectionUtils.isNotEmpty(paymentTypeList)) {
            paymentType = paymentTypeList.get(0);
            changePayentType();
        } else {
            paymentType = null;
        }
    }

    public void pay() {
        itemNotAvailable = new ArrayList<>();
        cartList.forEach(cart -> {
            Long i = stockInService.getAvailableQuantityByItemID(cart.getItem().getId());
            if (i <= 0 || i < cart.getQty()) {
                itemNotAvailable.add(cart);
            }

        });
        if (CollectionUtils.isNotEmpty(itemNotAvailable)) {

            JsfUtils.openDialog("dlgItemNotAvailable");
            JsfUtils.update("frmdlgItemNotAvailable");
            return;
        }
        Order order = new Order();
        try {

            if (!loginMB.isLoggedIn()) {
                return;
            }
            PayTabs payTabs = new PayTabs();

            order.setAccount(SecurityUtils.getCurrentAccount());
            order.setCurrency(Utils.getCurrentCurrency());
            order.setCurrencyValue(order.getCurrency().getValue());

            order.setPaymentFee(Utils.convertPrice(getPaymentRate()));
            order.setTotal(Utils.convertPrice(getCartList().stream().mapToDouble(c -> c.getQty() * c.getItem().getFinalPrice()).sum()));
            order.setDiscount(Utils.convertPrice(getDiscount()));
            order.setShippingFee(Utils.convertPrice(getShipment()));

            order.setCoupon(coupon);
            order.setPaymentStatus(PaymentStatusEnum.TNC_1);
            order.setPaymentType(paymentType);
            order.setLanguageCode(Utils.getCurrentLang().getLocale());
            order.setIp(Utils.getUserIp());
            List<OrderProduct> orderProductList = new ArrayList<>();
            OrderProduct orderProduct;

            for (Cart crt : getCartList()) {
                orderProduct = new OrderProduct();
                orderProduct.setItem(crt.getItem());
                orderProduct.setQty(crt.getQty());
                orderProduct.setPrice(Utils.convertPrice(crt.getItem().getFinalPrice()));
                orderProduct.setTax(Utils.convertPrice(crt.getItem().getTax()));
                orderProduct.setTotal(Utils.convertPrice(
                        crt.getQty() * crt.getItem().getFinalPrice() * (crt.getItem().getTax() != null && crt.getItem().getTax() > 0 ?
                                crt.getItem().getTax() :
                                1)));
                orderProductList.add(orderProduct);
            }

            orderProductList = orderProductService.save(orderProductList);
            order.setProductList(orderProductList);
            order = orderService.save(order);

            String url = Utils.getFullUrlApp() + "/customer/orderStatus.xhtml?id=" + order.getId();

            payTabs.setSiteReturnUrl(url);
            if (paymentType.getId().equals(2L)) {
                PayTabDTO payTabDTO = preparePayDto(order);
                Gson gson = new GsonBuilder().serializeNulls().create();

                order.setPayTabDTOJson(gson.toJson(payTabDTO));
                CreatePayPageRequest payPageRequest = payTabs.createPayPageRequest(payTabDTO);
                if (payPageRequest.getResponse_code().equalsIgnoreCase("4012")) {
                    order.setPaymentId(payPageRequest.getP_id());
                    order = orderService.save(order);
                    JsfUtils.executeScript("$.cookie('clearCart', 'true', {path: '/'});");
                    Utils.redirect(payPageRequest.getPayment_url());
                    //cartList.clear();
                } else {
                    JsfUtils.addErrorMessageDirect(payPageRequest.getResult());
                }
            } else {
                order.setPaymentStatus(PaymentStatusEnum.TNC_100);
                order.setShipmentStatus(ShipmentStatusEnum.Pending);
                order.setInvoiceNo(orderService.findMaxInvoice() + 1);
                order = orderService.save(order);
                JsfUtils.executeScript("$.cookie('clearCart', 'true', {path: '/'});");
                Utils.redirect(url);
                //cartList.clear();
            }
            //getCartList().clear();
        } catch (Exception e) {
            order.setException(e.getMessage());
            order.setPaymentStatus(PaymentStatusEnum.TNC_0);
            orderService.save(order);
            e.printStackTrace();
            JsfUtils.addErrorMessage("حصل خطأ ما يرجى المحاولة لاحقا او التواصل مع الدعم الفني للموقع ");
        }

    }

    public void sentOrderEmail(Long orderID) {

        Order order = orderService.findById(orderID);

        if (StringUtils.isEmpty(order.getTracking())) {

            addErrorMessage("common.mustAddTrackingNoBeforeSendEmail");
            JsfUtils.update("main_from");
            return;
        }
        if (order.getPaymentStatus().equals(PaymentStatusEnum.TNC_100)) {
            if (!order.isEmailSent()) {
                boolean suc = orderService.generateInvoiceAndSendIt(order);
                if (suc) {
                    order.setEmailSent(suc);
                    order = orderService.save(order);
                    addSuccessMessage();

                } else {
                    addErrorMessage("common.addErrorMessage");
                }

            } else {
                JsfUtils.addErrorMessage("common.emailHasAlreadyBeenSent");
            }

        }
        JsfUtils.update("main_from");

    }

    /**
     * @param orderID
     */
    public void checkPayment(Long orderID) {

        VerifyRequest request = orderService.checkOrderById(orderID);

        if (Objects.nonNull(request)) {
            addDirectInfoMessage(request.getResult());
        }

    }

    public void sentToAramex(Order order) {

        ShipmentType t = shipmentTypeService.findByStatusTrue();
        order = orderService.findById(order.getId());

        boolean sentToAramex = false;
        if (order.isAramexSent()) {
            addErrorMessage("تم ارسال الطلب مسبقا");
            return;
        }

        int numberOfPieces = order.getProductList().stream().mapToInt(m -> m.getQty().intValue()).sum();
        double shipmentWeight = order.getProductList().stream().mapToDouble(m -> m.getQty() * m.getItem().getWeight()).sum();
        if (shipmentWeight != 0) {
            shipmentWeight = shipmentWeight / 1000;
        }

        if (t.getId().equals(1L)) {

            boolean isPickup = false;

            CreateShipmentsDto response = new CreateShipmentsDto();
            String aramexResponseString = "";

            if (order.getPaymentStatus().equals(PaymentStatusEnum.TNC_100)) {
                if (!order.isAramexSent()) {

                    if (isPickup) {
                        //  response = aramexApi.createPickup(preparePickupDto(order));
                    } else {

                        Account account = order.getAccount();

                        String productGroup = account.getCountry().getIsoCode2().equalsIgnoreCase("AE") ? "DOM" : "EXP";
                        String productType = account.getCountry().getIsoCode2().equalsIgnoreCase("AE") ? "ONP" : "EPX";
                        //

                        //String payType = order.getPaymentType().getId().equals(PaymentTypeEnum.VISA.getId()) ? "P" : "C";
                        String payType = "P";

                        Shipment shipment = new Shipment();
                        Consignee consignee = new Consignee();
                        Contact contact = new Contact();
                        Shipper shipper = new Shipper();
                        PartyAddress address = new PartyAddress();

                        //add
                        address.setCity(account.getCity());
                        address.setLine1(account.getState() + " - " + account.getCity() + " - " + account.getCountry().getCountryNameEn());
                        address.setLine2(account.getAddress());
                        address.setCountryCode(account.getCountry().getIsoCode2());
                        address.setLatitude(0D);
                        address.setLongitude(0D);
                        //contact
                        String phoneKey =
                                String.valueOf(PhoneNumberUtil.getInstance()
                                        .getCountryCodeForRegion(new Locale("ar", account.getPhoneCode()).getCountry()));

                        contact.setPersonName(account.getFullName());
                        contact.setCompanyName(account.getFullName());
                        contact.setCellPhone(phoneKey + account.getPhone().replace(" ", ""));
                        contact.setPhoneNumber1(phoneKey + account.getPhone().replace(" ", ""));
                        contact.setEmailAddress(account.getEmail());
                        consignee.setContact(contact);
                        consignee.setPartyAddress(address);

                        //
                        shipment.setConsignee(consignee);
                        shipment.setShipper(shipper);


                        Details shipmentDetails = new Details();
                        shipmentDetails.getActualWeight().setValue(shipmentWeight);
                        shipmentDetails.getActualWeight().setUnit("KG");
                        shipmentDetails.setNumberOfPieces(numberOfPieces);
                        shipmentDetails.setProductGroup(productGroup);
                        shipmentDetails.setProductType(productType);
                        shipmentDetails.setPaymentType(payType);
                        if (!order.getPaymentType().getId().equals(PaymentTypeEnum.VISA.getId())) {
                            shipmentDetails.setServices("CODS");
                        }
                        shipmentDetails.setDescriptionOfGoods("cosmetic");
                        shipmentDetails.setGoodsOriginCountry("AE");

                        CustomsValueAmount customsValueAmount = new CustomsValueAmount();
                        customsValueAmount.setCurrencyCode(order.getCurrency().getCurrencyCode());
                        customsValueAmount.setValue(order.getTotal() - order.getDiscount() + order.getShippingFee() + order.getPaymentFee());

                        CashOnDeliveryAmount cashOnDeliveryAmount = new CashOnDeliveryAmount();
                        if (!order.getPaymentType().getId().equals(PaymentTypeEnum.VISA.getId())) {
                            cashOnDeliveryAmount.setCurrencyCode(order.getCurrency().getCurrencyCode());
                            cashOnDeliveryAmount.setValue(order.getTotal() - order.getDiscount() + order.getShippingFee() + order.getPaymentFee());
                        } else {
                            cashOnDeliveryAmount.setCurrencyCode(order.getCurrency().getCurrencyCode());
                            cashOnDeliveryAmount.setValue(0D);
                        }
                        shipmentDetails.setCashOnDeliveryAmount(cashOnDeliveryAmount);

                        shipmentDetails.setCustomsValueAmount(customsValueAmount);

                        shipment.setDetails(shipmentDetails);

                        CreateShipmentsDto createShipmentsDto = new CreateShipmentsDto();
                        createShipmentsDto.setShipments(Collections.singletonList(shipment));
                        Gson gson = new GsonBuilder().serializeNulls().create();

                        try {

                            aramexResponseString = aramexApi.createShipments(createShipmentsDto);
                            response = gson.fromJson(aramexResponseString, CreateShipmentsDto.class);

                        } catch (Exception e) {

                            e.printStackTrace();
                        }

                    }

                    sentToAramex = response.getHasErrors() != null && !response.getHasErrors();

                }
                if (sentToAramex) {
                    if (CollectionUtils.isNotEmpty(response.getShipments())) {
                        String id = response.getShipments().get(0).getId();
                        order.setTracking(id);
                        order.setShipmentStatus(ShipmentStatusEnum.Pending);
                    }

                    order.setAramexSent(true);
                    order = orderService.save(order);
                    addSuccessMessage();
                } else {
                    System.err.println(response.toString());
                    if (CollectionUtils.isNotEmpty(response.getNotifications())) {

                        JsfUtils.addErrorMessageDirect("common.addErrorMessage :" + response.getNotifications().get(0).getMessage());
                    } else {
                        addErrorMessage("common.addErrorMessage");

                    }
                }
                if (Objects.nonNull(response)) {

                    order.setAramexResponse(aramexResponseString);
                    order = orderService.save(order);
                }

            }
        } else if (t.getId().equals(2L)) {
            Unirest.setTimeouts(0, 0);
            try {
                HttpResponse<JsonNode> response = Unirest.post("http://test.iqsgsc.com/v2/shipment/create")
                        .header("content-type", "application/x-www-form-urlencoded")
                        .header("charset", "utf-8")
                        .header("access-token", GlobalConfigProperties.getInstance().getProperty("iqs.access-token"))
                        .field("task_id", GlobalConfigProperties.getInstance().getProperty("iqs.prefixTaskId") + order.getId())
                        .field("task_account", GlobalConfigProperties.getInstance().getProperty("iqs.task_account"))
                        .field("task_uid", GlobalConfigProperties.getInstance().getProperty("iqs.task_uid"))
                        .field("task_group", GlobalConfigProperties.getInstance().getProperty("iqs.task_group"))
                        .field("task_type", GlobalConfigProperties.getInstance().getProperty("iqs.task_type"))
                        .field("task_payment", GlobalConfigProperties.getInstance().getProperty("iqs.task_payment"))
                        .field("task_cod_amount", Utils.convertPriceToString(order.getTotal() - order.getDiscount() + order.getShippingFee() + order.getPaymentFee()))
                        .field("from_name", GlobalConfigProperties.getInstance().getProperty("iqs.from_name"))
                        .field("from_phone", GlobalConfigProperties.getInstance().getProperty("iqs.from_phone"))
                        .field("from_email", GlobalConfigProperties.getInstance().getProperty("iqs.from_email"))
                        .field("from_address1", GlobalConfigProperties.getInstance().getProperty("iqs.from_address1"))
                        .field("from_city", GlobalConfigProperties.getInstance().getProperty("iqs.from_city"))
                        .field("from_country", GlobalConfigProperties.getInstance().getProperty("iqs.from_country"))
                        .field("item_description", GlobalConfigProperties.getInstance().getProperty("iqs.item_description"))
                        .field("to_country", order.getAccount().getCountry().getIsoCode2())
                        .field("item_value", Utils.convertPriceToString(order.getTotal()))
                        .field("to_address1", order.getAccount().getAddress())
                        .field("to_name", order.getAccount().getFullName())
                        .field("to_mobile", order.getAccount().getPhone())
                        .field("to_email", order.getAccount().getEmail())
                        .field("to_city", order.getAccount().getCity())
                        .field("item_pieces", numberOfPieces)
                        .field("item_weight", shipmentWeight)
                        .field("item_weight_unit", "KG")
                        .asJson();


                if (response.getStatus() == 200) {
                    JSONObject data = response.getBody().getObject().getJSONObject("data");
                    String hawb = data.getString("hawb");
                    System.out.println(hawb);
                    order.setTracking(hawb);
                    order.setShipmentStatus(ShipmentStatusEnum.Pending);
                    order.setAramexSent(true);
                    order = orderService.save(order);
                    addSuccessMessage();
                }
                order.setAramexResponse(response.getBody().toString());

            } catch (UnirestException e) {
                addErrorMessage("common.addErrorMessage");

                e.printStackTrace();
            }
            order = orderService.save(order);


        }
        JsfUtils.update("main_from");

    }


    public void changeCountry(ValueChangeEvent changeEvent) {
        CountryShipment countryShipment = (CountryShipment) changeEvent.getNewValue();

        if (countryShipment != null) {
            JsfUtils.putToSessionMap(Constants.CURRENT_COUNTRY_SHIPMENT, countryShipment);
            JsfUtils.putToSessionMap(Constants.CURRENT_COUNTRY, countryShipment.getCountry());
            JsfUtils.putToSessionMap(Constants.CURRENT_CURRENCY, countryShipment.getCountry().getCurrency());
        }

        AccountMB accountMB = JsfUtils.findBean("accountMB");
        accountMB.getAccount().setCity(null);
        if (countryShipment != null) {
            accountMB.getAccount().setPhoneCode(countryShipment.getCountry().getIsoCode2());
        }
        preparePaymentType();
        JsfUtils.update("chGrd", "grdCity");
        JsfUtils.executeScript("$('select').selectize({sortField: 'text'});   ");

    }

    /**
     * @param order Order
     * @return PayTabDTO
     */
    private PayTabDTO preparePayDto(Order order) {

        if (loginMB.isLoggedIn()) {
            Account account = SecurityUtils.getCurrentAccount();

            PayTabDTO payTabDTO = new PayTabDTO();
            payTabDTO.setDiscount((float) getDiscount());
            payTabDTO.setOtherCharges((float) getShipment());

            double amount = getCartList().stream().mapToDouble(c -> c.getQty() * c.getItem().getFinalPrice()).sum();
            payTabDTO.setAmount((float) amount + payTabDTO.getOtherCharges());
            payTabDTO.setTitle("Billing for Order #" + order.getId());
            payTabDTO.setReferenceNo(order.getId().toString());
            payTabDTO.setCcFirstName(Objects.requireNonNull(account).getFirstName());
            payTabDTO.setCcLastName(account.getLastName());
            payTabDTO.setCcPhoneNumber(
                    PhoneNumberUtil.getInstance().getCountryCodeForRegion(new Locale("ar", account.getPhoneCode()).getCountry()) + "");
            payTabDTO.setPhoneNumber(account.getPhone());
            payTabDTO.setEmail(account.getEmail());
            payTabDTO.setCurrency("USD");
            payTabDTO.setBillingAddress(account.getAddress());
            payTabDTO.setState(account.getState());
            payTabDTO.setCity(account.getCity());
            payTabDTO.setPostalCode(account.getPostcode());
            payTabDTO.setCountry(account.getCountry().getIsoCode3());
            payTabDTO.setAddressShipping(account.getAddress());
            payTabDTO.setShippingFirstName(account.getFirstName());
            payTabDTO.setShippingLastName(account.getLastName());
            payTabDTO.setAddressShipping(account.getAddress());
            payTabDTO.setStateShipping(account.getState());
            payTabDTO.setPostalCodeShipping(account.getPostcode());
            payTabDTO.setCountryShipping(account.getCountry().getIsoCode3());
            payTabDTO.setMsgLang(Utils.getCurrentLang().getLocale());
            payTabDTO.setCity(account.getCity());
            payTabDTO.setCityShipping(account.getCity());
            String quantity = getCartList().stream().map(cart -> cart.getQty().toString()).collect(Collectors.joining(" || "));
            String unitPrice = getCartList().stream().map(cart -> cart.getItem().getFinalPrice().toString())
                    .collect(Collectors.joining(" || "));

            String productsPerTitle = getCartList().stream().map(cart -> cart.getItem().getName().get(cart.getCurrentLanguage()))
                    .collect(Collectors.joining(" || "));

            payTabDTO.setIpCustomer(order.getIp());
            payTabDTO.setQuantity(quantity);
            payTabDTO.setUnitPrice(unitPrice);
            payTabDTO.setProductsPerTitle(productsPerTitle);

            return payTabDTO;
        }
        return null;
    }

    private PayTabDTO preparePayDtoWitCurrentCurrency(Order order) {

        if (loginMB.isLoggedIn()) {
            Account account = SecurityUtils.getCurrentAccount();

            PayTabDTO payTabDTO = new PayTabDTO();

            payTabDTO.setDiscount((float) order.getDiscount());
            payTabDTO.setOtherCharges((float) order.getShippingFee());
            payTabDTO.setAmount(order.getTotal().floatValue() + payTabDTO.getOtherCharges());

            payTabDTO.setTitle("Billing for Order #" + order.getId());
            payTabDTO.setReferenceNo(order.getId().toString());

            payTabDTO.setCcFirstName(account.getFirstName());
            payTabDTO.setCcLastName(account.getLastName());

            payTabDTO.setCcPhoneNumber(
                    PhoneNumberUtil.getInstance().getCountryCodeForRegion(new Locale("ar", account.getPhoneCode()).getCountry()) + "");
            payTabDTO.setPhoneNumber(account.getPhone());

            payTabDTO.setEmail(account.getEmail());

            payTabDTO.setCurrency(Utils.getCurrentCurrency().getCurrencyCode());

            payTabDTO.setBillingAddress(account.getAddress());
            payTabDTO.setState(account.getState());
            payTabDTO.setCity(account.getCity());
            payTabDTO.setPostalCode(account.getPostcode());
            payTabDTO.setCountry(account.getCountry().getIsoCode3());
            payTabDTO.setAddressShipping(account.getAddress());
            payTabDTO.setShippingFirstName(account.getFirstName());
            payTabDTO.setShippingLastName(account.getLastName());
            payTabDTO.setAddressShipping(account.getAddress());
            payTabDTO.setStateShipping(account.getState());
            payTabDTO.setPostalCodeShipping(account.getPostcode());
            payTabDTO.setCountryShipping(account.getCountry().getIsoCode3());
            payTabDTO.setMsgLang(Utils.getCurrentLang().getLocale());
            payTabDTO.setCity(account.getCity());
            payTabDTO.setCityShipping(account.getCity());

            String quantity = getCartList().stream().map(cart -> cart.getQty().toString()).collect(Collectors.joining(" || "));
            String unitPrice = getCartList().stream().map(cart -> Utils.convertPrice(cart.getItem().getFinalPrice()).toString())
                    .collect(Collectors.joining(" || "));
            String productsPerTitle = getCartList().stream().map(cart -> cart.getItem().getName().get(cart.getCurrentLanguage()))
                    .collect(Collectors.joining(" || "));

            payTabDTO.setIpCustomer(order.getIp());
            payTabDTO.setQuantity(quantity);
            payTabDTO.setUnitPrice(unitPrice);
            payTabDTO.setProductsPerTitle(productsPerTitle);

            return payTabDTO;
        }
        return null;
    }
/*

    public CreatePickupDto preparePickupDto(Order order) {

        Account account = SecurityUtils.getCurrentAccount();

        LocalDateTime a1 = LocalDateTime.now();
        //TODO remove AE
        String productGroup = "AE".equalsIgnoreCase("AE") ? "DOM" : "EXP";
        String productType = productGroup.equalsIgnoreCase("DOM") ? "OND" : "EPX";
        String payType = order.getPaymentType().getId().equals(2L) ? "P" : "C";

        List<PickupItem> pickupItems = new ArrayList<>();
        PickupAddress pickupAddress = new PickupAddress();
        PickupContact pickupContact = new PickupContact();
        ClientInfo clientInfo = new ClientInfo();
        LabelInfo labelInfo = new LabelInfo();
        Shipment shipment = new Shipment();
        // prepare Customer

        Consignee consignee = new Consignee();
        PartyAddress address = new PartyAddress();
        address.setCity(account.getCity());
        address.setLine1(account.getState() + " - " + account.getCity() + " - " + account.getCountry().getCountryNameEn());
        address.setLine2(account.getAddress());
        address.setCountryCode(account.getCountry().getIsoCode2());
        address.setLatitude(0D);
        address.setLongitude(0D);

        Contact contact = new Contact();
        contact.setCellPhone(account.getPhoneCode() + account.getPhone());
        contact.setPhoneNumber1(account.getPhoneCode() + account.getPhone());
        contact.setEmailAddress(account.getEmail());

        consignee.setContact(contact);
        consignee.setPartyAddress(address);

        shipment.setConsignee(consignee);

        int numberOfPieces = order.getProductList().stream().mapToInt(m -> m.getQty().intValue()).sum();
        double shipmentWeight = order.getProductList().stream().mapToDouble(m -> m.getQty() * m.getItem().getWeight()).sum();
        if (shipmentWeight != 0) {
            shipmentWeight = shipmentWeight / 1000;
        }
        PickupItem pickupItem = new PickupItem(productGroup, productType, 1, "Box",
                payType, new ShipmentWeight("KG", shipmentWeight), null, numberOfPieces, null,
                null, new ShipmentDimensions(0, 0, 0, ""), "Test");

        pickupItems.add(pickupItem);

        // Shiper
        Shipper shipper = new Shipper();
        shipment.setShipper(shipper);
        Details shipmentDetails = new Details();
        shipmentDetails.getActualWeight().setValue(shipmentWeight);
        shipmentDetails.getActualWeight().setUnit("KG");
        shipmentDetails.setNumberOfPieces(numberOfPieces);
        shipmentDetails.setProductType(productType);
        shipmentDetails.setPaymentType(payType);
        shipmentDetails.setDescriptionOfGoods("cosmetic");
        shipmentDetails.setGoodsOriginCountry("AE");
        shipment.setDetails(shipmentDetails);
        Transaction transaction = new Transaction("", "", "", "", "");

        Pickup pickup = new Pickup(pickupAddress, pickupContact, Collections.singletonList(shipment), pickupItems);

        CreatePickupDto pickupDto = new CreatePickupDto();
        pickupDto.setTransaction(transaction);
        pickupDto.setClientInfo(clientInfo);
        pickupDto.setLabelInfo(labelInfo);
        pickupDto.setPickup(pickup);

        JSONObject response = aramexApi.createPickup(pickupDto);

        //System.out.println(response);
        return pickupDto;
    }
*/

    public void changePayentType() {
        JsfUtils.putToSessionMap(Constants.CURRENT_COUNTRY_PAYMENT, null);

        if (paymentType != null && CollectionUtils.isNotEmpty(paymentType.getCountryCashOnDeliveries())) {
            Country country = JsfUtils.getFromSessionMap(Constants.CURRENT_COUNTRY);

            Optional<CountryCashOnDelivery> countryCashOn = paymentType.getCountryCashOnDeliveries().stream()
                    .filter(countryCashOnDelivery -> countryCashOnDelivery.getCountry()
                            .getId()
                            .equals(country
                                    .getId()))
                    .findFirst();
            if (countryCashOn.isPresent()) {
                JsfUtils.putToSessionMap(Constants.CURRENT_COUNTRY_PAYMENT, countryCashOn.get());
            } else {
                JsfUtils.putToSessionMap(Constants.CURRENT_COUNTRY_PAYMENT, null);

            }

        }
    }

    public void applyCoupon() {
        coupon = couponService.findActiceByCouponNumber(sCoupon.trim());
        sCoupon = null;
        if (coupon != null) {

            addInfoMessage("common.couponFounded", "applyCoupon:code");
        } else {

            sCoupon = null;
            addErrorMessage("common.couponNotFounded", "applyCoupon:code");
        }
        JsfUtils.update("orderDt", "form_messages", "frmPay");

    }

    public void addToCart() {
        Long productId = new Long(JsfUtils.getRequestParameterMap("productId"));
        Long qty = new Long(JsfUtils.getRequestParameterMap("qty"));
        Long sumqty = cartList.stream().filter(p -> p.getItem().getId().equals(productId)).mapToLong(Cart::getQty).sum();
        long aviQty = stockInService.getAvailableQuantityByItemID(productId);
        if (aviQty <= 0 || sumqty >= aviQty) {
            addErrorMessage("common.outStock");
            return;
        }

        boolean found = false;
        for (Cart cart : getCartList()) {
            if (cart.getItem().getId().equals(productId)) {
                cart.setQty(cart.getQty() + qty);
                found = true;
            }
        }
        if (!found) {
            Item item = itemService.findById(productId);
            Cart cart = new Cart();
            cart.setId((getCartList().size() + 1L) * -1);
            cart.setQty(qty);
            cart.setItem(item);
            getCartList().add(cart);
        }

        subtotal = 0D;
        sumQty = 0;
        for (Cart cart : getCartList()) {
            Long qty1 = cart.getQty();
            Double price = cart.getItem().getFinalPrice();
            double result = qty1 * price;
            subtotal = subtotal + result;
            sumQty = sumQty + qty1;

        }

        JsfUtils.update("sumQty", "cartList");
        addSuccessMessage("common.added");

    }

    public Double getSumFinalAmountPrice() {
        return getSumFinalPrice();
    }

    /**
     * @return
     */
    public double getDiscount() {
        Double finalPrice = getSumFinalPrice();
        if (coupon == null) {
            return 0;
        } else if (coupon.getPercentage()) {
            return finalPrice * coupon.getDiscount() / 100;
        } else if (coupon.getDiscount() > finalPrice) {
            return finalPrice;
        } else {
            return coupon.getDiscount();
        }
    }

    public double getShipment() {
        double weight = getCartList().stream().mapToDouble(a -> a.getItem().getWeight() * a.getQty()).sum();

        // System.err.println("weight is :" + weight);
        return Utils.calcShipment(weight);

    }

    public Double getSumFinalPrice() {

        AtomicReference<Double> sum = new AtomicReference<>(0D);
        getCartList().forEach(cart -> {
            sum.updateAndGet(
                    v -> v + cart.getQty() * cart.getItem().getFinalPrice());

        });
        return sum.get();
    }

    public List<Cart> getCartList() {

        cartList.stream().forEach(c -> {
            c.setItem(itemService.findById(c.getItem().getId()));
        });
        return cartList;
    }

    public void setCartList(List<Cart> cartList) {
        this.cartList = cartList;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public long getSumQty() {
        return sumQty;
    }

    public void setSumQty(long sumQty) {
        this.sumQty = sumQty;
    }

    public ItemService getItemService() {
        return itemService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }

    public CouponService getCouponService() {
        return couponService;
    }

    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }

    public String getsCoupon() {
        return sCoupon;
    }

    public void setsCoupon(String sCoupon) {
        this.sCoupon = sCoupon;
    }

    public void setLoginMB(LoginMB loginMB) {
        this.loginMB = loginMB;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void setOrderProductService(OrderProductService orderProductService) {
        this.orderProductService = orderProductService;
    }

    public void setAramexApi(AramexApi aramexApi) {
        this.aramexApi = aramexApi;
    }

    public void setStockInService(StockInService stockInService) {
        this.stockInService = stockInService;
    }

    public void setPaymentTypeService(PaymentTypeService paymentTypeService) {
        this.paymentTypeService = paymentTypeService;
    }

    public List<PaymentType> getPaymentTypeList() {
        return paymentTypeList;
    }

    public void setPaymentTypeList(List<PaymentType> paymentTypeList) {
        this.paymentTypeList = paymentTypeList;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Order getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(Order selectedOrder) {
        this.selectedOrder = selectedOrder;
    }

    public List<Cart> getItemNotAvailable() {
        return itemNotAvailable;
    }

    public void setItemNotAvailable(List<Cart> itemNotAvailable) {
        this.itemNotAvailable = itemNotAvailable;
    }

    public Double getPaymentRate() {
        CountryCashOnDelivery countryCashOnDelivery = JsfUtils.getFromSessionMap(Constants.CURRENT_COUNTRY_PAYMENT);
        if (countryCashOnDelivery != null) {
            CountryCashOnDelivery i = countryCashOnDeliveryRepo.findById(countryCashOnDelivery.getId());
            return i.getRate();
        }

        return 0D;
    }

    public void setCountryCashOnDeliveryRepo(CountryCashOnDeliveryRepo countryCashOnDeliveryRepo) {
        this.countryCashOnDeliveryRepo = countryCashOnDeliveryRepo;
    }

    public void setShipmentTypeService(ShipmentTypeService shipmentTypeService) {
        this.shipmentTypeService = shipmentTypeService;
    }
}
