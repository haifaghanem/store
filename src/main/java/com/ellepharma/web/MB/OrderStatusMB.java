package com.ellepharma.web.MB;

import com.ellepharma.data.dto.VerifyRequest;
import com.ellepharma.data.model.Order;
import com.ellepharma.data.model.PaymentStatusEnum;
import com.ellepharma.data.model.ShipmentStatusEnum;
import com.ellepharma.service.OrderService;
import com.ellepharma.utils.JsfUtils;
import com.ellepharma.utils.SecurityUtils;
import com.ellepharma.utils.Utils;
import com.ellepharma.web.api.PayTabs;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.Cookie;
import java.util.Objects;

/**
 * ProductMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 01, 2019
 */
@ManagedBean
@ViewScoped
public class OrderStatusMB extends BaseMB {

    @ManagedProperty("#{orderService}")
    private OrderService orderService;

    @ManagedProperty("#{cartMB}")
    private CartMB cartMB;

    private Order order;

    private VerifyRequest request;

    public void init(Long id) {

        if (id != null) {
            order = orderService.findByIdAndAccount(id, SecurityUtils.getCurrentAccount());
            if (Objects.nonNull(order) && order.getPaymentStatus().equals(PaymentStatusEnum.TNC_1)) {
                if (order.getPaymentType().getId().equals(2L) && !Objects.equals(PaymentStatusEnum.TNC_100, order.getPaymentStatus())) {
                    PayTabs payTabs = new PayTabs();
                    request = payTabs.verifyPayment(String.valueOf(order.getPaymentId()));
                    if (request.getResponse_code().equalsIgnoreCase("100")) {
                        order.setPaymentStatus(PaymentStatusEnum.TNC_100);
                        order.setShipmentStatus(ShipmentStatusEnum.Pending);
                        order.setInvoiceNo(orderService.findMaxInvoice() + 1);
                        orderService.save(order);

                        clearCarts();
                    }
                    else {
                        order.setPaymentStatus(PaymentStatusEnum.fomCode(request.getResponse_code()));
                        orderService.save(order);


                    }
                } else if (order.getPaymentType().getId().equals(1L)) {
                    request = new VerifyRequest();
                    request.setResponse_code("100");
                    clearCarts();

                }
            }
        }

    }

    private void clearCarts() {
        Cookie co = Utils.getCookie("clearCart");
        if (Objects.nonNull(co) && co.getValue().equalsIgnoreCase("true")) {
            cartMB.getCartList().clear();
            JsfUtils.executeScript("$.removeCookie('clearCart', {path: '/'});");
        }
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public VerifyRequest getRequest() {
        return request;
    }

    public void setRequest(VerifyRequest request) {
        this.request = request;
    }

    public void setCartMB(CartMB cartMB) {
        this.cartMB = cartMB;
    }


}