package com.ellepharma.web.MB;

import com.ellepharma.data.model.Category;
import com.ellepharma.data.model.Item;
import com.ellepharma.data.model.Manufacturer;
import com.ellepharma.data.model.StockIn;
import com.ellepharma.service.CategoryService;
import com.ellepharma.service.ItemService;
import com.ellepharma.service.ManufacturerService;
import com.ellepharma.service.StockInService;
import com.ellepharma.utils.DataBaseUtils;
import com.ellepharma.utils.JsfUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.zeroturnaround.zip.commons.IOUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * CategoryMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */
@ManagedBean
@ViewScoped
public class ExportDataMB extends BaseMB {

    @ManagedProperty("#{categoryService}")
    private CategoryService categoryService;

    @ManagedProperty("#{manufacturerService}")
    private ManufacturerService manufacturerService;

    @ManagedProperty("#{itemService}")
    private ItemService itemService;

    @ManagedProperty("#{stockInService}")
    private StockInService stockInService;

    private String voucherNo;

    private List<String> voucherNoList;

    @PostConstruct
    public void init() {

    }

    public void exportDataBase() throws IOException {
        File ds = DataBaseUtils.backupDataBase();

        HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        res.setContentType("application/octet-stream");
        res.setHeader("Content-disposition", "attachment; filename=dataBase.sql");

        try {

            ServletOutputStream out = res.getOutputStream();

            IOUtils.copy(new FileInputStream(ds), out);
            out.flush();
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        FacesContext faces = FacesContext.getCurrentInstance();
        faces.responseComplete();
    }

    public void export() throws IOException {
        String[] columns = { "كود المنتج", "اسم المنتج عربي", "اسم المنتج انجليزي", "السعر بالدولار", "الكمية محدودة؟", "الكمية",
                "لديه ضريبة؟", "الضريبة %", "باركود المنتج", "لديه خصم؟", "نسبة الخصم", "الوزن جرام", "تفاصيل المنتج عربي",
                "تفاصيل المنتج انجليزي", "كوبون الخصم ان وجد", "كود القسم", "كود الماركة", "فعّال", "نسبة المشهور" };
        Workbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Products");
        sheet.setRightToLeft(true);

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.RED.getIndex());
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        Row headerRow = sheet.createRow(0);
        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        int rowNum = 1;

        List<Item> items = itemService.findAll();

        for (Item item : items) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0).setCellValue(item.getCode());
            row.createCell(1).setCellValue(item.getName().get("ar"));
            row.createCell(2).setCellValue(item.getName().get("en"));
            row.createCell(3).setCellValue(item.getPrice());
            row.createCell(4).setCellValue(true);
            row.createCell(5).setCellValue(item.getQty());
            row.createCell(6).setCellValue(item.getHaveTax());
            row.createCell(7).setCellValue(item.getTax());
            row.createCell(8).setCellValue(item.getBarcodeString());
            row.createCell(9).setCellValue(item.getHaveDiscount());
            row.createCell(10).setCellValue(item.getDiscount());
            row.createCell(11).setCellValue(item.getWeight());
            row.createCell(12).setCellValue(item.getDesc().get("ar"));
            row.createCell(13).setCellValue(item.getDesc().get("en"));
            row.createCell(14).setCellValue(false);
            row.createCell(15).setCellValue(item.getCategoryCodesString());
            row.createCell(16).setCellValue(item.getManufacturer() != null ? item.getManufacturer().getCode() : "");
            row.createCell(17).setCellValue(item.getStatus());
            row.createCell(18).setCellValue(item.getDefaultCommission());

        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Closing the workbook

        HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        res.setContentType("application/vnd.ms-excel");
        res.setHeader("Content-disposition", "attachment; filename=All Items.xlsx");

        try {
            ServletOutputStream out = res.getOutputStream();

            workbook.write(out);
            out.flush();
            out.close();
            workbook.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        FacesContext faces = FacesContext.getCurrentInstance();
        faces.responseComplete();

        JsfUtils.update("script");

    }

    public void exportBrands() throws IOException {
        String[] columns = {"كود العلامة التجارية", "اسم العلامة التجارية عربي", "اسم العلامة التجارية انجليزي"};
        Workbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Products");
        sheet.setRightToLeft(true);

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.RED.getIndex());
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        Row headerRow = sheet.createRow(0);
        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        int rowNum = 1;

        List<Manufacturer> items = manufacturerService.findAll();

        for (Manufacturer item : items) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0).setCellValue(item.getCode());
            row.createCell(1).setCellValue(item.getName().get("ar"));
            row.createCell(2).setCellValue(item.getName().get("en"));

        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Closing the workbook

        HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        res.setContentType("application/vnd.ms-excel");
        res.setHeader("Content-disposition", "attachment; filename=All_Brands.xlsx");

        try {
            ServletOutputStream out = res.getOutputStream();

            workbook.write(out);
            out.flush();
            out.close();
            workbook.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        FacesContext faces = FacesContext.getCurrentInstance();
        faces.responseComplete();

        JsfUtils.update("script");

    }

    public void exportCategory() throws IOException {
        String[] columns = {"كود التصنيف", "اسم التصنيف عربي", "اسم التصنيف انجليزي", " التصنيف الأب"};
        Workbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Products");
        sheet.setRightToLeft(true);

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.RED.getIndex());
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        Row headerRow = sheet.createRow(0);
        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        int rowNum = 1;

        List<Category> items = categoryService.findAll();

        for (Category item : items) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0).setCellValue(item.getCode());
            row.createCell(1).setCellValue(item.getName().get("ar"));
            row.createCell(2).setCellValue(item.getName().get("en"));
            row.createCell(3).setCellValue(item.getParent() != null ? item.getParent().getCode() : "");

        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Closing the workbook

        HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        res.setContentType("application/vnd.ms-excel");
        res.setHeader("Content-disposition", "attachment; filename=All_Category.xlsx");

        try {
            ServletOutputStream out = res.getOutputStream();

            workbook.write(out);
            out.flush();
            out.close();
            workbook.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        FacesContext faces = FacesContext.getCurrentInstance();
        faces.responseComplete();

        JsfUtils.update("script");

    }

    public void exportStcokInvoice() throws IOException {
        String[] columns = {"رقم الفاتورة", "كود المنتج", "اسم المنتتج بالإنجليزي", "الكمية"};
        Workbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Products");
        sheet.setRightToLeft(true);

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.RED.getIndex());
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        Row headerRow = sheet.createRow(0);
        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        int rowNum = 1;

        List<StockIn> items = stockInService.findByVoucherNo(voucherNo);

        for (StockIn item : items) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0).setCellValue(item.getVoucherNo());
            row.createCell(1).setCellValue(item.getItem().getCode());
            row.createCell(2).setCellValue(item.getItem().getName().get("en"));
            row.createCell(3).setCellValue(item.getQuantity());

        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Closing the workbook

        HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        res.setContentType("application/vnd.ms-excel");
        res.setHeader("Content-disposition", "attachment; filename=quantities_ivoiceNO ( " + voucherNo + " ).xlsx");

        try {
            ServletOutputStream out = res.getOutputStream();

            workbook.write(out);
            out.flush();
            out.close();
            workbook.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        FacesContext faces = FacesContext.getCurrentInstance();
        faces.responseComplete();

        JsfUtils.update("script");
        JsfUtils.hideDialog("quantities");

    }

    public void openDlgQuantities() {
        voucherNoList = stockInService.findAllVoucherNo();
        JsfUtils.openDialog("quantities");
        voucherNo = null;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setManufacturerService(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public void setStockInService(StockInService stockInService) {
        this.stockInService = stockInService;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public List<String> getVoucherNoList() {
        return voucherNoList;
    }

    public void setVoucherNoList(List<String> voucherNoList) {
        this.voucherNoList = voucherNoList;
    }
}