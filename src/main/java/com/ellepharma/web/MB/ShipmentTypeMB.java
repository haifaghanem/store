package com.ellepharma.web.MB;

import com.ellepharma.data.model.Country;
import com.ellepharma.data.model.CountryShipment;
import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.ShipmentType;
import com.ellepharma.service.CountryService;
import com.ellepharma.service.CountryShipmentService;
import com.ellepharma.service.ShipmentTypeService;
import com.ellepharma.utils.ExceptionCodes;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.List;

/**
 * ShipmentTypeMB.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 22, 2019
 */
@ManagedBean
@ViewScoped
public class ShipmentTypeMB extends BaseMB {

    @ManagedProperty("#{shipmentTypeService}")
    private ShipmentTypeService shipmentTypeService;

    @ManagedProperty("#{countryShipmentService}")
    private CountryShipmentService countryShipmentService;

    @ManagedProperty("#{countryService}")
    private CountryService countryService;

    private ShipmentType selectedShipmentType;
    private ShipmentType selectedDefaultShipmentType;

    private CustomLazyDataModel<ShipmentType> shipmentTypeCustomLazyDataModel;

    private List<ShipmentType> shipmentTypeList;

    private CountryShipment selectedCountryShipment;

    private List<Country> countryList;

    private boolean editCountryMode;

    @PostConstruct
    public void init() {
        selectedShipmentType = new ShipmentType();
        countryList = countryService.findAll();
        shipmentTypeCustomLazyDataModel = new CustomLazyDataModel<>(shipmentTypeService);

        shipmentTypeList = shipmentTypeService.findAll();
        selectedDefaultShipmentType = shipmentTypeService.findByStatusTrue();

        selectedCountryShipment = new CountryShipment();
    }

    /**
     *
     */
    public void addNewShipmentType() {
        selectedShipmentType = new ShipmentType();
        setEditMode(false);
        showDialog("dlgShipmentType");
        updates("shipFrm");

    }

    /**
     *
     */
    public void saveDefaultShip() {
        shipmentTypeService.saveDefaultShip(selectedDefaultShipmentType);

    }

    public void addNewCountry() {
        selectedCountryShipment = new CountryShipment();
        editCountryMode = false;
        showDialog("dlgCountryShipmentType");
        updates("countryShipFrm");

    }

    /**
     * @param shipmentType
     */
    public void editShipmentType(ShipmentType shipmentType) {
        selectedShipmentType = shipmentType;
        setEditMode(true);
        showDialog("dlgShipmentType");
        updates("shipFrm", "shipDtl");
    }

    /**
     * @param countryShipment
     */
    public void editCountryShipment(CountryShipment countryShipment) {
        selectedCountryShipment = countryShipment;
        editCountryMode = true;
        showDialog("dlgCountryShipmentType");
        updates("countryShipFrm", "countryShipDtl");
    }

    /**
     * @param shipmentType
     */
    public void deleteShipmentType(ShipmentType shipmentType) {
        try {
            shipmentTypeService.delete(shipmentType);
        } catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
            addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
            return;
        }
        updates("shipDtl");
        addSuccessMessage();

    }

    /**
     * @param countryShipment
     */
    public void deleteCountryShipment(CountryShipment countryShipment) {
        selectedShipmentType.getCountryShipmentList().remove(countryShipment);
        updates("countryShipDtl");
        addSuccessMessage();

    }

    /**
     *
     */
    public boolean saveShipmentType() {
        try {
            selectedShipmentType.getCountryShipmentList().forEach(countryShipment -> {
                countryShipmentService.save(countryShipment);

            });
            selectedShipmentType = shipmentTypeService.save(selectedShipmentType);
            selectedShipmentType = new ShipmentType();
        } catch (DataIntegrityViolationException ex) {
            addErrorMessage("common.addErrorMessage");
            return false;
        }
        updates("shipDtl", "shipFrm");
        addSuccessMessage();
        return true;
    }

    public void saveShipmentTypeAndHide() {
        if (saveShipmentType()) {
            hideDialog("dlgShipmentType");

        }
    }

    public void saveCountryShipmentAndHide() {
        selectedShipmentType.getCountryShipmentList().add(selectedCountryShipment);
        selectedCountryShipment = new CountryShipment();
        updates("countryShipDtl", "countryShipFrm");
        hideDialog("dlgCountryShipmentType");
    }

    public void editCountryShipmentAndHide() {
        int index = selectedShipmentType.getCountryShipmentList().indexOf(selectedCountryShipment);
        selectedShipmentType.getCountryShipmentList().remove(selectedCountryShipment);
        selectedShipmentType.getCountryShipmentList().add(index, selectedCountryShipment);
        selectedCountryShipment = new CountryShipment();
        updates("countryShipDtl", "countryShipFrm");
        hideDialog("dlgCountryShipmentType");
    }

    public boolean saveCountryShipment() {
        selectedShipmentType.getCountryShipmentList().add(selectedCountryShipment);
        selectedCountryShipment = new CountryShipment();
        updates("countryShipDtl", "countryShipFrm");
        return true;
    }

    public ShipmentTypeService getShipmentTypeService() {
        return shipmentTypeService;
    }

    public void setShipmentTypeService(ShipmentTypeService shipmentTypeService) {
        this.shipmentTypeService = shipmentTypeService;
    }

    public ShipmentType getSelectedShipmentType() {
        return selectedShipmentType;
    }

    public void setSelectedShipmentType(ShipmentType selectedShipmentType) {
        this.selectedShipmentType = selectedShipmentType;
    }

    public CustomLazyDataModel<ShipmentType> getShipmentTypeCustomLazyDataModel() {
        return shipmentTypeCustomLazyDataModel;
    }

    public void setShipmentTypeCustomLazyDataModel(CustomLazyDataModel<ShipmentType> shipmentTypeCustomLazyDataModel) {
        this.shipmentTypeCustomLazyDataModel = shipmentTypeCustomLazyDataModel;
    }

    public CountryShipmentService getCountryShipmentService() {
        return countryShipmentService;
    }

    public void setCountryShipmentService(CountryShipmentService countryShipmentService) {
        this.countryShipmentService = countryShipmentService;
    }

    public CountryService getCountryService() {
        return countryService;
    }

    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }

    public CountryShipment getSelectedCountryShipment() {
        return selectedCountryShipment;
    }

    public void setSelectedCountryShipment(CountryShipment selectedCountryShipment) {
        this.selectedCountryShipment = selectedCountryShipment;
    }

    public List<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
    }

    public boolean isEditCountryMode() {
        return editCountryMode;
    }

    public void setEditCountryMode(boolean editCountryMode) {
        this.editCountryMode = editCountryMode;
    }

    public List<ShipmentType> getShipmentTypeList() {
        return shipmentTypeList;
    }

    public void setShipmentTypeList(List<ShipmentType> shipmentTypeList) {
        this.shipmentTypeList = shipmentTypeList;
    }

    public ShipmentType getSelectedDefaultShipmentType() {
        return selectedDefaultShipmentType;
    }

    public void setSelectedDefaultShipmentType(ShipmentType selectedDefaultShipmentType) {
        this.selectedDefaultShipmentType = selectedDefaultShipmentType;
    }
}
