package com.ellepharma.web.MB;

import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.Menu;
import com.ellepharma.service.MenuService;
import com.ellepharma.utils.ExceptionCodes;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

/**
 * CategoryMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 24, 2019
 */
@ManagedBean
@ViewScoped
public class MenuMB extends BaseMB {

    @ManagedProperty("#{menuService}")
    private MenuService menuService;

    private Menu selectedMenu;

    private CustomLazyDataModel<Menu> menuCustomLazyDataModel;

    private List<Menu> menuList;

    @PostConstruct
    public void init() {
        selectedMenu = new Menu();
        menuList = menuService.findAll();
        menuCustomLazyDataModel = new CustomLazyDataModel<>(menuService);
    }

    /**
     *
     */
    public void addNewMenu() {
        selectedMenu = new Menu();
        menuList = menuService.findAll();
        setEditMode(false);
        showDialog("dlgMenu");
        updates("menuFrm");

    }

    /**
     * @param menu
     */
    public void editMenu(Menu menu) {
        selectedMenu = menu;
        setEditMode(true);
        menuList = menuService.findAll();
        menuList.remove(menu);
        showDialog("dlgMenu");
        updates("menuFrm", "menuDtl");
    }

    /**
     * @param menu
     */
    public void deleteMenu(Menu menu) {
        try {
            menuService.delete(menu);
            menuList.remove(menu);
            updates("MenuDtl");
            addSuccessMessage();

        } catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
            addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
        }

    }

    /**
     *
     */
    public void saveMenu() throws SQLIntegrityConstraintViolationException {
        selectedMenu = menuService.save(selectedMenu);
        menuList.add(selectedMenu);

        selectedMenu = new Menu();
        updates("menuFrm", "menuDtl");
        addSuccessMessage();

    }

    public void saveMenuAndHide() throws SQLIntegrityConstraintViolationException {
        saveMenu();
        hideDialog("dlgMenu");

    }

    public MenuService getMenuService() {
        return menuService;
    }

    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }

    public Menu getSelectedMenu() {
        return selectedMenu;
    }

    public void setSelectedMenu(Menu selectedMenu) {
        this.selectedMenu = selectedMenu;
    }

    public CustomLazyDataModel<Menu> getMenuCustomLazyDataModel() {
        return menuCustomLazyDataModel;
    }

    public void setMenuCustomLazyDataModel(CustomLazyDataModel<Menu> menuCustomLazyDataModel) {
        this.menuCustomLazyDataModel = menuCustomLazyDataModel;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }
}
