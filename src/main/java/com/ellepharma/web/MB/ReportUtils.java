package com.ellepharma.web.MB;

import com.ellepharma.utils.ReportConfigUtil;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.j2ee.servlets.BaseHttpServlet;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public class ReportUtils {


    public enum ExportOption {

        PDF, HTML, EXCEL, RTF
    }

    private ExportOption exportOption = ExportOption.HTML;
    private static final String COMPILE_DIR = "/common/reports/iso/";
    private String message;


    public static JasperPrint doReport(String reportName, Map<String, Object> params) throws JRException, IOException {

        compileReport(reportName);
        File reportFile = new File(ReportConfigUtil.getJasperFilePath(getContext(), getCompileDir(), reportName + ".jasper"));

        JasperPrint jasperPrint = ReportConfigUtil.fillReport(reportFile, params, new JREmptyDataSource());

        return jasperPrint;
    }

    public JasperPrint doReport(String reportName, Map<String, Object> params, JRBeanCollectionDataSource jrBeanCollectionDataSource) throws JRException, IOException {

        File reportFile = new File(ReportConfigUtil.getJasperFilePath(getContext(), getCompileDir(), reportName + ".jasper"));


        JasperPrint jasperPrint = ReportConfigUtil.fillReport(reportFile, params, jrBeanCollectionDataSource);


        return jasperPrint;
    }


    public static void compileReport(String reportName) throws JRException, IOException {


        ReportConfigUtil.compileReport(getContext(), getCompileDir(), reportName);


    }


    private static ServletContext getContext() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

        ServletContext context = (ServletContext) externalContext.getContext();


        return context;
    }

    private HttpServletResponse getResponse() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();


        return response;
    }


    private HttpServletRequest getRequest() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();


        return request;
    }

    protected void pring(JasperPrint jasperPrint) throws JRException, IOException {

        if (getExportOption().equals(ExportOption.HTML)) {
            ReportConfigUtil.exportReportAsHtml(jasperPrint, getResponse().getWriter());
        } else if (getExportOption().equals(ExportOption.EXCEL)) {
            ReportConfigUtil.exportReportAsExcel(jasperPrint, getResponse().getWriter());
        } else {
            getRequest().getSession().setAttribute(BaseHttpServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
            getResponse().sendRedirect(getRequest().getContextPath() + "/reports/" + getExportOption());
        }


        getRequest().getSession().setAttribute(BaseHttpServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
        getResponse().sendRedirect(getRequest().getContextPath() + "/reports/" + getExportOption());
        FacesContext.getCurrentInstance().responseComplete();
    }


    public static String exportToPdf(final JasperPrint print) throws JRException, IOException {


        File pdf = File.createTempFile("output.", ".pdf");

        JasperExportManager.exportReportToPdfFile(print, pdf.getPath());

        return pdf.getAbsolutePath();


    }


    public static String export(final JasperPrint print) throws JRException, UnsupportedEncodingException {
        final Exporter exporter;
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        exporter = new HtmlExporter();
        exporter.setExporterOutput(new SimpleHtmlExporterOutput(out));


        exporter.setExporterInput(new SimpleExporterInput(print));
        exporter.exportReport();

        return out.toString("utf-8");
    }

    public ExportOption getExportOption() {
        return exportOption;
    }

    public void setExportOption(ExportOption exportOption) {
        this.exportOption = exportOption;
    }


    protected static String getCompileDir() {
        return COMPILE_DIR;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}