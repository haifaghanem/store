package com.ellepharma.web.MB;

import com.ellepharma.data.model.*;
import com.ellepharma.service.CategoryService;
import com.ellepharma.service.ItemService;
import com.ellepharma.service.ManufacturerService;
import com.ellepharma.service.StockInService;
import com.ellepharma.utils.JsfUtils;
import com.ellepharma.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.primefaces.event.FileUploadEvent;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * CategoryMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */
@ManagedBean
@ViewScoped
public class ImportDataMB extends BaseMB {

    @ManagedProperty("#{categoryService}")
    private CategoryService categoryService;

    @ManagedProperty("#{manufacturerService}")
    private ManufacturerService manufacturerService;

    @ManagedProperty("#{itemService}")
    private ItemService itemService;

    @ManagedProperty("#{stockInService}")
    private StockInService stockInService;

    private List<Category> categoryList = new ArrayList<>();

    private List<Item> itemList = new ArrayList<>();

    private List<Manufacturer> brandList = new ArrayList<>();

    private List<StockIn> stockIns = new ArrayList<>();

    private List<SystemLanguage> languages;

    @PostConstruct
    public void init() {

        languages = Utils.getSystemLangsSorted();

    }

    public void upload(FileUploadEvent event) throws Exception {

        Workbook workbook = WorkbookFactory.create(event.getFile().getInputstream());
        Sheet sheet = workbook.getSheetAt(0);
        categoryList = new ArrayList<>();
        DataFormatter formatter = new DataFormatter();

        int currentRow = 0;
        try {

            for (Row row : sheet) {
                if (currentRow == 0) {
                    currentRow++;
                    continue;
                }

                Category category = new Category();
                int i = 0;
                for (Cell cell : row) {
                    switch (i) {
                        case 0: {
                            category.setCode(formatter.formatCellValue(cell));
                            break;
                        }
                        case 1: {
                            category.getName().put(languages.get(0).getLocale(), formatter.formatCellValue(cell));
                            break;
                        }
                        case 2: {
                            category.getName().put(languages.get(1).getLocale(), formatter.formatCellValue(cell));
                            break;

                        }
                        case 3: {
                            category.setParentCode(formatter.formatCellValue(cell));
                            break;

                        }
                    }
                    i++;
                }
                categoryList.add(category);
            }
        } catch (Exception e) {
            JsfUtils.addErrorMessage("حصل خطا عند سطر رقم " + (currentRow + 1));
            return;

        }
        try {
            categoryList = categoryService.saveImportData(categoryList);

        } catch (Exception e) {
            JsfUtils.addErrorMessage(e.getMessage() + (currentRow + 1));

            return;
        }
        addSuccessMessage();
    }

    public void uploadBrand(FileUploadEvent event) throws Exception {

        Workbook workbook = WorkbookFactory.create(event.getFile().getInputstream());
        Sheet sheet = workbook.getSheetAt(0);
        brandList = new ArrayList<>();
        DataFormatter formatter = new DataFormatter();

        int currentRow = 0;
        for (Row row : sheet) {
            if (currentRow == 0) {
                currentRow++;
                continue;
            }

            Manufacturer brand = null;
            int i = 0;
            for (Cell cell : row) {
                if (isRowEmpty(row)) {
                    break;
                }
                switch (i) {
                    case 0: {
                        String sd = formatter.formatCellValue(cell);
                        brand = manufacturerService.getByCode(sd);
                        brand.setCode(sd);
                        break;
                    }
                    case 1: {
                        brand.getName().put(languages.get(0).getLocale(), formatter.formatCellValue(cell));
                        break;
                    }
                    case 2: {
                        brand.getName().put(languages.get(1).getLocale(), formatter.formatCellValue(cell));
                        break;

                    }

                }
                i++;
            }
            brandList.add(brand);
        }
        brandList = manufacturerService.saveImportData(brandList);
        addSuccessMessage();
    }

    public void uploadStock(FileUploadEvent event) throws Exception {

        Workbook workbook = WorkbookFactory.create(event.getFile().getInputstream());
        Sheet sheet = workbook.getSheetAt(0);
        brandList = new ArrayList<>();
        DataFormatter formatter = new DataFormatter();

        String val = "";
        String voucherNo = "";
        int currentRow = 0;
        int i = 0;
        try {
            for (Row row : sheet) {

                if (currentRow == 0) {
                    currentRow++;
                    continue;
                }
                currentRow++;
                StockIn stockIn = null;

                i = 0;
                voucherNo = "";
                for (Cell cell : row) {
                    val = formatter.formatCellValue(cell);
                    if (StringUtils.isEmpty(val)) {
                        i++;
                        continue;
                    }
                    if (isRowEmpty(row)) {
                        break;
                    }

                    switch (i) {

                        case 0: {
                            voucherNo = val;

                            break;
                        }
                        case 1: {
                            stockIn = stockInService.findByVoucherNoAndItem_Code(voucherNo, val);
                            Item item = null;
                            if (Objects.isNull(stockIn.getId())) {
                                item = itemService.findByCode(val);
                                if (Objects.isNull(item.getId())) {
                                    JsfUtils.addErrorMessage("لم يتم العثور على منتج بهذا الكود " + val);
                                    throw new Exception();

                                }

                            } else {
                                item = stockIn.getItem();
                            }
                            stockIn.setVoucherNo(voucherNo);
                            stockIn.setItem(item);
                            break;
                        }
                        case 2: {
                            break;
                        }
                        case 3: {
                            Double qty = 0D;
                            if (StringUtils.isNotEmpty(val)) {
                                qty = new Double(val);
                            }
                            stockIn.setQuantity(qty);
                            break;

                        }

                    }
                    i++;
                }
                stockIns.add(stockIn);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JsfUtils.addErrorMessageDirect("حصل خطا عند سطر رقم " + (currentRow) + " : عامود رقم  : " + i + e.toString() + "-" + voucherNo + "--" + val);

            return;
        }
        try {

            //stockIns = stockInService.saveIportData(stockIns);
            stockIns.stream().filter(stockIn -> stockIn != null).forEach(stockIn -> {
                stockIn = stockInService.save(stockIn);
            });

            addSuccessMessage();
        } catch (Exception e) {
            e.printStackTrace();
            stockIns = new ArrayList<>();
            JsfUtils.addErrorMessageDirect(e.getMessage());
        }

    }

    public void uploadProduct(FileUploadEvent event) throws Exception {

        Workbook workbook = WorkbookFactory.create(event.getFile().getInputstream());

        this.itemList = new ArrayList<>();
        Sheet sheet = workbook.getSheetAt(0);

        List itemList = new ArrayList<>();
        DataFormatter formatter = new DataFormatter();

        int currentRow = 0;
        int i = 0;
        try {

            for (Row row : sheet) {
                if (currentRow == 0) {
                    currentRow++;
                    continue;
                }
                if (isRowEmpty(row)) {
                    break;
                }
                currentRow++;
                System.out.printf(currentRow + "");
                Item item = null;
                i = 0;
                for (Cell cell : row) {
                    String val = formatter.formatCellValue(cell);
                    if (StringUtils.isEmpty(val)) {
                        i++;
                        continue;
                    }
                    switch (i) {
                        case 0: {
                            item = itemService.findByCode(val);
                            item.setCode(val);
                            break;
                        }
                        case 1: {
                            item.getName().put(languages.get(0).getLocale(), val);
                            break;
                        }
                        case 2: {
                            item.getName().put(languages.get(1).getLocale(), val);
                            break;

                        }

                        case 3: {
                            item.setPrice(new Double(val));
                            break;
                        }

                        case 4: {
                            item.setLimitQty(new Boolean(val));
                            break;
                        }
                        case 5: {
                            item.setQty(new Long(val));
                            break;
                        }

                        case 6: {
                            item.setHaveTax(new Boolean(val));
                            break;
                        }

                        case 7: {
                            item.setTax(new Double(val));
                            break;
                        }

                        case 8: {
                            if (StringUtils.isNotEmpty(val)) {

                                List<String> list = Stream.of(val.split(",")).collect(Collectors.toList());
                                item.setBarcodeList(list);
                            }
                            break;
                        }

                        case 9: {
                            item.setHaveDiscount(new Boolean(val));
                            break;
                        }
                        case 10: {
                            item.setDiscount(new Double(val));
                            break;
                        }
                        case 11: {
                            item.setWeight(new Double(val));
                            break;
                        }
                        case 12: {
                            item.getDesc().put(languages.get(0).getLocale(), val);
                            break;
                        }

                        case 13: {
                            item.getDesc().put(languages.get(1).getLocale(), val);
                            break;
                        }

                        case 14: {

                            break;
                        }

                        case 15: {

                            item.setCatCode(val);

                            break;
                        }

                        case 16: {
                            item.setBrandCode(val);
                            break;
                        }
                        case 17: {
                            if (StringUtils.isNotEmpty(val)) {
                                item.setStatus(new Boolean(val));
                            }
                            break;
                        }
                        case 18: {
                            if (StringUtils.isNotEmpty(val)) {
                                item.setDefaultCommission(new Double(val));
                            }
                            break;
                        }
                    }
                    i++;
                }
                itemList.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JsfUtils.addErrorMessageDirect("حصل خطا عند سطر رقم " + (currentRow) + " : عامود رقم  : " + i + e.toString());

            return;
        }
        try {

            this.itemList.addAll(itemService.saveImportData(itemList));
            addSuccessMessage();
        } catch (Exception e) {
            e.printStackTrace();
            itemList = new ArrayList<>();
            JsfUtils.addErrorMessageDirect(e.getMessage());
        }
    }

    public static boolean isRowEmpty(Row row) {
        Cell cell = row.getCell(0);

        return cell == null || cell.getCellTypeEnum().name().equalsIgnoreCase(CellType.BLANK.name());
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public List<Manufacturer> getBrandList() {
        return brandList;
    }

    public void setManufacturerService(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    public void setStockInService(StockInService stockInService) {
        this.stockInService = stockInService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public List<StockIn> getStockIns() {
        return stockIns;
    }
}
