package com.ellepharma.web.MB;

import com.ellepharma.data.dto.OrderReportFilterDTO;
import com.ellepharma.service.AccountService;
import com.ellepharma.service.OrderService;
import com.ellepharma.web.security.SessionCounter;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class DashboardMB extends BaseMB {


    @ManagedProperty("#{accountService}")
    private AccountService accountService;

    @ManagedProperty("#{orderService}")
    private OrderService orderService;

    private int countSessions;

    private long countUser;

    private long countCelebrity;

    private long countPendingEmailOrder;

    private long countPendingAramexOrder;

    private Double sumTotalShipmentOrders;

    private Double sumLastMonthShipmentOrders;

    private Double sumTotalOrder;

    @PostConstruct
    public void init() {

        SessionCounter sessionCounter = (SessionCounter) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
                                                                     .get("session-counter");
        countSessions = sessionCounter.getActiveSessionNumber();
        countUser = accountService.countUser();
        countCelebrity = accountService.countCelebrity();

        countPendingEmailOrder = orderService.findPendingEmailOrder();
        countPendingAramexOrder = orderService.findPendingAramexOrder();
        sumTotalShipmentOrders = orderService.sumShipmentOrderReport(new OrderReportFilterDTO());
        sumLastMonthShipmentOrders = orderService.sumLastShipmentOrderReport(new OrderReportFilterDTO());
        sumTotalOrder = orderService.sumAllOrder();
    }

    public int getCountSessions() {

        return countSessions;
    }

    public void setCountSessions(int countSessions) {
        this.countSessions = countSessions;
    }

    public long getCountUser() {

        return countUser;
    }

    public void setCountUser(long countUser) {
        this.countUser = countUser;
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    public long getCountCelebrity() {
        return countCelebrity;
    }

    public void setCountCelebrity(long countCelebrity) {
        this.countCelebrity = countCelebrity;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public long getCountPendingEmailOrder() {
        return countPendingEmailOrder;
    }

    public void setCountPendingEmailOrder(long countPendingEmailOrder) {
        this.countPendingEmailOrder = countPendingEmailOrder;
    }

    public long getCountPendingAramexOrder() {
        return countPendingAramexOrder;
    }

    public void setCountPendingAramexOrder(long countPendingAramexOrder) {
        this.countPendingAramexOrder = countPendingAramexOrder;
    }

    public Double getSumTotalShipmentOrders() {
        return sumTotalShipmentOrders;
    }

    public Double getSumLastMonthShipmentOrders() {
        return sumLastMonthShipmentOrders;
    }

    public void setSumLastMonthShipmentOrders(Double sumLastMonthShipmentOrders) {
        this.sumLastMonthShipmentOrders = sumLastMonthShipmentOrders;
    }

    public Double getSumTotalOrder() {
        return sumTotalOrder;
    }

    public void setSumTotalOrder(Double sumTotalOrder) {
        this.sumTotalOrder = sumTotalOrder;
    }
}
