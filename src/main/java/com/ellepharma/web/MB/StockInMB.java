package com.ellepharma.web.MB;

import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.Item;
import com.ellepharma.data.model.StockIn;
import com.ellepharma.service.ItemService;
import com.ellepharma.service.StockInService;
import com.ellepharma.utils.ExceptionCodes;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

/**
 * StockInMB.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 19, 2019
 */
@ManagedBean
@ViewScoped
public class StockInMB extends BaseMB {

    @ManagedProperty("#{stockInService}")
    private StockInService stockInService;

    @ManagedProperty("#{itemService}")
    private ItemService itemService;

    private List<Item> itemList;

    private StockIn selectedStockItem;

    private CustomLazyDataModel<StockIn> StockInCustomLazyDataModel;

    private List<StockIn> stockInList;

    private List<StockIn> stockInAddedList;

    @PostConstruct
    public void init() {
        selectedStockItem = new StockIn();
        stockInList = stockInService.findAll();
        StockInCustomLazyDataModel = new CustomLazyDataModel<>(stockInService);
        itemList = itemService.findAllActive();
        stockInAddedList = new ArrayList<>();
    }

    /**
     *
     */
    public void addNewStockIn() {
        selectedStockItem = new StockIn();
        selectedStockItem.setQuantity(1d);
        stockInAddedList = new ArrayList<>();
        stockInList = stockInService.findAll();
        setEditMode(false);
        showDialog("dlgStockIn");
        updates("stckFrm");

    }

    /**
     * @param stockIn
     */
    public void deleteStockIn(StockIn stockIn) {
        try {
            stockInService.delete(stockIn);
            stockInList.remove(stockIn);

        } catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
            addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
            return;
        }
        updates("stckDtl");
        addSuccessMessage();

    }

    /**
     *
     */
    public boolean saveStockIn() {
        try {
//            selectedStockItem = stockInService.save(selectedStockItem);
            stockInService.save(stockInAddedList);
        } catch (DataIntegrityViolationException ex) {
            addErrorMessage(ex);
            return false;
        }
        updates("stckDtl", "stckFrm");
        addSuccessMessage();
        return true;
    }

    public void saveStockInAndHide() {
        if (saveStockIn()) {
            hideDialog("dlgStockIn");

        }
    }

    public boolean addNewItem() {
        try {
            StockIn stockInToAdd = selectedStockItem;
            stockInAddedList.add(selectedStockItem);
            selectedStockItem = new StockIn();
            selectedStockItem.setVoucherNo(stockInToAdd.getVoucherNo());
            selectedStockItem.setQuantity(1d);
            selectedStockItem.setItem(null);
        } catch (DataIntegrityViolationException ex) {
			addErrorMessage(ex);
            return false;
        }
        updates("itemDtl");
        return true;
    }

    public void deleteItem(StockIn stockIn) {
        stockInAddedList.remove(stockIn);
        updates("itemDtl");
    }

    public StockInService getStockInService() {
        return stockInService;
    }

    public void setStockInService(StockInService stockInService) {
        this.stockInService = stockInService;
    }

    public StockIn getSelectedStockItem() {
        return selectedStockItem;
    }

    public void setSelectedStockItem(StockIn selectedStockItem) {
        this.selectedStockItem = selectedStockItem;
    }

    public CustomLazyDataModel<StockIn> getStockInCustomLazyDataModel() {
        return StockInCustomLazyDataModel;
    }

    public void setStockInCustomLazyDataModel(CustomLazyDataModel<StockIn> stockInCustomLazyDataModel) {
        StockInCustomLazyDataModel = stockInCustomLazyDataModel;
    }

    public List<StockIn> getStockInList() {
        return stockInList;
    }

    public void setStockInList(List<StockIn> stockInList) {
        this.stockInList = stockInList;
    }

    public ItemService getItemService() {
        return itemService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public List<StockIn> getStockInAddedList() {
        return stockInAddedList;
    }

    public void setStockInAddedList(List<StockIn> stockInAddedList) {
        this.stockInAddedList = stockInAddedList;
    }

}
