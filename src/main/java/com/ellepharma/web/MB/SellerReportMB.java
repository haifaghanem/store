package com.ellepharma.web.MB;

import com.ellepharma.data.dto.OrderReportFilterDTO;
import com.ellepharma.data.dto.ReportItemDto;
import com.ellepharma.data.model.Item;
import com.ellepharma.service.ItemService;
import com.ellepharma.service.OrderService;
import com.ellepharma.service.StockInService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * OrderReportMB.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since August 18, 2019
 */
@ManagedBean
@SessionScoped
public class SellerReportMB extends BaseMB {

    @ManagedProperty("#{orderService}")
    private OrderService orderService;

    @ManagedProperty("#{itemService}")
    private ItemService itemService;

    @ManagedProperty("#{stockInService}")
    private StockInService stockInService;

    private OrderReportFilterDTO orderReportFilterDTO;

    private List<ReportItemDto> itemDtoList;

    private Boolean status;

    @PostConstruct
    public void init() {
        orderReportFilterDTO = new OrderReportFilterDTO();

    }

    public void clear() {
        orderReportFilterDTO = new OrderReportFilterDTO();
        find();
    }

    public void find() {
        itemDtoList = orderService.sellerReport(orderReportFilterDTO);
    }

    public void find2() {
        itemDtoList = new ArrayList<>();
        List<Item> i = new ArrayList<>();
        if (status != null) {
            i = itemService.findAllByStatus(status);

        } else {
            i = itemService.findAllActive();

        }
        i.forEach(item -> {
            ReportItemDto itemDto = new ReportItemDto();
            itemDto.setItem(item);
            itemDto.setAvailable(stockInService.getAvailableQuantityByItemID(item.getId()));

            itemDto.setInvoiceNo(stockInService.findInvoiceNos(itemDto.getItem().getId()).stream()
                    .map(Objects::toString).collect(Collectors.joining(" , ")));
            itemDtoList.add(itemDto);

        });
        // itemDtoList = orderService.sellerReport(orderReportFilterDTO);
    }


    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public List<ReportItemDto> getItemDtoList() {
        return itemDtoList;
    }

    public void setItemDtoList(List<ReportItemDto> itemDtoList) {
        this.itemDtoList = itemDtoList;
    }

    public OrderReportFilterDTO getOrderReportFilterDTO() {
        return orderReportFilterDTO;
    }

    public void setOrderReportFilterDTO(OrderReportFilterDTO orderReportFilterDTO) {
        this.orderReportFilterDTO = orderReportFilterDTO;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public void setStockInService(StockInService stockInService) {
        this.stockInService = stockInService;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
