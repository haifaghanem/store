package com.ellepharma.web.MB;

import com.ellepharma.data.model.*;
import com.ellepharma.service.AccountService;
import com.ellepharma.service.CountryService;
import com.ellepharma.service.ImageService;
import com.ellepharma.service.ShipmentTypeService;
import com.ellepharma.utils.Constants;
import com.ellepharma.utils.ExceptionCodes;
import com.ellepharma.utils.JsfUtils;
import com.ellepharma.utils.Utils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.extensions.model.inputphone.Country;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.ellepharma.utils.Constants.ACCOUNT;

/**
 * CategoryMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */
@ManagedBean
@ViewScoped
public class VendorMB extends BaseMB {

	@ManagedProperty("#{shipmentTypeService}")
	private ShipmentTypeService shipmentTypeService;

	@ManagedProperty("#{accountService}")
	private AccountService accountService;

	@ManagedProperty("#{imageService}")
	private ImageService imageService;

	@ManagedProperty("#{countryService}")
	private CountryService countryService;

	private Account selectedAccount;

	private CountryShipment selectedCountryShipment;

	private List<CountryShipment> countryShipmentList;

	private List<Account> allVendors;

	private CustomLazyDataModel<Account> celebrityCustomLazyDataModel;

	private ShipmentType defaultShipmentType;

	private boolean changePassword;

	@PostConstruct
	public void init() {
		defaultShipmentType = shipmentTypeService.findByStatusTrue();

		if (defaultShipmentType != null) {
			countryShipmentList = defaultShipmentType.getCountryShipmentList();
		}
		selectedAccount = new Account();
		celebrityCustomLazyDataModel = new CustomLazyDataModel(accountService, "findVendors", null);
		allVendors = accountService.findByAccountTypeAndActiveIsTrue(AccountType.VENDOR);
	}

	/**
	 * @param account
	 */
	public void editVendor(Account account) {
		selectedAccount = account;
		countryShipmentList.stream().forEach(countryShipment -> {
			if (countryShipment.getCountry().getId().equals(account.getCountry().getId())) {
				selectedCountryShipment = countryShipment;

				JsfUtils.putToSessionMap(Constants.CURRENT_COUNTRY_SHIPMENT, countryShipment);
				JsfUtils.putToSessionMap(Constants.CURRENT_COUNTRY, countryShipment.getCountry());
				JsfUtils.putToSessionMap(Constants.CURRENT_CURRENCY, countryShipment.getCountry().getCurrency());

			}
		});
		setEditMode(true);
		showDialog("dlgVendor");
		updates("catFrm", "catDtl");
	}

	/**
	 *
	 */
	public void addNewVendor() {
		selectedAccount = new Account();
		setEditMode(false);
		showDialog("dlgVendor");
		updates("catFrm");

	}

	/**
	 * @param account
	 */
	public void deleteVendor(Account account) {
		try {
			accountService.delete(account);
		} catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
			addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
			addErrorMessage(ex);
			return;
		}
		updates("catDtl");
		addSuccessMessage();

	}

	public void onCountrySelect(SelectEvent event) {
		Country country = (Country) event.getObject();
		selectedAccount.setPhoneCode(country.getIso2());
		selectedAccount.setPhone(null);

	}

	/**
	 * @param changeEvent
	 */
	public void changeCountry(ValueChangeEvent changeEvent) {

		CountryShipment countryShipment = (CountryShipment) changeEvent.getNewValue();

		selectedAccount.setCountry(countryShipment.getCountry());
		selectedAccount.setCity(null);
		selectedAccount.setPhoneCode(Objects.requireNonNull(countryShipment).getCountry().getIsoCode2());

	}

	public List<String> getCities() {
		if (selectedCountryShipment != null) {
			return countryService.cities(selectedCountryShipment.getCountry().getIsoCode2());

		}
		return new ArrayList<>();
	}

	/**
	 *
	 */
	public boolean saveVendor() {
		if (checkUser()) {
			addErrorMessage("common.emailExists");
			return false;
		}

		try {

			selectedAccount.setCountry(selectedCountryShipment.getCountry());
			selectedAccount.setPhoneCode(selectedAccount.getCountry().getIsoCode2());

			selectedAccount = accountService.saveVendor(selectedAccount);
			selectedAccount = new Account();
		} catch (DataIntegrityViolationException ex) {
			addErrorMessage("duplicate.code");
			return false;
		}
		updates("catDtl", "catFrm");
		addSuccessMessage();
		return true;
	}

	public boolean checkUser() {

		return accountService
				.existsAccountByEmail(selectedAccount.getEmail(), selectedAccount.getId() != null ? selectedAccount.getId() : -1);
	}

	public void upload(FileUploadEvent event) throws Exception {
		String name = Utils.uploud(event, ACCOUNT);
		Image image = new Image();
		if (selectedAccount.getImage() != null) {
			image = selectedAccount.getImage();
		}
		image.setName(name);
		image.setPriorityOrder(1L);
		image = imageService.save(image);
		selectedAccount.setImage(image);
	}

	public void saveVendorAndHide() {
		if (saveVendor()) {
			hideDialog("dlgVendor");

		}

	}

	public AccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public Account getSelectedAccount() {
		return selectedAccount;
	}

	public void setSelectedAccount(Account selectedAccount) {
		this.selectedAccount = selectedAccount;
	}

	public CustomLazyDataModel<Account> getVendorCustomLazyDataModel() {
		return celebrityCustomLazyDataModel;
	}

	public void setVendorCustomLazyDataModel(
			CustomLazyDataModel<Account> celebrityCustomLazyDataModel) {
		this.celebrityCustomLazyDataModel = celebrityCustomLazyDataModel;
	}

	public CountryShipment getSelectedCountryShipment() {
		return selectedCountryShipment;
	}

	public void setSelectedCountryShipment(CountryShipment selectedCountryShipment) {
		this.selectedCountryShipment = selectedCountryShipment;
	}

	public void setCountryService(CountryService countryService) {
		this.countryService = countryService;
	}

	public CountryService getCountryService() {
		return countryService;
	}

	public List<CountryShipment> getCountryShipmentList() {
		return countryShipmentList;
	}

	public void setCountryShipmentList(List<CountryShipment> countryShipmentList) {
		this.countryShipmentList = countryShipmentList;
	}

	public void setShipmentTypeService(ShipmentTypeService shipmentTypeService) {
		this.shipmentTypeService = shipmentTypeService;
	}

	public List<Account> getAllVendors() {
		return allVendors;
	}

	public void setAllVendors(List<Account> allVendors) {
		this.allVendors = allVendors;
	}

	public boolean getChangePassword() {
		return changePassword;
	}

	public void setChangePassword(boolean changePassword) {
		this.changePassword = changePassword;
	}
}
