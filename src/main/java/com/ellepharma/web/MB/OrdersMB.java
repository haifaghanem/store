package com.ellepharma.web.MB;

import com.ellepharma.data.model.CustomLazyDataModel;
import com.ellepharma.data.model.Order;
import com.ellepharma.service.OrderService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 * OrdersMB.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since July 11, 2019
 */
@ManagedBean
@ViewScoped
public class OrdersMB extends BaseMB {

    @ManagedProperty("#{orderService}")
    private OrderService orderService;

    private Order selectedOrder;

    private CustomLazyDataModel<Order> orderCustomLazyDataModel;

    @PostConstruct
    public void init() {
        selectedOrder = new Order();
        orderCustomLazyDataModel = new CustomLazyDataModel<>(orderService);

    }


    public void checkAllPayemntToday(){
        orderService.checkAllPaymentToday();
        addSuccessMessage("msg.allOrderIsUpdates");
    }



    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public Order getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(Order selectedOrder) {
        this.selectedOrder = selectedOrder;
    }

    public CustomLazyDataModel<Order> getOrderCustomLazyDataModel() {
        return orderCustomLazyDataModel;
    }

    public void setOrderCustomLazyDataModel(CustomLazyDataModel<Order> orderCustomLazyDataModel) {
        this.orderCustomLazyDataModel = orderCustomLazyDataModel;
    }


}
