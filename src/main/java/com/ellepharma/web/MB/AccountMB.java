package com.ellepharma.web.MB;

import com.ellepharma.data.model.*;
import com.ellepharma.service.*;
import com.ellepharma.utils.*;
import org.apache.commons.collections4.CollectionUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.extensions.model.inputphone.Country;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.mail.MessagingException;
import java.util.List;
import java.util.Objects;

/**
 * AccountMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Jul 8, 2019
 */
@ManagedBean
@ViewScoped
public class AccountMB extends BaseMB {

    @ManagedProperty("#{shipmentTypeService}")
    private ShipmentTypeService shipmentTypeService;

    @ManagedProperty("#{countryService}")
    private CountryService countryService;


    @ManagedProperty("#{accountService}")
    private AccountService accountService;

    @ManagedProperty("#{orderService}")
    private OrderService orderService;

    @ManagedProperty("#{loginBean}")
    private LoginMB loginMB;


    private List<CountryShipment> countryShipmentList;

    private CountryShipment selectedCountryShipment;

    private List<String> cities;
    private ShipmentType defaultShipmentType;

    private List<Order> orderList;

    private Account account;
    private Account accountToChangePass;
    private double amountShipment;

    private String temp;

    private Order selectedOrder;

    private String email;

    private String cuPass;
    private String pass;
    private String confPass;

    private Account orderAccountDetails;

    @PostConstruct
    public void init() {

        defaultShipmentType = shipmentTypeService.findByStatusTrue();

        if (defaultShipmentType != null) {
            countryShipmentList = defaultShipmentType.getCountryShipmentList();
        }

        if (loginMB.isLoggedIn()) {


            account = SecurityUtils.getCurrentAccount();
            orderList = orderService.findByAccount(account);
            prepareAccount();

        } else {
            account = new Account();

            selectedCountryShipment = JsfUtils.getFromSessionMap(Constants.CURRENT_COUNTRY_SHIPMENT);

            if (selectedCountryShipment != null) {
                amountShipment = selectedCountryShipment.getShipmentRate();
                account.setCountry(selectedCountryShipment.getCountry());
                account.setPhoneCode(account.getCountry().getIsoCode2());
            }

        }

    }

    public void signUp() {
        if (!loginMB.isLoggedIn()) {
            if (checkUser()) {
                addErrorMessage("common.emailExists");
                return;
            } else {
                account.setCountry(selectedCountryShipment.getCountry());
                account.setPhoneCode(account.getCountry().getIsoCode2());
                account.setAccountType(AccountType.CUSTOMER);
                String pass = account.getPassword();
                account = accountService.save(account);
                loginMB.setUsername(account.getEmail());
                loginMB.setPassword(pass);
                loginMB.login(false);
            }

        } else {
            if (checkUser()) {
                addErrorMessage("common.emailExists");
                return;
            } else {
                account.setCountry(selectedCountryShipment.getCountry());
                account.setPhoneCode(account.getCountry().getIsoCode2());
                account = accountService.update(account);
            }

        }
        addSuccessMessage();

    }

    public void viewAccountDetails(Long id) {
        orderAccountDetails = accountService.findById(id);

        JsfUtils.openDialog("dlgAccountDetails");
        JsfUtils.update("headerdlgAccountDetails","dlgAccountDetailsFrm");
    }

    public void resetPassword() {

        Account account = accountService.findByEmail(email);

        if (Objects.nonNull(account)) {
            String token = accountService.generateToken(account);

            String url = Utils.getFullUrlApp() + "/change-password.xhtml?token=" + token;
            String emailTemplate =GlobalConfigProperties.getInstance().getProperty("resetEmailTemplate");
            try {
                MailUtil.sendEmail(email, "change password", emailTemplate.replaceAll("resetLink", url));
                addSuccessMessage("common.theLinkRestSend");

            } catch (MessagingException e) {
                e.printStackTrace();
                addErrorMessage("common.noAccount.linkWithMail");

            }

        } else {
            addErrorMessage("common.noAccount.linkWithMail");
        }

    }

    public void prepareAccount() {
        if (loginMB.isLoggedIn()) {
            account = SecurityUtils.getCurrentAccount();
            if (account != null && CollectionUtils.isNotEmpty(countryShipmentList)) {
                countryShipmentList.stream().forEach(countryShipment -> {
                    if (countryShipment.getCountry().getId().equals(account.getCountry().getId())) {
                        selectedCountryShipment = countryShipment;

                        JsfUtils.putToSessionMap(Constants.CURRENT_COUNTRY_SHIPMENT, countryShipment);
                        JsfUtils.putToSessionMap(Constants.CURRENT_COUNTRY, countryShipment.getCountry());
                        JsfUtils.putToSessionMap(Constants.CURRENT_CURRENCY, countryShipment.getCountry().getCurrency());


                    }
                });
            }

        }

        if (account == null)
            account = new Account();
    }

    public String getTemp() {

        if (selectedCountryShipment != null)
            return selectedCountryShipment.getCurrentLanguage();
        return temp;
    }


    public void prepareToChangePassword(String token) {

        try {

            if (!accountService.getJwtTokenUtil().isTokenExpired(token)) {

                String i = accountService.getJwtTokenUtil().getUsernameFromToken(token);
                accountToChangePass = accountService.findByEmail(i);
            } else {
                //  addErrorMessage("theLinkIsExpired");

            }
        } catch (Exception e) {
            //addErrorMessage("theLinkIsExpired");
        }


    }

    public void forceChangePassword() {

        pass = accountService.passwordEncoder().encode(pass);
        accountToChangePass.setPassword(pass);
        accountService.update(accountToChangePass);
        accountService = null;
        addSuccessMessage();
        Utils.redirect(Utils.getFullUrlApp());


    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public boolean checkUser() {

        return accountService.existsAccountByEmail(account.getEmail(), account.getId() != null ? account.getId() : -1);
    }

    public void onCountrySelect(SelectEvent event) {
        Country country = (Country) event.getObject();
        account.setPhoneCode(country.getIso2());
        account.setPhone(null);

    }

    public void showProductItem(Order selectedOrder) {
        this.selectedOrder = selectedOrder;
        showDialog("dlgProductItem");
        updates("ProductItemFrm", "headerOrder");
    }

    public List<String> getCities() {
        return countryService.cities(selectedCountryShipment.getCountry().getIsoCode2());
    }

    public void changeCountry(ValueChangeEvent changeEvent) {

        getAccount().setCity(null);
        JsfUtils.executeScript("$('select').selectize({sortField: 'text'});   ");

    }

    public void changePassword() {
        Account curAccount = SecurityUtils.getCurrentUser();
        boolean corr = accountService.passwordEncoder().matches(cuPass, curAccount.getPassword());
        if (!corr) {
            addErrorMessage("common.currentPassword.incorrect");
        }
        pass = accountService.passwordEncoder().encode(pass);
        curAccount.setPassword(pass);
        accountService.update(curAccount);
        addSuccessMessage();


    }

    public ShipmentTypeService getShipmentTypeService() {
        return shipmentTypeService;
    }

    public void setShipmentTypeService(ShipmentTypeService shipmentTypeService) {
        this.shipmentTypeService = shipmentTypeService;
    }

    public AccountService getAccountService() {
        return accountService;
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public LoginMB getLoginMB() {
        return loginMB;
    }

    public void setLoginMB(LoginMB loginMB) {
        this.loginMB = loginMB;
    }

    public List<CountryShipment> getCountryShipmentList() {
        return countryShipmentList;
    }

    public void setCountryShipmentList(List<CountryShipment> countryShipmentList) {
        this.countryShipmentList = countryShipmentList;
    }

    public CountryShipment getSelectedCountryShipment() {
        return selectedCountryShipment;
    }

    public void setSelectedCountryShipment(CountryShipment selectedCountryShipment) {
        this.selectedCountryShipment = selectedCountryShipment;
    }

    public ShipmentType getDefaultShipmentType() {
        return defaultShipmentType;
    }

    public void setDefaultShipmentType(ShipmentType defaultShipmentType) {
        this.defaultShipmentType = defaultShipmentType;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public Order getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(Order selectedOrder) {
        this.selectedOrder = selectedOrder;
    }

    public double getAmountShipment() {
        return amountShipment;
    }

    public void setAmountShipment(double amountShipment) {
        this.amountShipment = amountShipment;
    }


    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCuPass() {
        return cuPass;
    }

    public void setCuPass(String cuPass) {
        this.cuPass = cuPass;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getConfPass() {
        return confPass;
    }

    public void setConfPass(String confPass) {
        this.confPass = confPass;
    }


    public Account getAccountToChangePass() {
        return accountToChangePass;
    }

    public void setAccountToChangePass(Account accountToChangePass) {
        this.accountToChangePass = accountToChangePass;
    }


    public Account getOrderAccountDetails() {
        return orderAccountDetails;
    }

    public void setOrderAccountDetails(Account orderAccountDetails) {
        this.orderAccountDetails = orderAccountDetails;
    }
}
