package com.ellepharma.web.MB;

import com.ellepharma.data.model.Item;
import com.ellepharma.service.ItemService;
import com.ellepharma.service.StockInService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * ProductDetailsMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 08, 2019
 */
@ManagedBean
@ViewScoped
public class ProductDetailsMB extends BaseMB {

    @ManagedProperty("#{itemService}")
    private ItemService itemService;

    @ManagedProperty("#{stockInService}")
    private StockInService stockInService;

    private Item selectedProduct;

    private List<Item> relatedProduct;

    private Long qty;

    @PostConstruct
    public void init() {

    }

    public void init(Long id) {
        if (id != null) {
            selectedProduct = itemService.findById(id);
            relatedProduct = itemService.findRelatedItems(selectedProduct);
            qty = getAvailableQty(id);

        }

    }

    public long getAvailableQty(Long id) {
        return stockInService.getAvailableQuantityByItemID(id);
    }

    public Item getSelectedProduct() {
        return selectedProduct;
    }

    public List<Item> getRelatedProduct() {
        return relatedProduct;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public StockInService getStockInService() {
        return stockInService;
    }

    public void setStockInService(StockInService stockInService) {
        this.stockInService = stockInService;
    }

    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }
}
