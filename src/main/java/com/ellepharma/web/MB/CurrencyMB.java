package com.ellepharma.web.MB;

import com.ellepharma.data.model.Currency;
import com.ellepharma.service.CurrencyService;
import com.ellepharma.utils.Constants;
import com.ellepharma.utils.JsfUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.List;

/**
 * CountryMB.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 10, 2019
 */
@ManagedBean
@SessionScoped
public class CurrencyMB extends BaseMB {


    @ManagedProperty("#{currencyService}")
    private CurrencyService currencyService;

    private List<Currency> currencyList;

    @PostConstruct
    public void init() {
        currencyList = currencyService.findAllByStatusIsTrue();
    }


    public void changeCurrency(Currency currency) {
        JsfUtils.putToSessionMap(Constants.CURRENT_CURRENCY, currency);
    }

    public CurrencyService getCurrencyService() {
        return currencyService;
    }

    public void setCurrencyService(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    public List<Currency> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<Currency> currencyList) {
        this.currencyList = currencyList;
    }
}

