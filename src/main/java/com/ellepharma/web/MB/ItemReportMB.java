package com.ellepharma.web.MB;

import com.ellepharma.data.dto.ItemReportFilterDTO;
import com.ellepharma.data.dto.ItemReportResultDTO;
import com.ellepharma.data.model.Category;
import com.ellepharma.data.model.Manufacturer;
import com.ellepharma.service.*;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * ItemReportMB.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since August 16, 2019
 */
@ManagedBean
@ViewScoped
public class ItemReportMB extends BaseMB {

    @ManagedProperty("#{itemReportService}")
    private ItemReportService itemReportService;

    @ManagedProperty("#{categoryService}")
    private CategoryService categoryService;

    @ManagedProperty("#{manufacturerService}")
    private ManufacturerService manufacturerService;

    @ManagedProperty("#{stockInService}")
    private StockInService stockInService;

    @ManagedProperty("#{orderProductService}")
    private OrderProductService orderProductService;

    private ItemReportFilterDTO itemReportFilterDTO;

    private List<ItemReportResultDTO> itemReportResultDTOSList;

    private List<Category> categoryList;
    private List<Manufacturer> brandList;

    @PostConstruct
    public void init() {
        itemReportFilterDTO = new ItemReportFilterDTO();
        itemReportResultDTOSList = new ArrayList<>();
        categoryList = categoryService.findAllActive();
        brandList = manufacturerService.findAll();
        findItem();
    }

    public void findItem() {

        itemReportResultDTOSList = itemReportService.findAllReportItem(itemReportFilterDTO);
    }

    public ItemReportService getItemReportService() {
        return itemReportService;
    }

    public void setItemReportService(ItemReportService itemReportService) {
        this.itemReportService = itemReportService;
    }

    public ItemReportFilterDTO getItemReportFilterDTO() {
        return itemReportFilterDTO;
    }

    public void setItemReportFilterDTO(ItemReportFilterDTO itemReportFilterDTO) {
        this.itemReportFilterDTO = itemReportFilterDTO;
    }

    public List<ItemReportResultDTO> getItemReportResultDTOSList() {
        return itemReportResultDTOSList;
    }

    public void setItemReportResultDTOSList(List<ItemReportResultDTO> itemReportResultDTOSList) {
        this.itemReportResultDTOSList = itemReportResultDTOSList;
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public ManufacturerService getManufacturerService() {
        return manufacturerService;
    }

    public void setManufacturerService(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public List<Manufacturer> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<Manufacturer> brandList) {
        this.brandList = brandList;
    }

    public StockInService getStockInService() {
        return stockInService;
    }

    public void setStockInService(StockInService stockInService) {
        this.stockInService = stockInService;
    }

    public OrderProductService getOrderProductService() {
        return orderProductService;
    }

    public void setOrderProductService(OrderProductService orderProductService) {
        this.orderProductService = orderProductService;
    }
}
