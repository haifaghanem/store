package com.ellepharma.web.MB;

import com.ellepharma.data.model.Newsletter;
import com.ellepharma.service.NewsletterService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 * ArticleMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Oct 03, 2019
 */
@ManagedBean
@ViewScoped
public class NewsletterMB extends BaseMB {

    @ManagedProperty("#{NewsletterService}")
    private NewsletterService newsletterService;

    private Newsletter newsletter = new Newsletter();


    /**
     *
     */
    public void subscribe() {
        newsletter.setStatus(true);
        newsletterService.save(newsletter);
        addSuccessMessage("common.Successful.subscribe");

    }

    public NewsletterService getNewsletterService() {
        return newsletterService;
    }

    public void setNewsletterService(NewsletterService newsletterService) {
        this.newsletterService = newsletterService;
    }

    public Newsletter getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(Newsletter newsletter) {
        this.newsletter = newsletter;
    }
}
