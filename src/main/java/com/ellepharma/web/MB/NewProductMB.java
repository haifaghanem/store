package com.ellepharma.web.MB;

import com.ellepharma.data.model.Item;
import com.ellepharma.service.ItemService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * ProductMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 01, 2019
 */
@ManagedBean
@ViewScoped
public class NewProductMB extends BaseMB {

    @ManagedProperty("#{itemService}")
    private ItemService itemService;
    private List<Item> itemList;
    private List<Item> selectedItemList;


    @PostConstruct
    public void init() {
        itemList = itemService.findAllActive();
        selectedItemList = itemService.findAllByNewItemTrue();
    }

    /**
     *
     */
    public void save() {
        itemService.saveNewProduct(selectedItemList);
        addSuccessMessage();
    }

    public void removeAll() {
        selectedItemList = new ArrayList<>();
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public List<Item> getSelectedItemList() {
        return selectedItemList;
    }

    public void setSelectedItemList(List<Item> selectedItemList) {
        this.selectedItemList = selectedItemList;
    }


}
