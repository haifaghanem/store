package com.ellepharma.web.MB;

import com.ellepharma.data.model.Country;
import com.ellepharma.data.model.CountryShipment;
import com.ellepharma.data.model.Currency;
import com.ellepharma.data.model.ShipmentType;
import com.ellepharma.service.CountryService;
import com.ellepharma.service.CountryShipmentService;
import com.ellepharma.service.CurrencyService;
import com.ellepharma.service.ShipmentTypeService;
import com.ellepharma.utils.Constants;
import com.ellepharma.utils.JsfUtils;
import com.ellepharma.utils.Utils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.List;
import java.util.stream.Collectors;

/**
 * CountryDetectMB.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 11, 2019
 */
@ManagedBean
@SessionScoped
public class CountryDetectMB extends BaseMB {

    private Country currentCountry;

    @ManagedProperty("#{countryService}")
    private CountryService countryService;

    @ManagedProperty("#{currencyService}")
    private CurrencyService currencyService;

    @ManagedProperty("#{countryShipmentService}")
    private CountryShipmentService countryShipmentService;

    @ManagedProperty("#{shipmentTypeService}")
    private ShipmentTypeService shipmentTypeService;
    @ManagedProperty("#{guestPreferences}")
    private GuestPreferences guestPreferences;

    private Country tempCountry;

    private List<Country> cList;

    @PostConstruct
    public void init() {
        String ipAddress = Utils.getUserIp();
        //ipAddress = "46.166.142.220";
        tempCountry = getCountry(ipAddress);
        ShipmentType shipmentType = shipmentTypeService.findByStatusTrue();
        if (shipmentType != null) {
            cList = shipmentType.getCountryShipmentList().stream().map(CountryShipment::getCountry).collect(Collectors.toList());

            if (cList.contains(tempCountry)) {
                prepareCCS(tempCountry);
            }
        } else {
            Country defCountry = countryService.findByIsoCode3("SAU");
            prepareCCS(defCountry);


        }


    }

    public void prepareCCS() {

        //System.out.println("3456789");
    }

    public void prepareCCS(Country currentCountry) {
        this.currentCountry = currentCountry;
        Country defCountry = countryService.findByIsoCode3("SAU");

        if (currentCountry != null) {
            CountryShipment countryShipment = countryShipmentService.findCountryShipmentByCountry_Id(currentCountry.getId());
            selectCountry(currentCountry);
            selectDefaultCurrency(currentCountry.getCurrency());
            selectDefaultShip(countryShipment);
        } else {
            selectDefaultCurrency(defCountry.getCurrency());
        }
    }

    public void selectCountry(Country currentCountry) {
        JsfUtils.putToSessionMap(Constants.CURRENT_COUNTRY, currentCountry);
    }

    public void selectDefaultCurrency(Currency currentCurrency) {
        if (currentCurrency == null)
            currentCurrency = currencyService.findByCurrencyCode("USD");
        JsfUtils.putToSessionMap(Constants.CURRENT_CURRENCY, currentCurrency);
    }

    public void selectDefaultShip(CountryShipment currentCountryShipment) {
        JsfUtils.putToSessionMap(Constants.CURRENT_COUNTRY_SHIPMENT, currentCountryShipment);
    }

    public Country getCountry(String ipAddress) {
        return countryService.findByIP(ipAddress);

    }

    public Country getCurrentCountry() {
        return currentCountry;
    }

    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }

    public void setCountryShipmentService(CountryShipmentService countryShipmentService) {
        this.countryShipmentService = countryShipmentService;
    }

    public void setShipmentTypeService(ShipmentTypeService shipmentTypeService) {
        this.shipmentTypeService = shipmentTypeService;
    }

    public List<Country> getcList() {
        return cList;
    }

    public void setcList(List<Country> cList) {
        this.cList = cList;
    }

    public Country getTempCountry() {
        return tempCountry;
    }

    public void setTempCountry(Country tempCountry) {
        this.tempCountry = tempCountry;
    }

    public void setCurrencyService(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }


    public GuestPreferences getGuestPreferences() {
        return guestPreferences;
    }

    public void setGuestPreferences(GuestPreferences guestPreferences) {
        this.guestPreferences = guestPreferences;
    }
}
