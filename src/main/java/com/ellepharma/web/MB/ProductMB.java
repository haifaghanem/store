package com.ellepharma.web.MB;

import com.ellepharma.data.model.*;
import com.ellepharma.service.CategoryService;
import com.ellepharma.service.ImageService;
import com.ellepharma.service.ItemService;
import com.ellepharma.service.ManufacturerService;
import com.ellepharma.utils.ExceptionCodes;
import com.ellepharma.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.omnifaces.util.Components;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ProductMB.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 01, 2019
 */
@ManagedBean
@ViewScoped
public class ProductMB extends BaseMB {

	@ManagedProperty("#{itemService}")
	private ItemService itemService;

	@ManagedProperty("#{categoryService}")
	private CategoryService categoryService;

	@ManagedProperty("#{imageService}")
	private ImageService imageService;

	@ManagedProperty("#{manufacturerService}")
	private ManufacturerService manufacturerService;

	private Item selectedItem;

	private List<Category> categoryList;

	private List<Manufacturer> manufacturerList;

	private String barcode;

	private CustomLazyDataModel<Item> itemCustomLazyDataModel;

	@PostConstruct
	public void init() {
		selectedItem = new Item();
		categoryList = categoryService.findAllActive();
		manufacturerList = manufacturerService.findAll();
		itemCustomLazyDataModel = new CustomLazyDataModel<>(itemService);
	}

	/**
	 *
	 */
	public void addBarcode() {

		if (StringUtils.isEmpty(barcode)) {
			addErrorMessage("msg.barcode.req", "txtBrand");

		} else {
			selectedItem.getBarcodeList().add(barcode);
			selectedItem.setBarcodeList(selectedItem.getBarcodeList().stream().distinct().collect(Collectors.toList()));
			barcode = null;
		}
	}

	public void filterByBarcode() {

		String vak = Components.getAttribute(Components.getCurrentComponent(), "value");
		DataTable dataTable = Components.findComponent("main_from:itemDtl");

		itemCustomLazyDataModel = new CustomLazyDataModel<>(itemService, "findAllByBarcodeIn", Arrays.asList(vak, vak));

	}

	public String addNewItem() {
		selectedItem = new Item();
		selectedItem.setCategories(new ArrayList<>());
		selectedItem.setImage(new ArrayList<>());
		selectedItem.setBarcodeList(new ArrayList<>());
		selectedItem.setAttributeProduct(new ArrayList<>());
		selectedItem.setDateAdded(new Date());

		setEditMode(false);
		showDialog("dlgItem");
		updates("itemFrm");
		return null;

	}

	/**
	 * @param item
	 * @return
	 */
	public String editItem(Item item) {
		//selectedItem = item;
		barcode = "";
		selectedItem = itemService.findById(item.getId());

		setEditMode(true);
		showDialog("dlgItem");
		updates("itemFrm", "itemDtl");
		return null;
	}

	/**
	 * @param item
	 */
	public void deleteItem(Item item) {
		try {
			itemService.delete(item);

		} catch (DataIntegrityViolationException | ConstraintViolationException | PersistenceException ex) {
			addErrorMessage(ExceptionCodes.BR_COM_validateIsRecordUsed.getCode());
			return;
		}
		updates("itemDtl");
		addSuccessMessage();

	}

	/**
	 *
	 */
	public boolean saveItem() {
		try {
			selectedItem = itemService.save(selectedItem);
			selectedItem = new Item();
		} catch (DataIntegrityViolationException ex) {
			addErrorMessage(ex);
			return false;
		}
		updates("itemDtl", "itemFrm");
		addSuccessMessage();
		return true;
	}

	public void upload(FileUploadEvent event) throws Exception {
		String name = Utils.uploadWithFileName(event);

		Image image = new Image();
		image.setName(name);
		image.setPriorityOrder(1L);
		//image.setPic(event.getFile().getContents());

		image = imageService.save(image);
		selectedItem.getImage().add(image);
	}

	public void saveItemAndHide() {
		if (saveItem()) {
			hideDialog("dlgItem");

		}

	}

	public ItemService getItemService() {
		return itemService;
	}

	public void setItemService(ItemService itemService) {
		this.itemService = itemService;
	}

	public Item getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(Item selectedItem) {
		this.selectedItem = selectedItem;
	}

	public CustomLazyDataModel<Item> getItemCustomLazyDataModel() {
		return itemCustomLazyDataModel;
	}

	public void setItemCustomLazyDataModel(CustomLazyDataModel<Item> itemCustomLazyDataModel) {
		this.itemCustomLazyDataModel = itemCustomLazyDataModel;
	}

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

	public CategoryService getCategoryService() {
		return categoryService;
	}

	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	public List<Manufacturer> getManufacturerList() {
		return manufacturerList;
	}

	public void setManufacturerList(List<Manufacturer> manufacturerList) {
		this.manufacturerList = manufacturerList;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public ManufacturerService getManufacturerService() {
		return manufacturerService;
	}

	public void setManufacturerService(ManufacturerService manufacturerService) {
		this.manufacturerService = manufacturerService;
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

}
