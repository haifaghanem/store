package com.ellepharma.web.MB;

import com.ellepharma.data.model.Image;
import com.ellepharma.service.AdsService;
import com.ellepharma.service.GlobalConfigProperties;
import com.ellepharma.service.ImageService;
import com.ellepharma.utils.Utils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultStreamedContent;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;

@ManagedBean
@SessionScoped
public class ImageMb {

    @ManagedProperty("#{imageService}")
    private ImageService imageService;

    @ManagedProperty("#{adsService}")
    private AdsService adsService;

    private String imagesUrl;

    @PostConstruct
    public void init() {

        imagesUrl =GlobalConfigProperties.getInstance().getProperty("imageUrl");
    }

    public String getImageUrl(String name) {

        String url = "";
        if (StringUtils.isNotEmpty(name)) {
            url = imagesUrl.concat(name).concat("?v="+name);
        }
       /* String url = "";
        try {
            url = Utils.getContentService().getContentUrl("store", name);
        } catch (Exception e) {

            e.printStackTrace();
        }*/
        return url;
    }
    public String getImageUrl(Image image) {

        String url = "";
        if (image!=null) {
            url = imagesUrl.concat(image.getName()).concat("?v=").concat(String.valueOf(image.getVersionId()));
        }
       /* String url = "";
        try {
            url = Utils.getContentService().getContentUrl("store", name);
        } catch (Exception e) {

            e.printStackTrace();
        }*/
        return url;
    }


    public void trans() {
       /* List<Image> i = imageService.findAll();
        List<Long> ids = new ArrayList<>();

       *//* Image image = imageService.findById(11L);
        try {
            InputStream targetStream = new ByteArrayInputStream(image.getPic());
            Utils.getContentService().putContent("store", image.getName(), targetStream, image.getPic().length, "image/png");
        } catch (Exception e) {
            e.printStackTrace();
        }*//*

        try {
            Utils.getContentService().removeStorageUnit("store");
            Utils.getContentService().createStorageUnit("store");
        } catch (Exception e) {

        }

        i.forEach(image -> {
            if (image.getPic() != null) {
                InputStream targetStream = new ByteArrayInputStream(image.getPic());
                String imageName = image.getName();
                if (StringUtils.isEmpty(imageName)) {
                    imageName = System.currentTimeMillis() + ".jpg";
                    image.setName(imageName);
                    imageService.save(image);
                }
                try {
                    Utils.getContentService().createStorageUnit("store");
                    Utils.getContentService().putContent("store", imageName, targetStream, image.getPic().length, "image/png");
                } catch (Exception e) {
                    ids.add(image.getId());
                    e.printStackTrace();
                }
            }
        });

        System.out.println(ids);*/
    }

    public DefaultStreamedContent getImageStream() {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        } else {
            // So, browser is requesting the image. Return a real StreamedContent with the image bytes.
            String id = context.getExternalContext().getRequestParameterMap().get("ids");
            String code = context.getExternalContext().getRequestParameterMap().get("code");
            String url = context.getExternalContext().getRequestParameterMap().get("url");
            Image image = null;
            if (id == null && code != null) {
                image = adsService.findByCode(code).getImage();
            } else if (id != null) {
                image = imageService.findById(Long.parseLong(id));
            }
            InputStream is = null;

            //String type = FilenameUtils.getExtension(image.getName());
            try {
                if (url != null) {
                    is = new URL(url).openStream();
                    byte[] imageBytes = IOUtils.toByteArray(is);
                    return new DefaultStreamedContent(new ByteArrayInputStream(imageBytes), "image/png");

                }

                if (image.getName() != null) {
                    return new DefaultStreamedContent(Utils.getContentService().getContent("store", image.getName()), "image/png");
                }
                return new DefaultStreamedContent();

            } catch (Exception e) {
                return new DefaultStreamedContent();

            }
        }
    }

    public void setImageService(ImageService imageService) {
        this.imageService = imageService;
    }

    public void setAdsService(AdsService adsService) {
        this.adsService = adsService;
    }
}
