
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Files" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._HttpPost" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "files"
})
@XmlRootElement(name = "PostFiles", namespace = "http://tempuri.org/")
public class PostFiles {

    @XmlElement(name = "Files", namespace = "http://tempuri.org/")
    protected ARXEDIHttpPost files;

    /**
     * Gets the value of the files property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIHttpPost }
     *     
     */
    public ARXEDIHttpPost getFiles() {
        return files;
    }

    /**
     * Sets the value of the files property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIHttpPost }
     *     
     */
    public void setFiles(ARXEDIHttpPost value) {
        this.files = value;
    }

}
