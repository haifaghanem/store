
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ARX_EDI._FileTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ARX_EDI._FileTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SKU"/>
 *     &lt;enumeration value="SKU_IMG"/>
 *     &lt;enumeration value="ASN"/>
 *     &lt;enumeration value="SO"/>
 *     &lt;enumeration value="ASN_SA"/>
 *     &lt;enumeration value="SO_SA"/>
 *     &lt;enumeration value="ASN_KW"/>
 *     &lt;enumeration value="SO_KW"/>
 *     &lt;enumeration value="ASN_BH"/>
 *     &lt;enumeration value="SO_BH"/>
 *     &lt;enumeration value="ASN_OM"/>
 *     &lt;enumeration value="SO_OM"/>
 *     &lt;enumeration value="ASN_SA2"/>
 *     &lt;enumeration value="SO_SA2"/>
 *     &lt;enumeration value="SO_PDF"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ARX_EDI._FileTypeEnum")
@XmlEnum
public enum ARXEDIFileTypeEnum {

    SKU("SKU"),
    SKU_IMG("SKU_IMG"),
    ASN("ASN"),
    SO("SO"),
    ASN_SA("ASN_SA"),
    SO_SA("SO_SA"),
    ASN_KW("ASN_KW"),
    SO_KW("SO_KW"),
    ASN_BH("ASN_BH"),
    SO_BH("SO_BH"),
    ASN_OM("ASN_OM"),
    SO_OM("SO_OM"),
    @XmlEnumValue("ASN_SA2")
    ASN_SA_2("ASN_SA2"),
    @XmlEnumValue("SO_SA2")
    SO_SA_2("SO_SA2"),
    SO_PDF("SO_PDF");
    private final String value;

    ARXEDIFileTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ARXEDIFileTypeEnum fromValue(String v) {
        for (ARXEDIFileTypeEnum c: ARXEDIFileTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
