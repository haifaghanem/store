
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ARX_EDI._Console_Inquiry_LineDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ARX_EDI._Console_Inquiry_LineDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AWB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActualShipDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllocateDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClinetSystemRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EditDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExternalOrderKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Forwarder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsUpdateAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LatestWHStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PickDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SOTDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aramexCODCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aramexCODValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aramexService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ARX_EDI._Console_Inquiry_LineDetail", propOrder = {
    "awb",
    "actualShipDate",
    "addDate",
    "allocateDate",
    "clinetSystemRef",
    "editDate",
    "externalOrderKey",
    "forwarder",
    "isUpdateAllowed",
    "latestWHStatus",
    "orderKey",
    "pickDate",
    "sotDate",
    "aramexCODCurrency",
    "aramexCODValue",
    "aramexService"
})
public class ARXEDIConsoleInquiryLineDetail {

    @XmlElementRef(name = "AWB", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> awb;
    @XmlElementRef(name = "ActualShipDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> actualShipDate;
    @XmlElementRef(name = "AddDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> addDate;
    @XmlElementRef(name = "AllocateDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> allocateDate;
    @XmlElementRef(name = "ClinetSystemRef", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> clinetSystemRef;
    @XmlElementRef(name = "EditDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> editDate;
    @XmlElementRef(name = "ExternalOrderKey", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalOrderKey;
    @XmlElementRef(name = "Forwarder", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forwarder;
    @XmlElement(name = "IsUpdateAllowed")
    protected Boolean isUpdateAllowed;
    @XmlElementRef(name = "LatestWHStatus", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> latestWHStatus;
    @XmlElementRef(name = "OrderKey", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderKey;
    @XmlElementRef(name = "PickDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pickDate;
    @XmlElementRef(name = "SOTDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sotDate;
    @XmlElementRef(name = "aramexCODCurrency", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> aramexCODCurrency;
    @XmlElementRef(name = "aramexCODValue", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> aramexCODValue;
    @XmlElementRef(name = "aramexService", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> aramexService;

    /**
     * Gets the value of the awb property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAWB() {
        return awb;
    }

    /**
     * Sets the value of the awb property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAWB(JAXBElement<String> value) {
        this.awb = value;
    }

    /**
     * Gets the value of the actualShipDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActualShipDate() {
        return actualShipDate;
    }

    /**
     * Sets the value of the actualShipDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActualShipDate(JAXBElement<String> value) {
        this.actualShipDate = value;
    }

    /**
     * Gets the value of the addDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAddDate() {
        return addDate;
    }

    /**
     * Sets the value of the addDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAddDate(JAXBElement<String> value) {
        this.addDate = value;
    }

    /**
     * Gets the value of the allocateDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAllocateDate() {
        return allocateDate;
    }

    /**
     * Sets the value of the allocateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAllocateDate(JAXBElement<String> value) {
        this.allocateDate = value;
    }

    /**
     * Gets the value of the clinetSystemRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClinetSystemRef() {
        return clinetSystemRef;
    }

    /**
     * Sets the value of the clinetSystemRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClinetSystemRef(JAXBElement<String> value) {
        this.clinetSystemRef = value;
    }

    /**
     * Gets the value of the editDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEditDate() {
        return editDate;
    }

    /**
     * Sets the value of the editDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEditDate(JAXBElement<String> value) {
        this.editDate = value;
    }

    /**
     * Gets the value of the externalOrderKey property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalOrderKey() {
        return externalOrderKey;
    }

    /**
     * Sets the value of the externalOrderKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalOrderKey(JAXBElement<String> value) {
        this.externalOrderKey = value;
    }

    /**
     * Gets the value of the forwarder property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForwarder() {
        return forwarder;
    }

    /**
     * Sets the value of the forwarder property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForwarder(JAXBElement<String> value) {
        this.forwarder = value;
    }

    /**
     * Gets the value of the isUpdateAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsUpdateAllowed() {
        return isUpdateAllowed;
    }

    /**
     * Sets the value of the isUpdateAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsUpdateAllowed(Boolean value) {
        this.isUpdateAllowed = value;
    }

    /**
     * Gets the value of the latestWHStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLatestWHStatus() {
        return latestWHStatus;
    }

    /**
     * Sets the value of the latestWHStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLatestWHStatus(JAXBElement<String> value) {
        this.latestWHStatus = value;
    }

    /**
     * Gets the value of the orderKey property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderKey() {
        return orderKey;
    }

    /**
     * Sets the value of the orderKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderKey(JAXBElement<String> value) {
        this.orderKey = value;
    }

    /**
     * Gets the value of the pickDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPickDate() {
        return pickDate;
    }

    /**
     * Sets the value of the pickDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPickDate(JAXBElement<String> value) {
        this.pickDate = value;
    }

    /**
     * Gets the value of the sotDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSOTDate() {
        return sotDate;
    }

    /**
     * Sets the value of the sotDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSOTDate(JAXBElement<String> value) {
        this.sotDate = value;
    }

    /**
     * Gets the value of the aramexCODCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAramexCODCurrency() {
        return aramexCODCurrency;
    }

    /**
     * Sets the value of the aramexCODCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAramexCODCurrency(JAXBElement<String> value) {
        this.aramexCODCurrency = value;
    }

    /**
     * Gets the value of the aramexCODValue property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAramexCODValue() {
        return aramexCODValue;
    }

    /**
     * Sets the value of the aramexCODValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAramexCODValue(JAXBElement<String> value) {
        this.aramexCODValue = value;
    }

    /**
     * Gets the value of the aramexService property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAramexService() {
        return aramexService;
    }

    /**
     * Sets the value of the aramexService property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAramexService(JAXBElement<String> value) {
        this.aramexService = value;
    }

}
