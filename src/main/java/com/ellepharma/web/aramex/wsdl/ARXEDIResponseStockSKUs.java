
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ARX_EDI._Response_Stock_SKUs complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ARX_EDI._Response_Stock_SKUs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Lines" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ArrayOfARX_EDI._Stock_SKUsLine" minOccurs="0"/>
 *         &lt;element name="Response" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._Response" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ARX_EDI._Response_Stock_SKUs", propOrder = {
    "lines",
    "response"
})
public class ARXEDIResponseStockSKUs {

    @XmlElementRef(name = "Lines", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfARXEDIStockSKUsLine> lines;
    @XmlElement(name = "Response")
    protected ARXEDIResponse response;

    /**
     * Gets the value of the lines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfARXEDIStockSKUsLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfARXEDIStockSKUsLine> getLines() {
        return lines;
    }

    /**
     * Sets the value of the lines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfARXEDIStockSKUsLine }{@code >}
     *     
     */
    public void setLines(JAXBElement<ArrayOfARXEDIStockSKUsLine> value) {
        this.lines = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIResponse }
     *     
     */
    public ARXEDIResponse getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIResponse }
     *     
     */
    public void setResponse(ARXEDIResponse value) {
        this.response = value;
    }

}
