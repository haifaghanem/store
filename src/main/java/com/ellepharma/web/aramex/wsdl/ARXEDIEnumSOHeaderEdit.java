
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ARX_EDI._Enum_SO_Header_Edit.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ARX_EDI._Enum_SO_Header_Edit">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Forwarder"/>
 *     &lt;enumeration value="D_Address1"/>
 *     &lt;enumeration value="CTSValue"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ARX_EDI._Enum_SO_Header_Edit")
@XmlEnum
public enum ARXEDIEnumSOHeaderEdit {

    @XmlEnumValue("Forwarder")
    FORWARDER("Forwarder"),
    @XmlEnumValue("D_Address1")
    D_ADDRESS_1("D_Address1"),
    @XmlEnumValue("CTSValue")
    CTS_VALUE("CTSValue");
    private final String value;

    ARXEDIEnumSOHeaderEdit(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ARXEDIEnumSOHeaderEdit fromValue(String v) {
        for (ARXEDIEnumSOHeaderEdit c: ARXEDIEnumSOHeaderEdit.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
