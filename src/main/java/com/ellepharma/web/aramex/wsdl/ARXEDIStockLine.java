
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ARX_EDI._StockLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ARX_EDI._StockLine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BOE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HSCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable01" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable02" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable03" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable04" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable05" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable06" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable07" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable08" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable09" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QtyAllocated" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyAvailable" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyDamage" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyHold" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyLost" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyOnHand" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyPicked" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RecordDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SKU_SUSR10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SKU_SUSR2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SKU_SUSR3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SKU_SUSR4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SKU_SUSR5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SKU_SUSR6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SKU_SUSR7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SKU_SUSR8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SKU_SUSR9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UnitCBM" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="UnitWGT" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ARX_EDI._StockLine", propOrder = {
    "boe",
    "description",
    "hsCode",
    "lottable01",
    "lottable02",
    "lottable03",
    "lottable04",
    "lottable05",
    "lottable06",
    "lottable07",
    "lottable08",
    "lottable09",
    "lottable10",
    "qtyAllocated",
    "qtyAvailable",
    "qtyDamage",
    "qtyHold",
    "qtyLost",
    "qtyOnHand",
    "qtyPicked",
    "recordDate",
    "sku",
    "skususr10",
    "skususr2",
    "skususr3",
    "skususr4",
    "skususr5",
    "skususr6",
    "skususr7",
    "skususr8",
    "skususr9",
    "unitCBM",
    "unitWGT"
})
public class ARXEDIStockLine {

    @XmlElementRef(name = "BOE", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> boe;
    @XmlElementRef(name = "Description", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "HSCode", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> hsCode;
    @XmlElementRef(name = "Lottable01", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable01;
    @XmlElementRef(name = "Lottable02", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable02;
    @XmlElementRef(name = "Lottable03", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable03;
    @XmlElementRef(name = "Lottable04", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable04;
    @XmlElementRef(name = "Lottable05", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable05;
    @XmlElementRef(name = "Lottable06", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable06;
    @XmlElementRef(name = "Lottable07", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable07;
    @XmlElementRef(name = "Lottable08", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable08;
    @XmlElementRef(name = "Lottable09", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable09;
    @XmlElementRef(name = "Lottable10", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable10;
    @XmlElement(name = "QtyAllocated")
    protected Integer qtyAllocated;
    @XmlElement(name = "QtyAvailable")
    protected Integer qtyAvailable;
    @XmlElement(name = "QtyDamage")
    protected Integer qtyDamage;
    @XmlElement(name = "QtyHold")
    protected Integer qtyHold;
    @XmlElement(name = "QtyLost")
    protected Integer qtyLost;
    @XmlElement(name = "QtyOnHand")
    protected Integer qtyOnHand;
    @XmlElement(name = "QtyPicked")
    protected Integer qtyPicked;
    @XmlElement(name = "RecordDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar recordDate;
    @XmlElementRef(name = "SKU", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sku;
    @XmlElementRef(name = "SKU_SUSR10", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> skususr10;
    @XmlElementRef(name = "SKU_SUSR2", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> skususr2;
    @XmlElementRef(name = "SKU_SUSR3", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> skususr3;
    @XmlElementRef(name = "SKU_SUSR4", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> skususr4;
    @XmlElementRef(name = "SKU_SUSR5", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> skususr5;
    @XmlElementRef(name = "SKU_SUSR6", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> skususr6;
    @XmlElementRef(name = "SKU_SUSR7", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> skususr7;
    @XmlElementRef(name = "SKU_SUSR8", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> skususr8;
    @XmlElementRef(name = "SKU_SUSR9", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> skususr9;
    @XmlElement(name = "UnitCBM")
    protected Double unitCBM;
    @XmlElement(name = "UnitWGT")
    protected Double unitWGT;

    /**
     * Gets the value of the boe property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBOE() {
        return boe;
    }

    /**
     * Sets the value of the boe property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBOE(JAXBElement<String> value) {
        this.boe = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Gets the value of the hsCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHSCode() {
        return hsCode;
    }

    /**
     * Sets the value of the hsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHSCode(JAXBElement<String> value) {
        this.hsCode = value;
    }

    /**
     * Gets the value of the lottable01 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable01() {
        return lottable01;
    }

    /**
     * Sets the value of the lottable01 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable01(JAXBElement<String> value) {
        this.lottable01 = value;
    }

    /**
     * Gets the value of the lottable02 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable02() {
        return lottable02;
    }

    /**
     * Sets the value of the lottable02 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable02(JAXBElement<String> value) {
        this.lottable02 = value;
    }

    /**
     * Gets the value of the lottable03 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable03() {
        return lottable03;
    }

    /**
     * Sets the value of the lottable03 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable03(JAXBElement<String> value) {
        this.lottable03 = value;
    }

    /**
     * Gets the value of the lottable04 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable04() {
        return lottable04;
    }

    /**
     * Sets the value of the lottable04 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable04(JAXBElement<String> value) {
        this.lottable04 = value;
    }

    /**
     * Gets the value of the lottable05 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable05() {
        return lottable05;
    }

    /**
     * Sets the value of the lottable05 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable05(JAXBElement<String> value) {
        this.lottable05 = value;
    }

    /**
     * Gets the value of the lottable06 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable06() {
        return lottable06;
    }

    /**
     * Sets the value of the lottable06 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable06(JAXBElement<String> value) {
        this.lottable06 = value;
    }

    /**
     * Gets the value of the lottable07 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable07() {
        return lottable07;
    }

    /**
     * Sets the value of the lottable07 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable07(JAXBElement<String> value) {
        this.lottable07 = value;
    }

    /**
     * Gets the value of the lottable08 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable08() {
        return lottable08;
    }

    /**
     * Sets the value of the lottable08 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable08(JAXBElement<String> value) {
        this.lottable08 = value;
    }

    /**
     * Gets the value of the lottable09 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable09() {
        return lottable09;
    }

    /**
     * Sets the value of the lottable09 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable09(JAXBElement<String> value) {
        this.lottable09 = value;
    }

    /**
     * Gets the value of the lottable10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable10() {
        return lottable10;
    }

    /**
     * Sets the value of the lottable10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable10(JAXBElement<String> value) {
        this.lottable10 = value;
    }

    /**
     * Gets the value of the qtyAllocated property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyAllocated() {
        return qtyAllocated;
    }

    /**
     * Sets the value of the qtyAllocated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyAllocated(Integer value) {
        this.qtyAllocated = value;
    }

    /**
     * Gets the value of the qtyAvailable property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyAvailable() {
        return qtyAvailable;
    }

    /**
     * Sets the value of the qtyAvailable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyAvailable(Integer value) {
        this.qtyAvailable = value;
    }

    /**
     * Gets the value of the qtyDamage property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyDamage() {
        return qtyDamage;
    }

    /**
     * Sets the value of the qtyDamage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyDamage(Integer value) {
        this.qtyDamage = value;
    }

    /**
     * Gets the value of the qtyHold property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyHold() {
        return qtyHold;
    }

    /**
     * Sets the value of the qtyHold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyHold(Integer value) {
        this.qtyHold = value;
    }

    /**
     * Gets the value of the qtyLost property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyLost() {
        return qtyLost;
    }

    /**
     * Sets the value of the qtyLost property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyLost(Integer value) {
        this.qtyLost = value;
    }

    /**
     * Gets the value of the qtyOnHand property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyOnHand() {
        return qtyOnHand;
    }

    /**
     * Sets the value of the qtyOnHand property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyOnHand(Integer value) {
        this.qtyOnHand = value;
    }

    /**
     * Gets the value of the qtyPicked property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyPicked() {
        return qtyPicked;
    }

    /**
     * Sets the value of the qtyPicked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyPicked(Integer value) {
        this.qtyPicked = value;
    }

    /**
     * Gets the value of the recordDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRecordDate() {
        return recordDate;
    }

    /**
     * Sets the value of the recordDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRecordDate(XMLGregorianCalendar value) {
        this.recordDate = value;
    }

    /**
     * Gets the value of the sku property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKU() {
        return sku;
    }

    /**
     * Sets the value of the sku property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKU(JAXBElement<String> value) {
        this.sku = value;
    }

    /**
     * Gets the value of the skususr10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKUSUSR10() {
        return skususr10;
    }

    /**
     * Sets the value of the skususr10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKUSUSR10(JAXBElement<String> value) {
        this.skususr10 = value;
    }

    /**
     * Gets the value of the skususr2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKUSUSR2() {
        return skususr2;
    }

    /**
     * Sets the value of the skususr2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKUSUSR2(JAXBElement<String> value) {
        this.skususr2 = value;
    }

    /**
     * Gets the value of the skususr3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKUSUSR3() {
        return skususr3;
    }

    /**
     * Sets the value of the skususr3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKUSUSR3(JAXBElement<String> value) {
        this.skususr3 = value;
    }

    /**
     * Gets the value of the skususr4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKUSUSR4() {
        return skususr4;
    }

    /**
     * Sets the value of the skususr4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKUSUSR4(JAXBElement<String> value) {
        this.skususr4 = value;
    }

    /**
     * Gets the value of the skususr5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKUSUSR5() {
        return skususr5;
    }

    /**
     * Sets the value of the skususr5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKUSUSR5(JAXBElement<String> value) {
        this.skususr5 = value;
    }

    /**
     * Gets the value of the skususr6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKUSUSR6() {
        return skususr6;
    }

    /**
     * Sets the value of the skususr6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKUSUSR6(JAXBElement<String> value) {
        this.skususr6 = value;
    }

    /**
     * Gets the value of the skususr7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKUSUSR7() {
        return skususr7;
    }

    /**
     * Sets the value of the skususr7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKUSUSR7(JAXBElement<String> value) {
        this.skususr7 = value;
    }

    /**
     * Gets the value of the skususr8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKUSUSR8() {
        return skususr8;
    }

    /**
     * Sets the value of the skususr8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKUSUSR8(JAXBElement<String> value) {
        this.skususr8 = value;
    }

    /**
     * Gets the value of the skususr9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKUSUSR9() {
        return skususr9;
    }

    /**
     * Sets the value of the skususr9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKUSUSR9(JAXBElement<String> value) {
        this.skususr9 = value;
    }

    /**
     * Gets the value of the unitCBM property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getUnitCBM() {
        return unitCBM;
    }

    /**
     * Sets the value of the unitCBM property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setUnitCBM(Double value) {
        this.unitCBM = value;
    }

    /**
     * Gets the value of the unitWGT property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getUnitWGT() {
        return unitWGT;
    }

    /**
     * Sets the value of the unitWGT property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setUnitWGT(Double value) {
        this.unitWGT = value;
    }

}
