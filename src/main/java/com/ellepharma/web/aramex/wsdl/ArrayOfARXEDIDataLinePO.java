
package com.ellepharma.web.aramex.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfARX_EDI._DataLine_PO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfARX_EDI._DataLine_PO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ARX_EDI._DataLine_PO" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._DataLine_PO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfARX_EDI._DataLine_PO", propOrder = {
    "arxediDataLinePO"
})
public class ArrayOfARXEDIDataLinePO {

    @XmlElement(name = "ARX_EDI._DataLine_PO")
    protected List<ARXEDIDataLinePO> arxediDataLinePO;

    /**
     * Gets the value of the arxediDataLinePO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the arxediDataLinePO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getARXEDIDataLinePO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ARXEDIDataLinePO }
     * 
     * 
     */
    public List<ARXEDIDataLinePO> getARXEDIDataLinePO() {
        if (arxediDataLinePO == null) {
            arxediDataLinePO = new ArrayList<ARXEDIDataLinePO>();
        }
        return this.arxediDataLinePO;
    }

}
