
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CancelConsoleAllResult" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._Response" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cancelConsoleAllResult"
})
@XmlRootElement(name = "CancelConsoleAllResponse", namespace = "http://tempuri.org/")
public class CancelConsoleAllResponse {

    @XmlElement(name = "CancelConsoleAllResult", namespace = "http://tempuri.org/")
    protected ARXEDIResponse cancelConsoleAllResult;

    /**
     * Gets the value of the cancelConsoleAllResult property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIResponse }
     *     
     */
    public ARXEDIResponse getCancelConsoleAllResult() {
        return cancelConsoleAllResult;
    }

    /**
     * Sets the value of the cancelConsoleAllResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIResponse }
     *     
     */
    public void setCancelConsoleAllResult(ARXEDIResponse value) {
        this.cancelConsoleAllResult = value;
    }

}
