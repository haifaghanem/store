
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CSO" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._SO_Line_Add" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cso"
})
@XmlRootElement(name = "AddSOLine", namespace = "http://tempuri.org/")
public class AddSOLine {

    @XmlElement(name = "CSO", namespace = "http://tempuri.org/")
    protected ARXEDISOLineAdd cso;

    /**
     * Gets the value of the cso property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDISOLineAdd }
     *     
     */
    public ARXEDISOLineAdd getCSO() {
        return cso;
    }

    /**
     * Sets the value of the cso property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDISOLineAdd }
     *     
     */
    public void setCSO(ARXEDISOLineAdd value) {
        this.cso = value;
    }

}
