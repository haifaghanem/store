
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InquiryASNResult" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._Response_ASN_InquiryMultiple" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inquiryASNResult"
})
@XmlRootElement(name = "InquiryASNResponse", namespace = "http://tempuri.org/")
public class InquiryASNResponse {

    @XmlElement(name = "InquiryASNResult", namespace = "http://tempuri.org/")
    protected ARXEDIResponseASNInquiryMultiple inquiryASNResult;

    /**
     * Gets the value of the inquiryASNResult property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIResponseASNInquiryMultiple }
     *     
     */
    public ARXEDIResponseASNInquiryMultiple getInquiryASNResult() {
        return inquiryASNResult;
    }

    /**
     * Sets the value of the inquiryASNResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIResponseASNInquiryMultiple }
     *     
     */
    public void setInquiryASNResult(ARXEDIResponseASNInquiryMultiple value) {
        this.inquiryASNResult = value;
    }

}
