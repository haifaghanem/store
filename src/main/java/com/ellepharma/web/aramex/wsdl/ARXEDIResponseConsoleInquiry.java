
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ARX_EDI._Response_Console_Inquiry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ARX_EDI._Response_Console_Inquiry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsoleKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsUpdateAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LatestTrackingStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lines" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ArrayOfARX_EDI._SO_Inquiry_Line" minOccurs="0"/>
 *         &lt;element name="MAWB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxActualShipDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxAddDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxAllocateDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxEditDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxPickDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxSOTDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Orders" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ArrayOfARX_EDI._Console_Inquiry_LineDetail" minOccurs="0"/>
 *         &lt;element name="Response" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._Response" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ARX_EDI._Response_Console_Inquiry", propOrder = {
    "consoleKey",
    "isUpdateAllowed",
    "latestTrackingStatus",
    "lines",
    "mawb",
    "maxActualShipDate",
    "maxAddDate",
    "maxAllocateDate",
    "maxEditDate",
    "maxPickDate",
    "maxSOTDate",
    "orders",
    "response"
})
public class ARXEDIResponseConsoleInquiry {

    @XmlElementRef(name = "ConsoleKey", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> consoleKey;
    @XmlElement(name = "IsUpdateAllowed")
    protected Boolean isUpdateAllowed;
    @XmlElementRef(name = "LatestTrackingStatus", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> latestTrackingStatus;
    @XmlElementRef(name = "Lines", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfARXEDISOInquiryLine> lines;
    @XmlElementRef(name = "MAWB", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mawb;
    @XmlElementRef(name = "MaxActualShipDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> maxActualShipDate;
    @XmlElementRef(name = "MaxAddDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> maxAddDate;
    @XmlElementRef(name = "MaxAllocateDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> maxAllocateDate;
    @XmlElementRef(name = "MaxEditDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> maxEditDate;
    @XmlElementRef(name = "MaxPickDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> maxPickDate;
    @XmlElementRef(name = "MaxSOTDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> maxSOTDate;
    @XmlElementRef(name = "Orders", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfARXEDIConsoleInquiryLineDetail> orders;
    @XmlElement(name = "Response")
    protected ARXEDIResponse response;

    /**
     * Gets the value of the consoleKey property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getConsoleKey() {
        return consoleKey;
    }

    /**
     * Sets the value of the consoleKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setConsoleKey(JAXBElement<String> value) {
        this.consoleKey = value;
    }

    /**
     * Gets the value of the isUpdateAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsUpdateAllowed() {
        return isUpdateAllowed;
    }

    /**
     * Sets the value of the isUpdateAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsUpdateAllowed(Boolean value) {
        this.isUpdateAllowed = value;
    }

    /**
     * Gets the value of the latestTrackingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLatestTrackingStatus() {
        return latestTrackingStatus;
    }

    /**
     * Sets the value of the latestTrackingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLatestTrackingStatus(JAXBElement<String> value) {
        this.latestTrackingStatus = value;
    }

    /**
     * Gets the value of the lines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfARXEDISOInquiryLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfARXEDISOInquiryLine> getLines() {
        return lines;
    }

    /**
     * Sets the value of the lines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfARXEDISOInquiryLine }{@code >}
     *     
     */
    public void setLines(JAXBElement<ArrayOfARXEDISOInquiryLine> value) {
        this.lines = value;
    }

    /**
     * Gets the value of the mawb property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMAWB() {
        return mawb;
    }

    /**
     * Sets the value of the mawb property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMAWB(JAXBElement<String> value) {
        this.mawb = value;
    }

    /**
     * Gets the value of the maxActualShipDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMaxActualShipDate() {
        return maxActualShipDate;
    }

    /**
     * Sets the value of the maxActualShipDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMaxActualShipDate(JAXBElement<String> value) {
        this.maxActualShipDate = value;
    }

    /**
     * Gets the value of the maxAddDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMaxAddDate() {
        return maxAddDate;
    }

    /**
     * Sets the value of the maxAddDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMaxAddDate(JAXBElement<String> value) {
        this.maxAddDate = value;
    }

    /**
     * Gets the value of the maxAllocateDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMaxAllocateDate() {
        return maxAllocateDate;
    }

    /**
     * Sets the value of the maxAllocateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMaxAllocateDate(JAXBElement<String> value) {
        this.maxAllocateDate = value;
    }

    /**
     * Gets the value of the maxEditDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMaxEditDate() {
        return maxEditDate;
    }

    /**
     * Sets the value of the maxEditDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMaxEditDate(JAXBElement<String> value) {
        this.maxEditDate = value;
    }

    /**
     * Gets the value of the maxPickDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMaxPickDate() {
        return maxPickDate;
    }

    /**
     * Sets the value of the maxPickDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMaxPickDate(JAXBElement<String> value) {
        this.maxPickDate = value;
    }

    /**
     * Gets the value of the maxSOTDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMaxSOTDate() {
        return maxSOTDate;
    }

    /**
     * Sets the value of the maxSOTDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMaxSOTDate(JAXBElement<String> value) {
        this.maxSOTDate = value;
    }

    /**
     * Gets the value of the orders property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfARXEDIConsoleInquiryLineDetail }{@code >}
     *     
     */
    public JAXBElement<ArrayOfARXEDIConsoleInquiryLineDetail> getOrders() {
        return orders;
    }

    /**
     * Sets the value of the orders property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfARXEDIConsoleInquiryLineDetail }{@code >}
     *     
     */
    public void setOrders(JAXBElement<ArrayOfARXEDIConsoleInquiryLineDetail> value) {
        this.orders = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIResponse }
     *     
     */
    public ARXEDIResponse getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIResponse }
     *     
     */
    public void setResponse(ARXEDIResponse value) {
        this.response = value;
    }

}
