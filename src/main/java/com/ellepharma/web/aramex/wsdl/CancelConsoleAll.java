
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Console" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._Console_Inquiry" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "console"
})
@XmlRootElement(name = "CancelConsoleAll", namespace = "http://tempuri.org/")
public class CancelConsoleAll {

    @XmlElement(name = "Console", namespace = "http://tempuri.org/")
    protected ARXEDIConsoleInquiry console;

    /**
     * Gets the value of the console property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIConsoleInquiry }
     *     
     */
    public ARXEDIConsoleInquiry getConsole() {
        return console;
    }

    /**
     * Sets the value of the console property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIConsoleInquiry }
     *     
     */
    public void setConsole(ARXEDIConsoleInquiry value) {
        this.console = value;
    }

}
