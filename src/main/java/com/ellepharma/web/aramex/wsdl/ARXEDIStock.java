
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ARX_EDI._Stock complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ARX_EDI._Stock">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationHeader" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._ApplicationHeader" minOccurs="0"/>
 *         &lt;element name="DataHeader" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._DataHeader_Stock" minOccurs="0"/>
 *         &lt;element name="SSA" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._SSAServer" minOccurs="0"/>
 *         &lt;element name="UserDate" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ArrayOfARX_EDI._UserDefineData" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ARX_EDI._Stock", propOrder = {
    "applicationHeader",
    "dataHeader",
    "ssa",
    "userDate"
})
public class ARXEDIStock {

    @XmlElement(name = "ApplicationHeader")
    protected ARXEDIApplicationHeader applicationHeader;
    @XmlElement(name = "DataHeader")
    protected ARXEDIDataHeaderStock dataHeader;
    @XmlElement(name = "SSA")
    protected ARXEDISSAServer ssa;
    @XmlElementRef(name = "UserDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfARXEDIUserDefineData> userDate;

    /**
     * Gets the value of the applicationHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIApplicationHeader }
     *     
     */
    public ARXEDIApplicationHeader getApplicationHeader() {
        return applicationHeader;
    }

    /**
     * Sets the value of the applicationHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIApplicationHeader }
     *     
     */
    public void setApplicationHeader(ARXEDIApplicationHeader value) {
        this.applicationHeader = value;
    }

    /**
     * Gets the value of the dataHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIDataHeaderStock }
     *     
     */
    public ARXEDIDataHeaderStock getDataHeader() {
        return dataHeader;
    }

    /**
     * Sets the value of the dataHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIDataHeaderStock }
     *     
     */
    public void setDataHeader(ARXEDIDataHeaderStock value) {
        this.dataHeader = value;
    }

    /**
     * Gets the value of the ssa property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDISSAServer }
     *     
     */
    public ARXEDISSAServer getSSA() {
        return ssa;
    }

    /**
     * Sets the value of the ssa property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDISSAServer }
     *     
     */
    public void setSSA(ARXEDISSAServer value) {
        this.ssa = value;
    }

    /**
     * Gets the value of the userDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}
     *     
     */
    public JAXBElement<ArrayOfARXEDIUserDefineData> getUserDate() {
        return userDate;
    }

    /**
     * Sets the value of the userDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}
     *     
     */
    public void setUserDate(JAXBElement<ArrayOfARXEDIUserDefineData> value) {
        this.userDate = value;
    }

}
