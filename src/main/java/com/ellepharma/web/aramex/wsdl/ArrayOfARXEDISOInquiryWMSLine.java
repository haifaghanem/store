
package com.ellepharma.web.aramex.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfARX_EDI._SO_Inquiry_WMSLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfARX_EDI._SO_Inquiry_WMSLine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ARX_EDI._SO_Inquiry_WMSLine" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._SO_Inquiry_WMSLine" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfARX_EDI._SO_Inquiry_WMSLine", propOrder = {
    "arxedisoInquiryWMSLine"
})
public class ArrayOfARXEDISOInquiryWMSLine {

    @XmlElement(name = "ARX_EDI._SO_Inquiry_WMSLine")
    protected List<ARXEDISOInquiryWMSLine> arxedisoInquiryWMSLine;

    /**
     * Gets the value of the arxedisoInquiryWMSLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the arxedisoInquiryWMSLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getARXEDISOInquiryWMSLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ARXEDISOInquiryWMSLine }
     * 
     * 
     */
    public List<ARXEDISOInquiryWMSLine> getARXEDISOInquiryWMSLine() {
        if (arxedisoInquiryWMSLine == null) {
            arxedisoInquiryWMSLine = new ArrayList<ARXEDISOInquiryWMSLine>();
        }
        return this.arxedisoInquiryWMSLine;
    }

}
