
package com.ellepharma.web.aramex.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfARX_EDI._Stock_SKUsLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfARX_EDI._Stock_SKUsLine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ARX_EDI._Stock_SKUsLine" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._Stock_SKUsLine" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfARX_EDI._Stock_SKUsLine", propOrder = {
    "arxediStockSKUsLine"
})
public class ArrayOfARXEDIStockSKUsLine {

    @XmlElement(name = "ARX_EDI._Stock_SKUsLine")
    protected List<ARXEDIStockSKUsLine> arxediStockSKUsLine;

    /**
     * Gets the value of the arxediStockSKUsLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the arxediStockSKUsLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getARXEDIStockSKUsLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ARXEDIStockSKUsLine }
     * 
     * 
     */
    public List<ARXEDIStockSKUsLine> getARXEDIStockSKUsLine() {
        if (arxediStockSKUsLine == null) {
            arxediStockSKUsLine = new ArrayList<ARXEDIStockSKUsLine>();
        }
        return this.arxediStockSKUsLine;
    }

}
