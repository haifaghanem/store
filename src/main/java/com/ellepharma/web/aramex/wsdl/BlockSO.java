
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SO" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._SO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "so"
})
@XmlRootElement(name = "BlockSO", namespace = "http://tempuri.org/")
public class BlockSO {

    @XmlElement(name = "SO", namespace = "http://tempuri.org/")
    protected ARXEDISO so;

    /**
     * Gets the value of the so property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDISO }
     *     
     */
    public ARXEDISO getSO() {
        return so;
    }

    /**
     * Sets the value of the so property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDISO }
     *     
     */
    public void setSO(ARXEDISO value) {
        this.so = value;
    }

}
