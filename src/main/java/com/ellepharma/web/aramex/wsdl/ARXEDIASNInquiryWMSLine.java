
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ARX_EDI._ASN_Inquiry_WMSLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ARX_EDI._ASN_Inquiry_WMSLine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="COO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExpDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExternLineNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable01" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable02" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable07" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable08" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable09" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lottable10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ManuDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QtyExpected" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyReceivecdALL" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyReceivecdHOLD" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyReceivecdOK" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ARX_EDI._ASN_Inquiry_WMSLine", propOrder = {
    "coo",
    "expDate",
    "externLineNo",
    "lottable01",
    "lottable02",
    "lottable07",
    "lottable08",
    "lottable09",
    "lottable10",
    "manuDate",
    "qtyExpected",
    "qtyReceivecdALL",
    "qtyReceivecdHOLD",
    "qtyReceivecdOK",
    "sku"
})
public class ARXEDIASNInquiryWMSLine {

    @XmlElementRef(name = "COO", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> coo;
    @XmlElementRef(name = "ExpDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> expDate;
    @XmlElementRef(name = "ExternLineNo", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externLineNo;
    @XmlElementRef(name = "Lottable01", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable01;
    @XmlElementRef(name = "Lottable02", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable02;
    @XmlElementRef(name = "Lottable07", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable07;
    @XmlElementRef(name = "Lottable08", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable08;
    @XmlElementRef(name = "Lottable09", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable09;
    @XmlElementRef(name = "Lottable10", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lottable10;
    @XmlElementRef(name = "ManuDate", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> manuDate;
    @XmlElement(name = "QtyExpected")
    protected Integer qtyExpected;
    @XmlElement(name = "QtyReceivecdALL")
    protected Integer qtyReceivecdALL;
    @XmlElement(name = "QtyReceivecdHOLD")
    protected Integer qtyReceivecdHOLD;
    @XmlElement(name = "QtyReceivecdOK")
    protected Integer qtyReceivecdOK;
    @XmlElementRef(name = "SKU", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sku;

    /**
     * Gets the value of the coo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCOO() {
        return coo;
    }

    /**
     * Sets the value of the coo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCOO(JAXBElement<String> value) {
        this.coo = value;
    }

    /**
     * Gets the value of the expDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExpDate() {
        return expDate;
    }

    /**
     * Sets the value of the expDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExpDate(JAXBElement<String> value) {
        this.expDate = value;
    }

    /**
     * Gets the value of the externLineNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternLineNo() {
        return externLineNo;
    }

    /**
     * Sets the value of the externLineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternLineNo(JAXBElement<String> value) {
        this.externLineNo = value;
    }

    /**
     * Gets the value of the lottable01 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable01() {
        return lottable01;
    }

    /**
     * Sets the value of the lottable01 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable01(JAXBElement<String> value) {
        this.lottable01 = value;
    }

    /**
     * Gets the value of the lottable02 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable02() {
        return lottable02;
    }

    /**
     * Sets the value of the lottable02 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable02(JAXBElement<String> value) {
        this.lottable02 = value;
    }

    /**
     * Gets the value of the lottable07 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable07() {
        return lottable07;
    }

    /**
     * Sets the value of the lottable07 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable07(JAXBElement<String> value) {
        this.lottable07 = value;
    }

    /**
     * Gets the value of the lottable08 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable08() {
        return lottable08;
    }

    /**
     * Sets the value of the lottable08 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable08(JAXBElement<String> value) {
        this.lottable08 = value;
    }

    /**
     * Gets the value of the lottable09 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable09() {
        return lottable09;
    }

    /**
     * Sets the value of the lottable09 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable09(JAXBElement<String> value) {
        this.lottable09 = value;
    }

    /**
     * Gets the value of the lottable10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLottable10() {
        return lottable10;
    }

    /**
     * Sets the value of the lottable10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLottable10(JAXBElement<String> value) {
        this.lottable10 = value;
    }

    /**
     * Gets the value of the manuDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getManuDate() {
        return manuDate;
    }

    /**
     * Sets the value of the manuDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setManuDate(JAXBElement<String> value) {
        this.manuDate = value;
    }

    /**
     * Gets the value of the qtyExpected property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyExpected() {
        return qtyExpected;
    }

    /**
     * Sets the value of the qtyExpected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyExpected(Integer value) {
        this.qtyExpected = value;
    }

    /**
     * Gets the value of the qtyReceivecdALL property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyReceivecdALL() {
        return qtyReceivecdALL;
    }

    /**
     * Sets the value of the qtyReceivecdALL property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyReceivecdALL(Integer value) {
        this.qtyReceivecdALL = value;
    }

    /**
     * Gets the value of the qtyReceivecdHOLD property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyReceivecdHOLD() {
        return qtyReceivecdHOLD;
    }

    /**
     * Sets the value of the qtyReceivecdHOLD property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyReceivecdHOLD(Integer value) {
        this.qtyReceivecdHOLD = value;
    }

    /**
     * Gets the value of the qtyReceivecdOK property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyReceivecdOK() {
        return qtyReceivecdOK;
    }

    /**
     * Sets the value of the qtyReceivecdOK property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyReceivecdOK(Integer value) {
        this.qtyReceivecdOK = value;
    }

    /**
     * Gets the value of the sku property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKU() {
        return sku;
    }

    /**
     * Sets the value of the sku property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKU(JAXBElement<String> value) {
        this.sku = value;
    }

}
