
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InquirySOResult" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._Response_SO_Inquiry" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inquirySOResult"
})
@XmlRootElement(name = "InquirySOResponse", namespace = "http://tempuri.org/")
public class InquirySOResponse {

    @XmlElement(name = "InquirySOResult", namespace = "http://tempuri.org/")
    protected ARXEDIResponseSOInquiry inquirySOResult;

    /**
     * Gets the value of the inquirySOResult property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIResponseSOInquiry }
     *     
     */
    public ARXEDIResponseSOInquiry getInquirySOResult() {
        return inquirySOResult;
    }

    /**
     * Sets the value of the inquirySOResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIResponseSOInquiry }
     *     
     */
    public void setInquirySOResult(ARXEDIResponseSOInquiry value) {
        this.inquirySOResult = value;
    }

}
