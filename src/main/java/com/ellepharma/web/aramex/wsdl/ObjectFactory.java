
package com.ellepharma.web.aramex.wsdl;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ellepharma.web.aramex.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ARXEDIASNInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._ASN_Inquiry");
    private final static QName _ARXEDIResponseStock_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Response_Stock");
    private final static QName _ARXEDIUserDefineData_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._UserDefineData");
    private final static QName _ArrayOfARXEDIASNInquiryWMSLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._ASN_Inquiry_WMSLine");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _ARXEDIDataHeaderSOInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataHeader_SO_Inquiry");
    private final static QName _ARXEDIDataLineHttpPost_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataLine_HttpPost");
    private final static QName _ARXEDIFileTypeEnum_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._FileTypeEnum");
    private final static QName _ArrayOfstring_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _ARXEDISKUs_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._SKUs");
    private final static QName _ARXEDISSAServer_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._SSAServer");
    private final static QName _ARXEDIDataLinePO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataLine_PO");
    private final static QName _ArrayOfARXEDIDataLineHttpPost_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._DataLine_HttpPost");
    private final static QName _ARXEDISAPResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._SAP_Response");
    private final static QName _ARXEDIDataLineASN_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataLine_ASN");
    private final static QName _ARXEDIResponseSOInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Response_SO_Inquiry");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _ARXEDIDataHeaderPO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataHeader_PO");
    private final static QName _ArrayOfARXEDIDataLineASN_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._DataLine_ASN");
    private final static QName _ARXEDIDataHeaderSKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataHeader_SKU");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _ARXEDIResponseASNInquiryMultiple_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Response_ASN_InquiryMultiple");
    private final static QName _ARXEDISO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._SO");
    private final static QName _ARXEDIPackUOMEnum_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._PackUOMEnum");
    private final static QName _ARXEDISOInquiryLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._SO_Inquiry_Line");
    private final static QName _ARXEDIApplicationHeader_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._ApplicationHeader");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _ArrayOfARXEDIDataLinePO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._DataLine_PO");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _ARXEDIASNInquiryWMSLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._ASN_Inquiry_WMSLine");
    private final static QName _ARXEDIStock_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Stock");
    private final static QName _ARXEDISOLineEdit_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._SO_Line_Edit");
    private final static QName _ARXEDIStockLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._StockLine");
    private final static QName _WMSTablesAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "WMSTables_Address");
    private final static QName _ARXEDISOInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._SO_Inquiry");
    private final static QName _ARXEDIDataHeaderHttpPost_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataHeader_HttpPost");
    private final static QName _ARXEDIEnumSOHeaderEdit_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Enum_SO_Header_Edit");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ArrayOfARXEDIPack_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._Pack");
    private final static QName _ARXEDIPack_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Pack");
    private final static QName _ArrayOfARXEDIConsoleInquiryLineDetail_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._Console_Inquiry_LineDetail");
    private final static QName _ARXEDIDataHeaderASN_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataHeader_ASN");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _ARXEDIConsoleInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Console_Inquiry");
    private final static QName _ARXEDIASN_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._ASN");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _ArrayOfARXEDIUserDefineData_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._UserDefineData");
    private final static QName _ARXEDIDataHeaderSO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataHeader_SO");
    private final static QName _ARXEDISOLineAdd_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._SO_Line_Add");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _ARXEDIPO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._PO");
    private final static QName _ARXEDIAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Address");
    private final static QName _ARXEDIResponseStockSKUs_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Response_Stock_SKUs");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _ArrayOfARXEDIDataLineSO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._DataLine_SO");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _ArrayOfWMSTablesAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "ArrayOfWMSTables_Address");
    private final static QName _ARXEDIResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Response");
    private final static QName _ARXEDIDataHeaderStockSKUs_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataHeader_Stock_SKUs");
    private final static QName _ARXEDIResponseConsoleInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Response_Console_Inquiry");
    private final static QName _ARXEDIResponseASNInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Response_ASN_Inquiry");
    private final static QName _ARXEDIStockSKUsLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Stock_SKUsLine");
    private final static QName _ARXEDIStockSKUsLineDIMs_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Stock_SKUsLine_DIMs");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _ARXEDISKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._SKU");
    private final static QName _ARXEDIDataHeaderASNInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataHeader_ASN_Inquiry");
    private final static QName _ARXEDIASNLineEdit_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._ASN_Line_Edit");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _ARXEDIConsoleInquiryLineDetail_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Console_Inquiry_LineDetail");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _ArrayOfARXEDIDataHeaderSKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._DataHeader_SKU");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _ArrayOfARXEDISOInquiryWMSLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._SO_Inquiry_WMSLine");
    private final static QName _ARXEDIDataHeaderStock_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataHeader_Stock");
    private final static QName _ArrayOfARXEDIResponseASNInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._Response_ASN_Inquiry");
    private final static QName _ArrayOfARXEDIBOM_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._BOM");
    private final static QName _ArrayOfARXEDIStockSKUsLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._Stock_SKUsLine");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _ARXEDIDataLineSO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataLine_SO");
    private final static QName _ARXEDIStockSKUs_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._Stock_SKUs");
    private final static QName _ARXEDIDataHeaderConsoleInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._DataHeader_Console_Inquiry");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _ARXEDISOInquiryWMSLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._SO_Inquiry_WMSLine");
    private final static QName _ARXEDIHttpPost_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._HttpPost");
    private final static QName _ArrayOfARXEDISOInquiryLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._SO_Inquiry_Line");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _ARXEDIBOM_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._BOM");
    private final static QName _ArrayOfARXEDIStockLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ArrayOfARX_EDI._StockLine");
    private final static QName _ARXEDISOHeaderEdit_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_EDI._SO_Header_Edit");
    private final static QName _ARXEDIPackName_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Name");
    private final static QName _ARXEDIStockSKUsUserDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "UserDate");
    private final static QName _ARXEDISAPResponseSAPAcDoNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SAPAcDoNo");
    private final static QName _ARXEDISAPResponseSAPCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SAPCurrency");
    private final static QName _ARXEDIDataLineSONotes_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Notes");
    private final static QName _ARXEDIDataLineSOSKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKU");
    private final static QName _ARXEDIDataLineSOLottable08_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Lottable08");
    private final static QName _ARXEDIDataLineSOLottable07_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Lottable07");
    private final static QName _ARXEDIDataLineSOALTSKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ALTSKU");
    private final static QName _ARXEDIDataLineSOSubInvoice_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SubInvoice");
    private final static QName _ARXEDIDataLineSOLottable09_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Lottable09");
    private final static QName _ARXEDIDataLineSODSUSR5_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "DSUSR5");
    private final static QName _ARXEDIDataLineSOLottable04_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Lottable04");
    private final static QName _ARXEDIDataLineSOLottable05_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Lottable05");
    private final static QName _ARXEDIDataLineSOMANUFACTURERSKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "MANUFACTURERSKU");
    private final static QName _ARXEDIDataLineSODSUSR2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "DSUSR2");
    private final static QName _ARXEDIDataLineSOExternLineNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ExternLineNo");
    private final static QName _ARXEDIDataLineSOLottable10_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Lottable10");
    private final static QName _ARXEDIDataLineSODSUSR3_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "DSUSR3");
    private final static QName _ARXEDIDataLineSOLottable02_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Lottable02");
    private final static QName _ARXEDIDataLineSODSUSR4_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "DSUSR4");
    private final static QName _ARXEDIDataLineSOARXReceiptKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ARX_ReceiptKey");
    private final static QName _ARXEDIDataLineSOPickingInstructions_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "PickingInstructions");
    private final static QName _ARXEDIDataLineSOCOO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "COO");
    private final static QName _ARXEDIDataLineSORETAILSKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "RETAILSKU");
    private final static QName _ARXEDIConsoleInquiryLineDetailOrderKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "OrderKey");
    private final static QName _ARXEDIConsoleInquiryLineDetailAllocateDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "AllocateDate");
    private final static QName _ARXEDIConsoleInquiryLineDetailLatestWHStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "LatestWHStatus");
    private final static QName _ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ClinetSystemRef");
    private final static QName _ARXEDIConsoleInquiryLineDetailAddDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "AddDate");
    private final static QName _ARXEDIConsoleInquiryLineDetailSOTDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SOTDate");
    private final static QName _ARXEDIConsoleInquiryLineDetailAramexService_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "aramexService");
    private final static QName _ARXEDIConsoleInquiryLineDetailPickDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "PickDate");
    private final static QName _ARXEDIConsoleInquiryLineDetailForwarder_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Forwarder");
    private final static QName _ARXEDIConsoleInquiryLineDetailAWB_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "AWB");
    private final static QName _ARXEDIConsoleInquiryLineDetailActualShipDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ActualShipDate");
    private final static QName _ARXEDIConsoleInquiryLineDetailAramexCODCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "aramexCODCurrency");
    private final static QName _ARXEDIConsoleInquiryLineDetailEditDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "EditDate");
    private final static QName _ARXEDIConsoleInquiryLineDetailExternalOrderKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ExternalOrderKey");
    private final static QName _ARXEDIConsoleInquiryLineDetailAramexCODValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "aramexCODValue");
    private final static QName _ARXEDIStockSKUsLineDIMsL_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "L");
    private final static QName _ARXEDIStockSKUsLineDIMsH_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "H");
    private final static QName _ARXEDIStockSKUsLineDIMsW_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "W");
    private final static QName _ARXEDIDataHeaderStockFacility_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Facility");
    private final static QName _ARXEDIDataHeaderStockSKUContains_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKU_Contains");
    private final static QName _ARXEDIDataHeaderStockStorerKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "StorerKey");
    private final static QName _ARXEDIAddressFax_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Fax");
    private final static QName _ARXEDIAddressW3W_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "W3W");
    private final static QName _ARXEDIAddressCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Country");
    private final static QName _ARXEDIAddressState_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "State");
    private final static QName _ARXEDIAddressPhone2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Phone2");
    private final static QName _ARXEDIAddressPhone1_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Phone1");
    private final static QName _ARXEDIAddressRef_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Ref");
    private final static QName _ARXEDIAddressContact_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Contact");
    private final static QName _ARXEDIAddressAddress1_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Address1");
    private final static QName _ARXEDIAddressCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "City");
    private final static QName _ARXEDIAddressAddress3_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Address3");
    private final static QName _ARXEDIAddressAddress2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Address2");
    private final static QName _ARXEDIAddressCID_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "C_ID");
    private final static QName _ARXEDIAddressZipCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ZipCode");
    private final static QName _ARXEDIAddressEmail_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Email");
    private final static QName _ARXEDIAddressCompany_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Company");
    private final static QName _ARXEDIDataHeaderSKUSHELFLIFEINDICATOR_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SHELFLIFEINDICATOR");
    private final static QName _ARXEDIDataHeaderSKULottableValidationKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "LottableValidationKey");
    private final static QName _ARXEDIDataHeaderSKUCOLOR_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "COLOR");
    private final static QName _ARXEDIDataHeaderSKUHSCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "HSCode");
    private final static QName _ARXEDIDataHeaderSKUSUSR10_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SUSR10");
    private final static QName _ARXEDIDataHeaderSKUCAMPAIGNSTART_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "CAMPAIGNSTART");
    private final static QName _ARXEDIDataHeaderSKUManufacturerSKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ManufacturerSKU");
    private final static QName _ARXEDIDataHeaderSKUPacks_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Packs");
    private final static QName _ARXEDIDataHeaderSKUITEMCHARACTERISTIC1_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ITEMCHARACTERISTIC1");
    private final static QName _ARXEDIDataHeaderSKUBOMs_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "BOMs");
    private final static QName _ARXEDIDataHeaderSKUSKUSIZE_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKUSIZE");
    private final static QName _ARXEDIDataHeaderSKUITEMCHARACTERISTIC2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ITEMCHARACTERISTIC2");
    private final static QName _ARXEDIDataHeaderSKUUPC_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "UPC");
    private final static QName _ARXEDIDataHeaderSKUDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Description");
    private final static QName _ARXEDIDataHeaderSKUCOLLECTION_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "COLLECTION");
    private final static QName _ARXEDIDataHeaderSKUBillingGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "BillingGroup");
    private final static QName _ARXEDIDataHeaderSKUCOUNTRYOFORIGIN_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "COUNTRYOFORIGIN");
    private final static QName _ARXEDIDataHeaderSKUSHELFLIFECODETYPE_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SHELFLIFECODETYPE");
    private final static QName _ARXEDIDataHeaderSKUSUSR7_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SUSR7");
    private final static QName _ARXEDIDataHeaderSKUSEASON_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SEASON");
    private final static QName _ARXEDIDataHeaderSKUSUSR8_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SUSR8");
    private final static QName _ARXEDIDataHeaderSKUSUSR5_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SUSR5");
    private final static QName _ARXEDIDataHeaderSKUUPCUOM_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "UPC_UOM");
    private final static QName _ARXEDIDataHeaderSKUCAMPAIGNEND_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "CAMPAIGNEND");
    private final static QName _ARXEDIDataHeaderSKUSUSR6_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SUSR6");
    private final static QName _ARXEDIDataHeaderSKUSUSR4_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SUSR4");
    private final static QName _ARXEDIDataHeaderSKUSTYLE_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "STYLE");
    private final static QName _ARXEDIDataHeaderSKUSUSR2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SUSR2");
    private final static QName _ARXEDIDataHeaderSKUTHEME_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "THEME");
    private final static QName _ARXEDIDataHeaderSKUSUSR9_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SUSR9");
    private final static QName _ARXEDIDataHeaderSOForwarderWayBill_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ForwarderWayBill");
    private final static QName _ARXEDIDataHeaderSORequestedBy_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "RequestedBy");
    private final static QName _ARXEDIDataHeaderSOCTSService_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "CTSService");
    private final static QName _ARXEDIDataHeaderSOCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Currency");
    private final static QName _ARXEDIDataHeaderSOREFNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "REFNO");
    private final static QName _ARXEDIDataHeaderSOConsoleKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ConsoleKey");
    private final static QName _ARXEDIDataHeaderSOType_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Type");
    private final static QName _ARXEDIDataHeaderSOINCOTERM_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "INCOTERM");
    private final static QName _ARXEDIDataHeaderSOPRIORITY_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "PRIORITY");
    private final static QName _ARXEDIDataHeaderSOCTSValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "CTSValue");
    private final static QName _ARXEDIDataHeaderSOSuspendedIndicator_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SuspendedIndicator");
    private final static QName _ARXEDIDataHeaderSOTransportationMode_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "TransportationMode");
    private final static QName _ARXEDIUserDefineDataKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Key");
    private final static QName _ARXEDIUserDefineDataValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Value");
    private final static QName _ARXEDIDataHeaderPOEXTERNALPOKEY2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "EXTERNALPOKEY2");
    private final static QName _ARXEDIDataHeaderPOINCOTERMS_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "INCOTERMS");
    private final static QName _ARXEDIDataHeaderPOSUSR3_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SUSR3");
    private final static QName _ARXEDIDataHeaderPOSUSR1_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SUSR1");
    private final static QName _ARXEDIResponseStockLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Lines");
    private final static QName _ARXEDIASNDataLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "DataLines");
    private final static QName _ARXEDIASNExtraAddresses_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ExtraAddresses");
    private final static QName _UpdatePOLinesOperationType_QNAME = new QName("http://tempuri.org/", "_OperationType");
    private final static QName _WMSTablesAddressWMSRef_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "WMSRef");
    private final static QName _WMSTablesAddressAddress2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Address2");
    private final static QName _WMSTablesAddressAddress3_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Address3");
    private final static QName _WMSTablesAddressAddress1_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Address1");
    private final static QName _WMSTablesAddressCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "City");
    private final static QName _WMSTablesAddressEmail_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Email");
    private final static QName _WMSTablesAddressName1_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Name1");
    private final static QName _WMSTablesAddressPostal_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Postal");
    private final static QName _WMSTablesAddressName2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Name2");
    private final static QName _WMSTablesAddressCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Country");
    private final static QName _WMSTablesAddressTelephone1_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Telephone1");
    private final static QName _WMSTablesAddressAddress4_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Address4");
    private final static QName _WMSTablesAddressTelephone2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Telephone2");
    private final static QName _WMSTablesAddressName3_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Name3");
    private final static QName _WMSTablesAddressName4_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Name4");
    private final static QName _WMSTablesAddressRef_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Ref");
    private final static QName _WMSTablesAddressType_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", "Type");
    private final static QName _ARXEDIDataLinePOHSCODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "HSCODE");
    private final static QName _ARXEDIDataLinePORetailSKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "RetailSKU");
    private final static QName _ARXEDIDataLinePODescr_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Descr");
    private final static QName _ARXEDIDataLinePOAltSKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "AltSKU");
    private final static QName _ARXEDIResponseSOInquiryLatestTrackingStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "LatestTrackingStatus");
    private final static QName _ARXEDIResponseSOInquiryExternalOrderKey2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ExternalOrderKey2");
    private final static QName _ARXEDIResponseSOInquiryWMSLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "WMSLines");
    private final static QName _ARXEDIResponseSOInquiryLatestStatusDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "LatestStatusDate");
    private final static QName _ARXEDIResponseSOInquiryLatestStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "LatestStatus");
    private final static QName _ARXEDIResponseTransactionID_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "TransactionID");
    private final static QName _ARXEDIResponseErrorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ErrorCode");
    private final static QName _ARXEDIResponseReference_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Reference");
    private final static QName _ARXEDIResponseErrorType_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ErrorType");
    private final static QName _ARXEDIResponseErrorDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ErrorDescription");
    private final static QName _ARXEDIResponseStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Status");
    private final static QName _ARXEDIStockSKUsLineALTSKUs_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ALTSKUs");
    private final static QName _ARXEDISKUsDataHeader_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "DataHeader");
    private final static QName _ARXEDIDataHeaderConsoleInquiryMAWB_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "MAWB");
    private final static QName _ARXEDIBOMComponentSKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ComponentSKU");
    private final static QName _ARXEDIBOMComponentQTY_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ComponentQTY");
    private final static QName _ARXEDIDataLineASNBOE_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "BOE");
    private final static QName _ARXEDIDataLineASNPurchaseOrderDocument_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "PurchaseOrderDocument");
    private final static QName _ARXEDIDataLineASNPurchaseOrderLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "PurchaseOrderLine");
    private final static QName _ARXEDIDataLineASNDRMA_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "D_RMA");
    private final static QName _ARXEDIDataLineASNPOLINENUMBER_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "POLINENUMBER");
    private final static QName _ARXEDIDataLineASNOriginalLOT_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "OriginalLOT");
    private final static QName _ARXEDIDataLineASNLinePO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "LinePO");
    private final static QName _ARXEDIDataLineASNPOKEY_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "POKEY");
    private final static QName _ARXEDIDataLineASNCDOrderKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "CDOrderKey");
    private final static QName _ARXEDIASNInquiryWMSLineLottable01_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Lottable01");
    private final static QName _ARXEDIASNInquiryWMSLineExpDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ExpDate");
    private final static QName _ARXEDIASNInquiryWMSLineManuDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ManuDate");
    private final static QName _ARXEDIDataHeaderHttpPostFiles_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Files");
    private final static QName _ARXEDIResponseConsoleInquiryMaxEditDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "MaxEditDate");
    private final static QName _ARXEDIResponseConsoleInquiryMaxAllocateDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "MaxAllocateDate");
    private final static QName _ARXEDIResponseConsoleInquiryMaxActualShipDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "MaxActualShipDate");
    private final static QName _ARXEDIResponseConsoleInquiryMaxAddDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "MaxAddDate");
    private final static QName _ARXEDIResponseConsoleInquiryMaxSOTDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "MaxSOTDate");
    private final static QName _ARXEDIResponseConsoleInquiryMaxPickDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "MaxPickDate");
    private final static QName _ARXEDIResponseConsoleInquiryOrders_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Orders");
    private final static QName _CancelConsoleConsoleKey_QNAME = new QName("http://tempuri.org/", "ConsoleKey");
    private final static QName _ARXEDISSAServerSSALogin_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SSA_Login");
    private final static QName _ARXEDISSAServerSSAToken_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SSA_Token");
    private final static QName _ARXEDISSAServerSSAPassword_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SSA_Password");
    private final static QName _ARXEDISOInquiryLineActionDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ActionDate");
    private final static QName _ARXEDISOInquiryLineUpdateComment_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "UpdateComment");
    private final static QName _ARXEDISOInquiryLineUpdateLocationFormatted_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "UpdateLocationFormatted");
    private final static QName _ARXEDIApplicationHeaderRequestedSystem_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "RequestedSystem");
    private final static QName _ARXEDIApplicationHeaderRequestedDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "RequestedDate");
    private final static QName _ARXEDIResponseASNInquiryMultipleASNs_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ASNs");
    private final static QName _ARXEDIDataLineHttpPostBody_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Body");
    private final static QName _ARXEDIDataLineHttpPostFileName_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "FileName");
    private final static QName _ARXEDIDataLineHttpPostTypeRef_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "TypeRef");
    private final static QName _ARXEDIStockLineSKUSUSR3_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKU_SUSR3");
    private final static QName _ARXEDIStockLineSKUSUSR4_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKU_SUSR4");
    private final static QName _ARXEDIStockLineSKUSUSR5_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKU_SUSR5");
    private final static QName _ARXEDIStockLineSKUSUSR6_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKU_SUSR6");
    private final static QName _ARXEDIStockLineSKUSUSR7_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKU_SUSR7");
    private final static QName _ARXEDIStockLineSKUSUSR8_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKU_SUSR8");
    private final static QName _ARXEDIStockLineSKUSUSR9_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKU_SUSR9");
    private final static QName _ARXEDIStockLineLottable03_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Lottable03");
    private final static QName _ARXEDIStockLineLottable06_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Lottable06");
    private final static QName _ARXEDIStockLineSKUSUSR10_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKU_SUSR10");
    private final static QName _ARXEDIStockLineSKUSUSR2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "SKU_SUSR2");
    private final static QName _ARXEDIDataHeaderStockSKUsEditDateFrom_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "EditDateFrom");
    private final static QName _ARXEDIDataHeaderStockSKUsEditDateTo_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "EditDateTo");
    private final static QName _ARXEDIDataHeaderASNInquiryReceiptKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ReceiptKey");
    private final static QName _ARXEDIDataHeaderASNHSUSR2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "HSUSR2");
    private final static QName _ARXEDIDataHeaderASNHSUSR1_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "HSUSR1");
    private final static QName _ARXEDIDataHeaderASNHSUSR4_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "HSUSR4");
    private final static QName _ARXEDIDataHeaderASNHSUSR3_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "HSUSR3");
    private final static QName _ARXEDIDataHeaderASNHSUSR5_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "HSUSR5");
    private final static QName _ARXEDIDataHeaderASNSupplier_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "Supplier");
    private final static QName _ARXEDIDataHeaderASNExterReceiptKey2_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ExterReceiptKey2");
    private final static QName _ARXEDIDataHeaderASNRMA_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "RMA");
    private final static QName _ARXEDIResponseASNInquiryClosedDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ClosedDate");
    private final static QName _ARXEDIResponseASNInquiryExternalReceiptKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", "ExternalReceiptKey");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ellepharma.web.aramex.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ARXEDIASNLineEdit }
     * 
     */
    public ARXEDIASNLineEdit createARXEDIASNLineEdit() {
        return new ARXEDIASNLineEdit();
    }

    /**
     * Create an instance of {@link ARXEDIDataHeaderASNInquiry }
     * 
     */
    public ARXEDIDataHeaderASNInquiry createARXEDIDataHeaderASNInquiry() {
        return new ARXEDIDataHeaderASNInquiry();
    }

    /**
     * Create an instance of {@link ARXEDISKU }
     * 
     */
    public ARXEDISKU createARXEDISKU() {
        return new ARXEDISKU();
    }

    /**
     * Create an instance of {@link ARXEDIStockSKUsLine }
     * 
     */
    public ARXEDIStockSKUsLine createARXEDIStockSKUsLine() {
        return new ARXEDIStockSKUsLine();
    }

    /**
     * Create an instance of {@link ARXEDIStockSKUsLineDIMs }
     * 
     */
    public ARXEDIStockSKUsLineDIMs createARXEDIStockSKUsLineDIMs() {
        return new ARXEDIStockSKUsLineDIMs();
    }

    /**
     * Create an instance of {@link ARXEDIResponseASNInquiry }
     * 
     */
    public ARXEDIResponseASNInquiry createARXEDIResponseASNInquiry() {
        return new ARXEDIResponseASNInquiry();
    }

    /**
     * Create an instance of {@link ARXEDIDataHeaderStockSKUs }
     * 
     */
    public ARXEDIDataHeaderStockSKUs createARXEDIDataHeaderStockSKUs() {
        return new ARXEDIDataHeaderStockSKUs();
    }

    /**
     * Create an instance of {@link ARXEDIResponseConsoleInquiry }
     * 
     */
    public ARXEDIResponseConsoleInquiry createARXEDIResponseConsoleInquiry() {
        return new ARXEDIResponseConsoleInquiry();
    }

    /**
     * Create an instance of {@link ARXEDIResponse }
     * 
     */
    public ARXEDIResponse createARXEDIResponse() {
        return new ARXEDIResponse();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIDataLineSO }
     * 
     */
    public ArrayOfARXEDIDataLineSO createArrayOfARXEDIDataLineSO() {
        return new ArrayOfARXEDIDataLineSO();
    }

    /**
     * Create an instance of {@link ARXEDIPO }
     * 
     */
    public ARXEDIPO createARXEDIPO() {
        return new ARXEDIPO();
    }

    /**
     * Create an instance of {@link ARXEDIAddress }
     * 
     */
    public ARXEDIAddress createARXEDIAddress() {
        return new ARXEDIAddress();
    }

    /**
     * Create an instance of {@link ARXEDIResponseStockSKUs }
     * 
     */
    public ARXEDIResponseStockSKUs createARXEDIResponseStockSKUs() {
        return new ARXEDIResponseStockSKUs();
    }

    /**
     * Create an instance of {@link ARXEDISOHeaderEdit }
     * 
     */
    public ARXEDISOHeaderEdit createARXEDISOHeaderEdit() {
        return new ARXEDISOHeaderEdit();
    }

    /**
     * Create an instance of {@link ARXEDIBOM }
     * 
     */
    public ARXEDIBOM createARXEDIBOM() {
        return new ARXEDIBOM();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIStockLine }
     * 
     */
    public ArrayOfARXEDIStockLine createArrayOfARXEDIStockLine() {
        return new ArrayOfARXEDIStockLine();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDISOInquiryLine }
     * 
     */
    public ArrayOfARXEDISOInquiryLine createArrayOfARXEDISOInquiryLine() {
        return new ArrayOfARXEDISOInquiryLine();
    }

    /**
     * Create an instance of {@link ARXEDIHttpPost }
     * 
     */
    public ARXEDIHttpPost createARXEDIHttpPost() {
        return new ARXEDIHttpPost();
    }

    /**
     * Create an instance of {@link ARXEDISOInquiryWMSLine }
     * 
     */
    public ARXEDISOInquiryWMSLine createARXEDISOInquiryWMSLine() {
        return new ARXEDISOInquiryWMSLine();
    }

    /**
     * Create an instance of {@link ARXEDIDataHeaderConsoleInquiry }
     * 
     */
    public ARXEDIDataHeaderConsoleInquiry createARXEDIDataHeaderConsoleInquiry() {
        return new ARXEDIDataHeaderConsoleInquiry();
    }

    /**
     * Create an instance of {@link ARXEDIDataLineSO }
     * 
     */
    public ARXEDIDataLineSO createARXEDIDataLineSO() {
        return new ARXEDIDataLineSO();
    }

    /**
     * Create an instance of {@link ARXEDIStockSKUs }
     * 
     */
    public ARXEDIStockSKUs createARXEDIStockSKUs() {
        return new ARXEDIStockSKUs();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIBOM }
     * 
     */
    public ArrayOfARXEDIBOM createArrayOfARXEDIBOM() {
        return new ArrayOfARXEDIBOM();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIStockSKUsLine }
     * 
     */
    public ArrayOfARXEDIStockSKUsLine createArrayOfARXEDIStockSKUsLine() {
        return new ArrayOfARXEDIStockSKUsLine();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIResponseASNInquiry }
     * 
     */
    public ArrayOfARXEDIResponseASNInquiry createArrayOfARXEDIResponseASNInquiry() {
        return new ArrayOfARXEDIResponseASNInquiry();
    }

    /**
     * Create an instance of {@link ARXEDIDataHeaderStock }
     * 
     */
    public ARXEDIDataHeaderStock createARXEDIDataHeaderStock() {
        return new ARXEDIDataHeaderStock();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDISOInquiryWMSLine }
     * 
     */
    public ArrayOfARXEDISOInquiryWMSLine createArrayOfARXEDISOInquiryWMSLine() {
        return new ArrayOfARXEDISOInquiryWMSLine();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIDataHeaderSKU }
     * 
     */
    public ArrayOfARXEDIDataHeaderSKU createArrayOfARXEDIDataHeaderSKU() {
        return new ArrayOfARXEDIDataHeaderSKU();
    }

    /**
     * Create an instance of {@link ARXEDIConsoleInquiryLineDetail }
     * 
     */
    public ARXEDIConsoleInquiryLineDetail createARXEDIConsoleInquiryLineDetail() {
        return new ARXEDIConsoleInquiryLineDetail();
    }

    /**
     * Create an instance of {@link ARXEDIResponseASNInquiryMultiple }
     * 
     */
    public ARXEDIResponseASNInquiryMultiple createARXEDIResponseASNInquiryMultiple() {
        return new ARXEDIResponseASNInquiryMultiple();
    }

    /**
     * Create an instance of {@link ARXEDISO }
     * 
     */
    public ARXEDISO createARXEDISO() {
        return new ARXEDISO();
    }

    /**
     * Create an instance of {@link ARXEDIDataHeaderSKU }
     * 
     */
    public ARXEDIDataHeaderSKU createARXEDIDataHeaderSKU() {
        return new ARXEDIDataHeaderSKU();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIDataLineASN }
     * 
     */
    public ArrayOfARXEDIDataLineASN createArrayOfARXEDIDataLineASN() {
        return new ArrayOfARXEDIDataLineASN();
    }

    /**
     * Create an instance of {@link ARXEDIDataHeaderPO }
     * 
     */
    public ARXEDIDataHeaderPO createARXEDIDataHeaderPO() {
        return new ARXEDIDataHeaderPO();
    }

    /**
     * Create an instance of {@link ARXEDIResponseSOInquiry }
     * 
     */
    public ARXEDIResponseSOInquiry createARXEDIResponseSOInquiry() {
        return new ARXEDIResponseSOInquiry();
    }

    /**
     * Create an instance of {@link ARXEDIDataLineASN }
     * 
     */
    public ARXEDIDataLineASN createARXEDIDataLineASN() {
        return new ARXEDIDataLineASN();
    }

    /**
     * Create an instance of {@link ARXEDISSAServer }
     * 
     */
    public ARXEDISSAServer createARXEDISSAServer() {
        return new ARXEDISSAServer();
    }

    /**
     * Create an instance of {@link ARXEDIDataLinePO }
     * 
     */
    public ARXEDIDataLinePO createARXEDIDataLinePO() {
        return new ARXEDIDataLinePO();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIDataLineHttpPost }
     * 
     */
    public ArrayOfARXEDIDataLineHttpPost createArrayOfARXEDIDataLineHttpPost() {
        return new ArrayOfARXEDIDataLineHttpPost();
    }

    /**
     * Create an instance of {@link ARXEDISAPResponse }
     * 
     */
    public ARXEDISAPResponse createARXEDISAPResponse() {
        return new ARXEDISAPResponse();
    }

    /**
     * Create an instance of {@link ARXEDISKUs }
     * 
     */
    public ARXEDISKUs createARXEDISKUs() {
        return new ARXEDISKUs();
    }

    /**
     * Create an instance of {@link ARXEDIDataLineHttpPost }
     * 
     */
    public ARXEDIDataLineHttpPost createARXEDIDataLineHttpPost() {
        return new ARXEDIDataLineHttpPost();
    }

    /**
     * Create an instance of {@link ARXEDIDataHeaderSOInquiry }
     * 
     */
    public ARXEDIDataHeaderSOInquiry createARXEDIDataHeaderSOInquiry() {
        return new ARXEDIDataHeaderSOInquiry();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIASNInquiryWMSLine }
     * 
     */
    public ArrayOfARXEDIASNInquiryWMSLine createArrayOfARXEDIASNInquiryWMSLine() {
        return new ArrayOfARXEDIASNInquiryWMSLine();
    }

    /**
     * Create an instance of {@link ARXEDIUserDefineData }
     * 
     */
    public ARXEDIUserDefineData createARXEDIUserDefineData() {
        return new ARXEDIUserDefineData();
    }

    /**
     * Create an instance of {@link ARXEDIResponseStock }
     * 
     */
    public ARXEDIResponseStock createARXEDIResponseStock() {
        return new ARXEDIResponseStock();
    }

    /**
     * Create an instance of {@link ARXEDIASNInquiry }
     * 
     */
    public ARXEDIASNInquiry createARXEDIASNInquiry() {
        return new ARXEDIASNInquiry();
    }

    /**
     * Create an instance of {@link ARXEDISOLineAdd }
     * 
     */
    public ARXEDISOLineAdd createARXEDISOLineAdd() {
        return new ARXEDISOLineAdd();
    }

    /**
     * Create an instance of {@link ARXEDIDataHeaderSO }
     * 
     */
    public ARXEDIDataHeaderSO createARXEDIDataHeaderSO() {
        return new ARXEDIDataHeaderSO();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIUserDefineData }
     * 
     */
    public ArrayOfARXEDIUserDefineData createArrayOfARXEDIUserDefineData() {
        return new ArrayOfARXEDIUserDefineData();
    }

    /**
     * Create an instance of {@link ARXEDIASN }
     * 
     */
    public ARXEDIASN createARXEDIASN() {
        return new ARXEDIASN();
    }

    /**
     * Create an instance of {@link ARXEDIConsoleInquiry }
     * 
     */
    public ARXEDIConsoleInquiry createARXEDIConsoleInquiry() {
        return new ARXEDIConsoleInquiry();
    }

    /**
     * Create an instance of {@link ARXEDIDataHeaderASN }
     * 
     */
    public ARXEDIDataHeaderASN createARXEDIDataHeaderASN() {
        return new ARXEDIDataHeaderASN();
    }

    /**
     * Create an instance of {@link ARXEDIPack }
     * 
     */
    public ARXEDIPack createARXEDIPack() {
        return new ARXEDIPack();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIConsoleInquiryLineDetail }
     * 
     */
    public ArrayOfARXEDIConsoleInquiryLineDetail createArrayOfARXEDIConsoleInquiryLineDetail() {
        return new ArrayOfARXEDIConsoleInquiryLineDetail();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIPack }
     * 
     */
    public ArrayOfARXEDIPack createArrayOfARXEDIPack() {
        return new ArrayOfARXEDIPack();
    }

    /**
     * Create an instance of {@link ARXEDISOInquiry }
     * 
     */
    public ARXEDISOInquiry createARXEDISOInquiry() {
        return new ARXEDISOInquiry();
    }

    /**
     * Create an instance of {@link ARXEDIDataHeaderHttpPost }
     * 
     */
    public ARXEDIDataHeaderHttpPost createARXEDIDataHeaderHttpPost() {
        return new ARXEDIDataHeaderHttpPost();
    }

    /**
     * Create an instance of {@link ARXEDIStockLine }
     * 
     */
    public ARXEDIStockLine createARXEDIStockLine() {
        return new ARXEDIStockLine();
    }

    /**
     * Create an instance of {@link ARXEDIASNInquiryWMSLine }
     * 
     */
    public ARXEDIASNInquiryWMSLine createARXEDIASNInquiryWMSLine() {
        return new ARXEDIASNInquiryWMSLine();
    }

    /**
     * Create an instance of {@link ARXEDIStock }
     * 
     */
    public ARXEDIStock createARXEDIStock() {
        return new ARXEDIStock();
    }

    /**
     * Create an instance of {@link ARXEDISOLineEdit }
     * 
     */
    public ARXEDISOLineEdit createARXEDISOLineEdit() {
        return new ARXEDISOLineEdit();
    }

    /**
     * Create an instance of {@link ArrayOfARXEDIDataLinePO }
     * 
     */
    public ArrayOfARXEDIDataLinePO createArrayOfARXEDIDataLinePO() {
        return new ArrayOfARXEDIDataLinePO();
    }

    /**
     * Create an instance of {@link ARXEDIApplicationHeader }
     * 
     */
    public ARXEDIApplicationHeader createARXEDIApplicationHeader() {
        return new ARXEDIApplicationHeader();
    }

    /**
     * Create an instance of {@link ARXEDISOInquiryLine }
     * 
     */
    public ARXEDISOInquiryLine createARXEDISOInquiryLine() {
        return new ARXEDISOInquiryLine();
    }

    /**
     * Create an instance of {@link ArrayOfstring }
     * 
     */
    public ArrayOfstring createArrayOfstring() {
        return new ArrayOfstring();
    }

    /**
     * Create an instance of {@link ArrayOfWMSTablesAddress }
     * 
     */
    public ArrayOfWMSTablesAddress createArrayOfWMSTablesAddress() {
        return new ArrayOfWMSTablesAddress();
    }

    /**
     * Create an instance of {@link WMSTablesAddress }
     * 
     */
    public WMSTablesAddress createWMSTablesAddress() {
        return new WMSTablesAddress();
    }

    /**
     * Create an instance of {@link ImportASNResponse }
     * 
     */
    public ImportASNResponse createImportASNResponse() {
        return new ImportASNResponse();
    }

    /**
     * Create an instance of {@link EditASNLineResponse }
     * 
     */
    public EditASNLineResponse createEditASNLineResponse() {
        return new EditASNLineResponse();
    }

    /**
     * Create an instance of {@link UpdateSOResponse }
     * 
     */
    public UpdateSOResponse createUpdateSOResponse() {
        return new UpdateSOResponse();
    }

    /**
     * Create an instance of {@link UpdatePOHeaderResponse }
     * 
     */
    public UpdatePOHeaderResponse createUpdatePOHeaderResponse() {
        return new UpdatePOHeaderResponse();
    }

    /**
     * Create an instance of {@link UpdateASNResponse }
     * 
     */
    public UpdateASNResponse createUpdateASNResponse() {
        return new UpdateASNResponse();
    }

    /**
     * Create an instance of {@link UpdateASN }
     * 
     */
    public UpdateASN createUpdateASN() {
        return new UpdateASN();
    }

    /**
     * Create an instance of {@link EditSOLineResponse }
     * 
     */
    public EditSOLineResponse createEditSOLineResponse() {
        return new EditSOLineResponse();
    }

    /**
     * Create an instance of {@link IsSOUpdateAllowedResponse }
     * 
     */
    public IsSOUpdateAllowedResponse createIsSOUpdateAllowedResponse() {
        return new IsSOUpdateAllowedResponse();
    }

    /**
     * Create an instance of {@link InquirySKUs }
     * 
     */
    public InquirySKUs createInquirySKUs() {
        return new InquirySKUs();
    }

    /**
     * Create an instance of {@link UpdatePOHeader }
     * 
     */
    public UpdatePOHeader createUpdatePOHeader() {
        return new UpdatePOHeader();
    }

    /**
     * Create an instance of {@link ImportSKU }
     * 
     */
    public ImportSKU createImportSKU() {
        return new ImportSKU();
    }

    /**
     * Create an instance of {@link SAPResponse }
     * 
     */
    public SAPResponse createSAPResponse() {
        return new SAPResponse();
    }

    /**
     * Create an instance of {@link PostFilesResponse }
     * 
     */
    public PostFilesResponse createPostFilesResponse() {
        return new PostFilesResponse();
    }

    /**
     * Create an instance of {@link ImportSKUs }
     * 
     */
    public ImportSKUs createImportSKUs() {
        return new ImportSKUs();
    }

    /**
     * Create an instance of {@link IsASNUpdateAllowed }
     * 
     */
    public IsASNUpdateAllowed createIsASNUpdateAllowed() {
        return new IsASNUpdateAllowed();
    }

    /**
     * Create an instance of {@link ImportPO }
     * 
     */
    public ImportPO createImportPO() {
        return new ImportPO();
    }

    /**
     * Create an instance of {@link UpdateSO }
     * 
     */
    public UpdateSO createUpdateSO() {
        return new UpdateSO();
    }

    /**
     * Create an instance of {@link EditSOHeaderResponse }
     * 
     */
    public EditSOHeaderResponse createEditSOHeaderResponse() {
        return new EditSOHeaderResponse();
    }

    /**
     * Create an instance of {@link IsASNUpdateAllowedResponse }
     * 
     */
    public IsASNUpdateAllowedResponse createIsASNUpdateAllowedResponse() {
        return new IsASNUpdateAllowedResponse();
    }

    /**
     * Create an instance of {@link EditSOHeader }
     * 
     */
    public EditSOHeader createEditSOHeader() {
        return new EditSOHeader();
    }

    /**
     * Create an instance of {@link IsSOUpdateAllowed }
     * 
     */
    public IsSOUpdateAllowed createIsSOUpdateAllowed() {
        return new IsSOUpdateAllowed();
    }

    /**
     * Create an instance of {@link InquiryConsole }
     * 
     */
    public InquiryConsole createInquiryConsole() {
        return new InquiryConsole();
    }

    /**
     * Create an instance of {@link AddSOLineResponse }
     * 
     */
    public AddSOLineResponse createAddSOLineResponse() {
        return new AddSOLineResponse();
    }

    /**
     * Create an instance of {@link CancelConsoleAllResponse }
     * 
     */
    public CancelConsoleAllResponse createCancelConsoleAllResponse() {
        return new CancelConsoleAllResponse();
    }

    /**
     * Create an instance of {@link InquirySO }
     * 
     */
    public InquirySO createInquirySO() {
        return new InquirySO();
    }

    /**
     * Create an instance of {@link ImportSO }
     * 
     */
    public ImportSO createImportSO() {
        return new ImportSO();
    }

    /**
     * Create an instance of {@link CancelConsoleResponse }
     * 
     */
    public CancelConsoleResponse createCancelConsoleResponse() {
        return new CancelConsoleResponse();
    }

    /**
     * Create an instance of {@link EditSOLine }
     * 
     */
    public EditSOLine createEditSOLine() {
        return new EditSOLine();
    }

    /**
     * Create an instance of {@link BlockSOResponse }
     * 
     */
    public BlockSOResponse createBlockSOResponse() {
        return new BlockSOResponse();
    }

    /**
     * Create an instance of {@link AddSOLine }
     * 
     */
    public AddSOLine createAddSOLine() {
        return new AddSOLine();
    }

    /**
     * Create an instance of {@link ImportPOResponse }
     * 
     */
    public ImportPOResponse createImportPOResponse() {
        return new ImportPOResponse();
    }

    /**
     * Create an instance of {@link ImportSOResponse }
     * 
     */
    public ImportSOResponse createImportSOResponse() {
        return new ImportSOResponse();
    }

    /**
     * Create an instance of {@link UpdatePOLinesResponse }
     * 
     */
    public UpdatePOLinesResponse createUpdatePOLinesResponse() {
        return new UpdatePOLinesResponse();
    }

    /**
     * Create an instance of {@link InquirySOResponse }
     * 
     */
    public InquirySOResponse createInquirySOResponse() {
        return new InquirySOResponse();
    }

    /**
     * Create an instance of {@link InquiryASNResponse }
     * 
     */
    public InquiryASNResponse createInquiryASNResponse() {
        return new InquiryASNResponse();
    }

    /**
     * Create an instance of {@link InquiryStockResponse }
     * 
     */
    public InquiryStockResponse createInquiryStockResponse() {
        return new InquiryStockResponse();
    }

    /**
     * Create an instance of {@link CancelConsole }
     * 
     */
    public CancelConsole createCancelConsole() {
        return new CancelConsole();
    }

    /**
     * Create an instance of {@link UpdatePOLines }
     * 
     */
    public UpdatePOLines createUpdatePOLines() {
        return new UpdatePOLines();
    }

    /**
     * Create an instance of {@link InquiryStock }
     * 
     */
    public InquiryStock createInquiryStock() {
        return new InquiryStock();
    }

    /**
     * Create an instance of {@link InquiryConsoleResponse }
     * 
     */
    public InquiryConsoleResponse createInquiryConsoleResponse() {
        return new InquiryConsoleResponse();
    }

    /**
     * Create an instance of {@link ImportSKUsResponse }
     * 
     */
    public ImportSKUsResponse createImportSKUsResponse() {
        return new ImportSKUsResponse();
    }

    /**
     * Create an instance of {@link EditASNLine }
     * 
     */
    public EditASNLine createEditASNLine() {
        return new EditASNLine();
    }

    /**
     * Create an instance of {@link SAPResponseResponse }
     * 
     */
    public SAPResponseResponse createSAPResponseResponse() {
        return new SAPResponseResponse();
    }

    /**
     * Create an instance of {@link CancelConsoleAll }
     * 
     */
    public CancelConsoleAll createCancelConsoleAll() {
        return new CancelConsoleAll();
    }

    /**
     * Create an instance of {@link ImportASN }
     * 
     */
    public ImportASN createImportASN() {
        return new ImportASN();
    }

    /**
     * Create an instance of {@link InquiryASN }
     * 
     */
    public InquiryASN createInquiryASN() {
        return new InquiryASN();
    }

    /**
     * Create an instance of {@link BlockSO }
     * 
     */
    public BlockSO createBlockSO() {
        return new BlockSO();
    }

    /**
     * Create an instance of {@link PostFiles }
     * 
     */
    public PostFiles createPostFiles() {
        return new PostFiles();
    }

    /**
     * Create an instance of {@link ImportSKUResponse }
     * 
     */
    public ImportSKUResponse createImportSKUResponse() {
        return new ImportSKUResponse();
    }

    /**
     * Create an instance of {@link InquirySKUsResponse }
     * 
     */
    public InquirySKUsResponse createInquirySKUsResponse() {
        return new InquirySKUsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIASNInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._ASN_Inquiry")
    public JAXBElement<ARXEDIASNInquiry> createARXEDIASNInquiry(ARXEDIASNInquiry value) {
        return new JAXBElement<ARXEDIASNInquiry>(_ARXEDIASNInquiry_QNAME, ARXEDIASNInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIResponseStock }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Response_Stock")
    public JAXBElement<ARXEDIResponseStock> createARXEDIResponseStock(ARXEDIResponseStock value) {
        return new JAXBElement<ARXEDIResponseStock>(_ARXEDIResponseStock_QNAME, ARXEDIResponseStock.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._UserDefineData")
    public JAXBElement<ARXEDIUserDefineData> createARXEDIUserDefineData(ARXEDIUserDefineData value) {
        return new JAXBElement<ARXEDIUserDefineData>(_ARXEDIUserDefineData_QNAME, ARXEDIUserDefineData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIASNInquiryWMSLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._ASN_Inquiry_WMSLine")
    public JAXBElement<ArrayOfARXEDIASNInquiryWMSLine> createArrayOfARXEDIASNInquiryWMSLine(ArrayOfARXEDIASNInquiryWMSLine value) {
        return new JAXBElement<ArrayOfARXEDIASNInquiryWMSLine>(_ArrayOfARXEDIASNInquiryWMSLine_QNAME, ArrayOfARXEDIASNInquiryWMSLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataHeaderSOInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataHeader_SO_Inquiry")
    public JAXBElement<ARXEDIDataHeaderSOInquiry> createARXEDIDataHeaderSOInquiry(ARXEDIDataHeaderSOInquiry value) {
        return new JAXBElement<ARXEDIDataHeaderSOInquiry>(_ARXEDIDataHeaderSOInquiry_QNAME, ARXEDIDataHeaderSOInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataLineHttpPost }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataLine_HttpPost")
    public JAXBElement<ARXEDIDataLineHttpPost> createARXEDIDataLineHttpPost(ARXEDIDataLineHttpPost value) {
        return new JAXBElement<ARXEDIDataLineHttpPost>(_ARXEDIDataLineHttpPost_QNAME, ARXEDIDataLineHttpPost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIFileTypeEnum }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._FileTypeEnum")
    public JAXBElement<ARXEDIFileTypeEnum> createARXEDIFileTypeEnum(ARXEDIFileTypeEnum value) {
        return new JAXBElement<ARXEDIFileTypeEnum>(_ARXEDIFileTypeEnum_QNAME, ARXEDIFileTypeEnum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfstring")
    public JAXBElement<ArrayOfstring> createArrayOfstring(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_ArrayOfstring_QNAME, ArrayOfstring.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDISKUs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._SKUs")
    public JAXBElement<ARXEDISKUs> createARXEDISKUs(ARXEDISKUs value) {
        return new JAXBElement<ARXEDISKUs>(_ARXEDISKUs_QNAME, ARXEDISKUs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDISSAServer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._SSAServer")
    public JAXBElement<ARXEDISSAServer> createARXEDISSAServer(ARXEDISSAServer value) {
        return new JAXBElement<ARXEDISSAServer>(_ARXEDISSAServer_QNAME, ARXEDISSAServer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataLinePO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataLine_PO")
    public JAXBElement<ARXEDIDataLinePO> createARXEDIDataLinePO(ARXEDIDataLinePO value) {
        return new JAXBElement<ARXEDIDataLinePO>(_ARXEDIDataLinePO_QNAME, ARXEDIDataLinePO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIDataLineHttpPost }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._DataLine_HttpPost")
    public JAXBElement<ArrayOfARXEDIDataLineHttpPost> createArrayOfARXEDIDataLineHttpPost(ArrayOfARXEDIDataLineHttpPost value) {
        return new JAXBElement<ArrayOfARXEDIDataLineHttpPost>(_ArrayOfARXEDIDataLineHttpPost_QNAME, ArrayOfARXEDIDataLineHttpPost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDISAPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._SAP_Response")
    public JAXBElement<ARXEDISAPResponse> createARXEDISAPResponse(ARXEDISAPResponse value) {
        return new JAXBElement<ARXEDISAPResponse>(_ARXEDISAPResponse_QNAME, ARXEDISAPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataLineASN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataLine_ASN")
    public JAXBElement<ARXEDIDataLineASN> createARXEDIDataLineASN(ARXEDIDataLineASN value) {
        return new JAXBElement<ARXEDIDataLineASN>(_ARXEDIDataLineASN_QNAME, ARXEDIDataLineASN.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIResponseSOInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Response_SO_Inquiry")
    public JAXBElement<ARXEDIResponseSOInquiry> createARXEDIResponseSOInquiry(ARXEDIResponseSOInquiry value) {
        return new JAXBElement<ARXEDIResponseSOInquiry>(_ARXEDIResponseSOInquiry_QNAME, ARXEDIResponseSOInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataHeaderPO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataHeader_PO")
    public JAXBElement<ARXEDIDataHeaderPO> createARXEDIDataHeaderPO(ARXEDIDataHeaderPO value) {
        return new JAXBElement<ARXEDIDataHeaderPO>(_ARXEDIDataHeaderPO_QNAME, ARXEDIDataHeaderPO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIDataLineASN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._DataLine_ASN")
    public JAXBElement<ArrayOfARXEDIDataLineASN> createArrayOfARXEDIDataLineASN(ArrayOfARXEDIDataLineASN value) {
        return new JAXBElement<ArrayOfARXEDIDataLineASN>(_ArrayOfARXEDIDataLineASN_QNAME, ArrayOfARXEDIDataLineASN.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataHeaderSKU }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataHeader_SKU")
    public JAXBElement<ARXEDIDataHeaderSKU> createARXEDIDataHeaderSKU(ARXEDIDataHeaderSKU value) {
        return new JAXBElement<ARXEDIDataHeaderSKU>(_ARXEDIDataHeaderSKU_QNAME, ARXEDIDataHeaderSKU.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIResponseASNInquiryMultiple }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Response_ASN_InquiryMultiple")
    public JAXBElement<ARXEDIResponseASNInquiryMultiple> createARXEDIResponseASNInquiryMultiple(ARXEDIResponseASNInquiryMultiple value) {
        return new JAXBElement<ARXEDIResponseASNInquiryMultiple>(_ARXEDIResponseASNInquiryMultiple_QNAME, ARXEDIResponseASNInquiryMultiple.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDISO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._SO")
    public JAXBElement<ARXEDISO> createARXEDISO(ARXEDISO value) {
        return new JAXBElement<ARXEDISO>(_ARXEDISO_QNAME, ARXEDISO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIPackUOMEnum }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._PackUOMEnum")
    public JAXBElement<ARXEDIPackUOMEnum> createARXEDIPackUOMEnum(ARXEDIPackUOMEnum value) {
        return new JAXBElement<ARXEDIPackUOMEnum>(_ARXEDIPackUOMEnum_QNAME, ARXEDIPackUOMEnum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDISOInquiryLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._SO_Inquiry_Line")
    public JAXBElement<ARXEDISOInquiryLine> createARXEDISOInquiryLine(ARXEDISOInquiryLine value) {
        return new JAXBElement<ARXEDISOInquiryLine>(_ARXEDISOInquiryLine_QNAME, ARXEDISOInquiryLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIApplicationHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._ApplicationHeader")
    public JAXBElement<ARXEDIApplicationHeader> createARXEDIApplicationHeader(ARXEDIApplicationHeader value) {
        return new JAXBElement<ARXEDIApplicationHeader>(_ARXEDIApplicationHeader_QNAME, ARXEDIApplicationHeader.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIDataLinePO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._DataLine_PO")
    public JAXBElement<ArrayOfARXEDIDataLinePO> createArrayOfARXEDIDataLinePO(ArrayOfARXEDIDataLinePO value) {
        return new JAXBElement<ArrayOfARXEDIDataLinePO>(_ArrayOfARXEDIDataLinePO_QNAME, ArrayOfARXEDIDataLinePO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIASNInquiryWMSLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._ASN_Inquiry_WMSLine")
    public JAXBElement<ARXEDIASNInquiryWMSLine> createARXEDIASNInquiryWMSLine(ARXEDIASNInquiryWMSLine value) {
        return new JAXBElement<ARXEDIASNInquiryWMSLine>(_ARXEDIASNInquiryWMSLine_QNAME, ARXEDIASNInquiryWMSLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIStock }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Stock")
    public JAXBElement<ARXEDIStock> createARXEDIStock(ARXEDIStock value) {
        return new JAXBElement<ARXEDIStock>(_ARXEDIStock_QNAME, ARXEDIStock.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDISOLineEdit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._SO_Line_Edit")
    public JAXBElement<ARXEDISOLineEdit> createARXEDISOLineEdit(ARXEDISOLineEdit value) {
        return new JAXBElement<ARXEDISOLineEdit>(_ARXEDISOLineEdit_QNAME, ARXEDISOLineEdit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIStockLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._StockLine")
    public JAXBElement<ARXEDIStockLine> createARXEDIStockLine(ARXEDIStockLine value) {
        return new JAXBElement<ARXEDIStockLine>(_ARXEDIStockLine_QNAME, ARXEDIStockLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WMSTablesAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "WMSTables_Address")
    public JAXBElement<WMSTablesAddress> createWMSTablesAddress(WMSTablesAddress value) {
        return new JAXBElement<WMSTablesAddress>(_WMSTablesAddress_QNAME, WMSTablesAddress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDISOInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._SO_Inquiry")
    public JAXBElement<ARXEDISOInquiry> createARXEDISOInquiry(ARXEDISOInquiry value) {
        return new JAXBElement<ARXEDISOInquiry>(_ARXEDISOInquiry_QNAME, ARXEDISOInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataHeaderHttpPost }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataHeader_HttpPost")
    public JAXBElement<ARXEDIDataHeaderHttpPost> createARXEDIDataHeaderHttpPost(ARXEDIDataHeaderHttpPost value) {
        return new JAXBElement<ARXEDIDataHeaderHttpPost>(_ARXEDIDataHeaderHttpPost_QNAME, ARXEDIDataHeaderHttpPost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIEnumSOHeaderEdit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Enum_SO_Header_Edit")
    public JAXBElement<ARXEDIEnumSOHeaderEdit> createARXEDIEnumSOHeaderEdit(ARXEDIEnumSOHeaderEdit value) {
        return new JAXBElement<ARXEDIEnumSOHeaderEdit>(_ARXEDIEnumSOHeaderEdit_QNAME, ARXEDIEnumSOHeaderEdit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIPack }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._Pack")
    public JAXBElement<ArrayOfARXEDIPack> createArrayOfARXEDIPack(ArrayOfARXEDIPack value) {
        return new JAXBElement<ArrayOfARXEDIPack>(_ArrayOfARXEDIPack_QNAME, ArrayOfARXEDIPack.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIPack }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Pack")
    public JAXBElement<ARXEDIPack> createARXEDIPack(ARXEDIPack value) {
        return new JAXBElement<ARXEDIPack>(_ARXEDIPack_QNAME, ARXEDIPack.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIConsoleInquiryLineDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._Console_Inquiry_LineDetail")
    public JAXBElement<ArrayOfARXEDIConsoleInquiryLineDetail> createArrayOfARXEDIConsoleInquiryLineDetail(ArrayOfARXEDIConsoleInquiryLineDetail value) {
        return new JAXBElement<ArrayOfARXEDIConsoleInquiryLineDetail>(_ArrayOfARXEDIConsoleInquiryLineDetail_QNAME, ArrayOfARXEDIConsoleInquiryLineDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataHeaderASN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataHeader_ASN")
    public JAXBElement<ARXEDIDataHeaderASN> createARXEDIDataHeaderASN(ARXEDIDataHeaderASN value) {
        return new JAXBElement<ARXEDIDataHeaderASN>(_ARXEDIDataHeaderASN_QNAME, ARXEDIDataHeaderASN.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIConsoleInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Console_Inquiry")
    public JAXBElement<ARXEDIConsoleInquiry> createARXEDIConsoleInquiry(ARXEDIConsoleInquiry value) {
        return new JAXBElement<ARXEDIConsoleInquiry>(_ARXEDIConsoleInquiry_QNAME, ARXEDIConsoleInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIASN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._ASN")
    public JAXBElement<ARXEDIASN> createARXEDIASN(ARXEDIASN value) {
        return new JAXBElement<ARXEDIASN>(_ARXEDIASN_QNAME, ARXEDIASN.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._UserDefineData")
    public JAXBElement<ArrayOfARXEDIUserDefineData> createArrayOfARXEDIUserDefineData(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ArrayOfARXEDIUserDefineData_QNAME, ArrayOfARXEDIUserDefineData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataHeaderSO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataHeader_SO")
    public JAXBElement<ARXEDIDataHeaderSO> createARXEDIDataHeaderSO(ARXEDIDataHeaderSO value) {
        return new JAXBElement<ARXEDIDataHeaderSO>(_ARXEDIDataHeaderSO_QNAME, ARXEDIDataHeaderSO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDISOLineAdd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._SO_Line_Add")
    public JAXBElement<ARXEDISOLineAdd> createARXEDISOLineAdd(ARXEDISOLineAdd value) {
        return new JAXBElement<ARXEDISOLineAdd>(_ARXEDISOLineAdd_QNAME, ARXEDISOLineAdd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIPO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._PO")
    public JAXBElement<ARXEDIPO> createARXEDIPO(ARXEDIPO value) {
        return new JAXBElement<ARXEDIPO>(_ARXEDIPO_QNAME, ARXEDIPO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Address")
    public JAXBElement<ARXEDIAddress> createARXEDIAddress(ARXEDIAddress value) {
        return new JAXBElement<ARXEDIAddress>(_ARXEDIAddress_QNAME, ARXEDIAddress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIResponseStockSKUs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Response_Stock_SKUs")
    public JAXBElement<ARXEDIResponseStockSKUs> createARXEDIResponseStockSKUs(ARXEDIResponseStockSKUs value) {
        return new JAXBElement<ARXEDIResponseStockSKUs>(_ARXEDIResponseStockSKUs_QNAME, ARXEDIResponseStockSKUs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIDataLineSO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._DataLine_SO")
    public JAXBElement<ArrayOfARXEDIDataLineSO> createArrayOfARXEDIDataLineSO(ArrayOfARXEDIDataLineSO value) {
        return new JAXBElement<ArrayOfARXEDIDataLineSO>(_ArrayOfARXEDIDataLineSO_QNAME, ArrayOfARXEDIDataLineSO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfWMSTablesAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "ArrayOfWMSTables_Address")
    public JAXBElement<ArrayOfWMSTablesAddress> createArrayOfWMSTablesAddress(ArrayOfWMSTablesAddress value) {
        return new JAXBElement<ArrayOfWMSTablesAddress>(_ArrayOfWMSTablesAddress_QNAME, ArrayOfWMSTablesAddress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Response")
    public JAXBElement<ARXEDIResponse> createARXEDIResponse(ARXEDIResponse value) {
        return new JAXBElement<ARXEDIResponse>(_ARXEDIResponse_QNAME, ARXEDIResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataHeaderStockSKUs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataHeader_Stock_SKUs")
    public JAXBElement<ARXEDIDataHeaderStockSKUs> createARXEDIDataHeaderStockSKUs(ARXEDIDataHeaderStockSKUs value) {
        return new JAXBElement<ARXEDIDataHeaderStockSKUs>(_ARXEDIDataHeaderStockSKUs_QNAME, ARXEDIDataHeaderStockSKUs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIResponseConsoleInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Response_Console_Inquiry")
    public JAXBElement<ARXEDIResponseConsoleInquiry> createARXEDIResponseConsoleInquiry(ARXEDIResponseConsoleInquiry value) {
        return new JAXBElement<ARXEDIResponseConsoleInquiry>(_ARXEDIResponseConsoleInquiry_QNAME, ARXEDIResponseConsoleInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIResponseASNInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Response_ASN_Inquiry")
    public JAXBElement<ARXEDIResponseASNInquiry> createARXEDIResponseASNInquiry(ARXEDIResponseASNInquiry value) {
        return new JAXBElement<ARXEDIResponseASNInquiry>(_ARXEDIResponseASNInquiry_QNAME, ARXEDIResponseASNInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIStockSKUsLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Stock_SKUsLine")
    public JAXBElement<ARXEDIStockSKUsLine> createARXEDIStockSKUsLine(ARXEDIStockSKUsLine value) {
        return new JAXBElement<ARXEDIStockSKUsLine>(_ARXEDIStockSKUsLine_QNAME, ARXEDIStockSKUsLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIStockSKUsLineDIMs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Stock_SKUsLine_DIMs")
    public JAXBElement<ARXEDIStockSKUsLineDIMs> createARXEDIStockSKUsLineDIMs(ARXEDIStockSKUsLineDIMs value) {
        return new JAXBElement<ARXEDIStockSKUsLineDIMs>(_ARXEDIStockSKUsLineDIMs_QNAME, ARXEDIStockSKUsLineDIMs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDISKU }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._SKU")
    public JAXBElement<ARXEDISKU> createARXEDISKU(ARXEDISKU value) {
        return new JAXBElement<ARXEDISKU>(_ARXEDISKU_QNAME, ARXEDISKU.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataHeaderASNInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataHeader_ASN_Inquiry")
    public JAXBElement<ARXEDIDataHeaderASNInquiry> createARXEDIDataHeaderASNInquiry(ARXEDIDataHeaderASNInquiry value) {
        return new JAXBElement<ARXEDIDataHeaderASNInquiry>(_ARXEDIDataHeaderASNInquiry_QNAME, ARXEDIDataHeaderASNInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIASNLineEdit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._ASN_Line_Edit")
    public JAXBElement<ARXEDIASNLineEdit> createARXEDIASNLineEdit(ARXEDIASNLineEdit value) {
        return new JAXBElement<ARXEDIASNLineEdit>(_ARXEDIASNLineEdit_QNAME, ARXEDIASNLineEdit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIConsoleInquiryLineDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Console_Inquiry_LineDetail")
    public JAXBElement<ARXEDIConsoleInquiryLineDetail> createARXEDIConsoleInquiryLineDetail(ARXEDIConsoleInquiryLineDetail value) {
        return new JAXBElement<ARXEDIConsoleInquiryLineDetail>(_ARXEDIConsoleInquiryLineDetail_QNAME, ARXEDIConsoleInquiryLineDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIDataHeaderSKU }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._DataHeader_SKU")
    public JAXBElement<ArrayOfARXEDIDataHeaderSKU> createArrayOfARXEDIDataHeaderSKU(ArrayOfARXEDIDataHeaderSKU value) {
        return new JAXBElement<ArrayOfARXEDIDataHeaderSKU>(_ArrayOfARXEDIDataHeaderSKU_QNAME, ArrayOfARXEDIDataHeaderSKU.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDISOInquiryWMSLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._SO_Inquiry_WMSLine")
    public JAXBElement<ArrayOfARXEDISOInquiryWMSLine> createArrayOfARXEDISOInquiryWMSLine(ArrayOfARXEDISOInquiryWMSLine value) {
        return new JAXBElement<ArrayOfARXEDISOInquiryWMSLine>(_ArrayOfARXEDISOInquiryWMSLine_QNAME, ArrayOfARXEDISOInquiryWMSLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataHeaderStock }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataHeader_Stock")
    public JAXBElement<ARXEDIDataHeaderStock> createARXEDIDataHeaderStock(ARXEDIDataHeaderStock value) {
        return new JAXBElement<ARXEDIDataHeaderStock>(_ARXEDIDataHeaderStock_QNAME, ARXEDIDataHeaderStock.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIResponseASNInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._Response_ASN_Inquiry")
    public JAXBElement<ArrayOfARXEDIResponseASNInquiry> createArrayOfARXEDIResponseASNInquiry(ArrayOfARXEDIResponseASNInquiry value) {
        return new JAXBElement<ArrayOfARXEDIResponseASNInquiry>(_ArrayOfARXEDIResponseASNInquiry_QNAME, ArrayOfARXEDIResponseASNInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIBOM }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._BOM")
    public JAXBElement<ArrayOfARXEDIBOM> createArrayOfARXEDIBOM(ArrayOfARXEDIBOM value) {
        return new JAXBElement<ArrayOfARXEDIBOM>(_ArrayOfARXEDIBOM_QNAME, ArrayOfARXEDIBOM.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIStockSKUsLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._Stock_SKUsLine")
    public JAXBElement<ArrayOfARXEDIStockSKUsLine> createArrayOfARXEDIStockSKUsLine(ArrayOfARXEDIStockSKUsLine value) {
        return new JAXBElement<ArrayOfARXEDIStockSKUsLine>(_ArrayOfARXEDIStockSKUsLine_QNAME, ArrayOfARXEDIStockSKUsLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataLineSO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataLine_SO")
    public JAXBElement<ARXEDIDataLineSO> createARXEDIDataLineSO(ARXEDIDataLineSO value) {
        return new JAXBElement<ARXEDIDataLineSO>(_ARXEDIDataLineSO_QNAME, ARXEDIDataLineSO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIStockSKUs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._Stock_SKUs")
    public JAXBElement<ARXEDIStockSKUs> createARXEDIStockSKUs(ARXEDIStockSKUs value) {
        return new JAXBElement<ARXEDIStockSKUs>(_ARXEDIStockSKUs_QNAME, ARXEDIStockSKUs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIDataHeaderConsoleInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._DataHeader_Console_Inquiry")
    public JAXBElement<ARXEDIDataHeaderConsoleInquiry> createARXEDIDataHeaderConsoleInquiry(ARXEDIDataHeaderConsoleInquiry value) {
        return new JAXBElement<ARXEDIDataHeaderConsoleInquiry>(_ARXEDIDataHeaderConsoleInquiry_QNAME, ARXEDIDataHeaderConsoleInquiry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDISOInquiryWMSLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._SO_Inquiry_WMSLine")
    public JAXBElement<ARXEDISOInquiryWMSLine> createARXEDISOInquiryWMSLine(ARXEDISOInquiryWMSLine value) {
        return new JAXBElement<ARXEDISOInquiryWMSLine>(_ARXEDISOInquiryWMSLine_QNAME, ARXEDISOInquiryWMSLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIHttpPost }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._HttpPost")
    public JAXBElement<ARXEDIHttpPost> createARXEDIHttpPost(ARXEDIHttpPost value) {
        return new JAXBElement<ARXEDIHttpPost>(_ARXEDIHttpPost_QNAME, ARXEDIHttpPost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDISOInquiryLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._SO_Inquiry_Line")
    public JAXBElement<ArrayOfARXEDISOInquiryLine> createArrayOfARXEDISOInquiryLine(ArrayOfARXEDISOInquiryLine value) {
        return new JAXBElement<ArrayOfARXEDISOInquiryLine>(_ArrayOfARXEDISOInquiryLine_QNAME, ArrayOfARXEDISOInquiryLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDIBOM }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._BOM")
    public JAXBElement<ARXEDIBOM> createARXEDIBOM(ARXEDIBOM value) {
        return new JAXBElement<ARXEDIBOM>(_ARXEDIBOM_QNAME, ARXEDIBOM.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIStockLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ArrayOfARX_EDI._StockLine")
    public JAXBElement<ArrayOfARXEDIStockLine> createArrayOfARXEDIStockLine(ArrayOfARXEDIStockLine value) {
        return new JAXBElement<ArrayOfARXEDIStockLine>(_ArrayOfARXEDIStockLine_QNAME, ArrayOfARXEDIStockLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ARXEDISOHeaderEdit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_EDI._SO_Header_Edit")
    public JAXBElement<ARXEDISOHeaderEdit> createARXEDISOHeaderEdit(ARXEDISOHeaderEdit value) {
        return new JAXBElement<ARXEDISOHeaderEdit>(_ARXEDISOHeaderEdit_QNAME, ARXEDISOHeaderEdit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Name", scope = ARXEDIPack.class)
    public JAXBElement<String> createARXEDIPackName(String value) {
        return new JAXBElement<String>(_ARXEDIPackName_QNAME, String.class, ARXEDIPack.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDIStockSKUs.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDIStockSKUsUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDIStockSKUs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SAPAcDoNo", scope = ARXEDISAPResponse.class)
    public JAXBElement<String> createARXEDISAPResponseSAPAcDoNo(String value) {
        return new JAXBElement<String>(_ARXEDISAPResponseSAPAcDoNo_QNAME, String.class, ARXEDISAPResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDISAPResponse.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDISAPResponseUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDISAPResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SAPCurrency", scope = ARXEDISAPResponse.class)
    public JAXBElement<String> createARXEDISAPResponseSAPCurrency(String value) {
        return new JAXBElement<String>(_ARXEDISAPResponseSAPCurrency_QNAME, String.class, ARXEDISAPResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Notes", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSONotes(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSONotes_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOSKU_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable08", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOLottable08(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable08_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable07", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOLottable07(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable07_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ALTSKU", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOALTSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOALTSKU_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SubInvoice", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOSubInvoice(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOSubInvoice_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable09", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOLottable09(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable09_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR5", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSODSUSR5(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR5_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable04", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOLottable04(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable04_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable05", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOLottable05(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable05_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "MANUFACTURERSKU", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOMANUFACTURERSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOMANUFACTURERSKU_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR2", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSODSUSR2(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR2_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExternLineNo", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOExternLineNo(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOExternLineNo_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable10", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOLottable10(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable10_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR3", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSODSUSR3(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR3_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable02", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOLottable02(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable02_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR4", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSODSUSR4(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR4_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ARX_ReceiptKey", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOARXReceiptKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOARXReceiptKey_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "PickingInstructions", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOPickingInstructions(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOPickingInstructions_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDIDataLineSO.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDIDataLineSOUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "COO", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSOCOO(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOCOO_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "RETAILSKU", scope = ARXEDIDataLineSO.class)
    public JAXBElement<String> createARXEDIDataLineSORETAILSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSORETAILSKU_QNAME, String.class, ARXEDIDataLineSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "OrderKey", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailOrderKey(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailOrderKey_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "AllocateDate", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailAllocateDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAllocateDate_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "LatestWHStatus", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailLatestWHStatus(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailLatestWHStatus_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "AddDate", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailAddDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAddDate_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SOTDate", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailSOTDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailSOTDate_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "aramexService", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailAramexService(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAramexService_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "PickDate", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailPickDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailPickDate_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Forwarder", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailForwarder(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailForwarder_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "AWB", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailAWB(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAWB_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ActualShipDate", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailActualShipDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailActualShipDate_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "aramexCODCurrency", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailAramexCODCurrency(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAramexCODCurrency_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "EditDate", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailEditDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailEditDate_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExternalOrderKey", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailExternalOrderKey(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailExternalOrderKey_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "aramexCODValue", scope = ARXEDIConsoleInquiryLineDetail.class)
    public JAXBElement<String> createARXEDIConsoleInquiryLineDetailAramexCODValue(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAramexCODValue_QNAME, String.class, ARXEDIConsoleInquiryLineDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "L", scope = ARXEDIStockSKUsLineDIMs.class)
    public JAXBElement<String> createARXEDIStockSKUsLineDIMsL(String value) {
        return new JAXBElement<String>(_ARXEDIStockSKUsLineDIMsL_QNAME, String.class, ARXEDIStockSKUsLineDIMs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "H", scope = ARXEDIStockSKUsLineDIMs.class)
    public JAXBElement<String> createARXEDIStockSKUsLineDIMsH(String value) {
        return new JAXBElement<String>(_ARXEDIStockSKUsLineDIMsH_QNAME, String.class, ARXEDIStockSKUsLineDIMs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "W", scope = ARXEDIStockSKUsLineDIMs.class)
    public JAXBElement<String> createARXEDIStockSKUsLineDIMsW(String value) {
        return new JAXBElement<String>(_ARXEDIStockSKUsLineDIMsW_QNAME, String.class, ARXEDIStockSKUsLineDIMs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDIDataHeaderStock.class)
    public JAXBElement<String> createARXEDIDataHeaderStockFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDIDataHeaderStock.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU_Contains", scope = ARXEDIDataHeaderStock.class)
    public JAXBElement<String> createARXEDIDataHeaderStockSKUContains(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockSKUContains_QNAME, String.class, ARXEDIDataHeaderStock.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDIDataHeaderStock.class)
    public JAXBElement<String> createARXEDIDataHeaderStockStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDIDataHeaderStock.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Fax", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressFax(String value) {
        return new JAXBElement<String>(_ARXEDIAddressFax_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "W3W", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressW3W(String value) {
        return new JAXBElement<String>(_ARXEDIAddressW3W_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Country", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressCountry(String value) {
        return new JAXBElement<String>(_ARXEDIAddressCountry_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "State", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressState(String value) {
        return new JAXBElement<String>(_ARXEDIAddressState_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Phone2", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressPhone2(String value) {
        return new JAXBElement<String>(_ARXEDIAddressPhone2_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Phone1", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressPhone1(String value) {
        return new JAXBElement<String>(_ARXEDIAddressPhone1_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Ref", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressRef(String value) {
        return new JAXBElement<String>(_ARXEDIAddressRef_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Contact", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressContact(String value) {
        return new JAXBElement<String>(_ARXEDIAddressContact_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Address1", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressAddress1(String value) {
        return new JAXBElement<String>(_ARXEDIAddressAddress1_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "City", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressCity(String value) {
        return new JAXBElement<String>(_ARXEDIAddressCity_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Address3", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressAddress3(String value) {
        return new JAXBElement<String>(_ARXEDIAddressAddress3_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Address2", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressAddress2(String value) {
        return new JAXBElement<String>(_ARXEDIAddressAddress2_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "C_ID", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressCID(String value) {
        return new JAXBElement<String>(_ARXEDIAddressCID_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ZipCode", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressZipCode(String value) {
        return new JAXBElement<String>(_ARXEDIAddressZipCode_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Email", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressEmail(String value) {
        return new JAXBElement<String>(_ARXEDIAddressEmail_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Company", scope = ARXEDIAddress.class)
    public JAXBElement<String> createARXEDIAddressCompany(String value) {
        return new JAXBElement<String>(_ARXEDIAddressCompany_QNAME, String.class, ARXEDIAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SHELFLIFEINDICATOR", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSHELFLIFEINDICATOR(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSHELFLIFEINDICATOR_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "LottableValidationKey", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKULottableValidationKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKULottableValidationKey_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "COLOR", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUCOLOR(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUCOLOR_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "HSCode", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUHSCode(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUHSCode_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR10", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSUSR10(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR10_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "CAMPAIGNSTART", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUCAMPAIGNSTART(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUCAMPAIGNSTART_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ManufacturerSKU", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUManufacturerSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUManufacturerSKU_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIPack }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Packs", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<ArrayOfARXEDIPack> createARXEDIDataHeaderSKUPacks(ArrayOfARXEDIPack value) {
        return new JAXBElement<ArrayOfARXEDIPack>(_ARXEDIDataHeaderSKUPacks_QNAME, ArrayOfARXEDIPack.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ITEMCHARACTERISTIC1", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUITEMCHARACTERISTIC1(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUITEMCHARACTERISTIC1_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIBOM }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "BOMs", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<ArrayOfARXEDIBOM> createARXEDIDataHeaderSKUBOMs(ArrayOfARXEDIBOM value) {
        return new JAXBElement<ArrayOfARXEDIBOM>(_ARXEDIDataHeaderSKUBOMs_QNAME, ArrayOfARXEDIBOM.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKUSIZE", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSKUSIZE(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSKUSIZE_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ITEMCHARACTERISTIC2", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUITEMCHARACTERISTIC2(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUITEMCHARACTERISTIC2_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UPC", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUUPC(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUUPC_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDIDataHeaderSKUUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Description", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUDescription(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUDescription_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "COLLECTION", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUCOLLECTION(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUCOLLECTION_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOSKU_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "BillingGroup", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUBillingGroup(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUBillingGroup_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "COUNTRYOFORIGIN", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUCOUNTRYOFORIGIN(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUCOUNTRYOFORIGIN_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SHELFLIFECODETYPE", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSHELFLIFECODETYPE(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSHELFLIFECODETYPE_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR7", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSUSR7(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR7_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SEASON", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSEASON(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSEASON_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR8", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSUSR8(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR8_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR5", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSUSR5(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR5_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UPC_UOM", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUUPCUOM(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUUPCUOM_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "CAMPAIGNEND", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUCAMPAIGNEND(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUCAMPAIGNEND_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR6", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSUSR6(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR6_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR4", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSUSR4(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR4_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "STYLE", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSTYLE(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSTYLE_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR2", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSUSR2(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR2_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "THEME", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUTHEME(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUTHEME_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR9", scope = ARXEDIDataHeaderSKU.class)
    public JAXBElement<String> createARXEDIDataHeaderSKUSUSR9(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR9_QNAME, String.class, ARXEDIDataHeaderSKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDIASNLineEdit.class)
    public JAXBElement<String> createARXEDIASNLineEditFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDIASNLineEdit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDIASNLineEdit.class)
    public JAXBElement<String> createARXEDIASNLineEditClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDIASNLineEdit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDIASNLineEdit.class)
    public JAXBElement<String> createARXEDIASNLineEditStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDIASNLineEdit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ForwarderWayBill", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOForwarderWayBill(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOForwarderWayBill_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Notes", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSONotes(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSONotes_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "RequestedBy", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSORequestedBy(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSORequestedBy_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "CTSService", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOCTSService(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOCTSService_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Currency", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOCurrency(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOCurrency_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "REFNO", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOREFNO(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOREFNO_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ConsoleKey", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOConsoleKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOConsoleKey_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Type", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOType(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOType_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Forwarder", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOForwarder(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailForwarder_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR5", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOSUSR5(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR5_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "INCOTERM", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOINCOTERM(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOINCOTERM_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "PRIORITY", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOPRIORITY(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOPRIORITY_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "CTSValue", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOCTSValue(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOCTSValue_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExternalOrderKey", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOExternalOrderKey(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailExternalOrderKey_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SuspendedIndicator", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOSuspendedIndicator(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOSuspendedIndicator_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "TransportationMode", scope = ARXEDIDataHeaderSO.class)
    public JAXBElement<String> createARXEDIDataHeaderSOTransportationMode(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOTransportationMode_QNAME, String.class, ARXEDIDataHeaderSO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Key", scope = ARXEDIUserDefineData.class)
    public JAXBElement<String> createARXEDIUserDefineDataKey(String value) {
        return new JAXBElement<String>(_ARXEDIUserDefineDataKey_QNAME, String.class, ARXEDIUserDefineData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Value", scope = ARXEDIUserDefineData.class)
    public JAXBElement<String> createARXEDIUserDefineDataValue(String value) {
        return new JAXBElement<String>(_ARXEDIUserDefineDataValue_QNAME, String.class, ARXEDIUserDefineData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Notes", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPONotes(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSONotes_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "EXTERNALPOKEY2", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOEXTERNALPOKEY2(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderPOEXTERNALPOKEY2_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "INCOTERMS", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOINCOTERMS(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderPOINCOTERMS_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Currency", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOCurrency(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOCurrency_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Type", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOType(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOType_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR5", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOSUSR5(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR5_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR3", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOSUSR3(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderPOSUSR3_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR4", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOSUSR4(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR4_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR1", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOSUSR1(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderPOSUSR1_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR2", scope = ARXEDIDataHeaderPO.class)
    public JAXBElement<String> createARXEDIDataHeaderPOSUSR2(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR2_QNAME, String.class, ARXEDIDataHeaderPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDISOLineEdit.class)
    public JAXBElement<String> createARXEDISOLineEditFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDISOLineEdit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDISOLineEdit.class)
    public JAXBElement<String> createARXEDISOLineEditClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDISOLineEdit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDISOLineEdit.class)
    public JAXBElement<String> createARXEDISOLineEditStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDISOLineEdit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIStockLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lines", scope = ARXEDIResponseStock.class)
    public JAXBElement<ArrayOfARXEDIStockLine> createARXEDIResponseStockLines(ArrayOfARXEDIStockLine value) {
        return new JAXBElement<ArrayOfARXEDIStockLine>(_ARXEDIResponseStockLines_QNAME, ArrayOfARXEDIStockLine.class, ARXEDIResponseStock.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIStockSKUsLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lines", scope = ARXEDIResponseStockSKUs.class)
    public JAXBElement<ArrayOfARXEDIStockSKUsLine> createARXEDIResponseStockSKUsLines(ArrayOfARXEDIStockSKUsLine value) {
        return new JAXBElement<ArrayOfARXEDIStockSKUsLine>(_ARXEDIResponseStockLines_QNAME, ArrayOfARXEDIStockSKUsLine.class, ARXEDIResponseStockSKUs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIDataLineASN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DataLines", scope = ARXEDIASN.class)
    public JAXBElement<ArrayOfARXEDIDataLineASN> createARXEDIASNDataLines(ArrayOfARXEDIDataLineASN value) {
        return new JAXBElement<ArrayOfARXEDIDataLineASN>(_ARXEDIASNDataLines_QNAME, ArrayOfARXEDIDataLineASN.class, ARXEDIASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfWMSTablesAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExtraAddresses", scope = ARXEDIASN.class)
    public JAXBElement<ArrayOfWMSTablesAddress> createARXEDIASNExtraAddresses(ArrayOfWMSTablesAddress value) {
        return new JAXBElement<ArrayOfWMSTablesAddress>(_ARXEDIASNExtraAddresses_QNAME, ArrayOfWMSTablesAddress.class, ARXEDIASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDIASN.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDIASNUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDIASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_OperationType", scope = UpdatePOLines.class)
    public JAXBElement<String> createUpdatePOLinesOperationType(String value) {
        return new JAXBElement<String>(_UpdatePOLinesOperationType_QNAME, String.class, UpdatePOLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "WMSRef", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressWMSRef(String value) {
        return new JAXBElement<String>(_WMSTablesAddressWMSRef_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Address2", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressAddress2(String value) {
        return new JAXBElement<String>(_WMSTablesAddressAddress2_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Address3", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressAddress3(String value) {
        return new JAXBElement<String>(_WMSTablesAddressAddress3_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Address1", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressAddress1(String value) {
        return new JAXBElement<String>(_WMSTablesAddressAddress1_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "City", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressCity(String value) {
        return new JAXBElement<String>(_WMSTablesAddressCity_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Email", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressEmail(String value) {
        return new JAXBElement<String>(_WMSTablesAddressEmail_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Name1", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressName1(String value) {
        return new JAXBElement<String>(_WMSTablesAddressName1_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Postal", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressPostal(String value) {
        return new JAXBElement<String>(_WMSTablesAddressPostal_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Name2", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressName2(String value) {
        return new JAXBElement<String>(_WMSTablesAddressName2_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Country", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressCountry(String value) {
        return new JAXBElement<String>(_WMSTablesAddressCountry_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Telephone1", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressTelephone1(String value) {
        return new JAXBElement<String>(_WMSTablesAddressTelephone1_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Address4", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressAddress4(String value) {
        return new JAXBElement<String>(_WMSTablesAddressAddress4_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Telephone2", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressTelephone2(String value) {
        return new JAXBElement<String>(_WMSTablesAddressTelephone2_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Name3", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressName3(String value) {
        return new JAXBElement<String>(_WMSTablesAddressName3_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Name4", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressName4(String value) {
        return new JAXBElement<String>(_WMSTablesAddressName4_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Ref", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressRef(String value) {
        return new JAXBElement<String>(_WMSTablesAddressRef_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", name = "Type", scope = WMSTablesAddress.class)
    public JAXBElement<String> createWMSTablesAddressType(String value) {
        return new JAXBElement<String>(_WMSTablesAddressType_QNAME, String.class, WMSTablesAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Notes", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePONotes(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSONotes_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePOSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOSKU_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "HSCODE", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePOHSCODE(String value) {
        return new JAXBElement<String>(_ARXEDIDataLinePOHSCODE_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ManufacturerSKU", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePOManufacturerSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUManufacturerSKU_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "RetailSKU", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePORetailSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLinePORetailSKU_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExternLineNo", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePOExternLineNo(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOExternLineNo_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR5", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePOSUSR5(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR5_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Descr", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePODescr(String value) {
        return new JAXBElement<String>(_ARXEDIDataLinePODescr_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR3", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePOSUSR3(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderPOSUSR3_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR4", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePOSUSR4(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR4_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR1", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePOSUSR1(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderPOSUSR1_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDIDataLinePO.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDIDataLinePOUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SUSR2", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePOSUSR2(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUSUSR2_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "AltSKU", scope = ARXEDIDataLinePO.class)
    public JAXBElement<String> createARXEDIDataLinePOAltSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLinePOAltSKU_QNAME, String.class, ARXEDIDataLinePO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDISOInquiryLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lines", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<ArrayOfARXEDISOInquiryLine> createARXEDIResponseSOInquiryLines(ArrayOfARXEDISOInquiryLine value) {
        return new JAXBElement<ArrayOfARXEDISOInquiryLine>(_ARXEDIResponseStockLines_QNAME, ArrayOfARXEDISOInquiryLine.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "OrderKey", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryOrderKey(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailOrderKey_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "AllocateDate", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryAllocateDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAllocateDate_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "LatestWHStatus", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryLatestWHStatus(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailLatestWHStatus_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "LatestTrackingStatus", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryLatestTrackingStatus(String value) {
        return new JAXBElement<String>(_ARXEDIResponseSOInquiryLatestTrackingStatus_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "AddDate", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryAddDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAddDate_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SOTDate", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquirySOTDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailSOTDate_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Type", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryType(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOType_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "aramexService", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryAramexService(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAramexService_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "PickDate", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryPickDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailPickDate_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Forwarder", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryForwarder(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailForwarder_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "AWB", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryAWB(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAWB_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ActualShipDate", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryActualShipDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailActualShipDate_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExternalOrderKey2", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryExternalOrderKey2(String value) {
        return new JAXBElement<String>(_ARXEDIResponseSOInquiryExternalOrderKey2_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDISOInquiryWMSLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "WMSLines", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<ArrayOfARXEDISOInquiryWMSLine> createARXEDIResponseSOInquiryWMSLines(ArrayOfARXEDISOInquiryWMSLine value) {
        return new JAXBElement<ArrayOfARXEDISOInquiryWMSLine>(_ARXEDIResponseSOInquiryWMSLines_QNAME, ArrayOfARXEDISOInquiryWMSLine.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "aramexCODCurrency", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryAramexCODCurrency(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAramexCODCurrency_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "EditDate", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryEditDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailEditDate_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExternalOrderKey", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryExternalOrderKey(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailExternalOrderKey_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "LatestStatusDate", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryLatestStatusDate(String value) {
        return new JAXBElement<String>(_ARXEDIResponseSOInquiryLatestStatusDate_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "aramexCODValue", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryAramexCODValue(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAramexCODValue_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "LatestStatus", scope = ARXEDIResponseSOInquiry.class)
    public JAXBElement<String> createARXEDIResponseSOInquiryLatestStatus(String value) {
        return new JAXBElement<String>(_ARXEDIResponseSOInquiryLatestStatus_QNAME, String.class, ARXEDIResponseSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "TransactionID", scope = ARXEDIResponse.class)
    public JAXBElement<String> createARXEDIResponseTransactionID(String value) {
        return new JAXBElement<String>(_ARXEDIResponseTransactionID_QNAME, String.class, ARXEDIResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ErrorCode", scope = ARXEDIResponse.class)
    public JAXBElement<String> createARXEDIResponseErrorCode(String value) {
        return new JAXBElement<String>(_ARXEDIResponseErrorCode_QNAME, String.class, ARXEDIResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Reference", scope = ARXEDIResponse.class)
    public JAXBElement<String> createARXEDIResponseReference(String value) {
        return new JAXBElement<String>(_ARXEDIResponseReference_QNAME, String.class, ARXEDIResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ErrorType", scope = ARXEDIResponse.class)
    public JAXBElement<String> createARXEDIResponseErrorType(String value) {
        return new JAXBElement<String>(_ARXEDIResponseErrorType_QNAME, String.class, ARXEDIResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ErrorDescription", scope = ARXEDIResponse.class)
    public JAXBElement<String> createARXEDIResponseErrorDescription(String value) {
        return new JAXBElement<String>(_ARXEDIResponseErrorDescription_QNAME, String.class, ARXEDIResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Status", scope = ARXEDIResponse.class)
    public JAXBElement<String> createARXEDIResponseStatus(String value) {
        return new JAXBElement<String>(_ARXEDIResponseStatus_QNAME, String.class, ARXEDIResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Descr", scope = ARXEDIStockSKUsLine.class)
    public JAXBElement<String> createARXEDIStockSKUsLineDescr(String value) {
        return new JAXBElement<String>(_ARXEDIDataLinePODescr_QNAME, String.class, ARXEDIStockSKUsLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU", scope = ARXEDIStockSKUsLine.class)
    public JAXBElement<String> createARXEDIStockSKUsLineSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOSKU_QNAME, String.class, ARXEDIStockSKUsLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ALTSKUs", scope = ARXEDIStockSKUsLine.class)
    public JAXBElement<ArrayOfstring> createARXEDIStockSKUsLineALTSKUs(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_ARXEDIStockSKUsLineALTSKUs_QNAME, ArrayOfstring.class, ARXEDIStockSKUsLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDISOLineAdd.class)
    public JAXBElement<String> createARXEDISOLineAddFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDISOLineAdd.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDISOLineAdd.class)
    public JAXBElement<String> createARXEDISOLineAddClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDISOLineAdd.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDISOLineAdd.class)
    public JAXBElement<String> createARXEDISOLineAddStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDISOLineAdd.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDIASNInquiry.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDIASNInquiryUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDIASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIDataHeaderSKU }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DataHeader", scope = ARXEDISKUs.class)
    public JAXBElement<ArrayOfARXEDIDataHeaderSKU> createARXEDISKUsDataHeader(ArrayOfARXEDIDataHeaderSKU value) {
        return new JAXBElement<ArrayOfARXEDIDataHeaderSKU>(_ARXEDISKUsDataHeader_QNAME, ArrayOfARXEDIDataHeaderSKU.class, ARXEDISKUs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDISKUs.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDISKUsUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDISKUs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "MAWB", scope = ARXEDIDataHeaderConsoleInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderConsoleInquiryMAWB(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderConsoleInquiryMAWB_QNAME, String.class, ARXEDIDataHeaderConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDIDataHeaderConsoleInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderConsoleInquiryFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDIDataHeaderConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDIDataHeaderConsoleInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderConsoleInquiryStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDIDataHeaderConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ConsoleKey", scope = ARXEDIDataHeaderConsoleInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderConsoleInquiryConsoleKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOConsoleKey_QNAME, String.class, ARXEDIDataHeaderConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR2", scope = ARXEDISOInquiryWMSLine.class)
    public JAXBElement<String> createARXEDISOInquiryWMSLineDSUSR2(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR2_QNAME, String.class, ARXEDISOInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExternLineNo", scope = ARXEDISOInquiryWMSLine.class)
    public JAXBElement<String> createARXEDISOInquiryWMSLineExternLineNo(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOExternLineNo_QNAME, String.class, ARXEDISOInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR3", scope = ARXEDISOInquiryWMSLine.class)
    public JAXBElement<String> createARXEDISOInquiryWMSLineDSUSR3(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR3_QNAME, String.class, ARXEDISOInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR4", scope = ARXEDISOInquiryWMSLine.class)
    public JAXBElement<String> createARXEDISOInquiryWMSLineDSUSR4(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR4_QNAME, String.class, ARXEDISOInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU", scope = ARXEDISOInquiryWMSLine.class)
    public JAXBElement<String> createARXEDISOInquiryWMSLineSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOSKU_QNAME, String.class, ARXEDISOInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR5", scope = ARXEDISOInquiryWMSLine.class)
    public JAXBElement<String> createARXEDISOInquiryWMSLineDSUSR5(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR5_QNAME, String.class, ARXEDISOInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ComponentSKU", scope = ARXEDIBOM.class)
    public JAXBElement<String> createARXEDIBOMComponentSKU(String value) {
        return new JAXBElement<String>(_ARXEDIBOMComponentSKU_QNAME, String.class, ARXEDIBOM.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ComponentQTY", scope = ARXEDIBOM.class)
    public JAXBElement<String> createARXEDIBOMComponentQTY(String value) {
        return new JAXBElement<String>(_ARXEDIBOMComponentQTY_QNAME, String.class, ARXEDIBOM.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Notes", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNNotes(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSONotes_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "BOE", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNBOE(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineASNBOE_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "PurchaseOrderDocument", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNPurchaseOrderDocument(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineASNPurchaseOrderDocument_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR5", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNDSUSR5(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR5_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "PurchaseOrderLine", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNPurchaseOrderLine(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineASNPurchaseOrderLine_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR2", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNDSUSR2(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR2_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExternLineNo", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNExternLineNo(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOExternLineNo_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR3", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNDSUSR3(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR3_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable02", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNLottable02(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable02_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DSUSR4", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNDSUSR4(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSODSUSR4_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDIDataLineASN.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDIDataLineASNUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "D_RMA", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNDRMA(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineASNDRMA_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "POLINENUMBER", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNPOLINENUMBER(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineASNPOLINENUMBER_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "OriginalLOT", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNOriginalLOT(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineASNOriginalLOT_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOSKU_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "LinePO", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNLinePO(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineASNLinePO_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable08", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNLottable08(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable08_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "HSCODE", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNHSCODE(String value) {
        return new JAXBElement<String>(_ARXEDIDataLinePOHSCODE_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable07", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNLottable07(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable07_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable09", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNLottable09(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable09_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable04", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNLottable04(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable04_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable05", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNLottable05(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable05_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable10", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNLottable10(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable10_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "POKEY", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNPOKEY(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineASNPOKEY_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "COO", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNCOO(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOCOO_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "CDOrderKey", scope = ARXEDIDataLineASN.class)
    public JAXBElement<String> createARXEDIDataLineASNCDOrderKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineASNCDOrderKey_QNAME, String.class, ARXEDIDataLineASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExternLineNo", scope = ARXEDIASNInquiryWMSLine.class)
    public JAXBElement<String> createARXEDIASNInquiryWMSLineExternLineNo(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOExternLineNo_QNAME, String.class, ARXEDIASNInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable10", scope = ARXEDIASNInquiryWMSLine.class)
    public JAXBElement<String> createARXEDIASNInquiryWMSLineLottable10(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable10_QNAME, String.class, ARXEDIASNInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable02", scope = ARXEDIASNInquiryWMSLine.class)
    public JAXBElement<String> createARXEDIASNInquiryWMSLineLottable02(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable02_QNAME, String.class, ARXEDIASNInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable01", scope = ARXEDIASNInquiryWMSLine.class)
    public JAXBElement<String> createARXEDIASNInquiryWMSLineLottable01(String value) {
        return new JAXBElement<String>(_ARXEDIASNInquiryWMSLineLottable01_QNAME, String.class, ARXEDIASNInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU", scope = ARXEDIASNInquiryWMSLine.class)
    public JAXBElement<String> createARXEDIASNInquiryWMSLineSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOSKU_QNAME, String.class, ARXEDIASNInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable08", scope = ARXEDIASNInquiryWMSLine.class)
    public JAXBElement<String> createARXEDIASNInquiryWMSLineLottable08(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable08_QNAME, String.class, ARXEDIASNInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable07", scope = ARXEDIASNInquiryWMSLine.class)
    public JAXBElement<String> createARXEDIASNInquiryWMSLineLottable07(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable07_QNAME, String.class, ARXEDIASNInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExpDate", scope = ARXEDIASNInquiryWMSLine.class)
    public JAXBElement<String> createARXEDIASNInquiryWMSLineExpDate(String value) {
        return new JAXBElement<String>(_ARXEDIASNInquiryWMSLineExpDate_QNAME, String.class, ARXEDIASNInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable09", scope = ARXEDIASNInquiryWMSLine.class)
    public JAXBElement<String> createARXEDIASNInquiryWMSLineLottable09(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable09_QNAME, String.class, ARXEDIASNInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ManuDate", scope = ARXEDIASNInquiryWMSLine.class)
    public JAXBElement<String> createARXEDIASNInquiryWMSLineManuDate(String value) {
        return new JAXBElement<String>(_ARXEDIASNInquiryWMSLineManuDate_QNAME, String.class, ARXEDIASNInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "COO", scope = ARXEDIASNInquiryWMSLine.class)
    public JAXBElement<String> createARXEDIASNInquiryWMSLineCOO(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOCOO_QNAME, String.class, ARXEDIASNInquiryWMSLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIDataLineHttpPost }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Files", scope = ARXEDIDataHeaderHttpPost.class)
    public JAXBElement<ArrayOfARXEDIDataLineHttpPost> createARXEDIDataHeaderHttpPostFiles(ArrayOfARXEDIDataLineHttpPost value) {
        return new JAXBElement<ArrayOfARXEDIDataLineHttpPost>(_ARXEDIDataHeaderHttpPostFiles_QNAME, ArrayOfARXEDIDataLineHttpPost.class, ARXEDIDataHeaderHttpPost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDIDataHeaderHttpPost.class)
    public JAXBElement<String> createARXEDIDataHeaderHttpPostFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDIDataHeaderHttpPost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDIDataHeaderHttpPost.class)
    public JAXBElement<String> createARXEDIDataHeaderHttpPostStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDIDataHeaderHttpPost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDISOInquiryLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lines", scope = ARXEDIResponseConsoleInquiry.class)
    public JAXBElement<ArrayOfARXEDISOInquiryLine> createARXEDIResponseConsoleInquiryLines(ArrayOfARXEDISOInquiryLine value) {
        return new JAXBElement<ArrayOfARXEDISOInquiryLine>(_ARXEDIResponseStockLines_QNAME, ArrayOfARXEDISOInquiryLine.class, ARXEDIResponseConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "MaxEditDate", scope = ARXEDIResponseConsoleInquiry.class)
    public JAXBElement<String> createARXEDIResponseConsoleInquiryMaxEditDate(String value) {
        return new JAXBElement<String>(_ARXEDIResponseConsoleInquiryMaxEditDate_QNAME, String.class, ARXEDIResponseConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "MAWB", scope = ARXEDIResponseConsoleInquiry.class)
    public JAXBElement<String> createARXEDIResponseConsoleInquiryMAWB(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderConsoleInquiryMAWB_QNAME, String.class, ARXEDIResponseConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "MaxAllocateDate", scope = ARXEDIResponseConsoleInquiry.class)
    public JAXBElement<String> createARXEDIResponseConsoleInquiryMaxAllocateDate(String value) {
        return new JAXBElement<String>(_ARXEDIResponseConsoleInquiryMaxAllocateDate_QNAME, String.class, ARXEDIResponseConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "LatestTrackingStatus", scope = ARXEDIResponseConsoleInquiry.class)
    public JAXBElement<String> createARXEDIResponseConsoleInquiryLatestTrackingStatus(String value) {
        return new JAXBElement<String>(_ARXEDIResponseSOInquiryLatestTrackingStatus_QNAME, String.class, ARXEDIResponseConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "MaxActualShipDate", scope = ARXEDIResponseConsoleInquiry.class)
    public JAXBElement<String> createARXEDIResponseConsoleInquiryMaxActualShipDate(String value) {
        return new JAXBElement<String>(_ARXEDIResponseConsoleInquiryMaxActualShipDate_QNAME, String.class, ARXEDIResponseConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "MaxAddDate", scope = ARXEDIResponseConsoleInquiry.class)
    public JAXBElement<String> createARXEDIResponseConsoleInquiryMaxAddDate(String value) {
        return new JAXBElement<String>(_ARXEDIResponseConsoleInquiryMaxAddDate_QNAME, String.class, ARXEDIResponseConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ConsoleKey", scope = ARXEDIResponseConsoleInquiry.class)
    public JAXBElement<String> createARXEDIResponseConsoleInquiryConsoleKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOConsoleKey_QNAME, String.class, ARXEDIResponseConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "MaxSOTDate", scope = ARXEDIResponseConsoleInquiry.class)
    public JAXBElement<String> createARXEDIResponseConsoleInquiryMaxSOTDate(String value) {
        return new JAXBElement<String>(_ARXEDIResponseConsoleInquiryMaxSOTDate_QNAME, String.class, ARXEDIResponseConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "MaxPickDate", scope = ARXEDIResponseConsoleInquiry.class)
    public JAXBElement<String> createARXEDIResponseConsoleInquiryMaxPickDate(String value) {
        return new JAXBElement<String>(_ARXEDIResponseConsoleInquiryMaxPickDate_QNAME, String.class, ARXEDIResponseConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIConsoleInquiryLineDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Orders", scope = ARXEDIResponseConsoleInquiry.class)
    public JAXBElement<ArrayOfARXEDIConsoleInquiryLineDetail> createARXEDIResponseConsoleInquiryOrders(ArrayOfARXEDIConsoleInquiryLineDetail value) {
        return new JAXBElement<ArrayOfARXEDIConsoleInquiryLineDetail>(_ARXEDIResponseConsoleInquiryOrders_QNAME, ArrayOfARXEDIConsoleInquiryLineDetail.class, ARXEDIResponseConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDIConsoleInquiry.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDIConsoleInquiryUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDIConsoleInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ConsoleKey", scope = CancelConsole.class)
    public JAXBElement<String> createCancelConsoleConsoleKey(String value) {
        return new JAXBElement<String>(_CancelConsoleConsoleKey_QNAME, String.class, CancelConsole.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDISKU.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDISKUUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDISKU.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SSA_Login", scope = ARXEDISSAServer.class)
    public JAXBElement<String> createARXEDISSAServerSSALogin(String value) {
        return new JAXBElement<String>(_ARXEDISSAServerSSALogin_QNAME, String.class, ARXEDISSAServer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SSA_Token", scope = ARXEDISSAServer.class)
    public JAXBElement<String> createARXEDISSAServerSSAToken(String value) {
        return new JAXBElement<String>(_ARXEDISSAServerSSAToken_QNAME, String.class, ARXEDISSAServer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SSA_Password", scope = ARXEDISSAServer.class)
    public JAXBElement<String> createARXEDISSAServerSSAPassword(String value) {
        return new JAXBElement<String>(_ARXEDISSAServerSSAPassword_QNAME, String.class, ARXEDISSAServer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDIHttpPost.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDIHttpPostUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDIHttpPost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "OrderKey", scope = ARXEDIDataHeaderSOInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderSOInquiryOrderKey(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailOrderKey_QNAME, String.class, ARXEDIDataHeaderSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "AWB", scope = ARXEDIDataHeaderSOInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderSOInquiryAWB(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAWB_QNAME, String.class, ARXEDIDataHeaderSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDIDataHeaderSOInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderSOInquiryFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDIDataHeaderSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDIDataHeaderSOInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderSOInquiryClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDIDataHeaderSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExternalOrderKey", scope = ARXEDIDataHeaderSOInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderSOInquiryExternalOrderKey(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailExternalOrderKey_QNAME, String.class, ARXEDIDataHeaderSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDIDataHeaderSOInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderSOInquiryStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDIDataHeaderSOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ActionDate", scope = ARXEDISOInquiryLine.class)
    public JAXBElement<String> createARXEDISOInquiryLineActionDate(String value) {
        return new JAXBElement<String>(_ARXEDISOInquiryLineActionDate_QNAME, String.class, ARXEDISOInquiryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UpdateComment", scope = ARXEDISOInquiryLine.class)
    public JAXBElement<String> createARXEDISOInquiryLineUpdateComment(String value) {
        return new JAXBElement<String>(_ARXEDISOInquiryLineUpdateComment_QNAME, String.class, ARXEDISOInquiryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UpdateLocationFormatted", scope = ARXEDISOInquiryLine.class)
    public JAXBElement<String> createARXEDISOInquiryLineUpdateLocationFormatted(String value) {
        return new JAXBElement<String>(_ARXEDISOInquiryLineUpdateLocationFormatted_QNAME, String.class, ARXEDISOInquiryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Status", scope = ARXEDISOInquiryLine.class)
    public JAXBElement<String> createARXEDISOInquiryLineStatus(String value) {
        return new JAXBElement<String>(_ARXEDIResponseStatus_QNAME, String.class, ARXEDISOInquiryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "TransactionID", scope = ARXEDIApplicationHeader.class)
    public JAXBElement<String> createARXEDIApplicationHeaderTransactionID(String value) {
        return new JAXBElement<String>(_ARXEDIResponseTransactionID_QNAME, String.class, ARXEDIApplicationHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "RequestedSystem", scope = ARXEDIApplicationHeader.class)
    public JAXBElement<String> createARXEDIApplicationHeaderRequestedSystem(String value) {
        return new JAXBElement<String>(_ARXEDIApplicationHeaderRequestedSystem_QNAME, String.class, ARXEDIApplicationHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "RequestedDate", scope = ARXEDIApplicationHeader.class)
    public JAXBElement<String> createARXEDIApplicationHeaderRequestedDate(String value) {
        return new JAXBElement<String>(_ARXEDIApplicationHeaderRequestedDate_QNAME, String.class, ARXEDIApplicationHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDISOInquiry.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDISOInquiryUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDISOInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDISOHeaderEdit.class)
    public JAXBElement<String> createARXEDISOHeaderEditFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDISOHeaderEdit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Value", scope = ARXEDISOHeaderEdit.class)
    public JAXBElement<String> createARXEDISOHeaderEditValue(String value) {
        return new JAXBElement<String>(_ARXEDIUserDefineDataValue_QNAME, String.class, ARXEDISOHeaderEdit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDISOHeaderEdit.class)
    public JAXBElement<String> createARXEDISOHeaderEditClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDISOHeaderEdit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDISOHeaderEdit.class)
    public JAXBElement<String> createARXEDISOHeaderEditStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDISOHeaderEdit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDIStock.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDIStockUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDIStock.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIResponseASNInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ASNs", scope = ARXEDIResponseASNInquiryMultiple.class)
    public JAXBElement<ArrayOfARXEDIResponseASNInquiry> createARXEDIResponseASNInquiryMultipleASNs(ArrayOfARXEDIResponseASNInquiry value) {
        return new JAXBElement<ArrayOfARXEDIResponseASNInquiry>(_ARXEDIResponseASNInquiryMultipleASNs_QNAME, ArrayOfARXEDIResponseASNInquiry.class, ARXEDIResponseASNInquiryMultiple.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Body", scope = ARXEDIDataLineHttpPost.class)
    public JAXBElement<String> createARXEDIDataLineHttpPostBody(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineHttpPostBody_QNAME, String.class, ARXEDIDataLineHttpPost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "FileName", scope = ARXEDIDataLineHttpPost.class)
    public JAXBElement<String> createARXEDIDataLineHttpPostFileName(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineHttpPostFileName_QNAME, String.class, ARXEDIDataLineHttpPost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "TypeRef", scope = ARXEDIDataLineHttpPost.class)
    public JAXBElement<String> createARXEDIDataLineHttpPostTypeRef(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineHttpPostTypeRef_QNAME, String.class, ARXEDIDataLineHttpPost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIDataLineSO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DataLines", scope = ARXEDISO.class)
    public JAXBElement<ArrayOfARXEDIDataLineSO> createARXEDISODataLines(ArrayOfARXEDIDataLineSO value) {
        return new JAXBElement<ArrayOfARXEDIDataLineSO>(_ARXEDIASNDataLines_QNAME, ArrayOfARXEDIDataLineSO.class, ARXEDISO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfWMSTablesAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExtraAddresses", scope = ARXEDISO.class)
    public JAXBElement<ArrayOfWMSTablesAddress> createARXEDISOExtraAddresses(ArrayOfWMSTablesAddress value) {
        return new JAXBElement<ArrayOfWMSTablesAddress>(_ARXEDIASNExtraAddresses_QNAME, ArrayOfWMSTablesAddress.class, ARXEDISO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDISO.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDISOUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDISO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU_SUSR3", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineSKUSUSR3(String value) {
        return new JAXBElement<String>(_ARXEDIStockLineSKUSUSR3_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU_SUSR4", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineSKUSUSR4(String value) {
        return new JAXBElement<String>(_ARXEDIStockLineSKUSUSR4_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU_SUSR5", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineSKUSUSR5(String value) {
        return new JAXBElement<String>(_ARXEDIStockLineSKUSUSR5_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineSKU(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOSKU_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU_SUSR6", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineSKUSUSR6(String value) {
        return new JAXBElement<String>(_ARXEDIStockLineSKUSUSR6_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU_SUSR7", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineSKUSUSR7(String value) {
        return new JAXBElement<String>(_ARXEDIStockLineSKUSUSR7_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU_SUSR8", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineSKUSUSR8(String value) {
        return new JAXBElement<String>(_ARXEDIStockLineSKUSUSR8_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "BOE", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineBOE(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineASNBOE_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU_SUSR9", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineSKUSUSR9(String value) {
        return new JAXBElement<String>(_ARXEDIStockLineSKUSUSR9_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable08", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineLottable08(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable08_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "HSCode", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineHSCode(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUHSCode_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable07", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineLottable07(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable07_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable09", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineLottable09(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable09_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable04", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineLottable04(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable04_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable03", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineLottable03(String value) {
        return new JAXBElement<String>(_ARXEDIStockLineLottable03_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable06", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineLottable06(String value) {
        return new JAXBElement<String>(_ARXEDIStockLineLottable06_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable05", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineLottable05(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable05_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable10", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineLottable10(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable10_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable02", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineLottable02(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSOLottable02_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Lottable01", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineLottable01(String value) {
        return new JAXBElement<String>(_ARXEDIASNInquiryWMSLineLottable01_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU_SUSR10", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineSKUSUSR10(String value) {
        return new JAXBElement<String>(_ARXEDIStockLineSKUSUSR10_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Description", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineDescription(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSKUDescription_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU_SUSR2", scope = ARXEDIStockLine.class)
    public JAXBElement<String> createARXEDIStockLineSKUSUSR2(String value) {
        return new JAXBElement<String>(_ARXEDIStockLineSKUSUSR2_QNAME, String.class, ARXEDIStockLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDIDataHeaderStockSKUs.class)
    public JAXBElement<String> createARXEDIDataHeaderStockSKUsFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDIDataHeaderStockSKUs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "EditDateFrom", scope = ARXEDIDataHeaderStockSKUs.class)
    public JAXBElement<String> createARXEDIDataHeaderStockSKUsEditDateFrom(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockSKUsEditDateFrom_QNAME, String.class, ARXEDIDataHeaderStockSKUs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "SKU_Contains", scope = ARXEDIDataHeaderStockSKUs.class)
    public JAXBElement<String> createARXEDIDataHeaderStockSKUsSKUContains(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockSKUContains_QNAME, String.class, ARXEDIDataHeaderStockSKUs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "EditDateTo", scope = ARXEDIDataHeaderStockSKUs.class)
    public JAXBElement<String> createARXEDIDataHeaderStockSKUsEditDateTo(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockSKUsEditDateTo_QNAME, String.class, ARXEDIDataHeaderStockSKUs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDIDataHeaderStockSKUs.class)
    public JAXBElement<String> createARXEDIDataHeaderStockSKUsStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDIDataHeaderStockSKUs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDIDataHeaderASNInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderASNInquiryFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDIDataHeaderASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ReceiptKey", scope = ARXEDIDataHeaderASNInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderASNInquiryReceiptKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderASNInquiryReceiptKey_QNAME, String.class, ARXEDIDataHeaderASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDIDataHeaderASNInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderASNInquiryClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDIDataHeaderASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDIDataHeaderASNInquiry.class)
    public JAXBElement<String> createARXEDIDataHeaderASNInquiryStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDIDataHeaderASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "HSUSR2", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNHSUSR2(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderASNHSUSR2_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "HSUSR1", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNHSUSR1(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderASNHSUSR1_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Notes", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNNotes(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineSONotes_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "HSUSR4", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNHSUSR4(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderASNHSUSR4_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "HSUSR3", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNHSUSR3(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderASNHSUSR3_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Facility", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNFacility(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockFacility_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "HSUSR5", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNHSUSR5(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderASNHSUSR5_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Supplier", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNSupplier(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderASNSupplier_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "BOE", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNBOE(String value) {
        return new JAXBElement<String>(_ARXEDIDataLineASNBOE_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "INCOTERMS", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNINCOTERMS(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderPOINCOTERMS_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Currency", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNCurrency(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOCurrency_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "StorerKey", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNStorerKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderStockStorerKey_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Type", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNType(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOType_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExterReceiptKey2", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNExterReceiptKey2(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderASNExterReceiptKey2_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Forwarder", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNForwarder(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailForwarder_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "RMA", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNRMA(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderASNRMA_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "TransportationMode", scope = ARXEDIDataHeaderASN.class)
    public JAXBElement<String> createARXEDIDataHeaderASNTransportationMode(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOTransportationMode_QNAME, String.class, ARXEDIDataHeaderASN.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClosedDate", scope = ARXEDIResponseASNInquiry.class)
    public JAXBElement<String> createARXEDIResponseASNInquiryClosedDate(String value) {
        return new JAXBElement<String>(_ARXEDIResponseASNInquiryClosedDate_QNAME, String.class, ARXEDIResponseASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ReceiptKey", scope = ARXEDIResponseASNInquiry.class)
    public JAXBElement<String> createARXEDIResponseASNInquiryReceiptKey(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderASNInquiryReceiptKey_QNAME, String.class, ARXEDIResponseASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIASNInquiryWMSLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "WMSLines", scope = ARXEDIResponseASNInquiry.class)
    public JAXBElement<ArrayOfARXEDIASNInquiryWMSLine> createARXEDIResponseASNInquiryWMSLines(ArrayOfARXEDIASNInquiryWMSLine value) {
        return new JAXBElement<ArrayOfARXEDIASNInquiryWMSLine>(_ARXEDIResponseSOInquiryWMSLines_QNAME, ArrayOfARXEDIASNInquiryWMSLine.class, ARXEDIResponseASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "EditDate", scope = ARXEDIResponseASNInquiry.class)
    public JAXBElement<String> createARXEDIResponseASNInquiryEditDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailEditDate_QNAME, String.class, ARXEDIResponseASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ClinetSystemRef", scope = ARXEDIResponseASNInquiry.class)
    public JAXBElement<String> createARXEDIResponseASNInquiryClinetSystemRef(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailClinetSystemRef_QNAME, String.class, ARXEDIResponseASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "AddDate", scope = ARXEDIResponseASNInquiry.class)
    public JAXBElement<String> createARXEDIResponseASNInquiryAddDate(String value) {
        return new JAXBElement<String>(_ARXEDIConsoleInquiryLineDetailAddDate_QNAME, String.class, ARXEDIResponseASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Type", scope = ARXEDIResponseASNInquiry.class)
    public JAXBElement<String> createARXEDIResponseASNInquiryType(String value) {
        return new JAXBElement<String>(_ARXEDIDataHeaderSOType_QNAME, String.class, ARXEDIResponseASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExternalReceiptKey", scope = ARXEDIResponseASNInquiry.class)
    public JAXBElement<String> createARXEDIResponseASNInquiryExternalReceiptKey(String value) {
        return new JAXBElement<String>(_ARXEDIResponseASNInquiryExternalReceiptKey_QNAME, String.class, ARXEDIResponseASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "Status", scope = ARXEDIResponseASNInquiry.class)
    public JAXBElement<String> createARXEDIResponseASNInquiryStatus(String value) {
        return new JAXBElement<String>(_ARXEDIResponseStatus_QNAME, String.class, ARXEDIResponseASNInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIDataLinePO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "DataLines", scope = ARXEDIPO.class)
    public JAXBElement<ArrayOfARXEDIDataLinePO> createARXEDIPODataLines(ArrayOfARXEDIDataLinePO value) {
        return new JAXBElement<ArrayOfARXEDIDataLinePO>(_ARXEDIASNDataLines_QNAME, ArrayOfARXEDIDataLinePO.class, ARXEDIPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfWMSTablesAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "ExtraAddresses", scope = ARXEDIPO.class)
    public JAXBElement<ArrayOfWMSTablesAddress> createARXEDIPOExtraAddresses(ArrayOfWMSTablesAddress value) {
        return new JAXBElement<ArrayOfWMSTablesAddress>(_ARXEDIASNExtraAddresses_QNAME, ArrayOfWMSTablesAddress.class, ARXEDIPO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfARXEDIUserDefineData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", name = "UserDate", scope = ARXEDIPO.class)
    public JAXBElement<ArrayOfARXEDIUserDefineData> createARXEDIPOUserDate(ArrayOfARXEDIUserDefineData value) {
        return new JAXBElement<ArrayOfARXEDIUserDefineData>(_ARXEDIStockSKUsUserDate_QNAME, ArrayOfARXEDIUserDefineData.class, ARXEDIPO.class, value);
    }

}
