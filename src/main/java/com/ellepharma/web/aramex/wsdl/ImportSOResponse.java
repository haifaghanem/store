
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ImportSOResult" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._Response" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "importSOResult"
})
@XmlRootElement(name = "ImportSOResponse", namespace = "http://tempuri.org/")
public class ImportSOResponse {

    @XmlElement(name = "ImportSOResult", namespace = "http://tempuri.org/")
    protected ARXEDIResponse importSOResult;

    /**
     * Gets the value of the importSOResult property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIResponse }
     *     
     */
    public ARXEDIResponse getImportSOResult() {
        return importSOResult;
    }

    /**
     * Sets the value of the importSOResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIResponse }
     *     
     */
    public void setImportSOResult(ARXEDIResponse value) {
        this.importSOResult = value;
    }

}
