
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SKUs" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._Stock_SKUs" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "skUs"
})
@XmlRootElement(name = "InquirySKUs", namespace = "http://tempuri.org/")
public class InquirySKUs {

    @XmlElement(name = "SKUs", namespace = "http://tempuri.org/")
    protected ARXEDIStockSKUs skUs;

    /**
     * Gets the value of the skUs property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIStockSKUs }
     *     
     */
    public ARXEDIStockSKUs getSKUs() {
        return skUs;
    }

    /**
     * Sets the value of the skUs property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIStockSKUs }
     *     
     */
    public void setSKUs(ARXEDIStockSKUs value) {
        this.skUs = value;
    }

}
