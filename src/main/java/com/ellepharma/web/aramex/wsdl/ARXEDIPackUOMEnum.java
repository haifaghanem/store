
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ARX_EDI._PackUOMEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ARX_EDI._PackUOMEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="_EACH"/>
 *     &lt;enumeration value="_CASE"/>
 *     &lt;enumeration value="_InnerPack"/>
 *     &lt;enumeration value="_Pallet"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ARX_EDI._PackUOMEnum")
@XmlEnum
public enum ARXEDIPackUOMEnum {

    @XmlEnumValue("_EACH")
    EACH("_EACH"),
    @XmlEnumValue("_CASE")
    CASE("_CASE"),
    @XmlEnumValue("_InnerPack")
    INNER_PACK("_InnerPack"),
    @XmlEnumValue("_Pallet")
    PALLET("_Pallet");
    private final String value;

    ARXEDIPackUOMEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ARXEDIPackUOMEnum fromValue(String v) {
        for (ARXEDIPackUOMEnum c: ARXEDIPackUOMEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
