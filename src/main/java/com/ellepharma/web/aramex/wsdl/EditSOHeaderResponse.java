
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EditSOHeaderResult" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._Response" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "editSOHeaderResult"
})
@XmlRootElement(name = "EditSOHeaderResponse", namespace = "http://tempuri.org/")
public class EditSOHeaderResponse {

    @XmlElement(name = "EditSOHeaderResult", namespace = "http://tempuri.org/")
    protected ARXEDIResponse editSOHeaderResult;

    /**
     * Gets the value of the editSOHeaderResult property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIResponse }
     *     
     */
    public ARXEDIResponse getEditSOHeaderResult() {
        return editSOHeaderResult;
    }

    /**
     * Sets the value of the editSOHeaderResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIResponse }
     *     
     */
    public void setEditSOHeaderResult(ARXEDIResponse value) {
        this.editSOHeaderResult = value;
    }

}
