
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SAP" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._SAP_Response" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sap"
})
@XmlRootElement(name = "SAPResponse", namespace = "http://tempuri.org/")
public class SAPResponse {

    @XmlElement(name = "SAP", namespace = "http://tempuri.org/")
    protected ARXEDISAPResponse sap;

    /**
     * Gets the value of the sap property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDISAPResponse }
     *     
     */
    public ARXEDISAPResponse getSAP() {
        return sap;
    }

    /**
     * Sets the value of the sap property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDISAPResponse }
     *     
     */
    public void setSAP(ARXEDISAPResponse value) {
        this.sap = value;
    }

}
