
package com.ellepharma.web.aramex.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWMSTables_Address complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWMSTables_Address">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WMSTables_Address" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables}WMSTables_Address" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWMSTables_Address", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS.CEntities.WMSTables", propOrder = {
    "wmsTablesAddress"
})
public class ArrayOfWMSTablesAddress {

    @XmlElement(name = "WMSTables_Address", nillable = true)
    protected List<WMSTablesAddress> wmsTablesAddress;

    /**
     * Gets the value of the wmsTablesAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wmsTablesAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWMSTablesAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WMSTablesAddress }
     * 
     * 
     */
    public List<WMSTablesAddress> getWMSTablesAddress() {
        if (wmsTablesAddress == null) {
            wmsTablesAddress = new ArrayList<WMSTablesAddress>();
        }
        return this.wmsTablesAddress;
    }

}
