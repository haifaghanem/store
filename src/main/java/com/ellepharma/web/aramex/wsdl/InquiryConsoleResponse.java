
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InquiryConsoleResult" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._Response_Console_Inquiry" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inquiryConsoleResult"
})
@XmlRootElement(name = "InquiryConsoleResponse", namespace = "http://tempuri.org/")
public class InquiryConsoleResponse {

    @XmlElement(name = "InquiryConsoleResult", namespace = "http://tempuri.org/")
    protected ARXEDIResponseConsoleInquiry inquiryConsoleResult;

    /**
     * Gets the value of the inquiryConsoleResult property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIResponseConsoleInquiry }
     *     
     */
    public ARXEDIResponseConsoleInquiry getInquiryConsoleResult() {
        return inquiryConsoleResult;
    }

    /**
     * Sets the value of the inquiryConsoleResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIResponseConsoleInquiry }
     *     
     */
    public void setInquiryConsoleResult(ARXEDIResponseConsoleInquiry value) {
        this.inquiryConsoleResult = value;
    }

}
