
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ASN" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._ASN" minOccurs="0"/>
 *         &lt;element name="CreateAfterCancelling" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "asn",
    "createAfterCancelling"
})
@XmlRootElement(name = "UpdateASN", namespace = "http://tempuri.org/")
public class UpdateASN {

    @XmlElement(name = "ASN", namespace = "http://tempuri.org/")
    protected ARXEDIASN asn;
    @XmlElement(name = "CreateAfterCancelling", namespace = "http://tempuri.org/")
    protected Boolean createAfterCancelling;

    /**
     * Gets the value of the asn property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDIASN }
     *     
     */
    public ARXEDIASN getASN() {
        return asn;
    }

    /**
     * Sets the value of the asn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDIASN }
     *     
     */
    public void setASN(ARXEDIASN value) {
        this.asn = value;
    }

    /**
     * Gets the value of the createAfterCancelling property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCreateAfterCancelling() {
        return createAfterCancelling;
    }

    /**
     * Sets the value of the createAfterCancelling property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreateAfterCancelling(Boolean value) {
        this.createAfterCancelling = value;
    }

}
