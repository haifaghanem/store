
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SKUs" type="{http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS}ARX_EDI._SKUs" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "skUs"
})
@XmlRootElement(name = "ImportSKUs", namespace = "http://tempuri.org/")
public class ImportSKUs {

    @XmlElement(name = "SKUs", namespace = "http://tempuri.org/")
    protected ARXEDISKUs skUs;

    /**
     * Gets the value of the skUs property.
     * 
     * @return
     *     possible object is
     *     {@link ARXEDISKUs }
     *     
     */
    public ARXEDISKUs getSKUs() {
        return skUs;
    }

    /**
     * Sets the value of the skUs property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARXEDISKUs }
     *     
     */
    public void setSKUs(ARXEDISKUs value) {
        this.skUs = value;
    }

}
