
package com.ellepharma.web.aramex.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ARX_EDI._SO_Inquiry_WMSLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ARX_EDI._SO_Inquiry_WMSLine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DSUSR2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DSUSR3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DSUSR4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DSUSR5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExternLineNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QtyAllocated" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyOpen" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyOrdered" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyPicked" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QtyShipped" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ARX_EDI._SO_Inquiry_WMSLine", propOrder = {
    "dsusr2",
    "dsusr3",
    "dsusr4",
    "dsusr5",
    "externLineNo",
    "qtyAllocated",
    "qtyOpen",
    "qtyOrdered",
    "qtyPicked",
    "qtyShipped",
    "sku"
})
public class ARXEDISOInquiryWMSLine {

    @XmlElementRef(name = "DSUSR2", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dsusr2;
    @XmlElementRef(name = "DSUSR3", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dsusr3;
    @XmlElementRef(name = "DSUSR4", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dsusr4;
    @XmlElementRef(name = "DSUSR5", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dsusr5;
    @XmlElementRef(name = "ExternLineNo", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externLineNo;
    @XmlElement(name = "QtyAllocated")
    protected Integer qtyAllocated;
    @XmlElement(name = "QtyOpen")
    protected Integer qtyOpen;
    @XmlElement(name = "QtyOrdered")
    protected Integer qtyOrdered;
    @XmlElement(name = "QtyPicked")
    protected Integer qtyPicked;
    @XmlElement(name = "QtyShipped")
    protected Integer qtyShipped;
    @XmlElementRef(name = "SKU", namespace = "http://schemas.datacontract.org/2004/07/CORP.DXB.LOG.EDI_WS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sku;

    /**
     * Gets the value of the dsusr2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDSUSR2() {
        return dsusr2;
    }

    /**
     * Sets the value of the dsusr2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDSUSR2(JAXBElement<String> value) {
        this.dsusr2 = value;
    }

    /**
     * Gets the value of the dsusr3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDSUSR3() {
        return dsusr3;
    }

    /**
     * Sets the value of the dsusr3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDSUSR3(JAXBElement<String> value) {
        this.dsusr3 = value;
    }

    /**
     * Gets the value of the dsusr4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDSUSR4() {
        return dsusr4;
    }

    /**
     * Sets the value of the dsusr4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDSUSR4(JAXBElement<String> value) {
        this.dsusr4 = value;
    }

    /**
     * Gets the value of the dsusr5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDSUSR5() {
        return dsusr5;
    }

    /**
     * Sets the value of the dsusr5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDSUSR5(JAXBElement<String> value) {
        this.dsusr5 = value;
    }

    /**
     * Gets the value of the externLineNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternLineNo() {
        return externLineNo;
    }

    /**
     * Sets the value of the externLineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternLineNo(JAXBElement<String> value) {
        this.externLineNo = value;
    }

    /**
     * Gets the value of the qtyAllocated property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyAllocated() {
        return qtyAllocated;
    }

    /**
     * Sets the value of the qtyAllocated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyAllocated(Integer value) {
        this.qtyAllocated = value;
    }

    /**
     * Gets the value of the qtyOpen property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyOpen() {
        return qtyOpen;
    }

    /**
     * Sets the value of the qtyOpen property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyOpen(Integer value) {
        this.qtyOpen = value;
    }

    /**
     * Gets the value of the qtyOrdered property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyOrdered() {
        return qtyOrdered;
    }

    /**
     * Sets the value of the qtyOrdered property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyOrdered(Integer value) {
        this.qtyOrdered = value;
    }

    /**
     * Gets the value of the qtyPicked property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyPicked() {
        return qtyPicked;
    }

    /**
     * Sets the value of the qtyPicked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyPicked(Integer value) {
        this.qtyPicked = value;
    }

    /**
     * Gets the value of the qtyShipped property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyShipped() {
        return qtyShipped;
    }

    /**
     * Sets the value of the qtyShipped property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyShipped(Integer value) {
        this.qtyShipped = value;
    }

    /**
     * Gets the value of the sku property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSKU() {
        return sku;
    }

    /**
     * Sets the value of the sku property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSKU(JAXBElement<String> value) {
        this.sku = value;
    }

}
