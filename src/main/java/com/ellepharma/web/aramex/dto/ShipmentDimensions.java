
package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class ShipmentDimensions {

    @SerializedName("Length")
    @Expose
    private Integer length;
    @SerializedName("Width")
    @Expose
    private Integer width;
    @SerializedName("Height")
    @Expose
    private Integer height;
    @SerializedName("Unit")
    @Expose
    private String unit;

    public ShipmentDimensions() {
    }

    public ShipmentDimensions(Integer length, Integer width, Integer height, String unit) {
        this.length = length;
        this.width = width;
        this.height = height;
        this.unit = unit;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("length", length).append("width", width).append("height", height).append("unit", unit).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(unit).append(height).append(width).append(length).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ShipmentDimensions) == false) {
            return false;
        }
        ShipmentDimensions rhs = ((ShipmentDimensions) other);
        return new EqualsBuilder().append(unit, rhs.unit).append(height, rhs.height).append(width, rhs.width).append(length, rhs.length).isEquals();
    }

}
