package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Notifications.java
 *
 * @author Malek Yaseen <ma.yaseen@ats-ware.com>
 * @since Mar 01, 2020
 */
public class Notifications {

	@SerializedName("Message")
	@Expose
	private String message;

	@SerializedName("Code")
	@Expose
	private String code;

	public Notifications() {
	}

	public Notifications(String message, String code) {
		this.message = message;
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
