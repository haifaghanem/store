
package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

public class CreateShipmentsDto {

    @SerializedName("Shipments")
    @Expose
    private List<Shipment> shipments = null;

	@SerializedName("LabelInfo")
	@Expose
	private LabelInfo labelInfo;

	@SerializedName("ClientInfo")
	@Expose
	private ClientInfo clientInfo;

	@SerializedName("Transaction")
	@Expose
	private Transaction transaction;

	@SerializedName("Notifications")
	@Expose
	private List<Notifications> notifications;

	@SerializedName("HasErrors")
	@Expose
	private Boolean hasErrors;

	public CreateShipmentsDto() {
		shipments = new ArrayList<>();
		labelInfo = new LabelInfo();
		clientInfo = new ClientInfo();
		notifications = new ArrayList<>();
		transaction = new Transaction("001", "", "", "", "");
    }

    public List<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(List<Shipment> shipments) {
        this.shipments = shipments;
    }

    public LabelInfo getLabelInfo() {
        return labelInfo;
    }

    public void setLabelInfo(LabelInfo labelInfo) {
        this.labelInfo = labelInfo;
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("shipments", shipments).append("labelInfo", labelInfo).append("clientInfo", clientInfo).append("transaction", transaction).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(transaction).append(shipments).append(labelInfo).append(clientInfo).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if (!(other instanceof CreateShipmentsDto)) {
			return false;
		}
        CreateShipmentsDto rhs = ((CreateShipmentsDto) other);
		return new EqualsBuilder().append(transaction, rhs.transaction).append(shipments, rhs.shipments).append(labelInfo, rhs.labelInfo)
								  .append(clientInfo, rhs.clientInfo).isEquals();
	}

	public Boolean getHasErrors() {
		return hasErrors;
	}

	public void setHasErrors(Boolean hasErrors) {
		this.hasErrors = hasErrors;
	}

	public List<Notifications> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notifications> notifications) {
		this.notifications = notifications;
	}
}
