
package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Consignee {

    @SerializedName("Reference1")
    @Expose
    private Object reference1;
    @SerializedName("Reference2")
    @Expose
    private Object reference2;
    @SerializedName("AccountNumber")
    @Expose
    private Object accountNumber;
    @SerializedName("PartyAddress")
    @Expose
    private PartyAddress partyAddress;
    @SerializedName("Contact")
    @Expose
    private Contact contact;

    public Consignee() {
       // accountNumber = GlobalConfigProperties.getInstance().getProperty("ClientInfo.accountNumber");


    }

    public Object getReference1() {
        return reference1;
    }

    public void setReference1(Object reference1) {
        this.reference1 = reference1;
    }

    public Object getReference2() {
        return reference2;
    }

    public void setReference2(Object reference2) {
        this.reference2 = reference2;
    }

    public Object getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Object accountNumber) {
        this.accountNumber = accountNumber;
    }

    public PartyAddress getPartyAddress() {
        return partyAddress;
    }

    public void setPartyAddress(PartyAddress partyAddress) {
        this.partyAddress = partyAddress;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("reference1", reference1).append("reference2", reference2).append("accountNumber", accountNumber).append("partyAddress", partyAddress).append("contact", contact).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(reference2).append(reference1).append(accountNumber).append(partyAddress).append(contact).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Consignee) == false) {
            return false;
        }
        Consignee rhs = ((Consignee) other);
        return new EqualsBuilder().append(reference2, rhs.reference2).append(reference1, rhs.reference1).append(accountNumber, rhs.accountNumber).append(partyAddress, rhs.partyAddress).append(contact, rhs.contact).isEquals();
    }

}
