package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class CustomsValueAmount {

    @SerializedName("CurrencyCode")
    @Expose
    private String currencyCode;
    @SerializedName("Value")
    @Expose
    private Double value;


    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("currencyCode", currencyCode).append("value", value).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(currencyCode).append(value).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CustomsValueAmount) == false) {
            return false;
        }
        CustomsValueAmount rhs = ((CustomsValueAmount) other);
        return new EqualsBuilder().append(currencyCode, rhs.currencyCode).append(value, rhs.value).isEquals();
    }

}
