package com.ellepharma.web.aramex.dto;

import com.ellepharma.utils.Utils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;

public class Pickup {

    @SerializedName("PickupAddress")
    @Expose
    private PickupAddress pickupAddress;
    @SerializedName("PickupContact")
    @Expose
    private PickupContact pickupContact;
    @SerializedName("PickupLocation")
    @Expose
    private String pickupLocation;
    @SerializedName("PickupDate")
    @Expose
    private String pickupDate;
    @SerializedName("ReadyTime")
    @Expose
    private String readyTime;
    @SerializedName("LastPickupTime")
    @Expose
    private String lastPickupTime;
    @SerializedName("ClosingTime")
    @Expose
    private String closingTime;
    @SerializedName("Comments")
    @Expose
    private String comments;
    @SerializedName("Reference1")
    @Expose
    private String reference1;
    @SerializedName("Reference2")
    @Expose
    private String reference2;
    @SerializedName("Vehicle")
    @Expose
    private String vehicle;
    @SerializedName("Shipments")
    @Expose
    private List<Shipment> shipments;
    @SerializedName("PickupItems")
    @Expose
    private List<PickupItem> pickupItems = null;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("ExistingShipments")
    @Expose
    private List<Shipment> existingShipments;
    @SerializedName("Branch")
    @Expose
    private String branch;
    @SerializedName("RouteCode")
    @Expose
    private String routeCode;

    public Pickup(PickupAddress pickupAddress, PickupContact pickupContact, List<Shipment> shipments, List<PickupItem> pickupItems) {
        int hours = LocalDateTime.now().atZone(ZoneId.of(Utils.getDefaultZoneId())).getHour();
        int minute = LocalDateTime.now().atZone(ZoneId.of(Utils.getDefaultZoneId())).getMinute();
        int plusday = 0;
        if (hours >= 12) {
            hours = 8;
            minute = 0;
            plusday = 1;
        }
        long pickupDatel = LocalDateTime.of(LocalDate.now().plusDays(plusday), LocalTime.of(hours, minute)).atZone(ZoneId.of(Utils.getDefaultZoneId())).toEpochSecond() * 1000;
        long readyTimel = LocalDateTime.of(LocalDate.now().plusDays(plusday), LocalTime.of(15, 0)).atZone(ZoneId.of(Utils.getDefaultZoneId())).toEpochSecond() * 1000;
        long lastPickupTimel = LocalDateTime.of(LocalDate.now().plusDays(plusday), LocalTime.of(18, 0)).atZone(ZoneId.of(Utils.getDefaultZoneId())).toEpochSecond() * 1000;
        long closingTimel = LocalDateTime.of(LocalDate.now().plusDays(plusday), LocalTime.of(20, 0)).atZone(ZoneId.of(Utils.getDefaultZoneId())).toEpochSecond() * 1000;

        this.pickupAddress = pickupAddress;
        this.pickupContact = pickupContact;
        this.pickupLocation = "Reception";
        this.pickupDate = "/Date(" + pickupDatel + ")/";
        this.readyTime = "/Date(" + readyTimel + ")/";
        this.lastPickupTime = "/Date(" + lastPickupTimel + ")/";
        this.closingTime = "/Date(" + closingTimel + ")/";
        this.comments = "";
        this.reference1 = "001";
        this.reference2 = "";
        this.vehicle = "Car";
        this.shipments = shipments;
        this.pickupItems = pickupItems;
        this.status = "Ready";
        this.existingShipments = null;
        this.branch = "";
        this.routeCode = "";


    }

    public PickupAddress getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(PickupAddress pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public PickupContact getPickupContact() {
        return pickupContact;
    }

    public void setPickupContact(PickupContact pickupContact) {
        this.pickupContact = pickupContact;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getReadyTime() {
        return readyTime;
    }

    public void setReadyTime(String readyTime) {
        this.readyTime = readyTime;
    }

    public String getLastPickupTime() {
        return lastPickupTime;
    }

    public void setLastPickupTime(String lastPickupTime) {
        this.lastPickupTime = lastPickupTime;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getReference1() {
        return reference1;
    }

    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }

    public String getReference2() {
        return reference2;
    }

    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }


    public List<PickupItem> getPickupItems() {
        return pickupItems;
    }

    public void setPickupItems(List<PickupItem> pickupItems) {
        this.pickupItems = pickupItems;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("pickupAddress", pickupAddress).append("pickupContact", pickupContact).append("pickupLocation", pickupLocation).append("pickupDate", pickupDate).append("readyTime", readyTime).append("lastPickupTime", lastPickupTime).append("closingTime", closingTime).append("comments", comments).append("reference1", reference1).append("reference2", reference2).append("vehicle", vehicle).append("shipments", shipments).append("pickupItems", pickupItems).append("status", status).append("existingShipments", existingShipments).append("branch", branch).append("routeCode", routeCode).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(pickupContact).append(status).append(existingShipments).append(vehicle).append(shipments).append(reference2).append(reference1).append(routeCode).append(pickupLocation).append(readyTime).append(closingTime).append(pickupDate).append(branch).append(pickupItems).append(comments).append(lastPickupTime).append(pickupAddress).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Pickup) == false) {
            return false;
        }
        Pickup rhs = ((Pickup) other);
        return new EqualsBuilder().append(pickupContact, rhs.pickupContact).append(status, rhs.status).append(existingShipments, rhs.existingShipments).append(vehicle, rhs.vehicle).append(shipments, rhs.shipments).append(reference2, rhs.reference2).append(reference1, rhs.reference1).append(routeCode, rhs.routeCode).append(pickupLocation, rhs.pickupLocation).append(readyTime, rhs.readyTime).append(closingTime, rhs.closingTime).append(pickupDate, rhs.pickupDate).append(branch, rhs.branch).append(pickupItems, rhs.pickupItems).append(comments, rhs.comments).append(lastPickupTime, rhs.lastPickupTime).append(pickupAddress, rhs.pickupAddress).isEquals();
    }

    public List<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(List<Shipment> shipments) {
        this.shipments = shipments;
    }

    public List<Shipment> getExistingShipments() {
        return existingShipments;
    }

    public void setExistingShipments(List<Shipment> existingShipments) {
        this.existingShipments = existingShipments;
    }
}
