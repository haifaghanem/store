package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class LabelInfo {

    @SerializedName("ReportID")
    @Expose
    private Integer reportID;
    @SerializedName("ReportType")
    @Expose
    private String reportType;

    public LabelInfo() {
        reportID = 9729;
        reportType = "URL";
    }


    public Integer getReportID() {
        return reportID;
    }

    public void setReportID(Integer reportID) {
        this.reportID = reportID;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("reportID", reportID).append("reportType", reportType).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(reportType).append(reportID).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof LabelInfo) == false) {
            return false;
        }
        LabelInfo rhs = ((LabelInfo) other);
        return new EqualsBuilder().append(reportType, rhs.reportType).append(reportID, rhs.reportID).isEquals();
    }

}
