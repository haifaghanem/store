package com.ellepharma.web.aramex.dto;

import com.ellepharma.service.GlobalConfigProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClientInfo {

    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("Version")
    @Expose
    private String version;
    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("AccountPin")
    @Expose
    private String accountPin;
    @SerializedName("AccountEntity")
    @Expose
    private String accountEntity;
    @SerializedName("AccountCountryCode")
    @Expose
    private String accountCountryCode;
    @SerializedName("Source")
    @Expose
    private Integer source;

    public ClientInfo() {
        userName =GlobalConfigProperties.getInstance().getProperty("ClientInfo.userName");
        password =GlobalConfigProperties.getInstance().getProperty("ClientInfo.password");
        version =GlobalConfigProperties.getInstance().getProperty("ClientInfo.version");
        accountNumber =GlobalConfigProperties.getInstance().getProperty("ClientInfo.accountNumber");
        accountPin =GlobalConfigProperties.getInstance().getProperty("ClientInfo.accountPin");
        accountEntity =GlobalConfigProperties.getInstance().getProperty("ClientInfo.accountEntity");
        accountCountryCode =GlobalConfigProperties.getInstance().getProperty("ClientInfo.accountCountryCode");
        source = Integer.valueOf(GlobalConfigProperties.getInstance().getProperty("ClientInfo.source"));
    }


    private ClientInfo(String userName, String password, String version, String accountNumber, String accountPin, String accountEntity, String accountCountryCode, Integer source) {
        this.userName = userName;
        this.password = password;
        this.version = version;
        this.accountNumber = accountNumber;
        this.accountPin = accountPin;
        this.accountEntity = accountEntity;
        this.accountCountryCode = accountCountryCode;
        this.source = source;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountPin() {
        return accountPin;
    }

    public void setAccountPin(String accountPin) {
        this.accountPin = accountPin;
    }

    public String getAccountEntity() {
        return accountEntity;
    }

    public void setAccountEntity(String accountEntity) {
        this.accountEntity = accountEntity;
    }

    public String getAccountCountryCode() {
        return accountCountryCode;
    }

    public void setAccountCountryCode(String accountCountryCode) {
        this.accountCountryCode = accountCountryCode;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }
}