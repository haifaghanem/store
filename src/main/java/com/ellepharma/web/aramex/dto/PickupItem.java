
package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class PickupItem {

    @SerializedName("ProductGroup")
    @Expose
    private String productGroup;
    @SerializedName("ProductType")
    @Expose
    private String productType;
    @SerializedName("NumberOfShipments")
    @Expose
    private Integer numberOfShipments;
    @SerializedName("PackageType")
    @Expose
    private String packageType;
    @SerializedName("Payment")
    @Expose
    private String payment;
    @SerializedName("ShipmentWeight")
    @Expose
    private ShipmentWeight shipmentWeight;
    @SerializedName("ShipmentVolume")
    @Expose
    private Object shipmentVolume;
    @SerializedName("NumberOfPieces")
    @Expose
    private Integer numberOfPieces;
    @SerializedName("CashAmount")
    @Expose
    private Object cashAmount;
    @SerializedName("ExtraCharges")
    @Expose
    private Object extraCharges;
    @SerializedName("ShipmentDimensions")
    @Expose
    private ShipmentDimensions shipmentDimensions;
    @SerializedName("Comments")
    @Expose
    private String comments;

    public PickupItem(String productGroup, String productType, Integer numberOfShipments, String packageType, String payment, ShipmentWeight shipmentWeight, Object shipmentVolume, Integer numberOfPieces, Object cashAmount, Object extraCharges, ShipmentDimensions shipmentDimensions, String comments) {
        this.productGroup = productGroup;
        this.productType = productType;
        this.numberOfShipments = numberOfShipments;
        this.packageType = packageType;
        this.payment = payment;
        this.shipmentWeight = shipmentWeight;
        this.shipmentVolume = shipmentVolume;
        this.numberOfPieces = numberOfPieces;
        this.cashAmount = cashAmount;
        this.extraCharges = extraCharges;
        this.shipmentDimensions = shipmentDimensions;
        this.comments = comments;
    }

    public String getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Integer getNumberOfShipments() {
        return numberOfShipments;
    }

    public void setNumberOfShipments(Integer numberOfShipments) {
        this.numberOfShipments = numberOfShipments;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public ShipmentWeight getShipmentWeight() {
        return shipmentWeight;
    }

    public void setShipmentWeight(ShipmentWeight shipmentWeight) {
        this.shipmentWeight = shipmentWeight;
    }

    public Object getShipmentVolume() {
        return shipmentVolume;
    }

    public void setShipmentVolume(Object shipmentVolume) {
        this.shipmentVolume = shipmentVolume;
    }

    public Integer getNumberOfPieces() {
        return numberOfPieces;
    }

    public void setNumberOfPieces(Integer numberOfPieces) {
        this.numberOfPieces = numberOfPieces;
    }

    public Object getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(Object cashAmount) {
        this.cashAmount = cashAmount;
    }

    public Object getExtraCharges() {
        return extraCharges;
    }

    public void setExtraCharges(Object extraCharges) {
        this.extraCharges = extraCharges;
    }

    public ShipmentDimensions getShipmentDimensions() {
        return shipmentDimensions;
    }

    public void setShipmentDimensions(ShipmentDimensions shipmentDimensions) {
        this.shipmentDimensions = shipmentDimensions;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productGroup", productGroup).append("productType", productType).append("numberOfShipments", numberOfShipments).append("packageType", packageType).append("payment", payment).append("shipmentWeight", shipmentWeight).append("shipmentVolume", shipmentVolume).append("numberOfPieces", numberOfPieces).append("cashAmount", cashAmount).append("extraCharges", extraCharges).append("shipmentDimensions", shipmentDimensions).append("comments", comments).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(productGroup).append(packageType).append(payment).append(shipmentWeight).append(shipmentDimensions).append(numberOfPieces).append(productType).append(shipmentVolume).append(numberOfShipments).append(cashAmount).append(comments).append(extraCharges).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PickupItem) == false) {
            return false;
        }
        PickupItem rhs = ((PickupItem) other);
        return new EqualsBuilder().append(productGroup, rhs.productGroup).append(packageType, rhs.packageType).append(payment, rhs.payment).append(shipmentWeight, rhs.shipmentWeight).append(shipmentDimensions, rhs.shipmentDimensions).append(numberOfPieces, rhs.numberOfPieces).append(productType, rhs.productType).append(shipmentVolume, rhs.shipmentVolume).append(numberOfShipments, rhs.numberOfShipments).append(cashAmount, rhs.cashAmount).append(comments, rhs.comments).append(extraCharges, rhs.extraCharges).isEquals();
    }

}
