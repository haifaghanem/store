package com.ellepharma.web.aramex.dto;

import com.ellepharma.service.GlobalConfigProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class PickupAddress {

    @SerializedName("Line1")
    @Expose
    private String line1;
    @SerializedName("Line2")
    @Expose
    private String line2;
    @SerializedName("Line3")
    @Expose
    private String line3;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("StateOrProvinceCode")
    @Expose
    private String stateOrProvinceCode;
    @SerializedName("PostCode")
    @Expose
    private String postCode;
    @SerializedName("CountryCode")
    @Expose
    private String countryCode;
    @SerializedName("Longitude")
    @Expose
    private Double longitude;
    @SerializedName("Latitude")
    @Expose
    private Double latitude;
    @SerializedName("BuildingNumber")
    @Expose
    private Object buildingNumber;
    @SerializedName("BuildingName")
    @Expose
    private Object buildingName;
    @SerializedName("Floor")
    @Expose
    private Object floor;
    @SerializedName("Apartme nt")
    @Expose
    private Object apartmeNt;
    @SerializedName("POBox")
    @Expose
    private Object pOBox;
    @SerializedName("Description")
    @Expose
    private Object description;

    public PickupAddress() {
        this.line1 =GlobalConfigProperties.getInstance().getProperty("PickupAddress.line1");
        this.line2 =GlobalConfigProperties.getInstance().getProperty("PickupAddress.line2");
        this.line3 =GlobalConfigProperties.getInstance().getProperty("PickupAddress.line3");
        this.city =GlobalConfigProperties.getInstance().getProperty("PickupAddress.city");
        this.stateOrProvinceCode =GlobalConfigProperties.getInstance().getProperty("PickupAddress.stateOrProvinceCode");
        this.postCode =GlobalConfigProperties.getInstance().getProperty("PickupAddress.postCode");
        this.countryCode =GlobalConfigProperties.getInstance().getProperty("PickupAddress.countryCode");
        this.longitude = Double.parseDouble(GlobalConfigProperties.getInstance().getProperty("PickupAddress.longitude"));
        this.latitude = Double.parseDouble(GlobalConfigProperties.getInstance().getProperty("PickupAddress.latitude"));
        this.buildingNumber =GlobalConfigProperties.getInstance().getProperty("PickupAddress.buildingNumber");
        this.buildingName =GlobalConfigProperties.getInstance().getProperty("PickupAddress.buildingName");
        this.floor =GlobalConfigProperties.getInstance().getProperty("PickupAddress.floor");
        this.apartmeNt =GlobalConfigProperties.getInstance().getProperty("PickupAddress.apartmeNt");
        this.pOBox =GlobalConfigProperties.getInstance().getProperty("PickupAddress.pOBox");
        this.description =GlobalConfigProperties.getInstance().getProperty("PickupAddress.description");
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine3() {
        return line3;
    }

    public void setLine3(String line3) {
        this.line3 = line3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateOrProvinceCode() {
        return stateOrProvinceCode;
    }

    public void setStateOrProvinceCode(String stateOrProvinceCode) {
        this.stateOrProvinceCode = stateOrProvinceCode;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Object getpOBox() {
        return pOBox;
    }

    public void setpOBox(Object pOBox) {
        this.pOBox = pOBox;
    }

    public Object getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(Object buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public Object getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(Object buildingName) {
        this.buildingName = buildingName;
    }

    public Object getFloor() {
        return floor;
    }

    public void setFloor(Object floor) {
        this.floor = floor;
    }

    public Object getApartmeNt() {
        return apartmeNt;
    }

    public void setApartmeNt(Object apartmeNt) {
        this.apartmeNt = apartmeNt;
    }

    public Object getPOBox() {
        return pOBox;
    }

    public void setPOBox(Object pOBox) {
        this.pOBox = pOBox;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("line1", line1).append("line2", line2).append("line3", line3).append("city", city).append("stateOrProvinceCode", stateOrProvinceCode).append("postCode", postCode).append("countryCode", countryCode).append("longitude", longitude).append("latitude", latitude).append("buildingNumber", buildingNumber).append("buildingName", buildingName).append("floor", floor).append("apartmeNt", apartmeNt).append("pOBox", pOBox).append("description", description).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(pOBox).append(buildingName).append(apartmeNt).append(countryCode).append(postCode).append(stateOrProvinceCode).append(city).append(floor).append(description).append(longitude).append(latitude).append(line1).append(line3).append(buildingNumber).append(line2).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PickupAddress) == false) {
            return false;
        }
        PickupAddress rhs = ((PickupAddress) other);
        return new EqualsBuilder().append(pOBox, rhs.pOBox).append(buildingName, rhs.buildingName).append(apartmeNt, rhs.apartmeNt).append(countryCode, rhs.countryCode).append(postCode, rhs.postCode).append(stateOrProvinceCode, rhs.stateOrProvinceCode).append(city, rhs.city).append(floor, rhs.floor).append(description, rhs.description).append(longitude, rhs.longitude).append(latitude, rhs.latitude).append(line1, rhs.line1).append(line3, rhs.line3).append(buildingNumber, rhs.buildingNumber).append(line2, rhs.line2).isEquals();
    }

}
