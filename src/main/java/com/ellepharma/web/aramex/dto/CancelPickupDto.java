
package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class CancelPickupDto {

    @SerializedName("ClientInfo")
    @Expose
    private ClientInfo clientInfo;
    @SerializedName("Comments")
    @Expose
    private String comments;
    @SerializedName("PickupGUID")
    @Expose
    private String pickupGUID;
    @SerializedName("Transaction")
    @Expose
    private Transaction transaction;

    public CancelPickupDto() {
        clientInfo = new ClientInfo();

        comments = "Test";
        pickupGUID = "f9a1e0ed-25a3-42f8-a907-aff7675394dc";
        transaction = new Transaction("", "", "", "", "");
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPickupGUID() {
        return pickupGUID;
    }

    public void setPickupGUID(String pickupGUID) {
        this.pickupGUID = pickupGUID;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("clientInfo", clientInfo).append("comments", comments).append("pickupGUID", pickupGUID).append("transaction", transaction).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(transaction).append(clientInfo).append(comments).append(pickupGUID).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CancelPickupDto) == false) {
            return false;
        }
        CancelPickupDto rhs = ((CancelPickupDto) other);
        return new EqualsBuilder().append(transaction, rhs.transaction).append(clientInfo, rhs.clientInfo).append(comments, rhs.comments).append(pickupGUID, rhs.pickupGUID).isEquals();
    }

}
