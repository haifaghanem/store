package com.ellepharma.web.aramex.dto;


public class ShipmentDetails
{
    private String Origin;

    private String GoodsOriginCountry;

    private Object CashAdditionalAmount;

    private String Destination;

    private String ProductGroup;

    private String DescriptionOfGoods;

    private String ProductType;

    private Object PaymentOptions;

    private Object CollectAmount;

    private String NumberOfPieces;

    private CustomsValueAmount CustomsValueAmount;

    private ActualWeight ChargeableWeight;

    private Object Services;

    private Object InsuranceAmount;

    private CashOnDeliveryAmount CashOnDeliveryAmount;

    private String PaymentType;

    public String getOrigin ()
    {
        return Origin;
    }

    public void setOrigin (String Origin)
    {
        this.Origin = Origin;
    }

    public String getGoodsOriginCountry ()
    {
        return GoodsOriginCountry;
    }

    public void setGoodsOriginCountry (String GoodsOriginCountry)
    {
        this.GoodsOriginCountry = GoodsOriginCountry;
    }

    public Object getCashAdditionalAmount ()
{
    return CashAdditionalAmount;
}

    public void setCashAdditionalAmount (Object CashAdditionalAmount)
    {
        this.CashAdditionalAmount = CashAdditionalAmount;
    }

    public String getDestination ()
    {
        return Destination;
    }

    public void setDestination (String Destination)
    {
        this.Destination = Destination;
    }

    public String getProductGroup ()
    {
        return ProductGroup;
    }

    public void setProductGroup (String ProductGroup)
    {
        this.ProductGroup = ProductGroup;
    }

    public String getDescriptionOfGoods ()
    {
        return DescriptionOfGoods;
    }

    public void setDescriptionOfGoods (String DescriptionOfGoods)
    {
        this.DescriptionOfGoods = DescriptionOfGoods;
    }

    public String getProductType ()
    {
        return ProductType;
    }

    public void setProductType (String ProductType)
    {
        this.ProductType = ProductType;
    }

    public Object getPaymentOptions ()
{
    return PaymentOptions;
}

    public void setPaymentOptions (Object PaymentOptions)
    {
        this.PaymentOptions = PaymentOptions;
    }

    public Object getCollectAmount ()
{
    return CollectAmount;
}

    public void setCollectAmount (Object CollectAmount)
    {
        this.CollectAmount = CollectAmount;
    }

    public String getNumberOfPieces ()
    {
        return NumberOfPieces;
    }

    public void setNumberOfPieces (String NumberOfPieces)
    {
        this.NumberOfPieces = NumberOfPieces;
    }

    public CustomsValueAmount getCustomsValueAmount ()
    {
        return CustomsValueAmount;
    }

    public void setCustomsValueAmount (CustomsValueAmount CustomsValueAmount)
    {
        this.CustomsValueAmount = CustomsValueAmount;
    }

    public ActualWeight getChargeableWeight ()
    {
        return ChargeableWeight;
    }

    public void setChargeableWeight (ActualWeight ChargeableWeight)
    {
        this.ChargeableWeight = ChargeableWeight;
    }

    public Object getServices ()
{
    return Services;
}

    public void setServices (Object Services)
    {
        this.Services = Services;
    }

    public Object getInsuranceAmount ()
{
    return InsuranceAmount;
}

    public void setInsuranceAmount (Object InsuranceAmount)
    {
        this.InsuranceAmount = InsuranceAmount;
    }

    public CashOnDeliveryAmount getCashOnDeliveryAmount ()
    {
        return CashOnDeliveryAmount;
    }

    public void setCashOnDeliveryAmount (CashOnDeliveryAmount CashOnDeliveryAmount)
    {
        this.CashOnDeliveryAmount = CashOnDeliveryAmount;
    }

    public String getPaymentType ()
    {
        return PaymentType;
    }

    public void setPaymentType (String PaymentType)
    {
        this.PaymentType = PaymentType;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Origin = "+Origin+", GoodsOriginCountry = "+GoodsOriginCountry+", CashAdditionalAmount = "+CashAdditionalAmount+", Destination = "+Destination+", ProductGroup = "+ProductGroup+", DescriptionOfGoods = "+DescriptionOfGoods+", ProductType = "+ProductType+", PaymentOptions = "+PaymentOptions+", CollectAmount = "+CollectAmount+", NumberOfPieces = "+NumberOfPieces+", CustomsValueAmount = "+CustomsValueAmount+", ChargeableWeight = "+ChargeableWeight+", Services = "+Services+", InsuranceAmount = "+InsuranceAmount+", CashOnDeliveryAmount = "+CashOnDeliveryAmount+", PaymentType = "+PaymentType+"]";
    }
}
