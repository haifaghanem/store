package com.ellepharma.web.aramex.dto;

import com.ellepharma.utils.Utils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

public class Shipment {

    @SerializedName("Reference1")
    @Expose
    private Object reference1;

    @SerializedName("ID")
    @Expose
    private String id;

    @SerializedName("Reference2")
    @Expose
    private Object reference2;

    @SerializedName("Reference3")
    @Expose
    private Object reference3;

    @SerializedName("ShipmentLabel")
    @Expose
    private ShipmentLabel shipmentLabel;

    @SerializedName("Shipper")
    @Expose
    private Shipper shipper;

    @SerializedName("Consignee")
    @Expose
    private Consignee consignee;

    @SerializedName("ThirdParty")
    @Expose
    private Object thirdParty;

    @SerializedName("ShippingDateTime")
    @Expose
    private String shippingDateTime;

    @SerializedName("DueDate")
    @Expose
    private String dueDate;

    @SerializedName("Comments")
    @Expose
    private Object comments;

    @SerializedName("PickupLocation")
    @Expose
    private Object pickupLocation;

    @SerializedName("OperationsInstructions")
    @Expose
    private Object operationsInstructions;

    @SerializedName("AccountingInstrcutions")
    @Expose
    private Object accountingInstrcutions;

    @SerializedName("Details")
    @Expose
    private Details details;

    @SerializedName("ShipmentDetails")
    @Expose
    private ShipmentDetails shipmentDetails;

    @SerializedName("Attachments")
    @Expose
    private Object attachments;

    @SerializedName("ForeignHAWB")
    @Expose
    private Object foreignHAWB;

    @SerializedName("TransportType")
    @Expose
    private Integer transportType;

    @SerializedName("PickupGUID")
    @Expose
    private Object pickupGUID;

    @SerializedName("Number")
    @Expose
    private Object number;

    @SerializedName("ScheduledDelivery")
    @Expose
    private Object scheduledDelivery;

    @SerializedName("HasErrors")
    @Expose
    private Boolean hasErrors;

    public Shipment() {
        reference1 = null;
        reference2 = null;
        reference3 = null;
        long shippingDateTimeL = LocalDateTime.of(LocalDate.now(), LocalTime.now()).atZone(ZoneId.of(Utils.getDefaultZoneId())).toEpochSecond() * 1000;
        //dueDate = "/Date(1561887870000)/";
        shippingDateTime = "/Date(" + System.currentTimeMillis() + ")/";
        dueDate = "/Date(" + System.currentTimeMillis() + ")/";
        //shippingDateTime = "/Date({{$timestamp}}000)/";
        //dueDate = "/Date({{$timestamp}}000)/";
        transportType = 0;
        thirdParty = null;
        comments = null;
        pickupLocation = null;
        operationsInstructions = null;
        accountingInstrcutions = null;
        attachments = null;
        foreignHAWB = null;
        pickupGUID = null;
        number = null;
        scheduledDelivery = null;

    }

    public Object getReference1() {
        return reference1;
    }

    public void setReference1(Object reference1) {
        this.reference1 = reference1;
    }

    public Object getReference2() {
        return reference2;
    }

    public void setReference2(Object reference2) {
        this.reference2 = reference2;
    }

    public Object getReference3() {
        return reference3;
    }

    public void setReference3(Object reference3) {
        this.reference3 = reference3;
    }

    public Shipper getShipper() {
        return shipper;
    }

    public void setShipper(Shipper shipper) {
        this.shipper = shipper;
    }

    public Consignee getConsignee() {
        return consignee;
    }

    public void setConsignee(Consignee consignee) {
        this.consignee = consignee;
    }

    public Object getThirdParty() {
        return thirdParty;
    }

    public void setThirdParty(Object thirdParty) {
        this.thirdParty = thirdParty;
    }

    public String getShippingDateTime() {
        return shippingDateTime;
    }

    public void setShippingDateTime(String shippingDateTime) {
        this.shippingDateTime = shippingDateTime;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Object getComments() {
        return comments;
    }

    public void setComments(Object comments) {
        this.comments = comments;
    }

    public Object getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(Object pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public Object getOperationsInstructions() {
        return operationsInstructions;
    }

    public void setOperationsInstructions(Object operationsInstructions) {
        this.operationsInstructions = operationsInstructions;
    }

    public Object getAccountingInstrcutions() {
        return accountingInstrcutions;
    }

    public void setAccountingInstrcutions(Object accountingInstrcutions) {
        this.accountingInstrcutions = accountingInstrcutions;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public Object getAttachments() {
        return attachments;
    }

    public void setAttachments(Object attachments) {
        this.attachments = attachments;
    }

    public Object getForeignHAWB() {
        return foreignHAWB;
    }

    public void setForeignHAWB(Object foreignHAWB) {
        this.foreignHAWB = foreignHAWB;
    }

    public Integer getTransportType() {
        return transportType;
    }

    public void setTransportType(Integer transportType) {
        this.transportType = transportType;
    }

    public Object getPickupGUID() {
        return pickupGUID;
    }

    public void setPickupGUID(Object pickupGUID) {
        this.pickupGUID = pickupGUID;
    }

    public Object getNumber() {
        return number;
    }

    public void setNumber(Object number) {
        this.number = number;
    }

    public Object getScheduledDelivery() {
        return scheduledDelivery;
    }

    public void setScheduledDelivery(Object scheduledDelivery) {
        this.scheduledDelivery = scheduledDelivery;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("reference1", reference1).append("reference2", reference2).append("reference3", reference3)
                .append("shipper", shipper).append("consignee", consignee).append("thirdParty", thirdParty)
                .append("shippingDateTime", shippingDateTime).append("dueDate", dueDate)
                .append("comments", comments).append("pickupLocation", pickupLocation)
                .append("operationsInstructions", operationsInstructions)
                .append("accountingInstrcutions", accountingInstrcutions).append("details", details)
                .append("attachments", attachments).append("foreignHAWB", foreignHAWB)
                .append("transportType", transportType).append("pickupGUID", pickupGUID).append("number", number)
                .append("scheduledDelivery", scheduledDelivery).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(operationsInstructions).append(consignee).append(shippingDateTime).append(shipper)
                .append(number).append(foreignHAWB).append(pickupGUID).append(reference2).append(reference1)
                .append(pickupLocation).append(details).append(accountingInstrcutions).append(scheduledDelivery)
                .append(thirdParty).append(attachments).append(dueDate).append(transportType).append(comments)
                .append(reference3).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Shipment) == false) {
            return false;
        }
        Shipment rhs = ((Shipment) other);
        return new EqualsBuilder().append(operationsInstructions, rhs.operationsInstructions).append(consignee, rhs.consignee)
                .append(shippingDateTime, rhs.shippingDateTime).append(shipper, rhs.shipper).append(number, rhs.number)
                .append(foreignHAWB, rhs.foreignHAWB).append(pickupGUID, rhs.pickupGUID)
                .append(reference2, rhs.reference2).append(reference1, rhs.reference1)
                .append(pickupLocation, rhs.pickupLocation).append(details, rhs.details)
                .append(accountingInstrcutions, rhs.accountingInstrcutions)
                .append(scheduledDelivery, rhs.scheduledDelivery).append(thirdParty, rhs.thirdParty)
                .append(attachments, rhs.attachments).append(dueDate, rhs.dueDate)
                .append(transportType, rhs.transportType).append(comments, rhs.comments)
                .append(reference3, rhs.reference3).isEquals();
    }

    public Boolean getHasErrors() {
        return hasErrors;
    }

    public void setHasErrors(Boolean hasErrors) {
        this.hasErrors = hasErrors;
    }

    public ShipmentLabel getShipmentLabel() {
        return shipmentLabel;
    }

    public void setShipmentLabel(ShipmentLabel shipmentLabel) {
        this.shipmentLabel = shipmentLabel;
    }

    public ShipmentDetails getShipmentDetails() {
        return shipmentDetails;
    }

    public void setShipmentDetails(ShipmentDetails shipmentDetails) {
        this.shipmentDetails = shipmentDetails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
