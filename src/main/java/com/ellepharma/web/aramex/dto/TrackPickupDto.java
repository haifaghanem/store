package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrackPickupDto {
    @SerializedName("ClientInfo")
    @Expose
    private ClientInfo clientInfo;
    @SerializedName("Reference")
    @Expose
    private String reference;
    @SerializedName("Transaction")
    @Expose
    private Transaction transaction;

    public TrackPickupDto() {

        reference = "A09E775";
        transaction = new Transaction("", "", "", "", "");
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}