package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class FetchCitiesDto {

    @SerializedName("ClientInfo")
    @Expose
    private ClientInfo clientInfo;
    @SerializedName("CountryCode")
    @Expose
    private String countryCode;
    @SerializedName("NameStartsWith")
    @Expose
    private String nameStartsWith;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("Transaction")
    @Expose
    private Transaction transaction;

    public FetchCitiesDto() {
        clientInfo = new ClientInfo();

        countryCode = "JO";
        nameStartsWith = "";
        state = "";
        transaction = new Transaction("", "", "", "", "");
    }

    public FetchCitiesDto(String countryCode) {
        clientInfo = new ClientInfo();

        this.countryCode = countryCode;
        nameStartsWith = "";
        state = "";
        transaction = new Transaction("", "", "", "", "");
    }


    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNameStartsWith() {
        return nameStartsWith;
    }

    public void setNameStartsWith(String nameStartsWith) {
        this.nameStartsWith = nameStartsWith;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("clientInfo", clientInfo).append("countryCode", countryCode).append("nameStartsWith", nameStartsWith).append("state", state).append("transaction", transaction).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(transaction).append(state).append(countryCode).append(nameStartsWith).append(clientInfo).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof FetchCitiesDto) == false) {
            return false;
        }
        FetchCitiesDto rhs = ((FetchCitiesDto) other);
        return new EqualsBuilder().append(transaction, rhs.transaction).append(state, rhs.state).append(countryCode, rhs.countryCode).append(nameStartsWith, rhs.nameStartsWith).append(clientInfo, rhs.clientInfo).isEquals();
    }

}
