package com.ellepharma.web.aramex.dto;

import com.ellepharma.service.GlobalConfigProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Shipper {

    @SerializedName("Reference1")
    @Expose
    private Object reference1;
    @SerializedName("Reference2")
    @Expose
    private Object reference2;
    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("PartyAddress")
    @Expose
    private PartyAddress partyAddress;
    @SerializedName("Contact")
    @Expose
    private Contact contact;

    public Shipper() {
        reference1 = null;
        reference2 = null;
        accountNumber =GlobalConfigProperties.getInstance().getProperty("ClientInfo.accountNumber");
        partyAddress = new PartyAddress();
        this.partyAddress.setLine1(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.line1"));
        this.partyAddress.setLine2(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.line2"));
        this.partyAddress.setLine3(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.line3"));
        this.partyAddress.setCity(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.city"));
        this.partyAddress.setStateOrProvinceCode(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.stateOrProvinceCode"));
        this.partyAddress.setPostCode(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.postCode"));
        this.partyAddress.setCountryCode(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.countryCode"));
        this.partyAddress.setLongitude(Double.parseDouble(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.longitude")));
        this.partyAddress.setLatitude(Double.parseDouble(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.latitude")));
        this.partyAddress.setBuildingNumber(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.buildingNumber"));
        this.partyAddress.setBuildingName(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.buildingName"));
        this.partyAddress.setFloor(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.floor"));
        this.partyAddress.setApartmeNt(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.apartmeNt"));
        this.partyAddress.setPOBox(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.pOBox"));
        this.partyAddress.setDescription(GlobalConfigProperties.getInstance().getProperty("Shipper.PartyAddress.description"));
        this.contact = new Contact();

        this.contact.setDepartment(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.department"));
        this.contact.setPersonName(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.personName"));
        this.contact.setTitle(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.title"));
        this.contact.setCompanyName(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.companyName"));
        this.contact.setPhoneNumber1(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.phoneNumber1"));
        this.contact.setPhoneNumber1Ext(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.phoneNumber1Ext"));
        this.contact.setPhoneNumber2(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.phoneNumber2"));
        this.contact.setPhoneNumber2Ext(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.phoneNumber2Ext"));
        this.contact.setFaxNumber(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.faxNumber"));
        this.contact.setCellPhone(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.cellPhone"));
        this.contact.setEmailAddress(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.emailAddress"));
        this.contact.setType(GlobalConfigProperties.getInstance().getProperty("Shipper.contact.type"));

    }

    public Object getReference1() {
        return reference1;
    }

    public void setReference1(Object reference1) {
        this.reference1 = reference1;
    }

    public Object getReference2() {
        return reference2;
    }

    public void setReference2(Object reference2) {
        this.reference2 = reference2;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public PartyAddress getPartyAddress() {
        return partyAddress;
    }

    public void setPartyAddress(PartyAddress partyAddress) {
        this.partyAddress = partyAddress;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("reference1", reference1).append("reference2", reference2).append("accountNumber", accountNumber).append("partyAddress", partyAddress).append("contact", contact).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(reference2).append(reference1).append(accountNumber).append(partyAddress).append(contact).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Shipper) == false) {
            return false;
        }
        Shipper rhs = ((Shipper) other);
        return new EqualsBuilder().append(reference2, rhs.reference2).append(reference1, rhs.reference1).append(accountNumber, rhs.accountNumber).append(partyAddress, rhs.partyAddress).append(contact, rhs.contact).isEquals();
    }

}
