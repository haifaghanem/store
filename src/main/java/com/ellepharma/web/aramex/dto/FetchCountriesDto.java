
package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class FetchCountriesDto {

    @SerializedName("ClientInfo")
    @Expose
    private ClientInfo clientInfo;
    @SerializedName("Transaction")
    @Expose
    private Transaction transaction;

    public FetchCountriesDto() {
        clientInfo = new ClientInfo();

        transaction = new Transaction("", "", "", "", "");

    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("clientInfo", clientInfo).append("transaction", transaction).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(transaction).append(clientInfo).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof FetchCountriesDto) == false) {
            return false;
        }
        FetchCountriesDto rhs = ((FetchCountriesDto) other);
        return new EqualsBuilder().append(transaction, rhs.transaction).append(clientInfo, rhs.clientInfo).isEquals();
    }

}
