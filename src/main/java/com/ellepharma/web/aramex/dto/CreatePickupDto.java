package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class CreatePickupDto {

    @SerializedName("ClientInfo")
    @Expose
    private ClientInfo clientInfo;

    @SerializedName("LabelInfo")
    @Expose
    private LabelInfo labelInfo;

    @SerializedName("Pickup")
    @Expose
    private Pickup pickup;

    @SerializedName("Transaction")
    @Expose
    private Transaction transaction;

    public CreatePickupDto() {

    }

    public CreatePickupDto(ClientInfo clientInfo, LabelInfo labelInfo, Pickup pickup, Transaction transaction) {
        this.clientInfo = clientInfo;
        this.labelInfo = labelInfo;
        this.pickup = pickup;
        this.transaction = transaction;
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }

    public LabelInfo getLabelInfo() {
        return labelInfo;
    }

    public void setLabelInfo(LabelInfo labelInfo) {
        this.labelInfo = labelInfo;
    }

    public Pickup getPickup() {
        return pickup;
    }

    public void setPickup(Pickup pickup) {
        this.pickup = pickup;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("clientInfo", clientInfo).append("labelInfo", labelInfo).append("pickup", pickup).append("transaction", transaction).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(pickup).append(transaction).append(labelInfo).append(clientInfo).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CreatePickupDto) == false) {
            return false;
        }
        CreatePickupDto rhs = ((CreatePickupDto) other);
        return new EqualsBuilder().append(pickup, rhs.pickup).append(transaction, rhs.transaction).append(labelInfo, rhs.labelInfo).append(clientInfo, rhs.clientInfo).isEquals();
    }

}
