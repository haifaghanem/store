
package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class ActualWeight {

    @SerializedName("Unit")
    @Expose
    private String unit;
    @SerializedName("Value")
    @Expose
    private Double value;

    public ActualWeight() {
        unit = "KG";
        value = 0.5;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("unit", unit).append("value", value).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(unit).append(value).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ActualWeight) == false) {
            return false;
        }
        ActualWeight rhs = ((ActualWeight) other);
        return new EqualsBuilder().append(unit, rhs.unit).append(value, rhs.value).isEquals();
    }

}
