package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Details {

    @SerializedName("Dimensions")
    @Expose
    private Object dimensions;
    @SerializedName("ActualWeight")
    @Expose
    private ActualWeight actualWeight;
    @SerializedName("ChargeableWeight")
    @Expose
    private Object chargeableWeight;
    @SerializedName("DescriptionOfGoods")
    @Expose
    private String descriptionOfGoods;
    @SerializedName("GoodsOriginCountry")
    @Expose
    private String goodsOriginCountry;
    @SerializedName("NumberOfPieces")
    @Expose
    private Integer numberOfPieces;
    @SerializedName("ProductGroup")
    @Expose
    private String productGroup;
    @SerializedName("ProductType")
    @Expose
    private String productType;
    @SerializedName("PaymentType")
    @Expose
    private String paymentType;
    @SerializedName("PaymentOptions")
    @Expose
    private Object paymentOptions;
    @SerializedName("CustomsValueAmount")
    @Expose
    private CustomsValueAmount customsValueAmount;
    @SerializedName("CashOnDeliveryAmount")
    @Expose
    private CashOnDeliveryAmount cashOnDeliveryAmount;
    @SerializedName("InsuranceAmount")
    @Expose
    private Object insuranceAmount;
    @SerializedName("CashAdditionalAmount")
    @Expose
    private Object cashAdditionalAmount;
    @SerializedName("CashAdditionalAmountDescription")
    @Expose
    private Object cashAdditionalAmountDescription;
    @SerializedName("CollectAmou nt")
    @Expose
    private Object collectAmouNt;
    @SerializedName("Services")
    @Expose
    private Object services;
    @SerializedName("Items")
    @Expose
    private Object items;
    @SerializedName("DeliveryInstructions")
    @Expose
    private Object deliveryInstructions;

    public Details() {
        actualWeight = new ActualWeight();
        descriptionOfGoods = "Shoes";
        goodsOriginCountry = "AE";
        numberOfPieces = 1;
        productGroup = "DOM";
        productType = "CDS";
        paymentType = "P";
        dimensions = null;
        chargeableWeight = null;
        paymentOptions = null;
        customsValueAmount = null;
        cashOnDeliveryAmount = null;
        insuranceAmount = null;
        cashAdditionalAmount = null;
        cashAdditionalAmountDescription = null;
        collectAmouNt = null;
        services = null;
        items = null;
        deliveryInstructions = null;
    }

    public Object getDimensions() {
        return dimensions;
    }

    public void setDimensions(Object dimensions) {
        this.dimensions = dimensions;
    }

    public ActualWeight getActualWeight() {
        return actualWeight;
    }

    public void setActualWeight(ActualWeight actualWeight) {
        this.actualWeight = actualWeight;
    }

    public Object getChargeableWeight() {
        return chargeableWeight;
    }

    public void setChargeableWeight(Object chargeableWeight) {
        this.chargeableWeight = chargeableWeight;
    }

    public String getDescriptionOfGoods() {
        return descriptionOfGoods;
    }

    public void setDescriptionOfGoods(String descriptionOfGoods) {
        this.descriptionOfGoods = descriptionOfGoods;
    }

    public String getGoodsOriginCountry() {
        return goodsOriginCountry;
    }

    public void setGoodsOriginCountry(String goodsOriginCountry) {
        this.goodsOriginCountry = goodsOriginCountry;
    }

    public Integer getNumberOfPieces() {
        return numberOfPieces;
    }

    public void setNumberOfPieces(Integer numberOfPieces) {
        this.numberOfPieces = numberOfPieces;
    }

    public String getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Object getPaymentOptions() {
        return paymentOptions;
    }

    public void setPaymentOptions(Object paymentOptions) {
        this.paymentOptions = paymentOptions;
    }

    public Object getCustomsValueAmount() {
        return customsValueAmount;
    }


    public Object getInsuranceAmount() {
        return insuranceAmount;
    }

    public void setInsuranceAmount(Object insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }

    public Object getCashAdditionalAmount() {
        return cashAdditionalAmount;
    }

    public void setCashAdditionalAmount(Object cashAdditionalAmount) {
        this.cashAdditionalAmount = cashAdditionalAmount;
    }

    public Object getCashAdditionalAmountDescription() {
        return cashAdditionalAmountDescription;
    }

    public void setCashAdditionalAmountDescription(Object cashAdditionalAmountDescription) {
        this.cashAdditionalAmountDescription = cashAdditionalAmountDescription;
    }

    public Object getCollectAmouNt() {
        return collectAmouNt;
    }

    public void setCollectAmouNt(Object collectAmouNt) {
        this.collectAmouNt = collectAmouNt;
    }

    public Object getServices() {
        return services;
    }

    public void setServices(Object services) {
        this.services = services;
    }

    public Object getItems() {
        return items;
    }

    public void setItems(Object items) {
        this.items = items;
    }

    public Object getDeliveryInstructions() {
        return deliveryInstructions;
    }

    public void setDeliveryInstructions(Object deliveryInstructions) {
        this.deliveryInstructions = deliveryInstructions;
    }

    public void setCustomsValueAmount(CustomsValueAmount customsValueAmount) {
        this.customsValueAmount = customsValueAmount;
    }

    public CashOnDeliveryAmount getCashOnDeliveryAmount() {
        return cashOnDeliveryAmount;
    }

    public void setCashOnDeliveryAmount(CashOnDeliveryAmount cashOnDeliveryAmount) {
        this.cashOnDeliveryAmount = cashOnDeliveryAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("dimensions", dimensions).append("actualWeight", actualWeight).append("chargeableWeight", chargeableWeight).append("descriptionOfGoods", descriptionOfGoods).append("goodsOriginCountry", goodsOriginCountry).append("numberOfPieces", numberOfPieces).append("productGroup", productGroup).append("productType", productType).append("paymentType", paymentType).append("paymentOptions", paymentOptions).append("customsValueAmount", customsValueAmount).append("cashOnDeliveryAmount", cashOnDeliveryAmount).append("insuranceAmount", insuranceAmount).append("cashAdditionalAmount", cashAdditionalAmount).append("cashAdditionalAmountDescription", cashAdditionalAmountDescription).append("collectAmouNt", collectAmouNt).append("services", services).append("items", items).append("deliveryInstructions", deliveryInstructions).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(productGroup).append(deliveryInstructions).append(services).append(paymentType).append(numberOfPieces).append(descriptionOfGoods).append(customsValueAmount).append(goodsOriginCountry).append(cashAdditionalAmountDescription).append(productType).append(chargeableWeight).append(collectAmouNt).append(actualWeight).append(cashAdditionalAmount).append(cashOnDeliveryAmount).append(paymentOptions).append(items).append(insuranceAmount).append(dimensions).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Details) == false) {
            return false;
        }
        Details rhs = ((Details) other);
        return new EqualsBuilder().append(productGroup, rhs.productGroup).append(deliveryInstructions, rhs.deliveryInstructions).append(services, rhs.services).append(paymentType, rhs.paymentType).append(numberOfPieces, rhs.numberOfPieces).append(descriptionOfGoods, rhs.descriptionOfGoods).append(customsValueAmount, rhs.customsValueAmount).append(goodsOriginCountry, rhs.goodsOriginCountry).append(cashAdditionalAmountDescription, rhs.cashAdditionalAmountDescription).append(productType, rhs.productType).append(chargeableWeight, rhs.chargeableWeight).append(collectAmouNt, rhs.collectAmouNt).append(actualWeight, rhs.actualWeight).append(cashAdditionalAmount, rhs.cashAdditionalAmount).append(cashOnDeliveryAmount, rhs.cashOnDeliveryAmount).append(paymentOptions, rhs.paymentOptions).append(items, rhs.items).append(insuranceAmount, rhs.insuranceAmount).append(dimensions, rhs.dimensions).isEquals();
    }

}
