package com.ellepharma.web.aramex.dto;

import com.ellepharma.service.GlobalConfigProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class PickupContact {

    @SerializedName("Department")
    @Expose
    private String department;
    @SerializedName("PersonName")
    @Expose
    private String personName;
    @SerializedName("Title")
    @Expose
    private Object title;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("PhoneNumber1")
    @Expose
    private String phoneNumber1;
    @SerializedName("PhoneNumber1Ext")
    @Expose
    private Object phoneNumber1Ext;
    @SerializedName("PhoneNumber2")
    @Expose
    private Object phoneNumber2;
    @SerializedName("PhoneNumber2Ext")
    @Expose
    private Object phoneNumber2Ext;
    @SerializedName("FaxNumber")
    @Expose
    private Object faxNumber;
    @SerializedName("CellPhone")
    @Expose
    private String cellPhone;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("Type")
    @Expose
    private Object type;

    public PickupContact() {
        this.department = GlobalConfigProperties.getInstance().getProperty("PickupContact.department");
        this.personName =GlobalConfigProperties.getInstance().getProperty("PickupContact.personName");
        this.title =GlobalConfigProperties.getInstance().getProperty("PickupContact.title");
        this.companyName =GlobalConfigProperties.getInstance().getProperty("PickupContact.companyName");
        this.phoneNumber1 =GlobalConfigProperties.getInstance().getProperty("PickupContact.phoneNumber1");
        this.phoneNumber1Ext =GlobalConfigProperties.getInstance().getProperty("PickupContact.phoneNumber1Ext");
        this.phoneNumber2 =GlobalConfigProperties.getInstance().getProperty("PickupContact.phoneNumber2");
        this.phoneNumber2Ext =GlobalConfigProperties.getInstance().getProperty("PickupContact.phoneNumber2Ext");
        this.faxNumber =GlobalConfigProperties.getInstance().getProperty("PickupContact.faxNumber");
        this.cellPhone =GlobalConfigProperties.getInstance().getProperty("PickupContact.cellPhone");
        this.emailAddress =GlobalConfigProperties.getInstance().getProperty("PickupContact.emailAddress");
        this.type =GlobalConfigProperties.getInstance().getProperty("PickupContact.type");
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitle(Object title) {
        this.title = title;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public Object getPhoneNumber1Ext() {
        return phoneNumber1Ext;
    }

    public void setPhoneNumber1Ext(Object phoneNumber1Ext) {
        this.phoneNumber1Ext = phoneNumber1Ext;
    }

    public Object getPhoneNumber2() {
        return phoneNumber2;
    }

    public void setPhoneNumber2(Object phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }

    public Object getPhoneNumber2Ext() {
        return phoneNumber2Ext;
    }

    public void setPhoneNumber2Ext(Object phoneNumber2Ext) {
        this.phoneNumber2Ext = phoneNumber2Ext;
    }

    public Object getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(Object faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("department", department).append("personName", personName).append("title", title).append("companyName", companyName).append("phoneNumber1", phoneNumber1).append("phoneNumber1Ext", phoneNumber1Ext).append("phoneNumber2", phoneNumber2).append("phoneNumber2Ext", phoneNumber2Ext).append("faxNumber", faxNumber).append("cellPhone", cellPhone).append("emailAddress", emailAddress).append("type", type).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(phoneNumber1Ext).append(department).append(cellPhone).append(phoneNumber2).append(emailAddress).append(phoneNumber1).append(type).append(companyName).append(personName).append(title).append(faxNumber).append(phoneNumber2Ext).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PickupContact) == false) {
            return false;
        }
        PickupContact rhs = ((PickupContact) other);
        return new EqualsBuilder().append(phoneNumber1Ext, rhs.phoneNumber1Ext).append(department, rhs.department).append(cellPhone, rhs.cellPhone).append(phoneNumber2, rhs.phoneNumber2).append(emailAddress, rhs.emailAddress).append(phoneNumber1, rhs.phoneNumber1).append(type, rhs.type).append(companyName, rhs.companyName).append(personName, rhs.personName).append(title, rhs.title).append(faxNumber, rhs.faxNumber).append(phoneNumber2Ext, rhs.phoneNumber2Ext).isEquals();
    }

}
