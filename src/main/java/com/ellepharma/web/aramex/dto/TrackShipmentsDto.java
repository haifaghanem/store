
package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

public class TrackShipmentsDto {

    @SerializedName("ClientInfo")
    @Expose
    private ClientInfo clientInfo;
    @SerializedName("GetLastTrackingUpdateOnly")
    @Expose
    private Boolean getLastTrackingUpdateOnly;
    @SerializedName("Shipments")
    @Expose
    private List<String> shipments = null;
    @SerializedName("Transaction")
    @Expose
    private Transaction transaction;

    public TrackShipmentsDto() {

        this.getLastTrackingUpdateOnly=false;
        shipments = new ArrayList<>();
        shipments.add("1588423056");
        transaction = new Transaction("", "", "", "", "");
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }

    public Boolean getGetLastTrackingUpdateOnly() {
        return getLastTrackingUpdateOnly;
    }

    public void setGetLastTrackingUpdateOnly(Boolean getLastTrackingUpdateOnly) {
        this.getLastTrackingUpdateOnly = getLastTrackingUpdateOnly;
    }

    public List<String> getShipments() {
        return shipments;
    }

    public void setShipments(List<String> shipments) {
        this.shipments = shipments;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("clientInfo", clientInfo).append("getLastTrackingUpdateOnly", getLastTrackingUpdateOnly).append("shipments", shipments).append("transaction", transaction).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(transaction).append(shipments).append(getLastTrackingUpdateOnly).append(clientInfo).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TrackShipmentsDto) == false) {
            return false;
        }
        TrackShipmentsDto rhs = ((TrackShipmentsDto) other);
        return new EqualsBuilder().append(transaction, rhs.transaction).append(shipments, rhs.shipments).append(getLastTrackingUpdateOnly, rhs.getLastTrackingUpdateOnly).append(clientInfo, rhs.clientInfo).isEquals();
    }

}
