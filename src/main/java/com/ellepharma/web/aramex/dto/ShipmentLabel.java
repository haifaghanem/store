package com.ellepharma.web.aramex.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShipmentLabel
{


    @SerializedName("LabelFileContents")
    @Expose
    private String[] labelFileContents;

    @SerializedName("LabelURL")
    @Expose
    private String labelURL;

    public String[] getLabelFileContents() {
        return labelFileContents;
    }

    public void setLabelFileContents(String[] labelFileContents) {
        this.labelFileContents = labelFileContents;
    }

    public String getLabelURL() {
        return labelURL;
    }

    public void setLabelURL(String labelURL) {
        this.labelURL = labelURL;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [LabelFileContents = "+labelFileContents+", LabelURL = "+labelURL+"]";
    }
}

