package com.ellepharma.handler;

import javax.faces.FacesException;
import javax.faces.application.NavigationHandler;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import java.util.Iterator;
import java.util.Map;

public class CustomExceptionHandler extends ExceptionHandlerWrapper {

	private ExceptionHandler exceptionHandler;

	public CustomExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}

	@Override
	public ExceptionHandler getWrapped() {
		return exceptionHandler;
	}

	@Override
	public void handle() throws FacesException {
		final Iterator<ExceptionQueuedEvent> queue = getUnhandledExceptionQueuedEvents().iterator();

		while (queue.hasNext()) {
			ExceptionQueuedEvent item = queue.next();
			ExceptionQueuedEventContext exceptionQueuedEventContext = (ExceptionQueuedEventContext) item.getSource();

			try {
				Throwable throwable = exceptionQueuedEventContext.getException();
				System.err.println("Exception: " + throwable.getMessage());

				FacesContext context = FacesContext.getCurrentInstance();
				Map<String, Object> requestMap = context.getExternalContext().getRequestMap();
				NavigationHandler nav = context.getApplication().getNavigationHandler();

				requestMap.put("error-message", throwable.getMessage());
				requestMap.put("error-stack", throwable.getStackTrace());
				nav.handleNavigation(context, null, "/public/error.xhtml");
				context.renderResponse();

			} finally {
				queue.remove();
			}
		}
	}
}
/*

public class CustomExceptionHandler extends FullAjaxExceptionHandler {

	private static String ERROR_PAGE = "/public/error.xhtml";

	public CustomExceptionHandler(ExceptionHandler wrapped) {
		super(wrapped);
	}

	@Override
	protected Throwable findExceptionRootCause(FacesContext context, Throwable exception) {

		if (exception.getCause().getCause().getCause() instanceof BusinessException) {
			BusinessException businessException = (BusinessException) exception.getCause().getCause().getCause();

			context.addMessage("form_messages", new FacesMessage(FacesMessage.SEVERITY_ERROR,
					Utils.getMsg(exception.getMessage()), "dddddddd"));
			JsfUtils.update("form_messages");
			scrollToTop();
			return null;
		} else if (exception instanceof AccessDeniedException) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("public/access-denied.xhtml");
			} catch (IOException e) {
				throw new InternalSystemException("Wrong Access Denied Page Path");
			}
		} else if (exception instanceof ObjectOptimisticLockingFailureException) {
			context.addMessage("form_messages", new FacesMessage(FacesMessage.SEVERITY_ERROR,
					Utils.getMsg("common.locking.error"), ""));
			JsfUtils.update("form_messages");
			return exception;
		}

		return exception;
	}

	private void scrollToTop() {

		*/
/*PrimeFaces.current().scrollTo("");*//*

	}

	@Override
	// overridden to solve Omnifaces issue for not reading error page except from WebXML
	protected String findErrorPageLocation(FacesContext context, Throwable exception) {
		return ERROR_PAGE;
	}

}
*/
