package com.ellepharma.exception;

import com.ellepharma.data.model.BaseEntity;

public class BusinessException extends ElleException {

    private static final long serialVersionUID = 1L;

    public BusinessException(String msgCode) {
        super(msgCode);
    }

    public BusinessException(String msgCode, BaseEntity entity) {
        super(msgCode);
    }

}
