package com.ellepharma.exception;

public abstract class ElleException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	// private GenericProcessCodes processCode;

	public ElleException() {
	}

	// public ATSException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
	// super(message, cause, enableSuppression, writableStackTrace);
	// }
	//
	// public ATSException(String message, Throwable cause) {
	// super(message, cause);

	// }

	public ElleException(String message) {
		super(message);
	}

	// public GenericProcessCodes getProcessCode() {
	// return processCode;
	// }
	//
	// public void setProcessCode(GenericProcessCodes processCode) {
	// this.processCode = processCode;
	// }

	public ElleException(Throwable cause) {
		super(cause);
	}

}
