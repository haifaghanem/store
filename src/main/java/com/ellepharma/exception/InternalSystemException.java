package com.ellepharma.exception;

public class InternalSystemException extends ElleException {

	private static final long serialVersionUID = 1L;

	public InternalSystemException(String msgCode) {
		super(msgCode);

	}

}
