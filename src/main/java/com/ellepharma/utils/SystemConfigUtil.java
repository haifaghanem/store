package com.ellepharma.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component("SystemConfigUtil")
@Configuration
@PropertySource({"classpath:config.properties"})
public class SystemConfigUtil {

	@Autowired
	private Environment environment;

	public String getProperty(String key) {

		return environment.getProperty(key);
	}

}
