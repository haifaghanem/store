package com.ellepharma.utils;


public class FreeMarkerMailHelper {

	String header;

	String body;

	String footer = "This mail sent by e-Register®";

	String to;

	String from;


	public FreeMarkerMailHelper() {
	}

	/**
	 * @param header Mail header
	 * @param body   Mail Body
	 * @param to     Mail to to send 'to'
	 * @param from   Mail send 'from'
	 */
	public FreeMarkerMailHelper(String header, String body, String to, String from) {
		this.header = header;
		this.body = body;
		this.to = to;
		this.from = from;
	}

	/**
	 * @param header Mail header
	 * @param body   Mail Body
	 * @param to     Mail to to send 'to'
	 */
	public FreeMarkerMailHelper(String header, String body, String to) {
		this.header = header;
		this.body = body;
		this.to = to;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

}
