package com.ellepharma.utils;

import com.ellepharma.constant.TransField;

import javax.faces.context.FacesContext;

/**
 * TransUtil.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */
public class TransUtil {

	/**
	 * It get the current local translation of TransField field
	 *
	 * @param transField
	 * @return
	 */
	public static String transalte(final TransField transField) {

		if (transField == null) {
			return null;
		}
		return transField.get(getCurrentLanguage());

	}

	public static String transalte(final TransField transField, FacesContext facesContext) {

		if (transField == null) {
			return null;
		}
		return transField.get(getCurrentLanguage(facesContext));

	}

	/**
	 * It get the current local translation of ComMessage field
	 *
	 * @param message
	 * @return
	 */
/*	public static String transalte(final ComMessage message) {

		if (message == null || message.getMessageDesc() == null) {
			return "";
		}
		return message.getMessageDesc().get(getCurrentLanguage());

	}*/

	/**
	 * It get the current used locale of the system
	 *
	 * @return
	 */
	public static String getCurrentLanguage() {

		return FacesContext.getCurrentInstance().getViewRoot().getLocale().toString().toLowerCase();
	}

	private static String getCurrentLanguage(FacesContext facesContext) {

		return facesContext.getViewRoot().getLocale().toString().toLowerCase();
	}

	public static String getCurrentDirection() {
		String currentLanguage = getCurrentLanguage();
		if (currentLanguage.startsWith("ar"))
			return "RTL";
		return "LTR";
	}

}
