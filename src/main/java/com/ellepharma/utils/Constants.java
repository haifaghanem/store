package com.ellepharma.utils;

public class Constants {

    public final static String FOLDER_PATH = "folderPath";

    public final static String CATEGORY = "category";

    public final static String ADS = "ads";

    public final static String ACCOUNT = "account";

    public final static String PRODUCT = "product";

    public final static String SLIDER = "slider";

    public final static String BRAND = "brand";
    public final static String PAGE = "page";
    public final static String ARTICLE = "article";

    public final static String LANG = "lang";

    public static final String SLIDE = "slide";

    public static final String ID = "id";

    public static final String CURRENT_LANGUAGE = "currentLanguage";
    public static final String CURRENT_COUNTRY = "currentCountry";
    public static final String CURRENT_CURRENCY = "currentCurrency";
    public static final String CURRENT_COUNTRY_SHIPMENT = "currentCountryShipment";

    public static final String CURRENT_COUNTRY_PAYMENT = "currentCountryPayment";

    public static final String CAT_IDS = "cId";
    public static final String BRAND_IDS = "bId";
    public static final String SORT_TYPE = "sortType";
    public static final String SEARCH_BY_NAME = "searchByName";
    public static final String CURRENT_PAGE = "p";

}
