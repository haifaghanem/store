package com.ellepharma.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

public class SystemConfigReader {

	public static int getNumericFieldInteger() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Integer.parseInt(systemConfigUtil.getProperty("numeric.field.integer"));
	}

	public static int getNumericFieldFraction() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Integer.parseInt(systemConfigUtil.getProperty("numeric.field.fraction"));
	}

	public static int getMarkFieldInteger() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Integer.parseInt(systemConfigUtil.getProperty("mark.field.integer"));
	}

	public static int getMarkFieldFraction() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Integer.parseInt(systemConfigUtil.getProperty("mark.field.fraction"));
	}

	public static int getPercentFieldInteger() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Integer.parseInt(systemConfigUtil.getProperty("percent.field.integer"));
	}

	public static int getPercentFieldFraction() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Integer.parseInt(systemConfigUtil.getProperty("percent.field.fraction"));
	}

	public static int getAverageFieldInteger() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Integer.parseInt(systemConfigUtil.getProperty("average.field.integer"));
	}

	public static int getAverageFieldFraction() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Integer.parseInt(systemConfigUtil.getProperty("average.field.fraction"));
	}

	public static int getFinancialFieldInteger() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Integer.parseInt(systemConfigUtil.getProperty("financial.field.integer"));
	}

	public static int getFinancialFieldFraction() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Integer.parseInt(systemConfigUtil.getProperty("financial.field.fraction"));
	}

	public static long getFileUploadSizeMax() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Long.parseLong(systemConfigUtil.getProperty("sys.fileUploadSize.max"));
	}

	public static long getFileUploadSizePhotoMax() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return Long.parseLong(systemConfigUtil.getProperty("sys.fileUploadSize.photo.max"));
	}

	public static List<String> getFileUploadAllowedTypes() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String concatStr = systemConfigUtil.getProperty("sys.file.allowedTypes");
		return Arrays.asList(concatStr.split(","));
	}

	public static List<String> getIconAllowedTypes() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String concatStr = systemConfigUtil.getProperty("sys.icon.allowedTypes");
		return Arrays.asList(concatStr.split(","));
	}

	public static List<String> getPhotoAllowedTypes() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String concatStr = systemConfigUtil.getProperty("sys.photo.allowedTypes");
		return Arrays.asList(concatStr.split(","));
	}

	public static String getDefaultLatLng() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String defaultLatLng = systemConfigUtil.getProperty("gmap.defaultLatLng");
		return defaultLatLng;
	}

	public static String getFileUploadAllowedTypesStr() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String concatStr = systemConfigUtil.getProperty("sys.file.allowedTypes") + ")";
		concatStr = "/(\\.|\\/)(" + concatStr.replaceAll(",", "|") + "$/";
		return concatStr;
	}

	public static String getPhotoAllowedTypesStr() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String concatStr = systemConfigUtil.getProperty("sys.photo.allowedTypes") + ")";
		concatStr = "/(\\.|\\/)(" + concatStr.replaceAll(",", "|") + "$/";
		return concatStr;
	}

	public static String getIconAllowedTypesStr() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String concatStr = systemConfigUtil.getProperty("sys.icon.allowedTypes") + ")";
		concatStr = "/(\\.|\\/)(" + concatStr.replaceAll(",", "|") + "$/";
		return concatStr;
	}

	public static String getLogFileName() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return systemConfigUtil.getProperty("log.filename");
	}

	public static String getDefaultTemplate() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		return systemConfigUtil.getProperty("system.email.defaultTemplate");
	}

	public static String getWebSocketRelayHost() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String defaultWebSocketRelayHost = systemConfigUtil.getProperty("websocket.relay.host");
		return defaultWebSocketRelayHost;
	}

	public static Long getMinPasswordLenght() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String defaultMinPasswordLenght = systemConfigUtil.getProperty("ums.passwordlevel.length.min");
		return new Long(defaultMinPasswordLenght);
	}

	public static Long getMaxPasswordLenght() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String defaultMaxPasswordLenght = systemConfigUtil.getProperty("ums.passwordlevel.length.max");
		return new Long(defaultMaxPasswordLenght);
	}

	/**
	 * return with millisecond's
	 *
	 * @return
	 */
	public static int getSessionTimeout() {

		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String timeout = systemConfigUtil.getProperty("ats.idleTimeout");
		if (StringUtils.isEmpty(timeout)) {
			return 1800000;
		}
		return Integer.parseInt(timeout);
	}

	/**
	 * return with millisecond's
	 *
	 * @return
	 */
	public static String getTimeoutBlocked() {

		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		String timeout = systemConfigUtil.getProperty("security.cache.ttl");
		return timeout;
	}

	public static boolean isSystemPermissionsApplied() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");
		final String property = systemConfigUtil.getProperty("system.permissions.applied");
		if (property == null) {
			return true;
		}

		boolean permissionsApplied = property.trim().equals("1");
		return permissionsApplied;
	}

	public static String getReportFilePath() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");

		return systemConfigUtil.getProperty("report.file.path");

	}

	public static String getImageFilePath() {
		SystemConfigUtil systemConfigUtil = (SystemConfigUtil) SpringUtil.getBean("SystemConfigUtil");

		return systemConfigUtil.getProperty("image.path");

	}
}
