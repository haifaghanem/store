package com.ellepharma.utils;

import com.ellepharma.constant.FlexField;
import com.ellepharma.constant.JsonAttributeValue;
import com.ellepharma.constant.TransField;
import com.ellepharma.exception.BusinessException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class JSONUtil {

    /**
     * convert any object or list to JSON
     *
     * @param object
     * @return String
     */
    public static String convertObjectToJSON(Object object) {
        ObjectMapper mapper = new ObjectMapper();

        Hibernate5Module hbm = new Hibernate5Module();
        // hbm.enable(Hibernate4Module.Feature.SERIALIZE_IDENTIFIER_FOR_LAZY_NOT_LOADED_OBJECTS);
        hbm.disable(Hibernate5Module.Feature.SERIALIZE_IDENTIFIER_FOR_LAZY_NOT_LOADED_OBJECTS);

        mapper.registerModule(hbm);
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        try {
            return (mapper.writeValueAsString(object));
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * convert JSON in jsonString to List of type targetClass
     *
     * @param jsonString
     * @param targetClass
     * @return List<targetClass>
     */
    public static List<?> convertJSONToList(String jsonString, Class<?> targetClass) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        try {
            return (mapper.readValue(jsonString, mapper.getTypeFactory().constructCollectionType(List.class, targetClass)));
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * convert JSON in jsonString to List of type targetClass
     *
     * @param jsonString
     * @param keyClass
     * @return List<targetClass>
     */
    public static HashMap<?, ?> convertJSONToMap(String jsonString, Class<?> keyClass, Class<?> valueClass) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        Hibernate5Module hbm = new Hibernate5Module();
        // hbm.enable(Hibernate4Module.Feature.SERIALIZE_IDENTIFIER_FOR_LAZY_NOT_LOADED_OBJECTS);
        hbm.disable(Hibernate5Module.Feature.SERIALIZE_IDENTIFIER_FOR_LAZY_NOT_LOADED_OBJECTS);

        mapper.registerModule(hbm);

        try {
            return (mapper.readValue(jsonString, mapper.getTypeFactory().constructMapType(HashMap.class, keyClass, valueClass)));
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * convert JSON on jsonString to Object of type targetClass
     *
     * @param jsonString
     * @param targetClass
     * @return T Object of type targetClass
     */
    public static <T> T convertJSONToObject(String jsonString, Class<T> targetClass) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        try {
            return (mapper.readValue(jsonString, targetClass));
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException(e.getMessage());
        }
    }

    public static String convertTransFieldToJson(TransField transField) {
        Gson gson = new Gson();

        return gson.toJson(transField, TransField.class);
    }

    public static TransField convertJsonToTransField(String json) {
        Gson gson = new Gson();
        try {
            return gson.fromJson(json, TransField.class);
        } catch (JsonSyntaxException e) {
            System.err.println("JSON ERROR FOR " + json);
            return null;
        }
    }

    public static String convertFlexFieldToJson(FlexField flexField) {
        Gson gson = new Gson();

        return gson.toJson(flexField, FlexField.class);
    }

    public static FlexField convertJsonToFlexField(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, FlexField.class);
    }

    public static Object readJsonSimple(String obj) {
        JSONParser parser = new JSONParser();
        Object parse = null;
        try {
            parse = parser.parse(obj);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        JSONArray jsonObject = (JSONArray) parse;
     //   System.out.println(jsonObject);

        //		String name = (String) jsonObject.get("param");
        //		System.out.println(name);
        //
        //		long age = (Long) jsonObject.get("val");
        //		System.out.println(age);

        // loop array
        //		JSONArray msg = (JSONArray) jsonObject.get("messages");
        Iterator<Object> iterator = jsonObject.iterator();
        while (iterator.hasNext()) {
            JSONObject jsonObject1 = (JSONObject) iterator.next();
           // System.out.println(jsonObject1.keySet());
           // System.out.println(jsonObject1.get("param"));
           // System.out.println(jsonObject1.get("val"));

        }
        return parse;
    }

    public static String convertAttributeValuesToJsonString(JsonAttributeValue... attributeValues) {
        if (attributeValues == null || attributeValues.length == 0)
            return null;

        JSONArray list = new JSONArray();
        for (JsonAttributeValue attributeValue : attributeValues) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("a", attributeValue.getEntityAttributeEnum().getCode());
            jsonObject.put("v", attributeValue.getValue());
            list.add(jsonObject);
        }

        final String jsonString = list.toJSONString();
        return jsonString;
    }

    public static String formatString(String local, String str) {
        try {
            JsonParser parser = new JsonParser();
            JsonObject o = parser.parse(str).getAsJsonObject();
            return o.get(local).toString().replace("\"", "");
        } catch (Exception var5) {
            return "";
        }
    }

    public static String formatString(String local, String... str) {
        List<String> list = Arrays.asList(str);
        JsonParser parser = new JsonParser();

        String joinedStrs;
        try {
            joinedStrs = list.stream().map((s) -> {
                return parser.parse(s).getAsJsonObject().get(local).toString().replace("\"", "");
            }).collect(Collectors.joining(" "));
        } catch (Exception var7) {
            joinedStrs = "";
        }

        return joinedStrs;
    }

    public static String formatString(String local, String delimiter, String... str) {
        List<String> list = Arrays.asList(str);
        JsonParser parser = new JsonParser();

        String joinedStrs;
        try {
            joinedStrs = list.stream().map((s) -> {
                return parser.parse(s).getAsJsonObject().get(local).toString().replace("\"", "");
            }).collect(Collectors.joining(delimiter));
        } catch (Exception var8) {
            joinedStrs = "";
        }

        return joinedStrs;
    }

    public static void main(String[] args) throws IOException {
        // TransField title = new TransField();
        // title.put("en", "Sample Process");
        // title.put("ar", "AAAA");
        //
        // String convertTransFieldToJson = convertTransFieldToJson(title);
        // System.out.println(convertTransFieldToJson);

        String json = Files.lines(Paths.get("E:/JSON.txt")).collect(Collectors.joining());

        final Object o = readJsonSimple(json);
        System.out.println(o);

    }

}
