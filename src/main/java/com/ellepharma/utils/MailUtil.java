package com.ellepharma.utils;

import com.ellepharma.constant.ConfigEnum;
import com.ellepharma.service.GlobalConfigProperties;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.*;

public class MailUtil {

    public static void sendEmailWithAttachments(String toAddress,
                                                String subject, String message, List<String> attachFiles)
            throws MessagingException {

        String host =GlobalConfigProperties.getInstance().getProperty("mail.host");
        String port =GlobalConfigProperties.getInstance().getProperty("mail.port");
        String userName =GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_USER.name());
        String password =GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_PASSWORD.name());


        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.user", userName);
        properties.put("mail.password", password);

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };
        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);


        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};


        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject, "UTF-8");
        msg.setSentDate(new Date());

        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html");


        // creates multi-part
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);


        // adds attachments
        if (attachFiles != null && attachFiles.size() > 0) {
            for (String filePath : attachFiles) {
                MimeBodyPart attachPart = new MimeBodyPart();

                try {
                    attachPart.attachFile(filePath);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                multipart.addBodyPart(attachPart);
            }
        }

        // sets the multi-part as e-mail's content
        msg.setContent(multipart, "text/html");

        // sends the e-mail
        Transport.send(msg);

    }

    public static void sendEmail(String toAddress, String subject, String message)
            throws MessagingException {

        String host =GlobalConfigProperties.getInstance().getProperty("mail.host");
        String port =GlobalConfigProperties.getInstance().getProperty("mail.port");
        String userName =GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_USER.name());
        String password =GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_PASSWORD.name());

        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.user", userName);
        properties.put("mail.password", password);

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };
        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        List<InternetAddress> toAddresses = new ArrayList<>();

        msg.setFrom(new InternetAddress(userName));
        Arrays.stream(toAddress.split(",")).forEach(to -> {
            try {
                InternetAddress too = new InternetAddress(to);
                toAddresses.add(too);
            } catch (AddressException e) {
                e.printStackTrace();
            }
        });

        InternetAddress[] arry = toAddresses.toArray(new InternetAddress[toAddresses.size()]);
        msg.setRecipients(Message.RecipientType.TO, arry);
        msg.setSubject(subject, "UTF-8");
        msg.setSentDate(new Date());

        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html");



        // sets the multi-part as e-mail's content
        msg.setText(message, "utf-8", "html");

        // sends the e-mail
        Transport.send(msg);

    }

   /* public static void main(String[] args) {

        // message info
        String mailTo = "saeed.walweel@gmail.com";
        String subject = "New email with attachments";
        String message = "I have some attachments for you.";

        // attachments
        List<String> attachFiles = new ArrayList<>();
        attachFiles.add("f:/test.png");

        try {
            sendEmailWithAttachments(mailTo,
                    subject, message, attachFiles);
            System.out.println("Email sent.");
        } catch (Exception ex) {
            System.out.println("Could not send email.");
            ex.printStackTrace();
        }
    }*/

}
