package com.ellepharma.utils;

import com.ellepharma.constant.ConfigEnum;
import com.ellepharma.constant.TransField;
import com.ellepharma.data.model.CountryShipment;
import com.ellepharma.data.model.Currency;
import com.ellepharma.data.model.SystemLanguage;
import com.ellepharma.service.ContentService;
import com.ellepharma.service.CountryShipmentService;
import com.ellepharma.service.GlobalConfigProperties;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.*;
import java.util.*;

/**
 * Util.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 17, 2019
 */
@ManagedBean
@RequestScoped
public class Utils {

	private static ContentService contentService;

	public static Map<String, Object> getOptions() {

		int width = 0;
		int height = 0;
		try {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("surveyMB", null);
			width = Integer.parseInt(JsfUtils.getRequestParameterMap("width"));
			height = Integer.parseInt(JsfUtils.getRequestParameterMap("height"));
		} catch (Exception e) {
			width = 700;
			height = 700;
			// TODO: handle exception
		}
		Map<String, Object> options = new HashMap<>();
		options.put("resizable", true);
		options.put("modal", true);
		options.put("closable", false);
		options.put("maximizable", true);
		options.put("maximizable", true);
		options.put("width", width - 200);
		options.put("dir", "RTL");
		options.put("height", height - 50);
		options.put("contentWidth", width - 235);
		options.put("contentHeight", height - 60);
		return options;

	}

	public static Object[] createArray(int size) {
		return new Object[size];
	}

	public static List<SystemLanguage> getSystemLangsSorted() {
		//get ComponentsLanguageBean that contains session related language details
		//	CommonMB commonMB = JSFUtil.findBean("commonMB");
		List<SystemLanguage> systemLangs = new ArrayList<>();//commonMB.getActiveMB().getAllLangs();
		SystemLanguage ar = new SystemLanguage();
		ar.setId(1L);
		ar.setDirection("rtl");
		ar.setLocale("ar");
		ar.setShortcut("ع");
		TransField arD = new TransField();
		arD.put("ar", "العربية");
		arD.put("en", "Arabic");
		ar.setLanguageDesc(arD);
		ar.setIsMandatory(true);
		ar.setLanguageOrder(1L);
		ar.setIsRtl(true);
		SystemLanguage en = new SystemLanguage();
		en.setId(2L);
		en.setLanguageOrder(2L);
		en.setDirection("ltr");
		en.setLocale("en");
		en.setShortcut("En");
		TransField enD = new TransField();
		enD.put("ar", "الإنجليزية");
		enD.put("en", "English");
		en.setLanguageDesc(enD);
		en.setIsRtl(false);
		en.setIsMandatory(true);

		systemLangs.add(ar);
		systemLangs.add(en);

		//sort by language order
		systemLangs.sort(Comparator.comparing(SystemLanguage::getLanguageOrder));

		//		//remove languages where isEntry = false or isShow = false
		systemLangs.removeIf(s -> s.getLanguageOrder() > 2 && !s.getIsEntry() && s.getIsShow());

		return systemLangs;

	}

	public static String getUserIp() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		return ipAddress;
	}

	public static double calcShipment(double weight) {
		if (weight == 0)
			return 0;
		CountryShipment countryShipment = JsfUtils.getFromSessionMap(Constants.CURRENT_COUNTRY_SHIPMENT);

		CountryShipmentService countryShipmentService = JsfUtils.findBean("countryShipmentService");
		countryShipment = countryShipmentService.findById(countryShipment.getId());
		double ship = 0;
		if (countryShipment != null) {
			ship = countryShipment.getShipmentRate();
			if (weight > 500) {
				ship += (Math.ceil((weight - 500) / 500)) * countryShipment.getShipmentRateExtra();
			}

		}
		return ship;
	}

	public static String getDefaultZoneId() {
		String s = GlobalConfigProperties.getInstance().getProperty(ConfigEnum.DEFAULT_ZONE_ID.name());
		if (StringUtils.isEmpty(s)) {
			s = "Asia/Riyadh";

		}
		return s;

	}

	public static void redirect(String payment_url) {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		try {
			externalContext.redirect(payment_url);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static ContentService getContentService() {
		if (contentService == null) {
			contentService = JsfUtils.findBean("minioService");

		}
		return contentService;
	}



        /*public static String htmlToPdf(String html) throws IOException, DocumentException {
            File pdf = File.createTempFile("output.", ".pdf");
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream(pdf.getPath()));
            document.open();
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, new ByteArrayInputStream(html.getBytes()));
            document.close();
            return pdf.getAbsolutePath();
        }*/

	public List getListFromSet(Set set) {
		return new ArrayList(set);
	}

	public double Ceiling(double d) {
		return Math.ceil(d);
	}

	public double Round(double d) {
		return Math.round(d);
	}

	public static String uploud(FileUploadEvent event, String folderPath) {
		UploadedFile file = event.getFile();
		String fileName = System.currentTimeMillis() + "_" + FilenameUtils.getName(event.getFile().getFileName());
		//String fileType = FilenameUtils.getName(event.getFile().getContentType());
		String tempFileURL = null;
		try {
			getContentService().createStorageUnit("store");
			InputStream inputstream = file.getInputstream();
			//fileName = fileName.substring(fileName.indexOf("."));
			fileName = getContentService().putContent("store", fileName, inputstream, file.getSize(), file.getContentType());
		} catch (Exception e) {

			e.printStackTrace();
		}

		return fileName;
	}

	public static String uploadWithFileName(FileUploadEvent event) {
		UploadedFile file = event.getFile();
		String fileName = FilenameUtils.getName(event.getFile().getFileName());
		try {
			getContentService().createStorageUnit("store");
			InputStream inputstream = file.getInputstream();
			fileName = getContentService().putContent("store", fileName, inputstream, file.getSize(), file.getContentType());
		} catch (Exception e) {

			e.printStackTrace();
		}

		return fileName;
	}

	public static String uploudFileWithOrginalName(FileUploadEvent event, String folder) {

		UploadedFile file = event.getFile();

		String fileName = FilenameUtils.getName(event.getFile().getFileName());
		String fileType = FilenameUtils.getName(event.getFile().getContentType());
		try {
			if (prepareFile(file.getInputstream(), fileName, fileType, folder)) {
				return fileName;

			} else {
				return "";
			}
		} catch (IOException e) {
			return "";

		}
	}

	public static boolean prepareFile(InputStream inputStream2, String fileName, String fileType, String folderPath) {

		try {
			File catalinaBase = new File(GlobalConfigProperties.getInstance().getProperty(Constants.FOLDER_PATH))
					.getAbsoluteFile();

			File subDir = new File(catalinaBase.getPath() + File.separator + folderPath);
			if (!subDir.exists()) {
				subDir.mkdirs();
			}
			File uploadFile = new File(subDir.getPath() + File.separator + fileName);
			uploadFile.createNewFile();
			OutputStream outputStream = new FileOutputStream(uploadFile);
			byte[] buffer = new byte[1024];
			int length = 0;
			while ((length = inputStream2.read(buffer)) != -1) {
				outputStream.write(buffer, 0, length);
			}
			outputStream.flush();
			outputStream.close();
			inputStream2.close();
			JsfUtils.addSuccessMessage();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean renameFile(String oldFileName, String newFileName, String folderIndex, String docRef) {

		File catalinaBase = new File(System.getProperty("catalina.base")).getAbsoluteFile();
		File rootDir = new File(catalinaBase, "webapps/reports");
		// rootDir.mkdirs();
		File oldfile = new File(rootDir.getPath() + File.separator + folderIndex + File.separator + docRef
				+ File.separator + oldFileName);
		File newfile = new File(rootDir.getPath() + File.separator + folderIndex + File.separator + docRef
				+ File.separator + newFileName);

		//	System.out.println("Rename succesful");
		//System.out.println("Rename failed");
		return oldfile.renameTo(newfile);
	}

	public static void deleteFromIO(String folderIndex, String reportName, String fileName) {
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
																	 .getContext();
		String path = servletContext.getRealPath("/");
		File filePath = new File(path + "\\resources\\reports\\" + folderIndex + File.separator + reportName
				+ File.separator + fileName);
		filePath.delete();
	}

	public static String convertTime(Date time) {
		if (null == time) {
			return "";
		}

		Format format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(time);
	}

	public static Date convertStringTime(String timeString) {
		if (StringUtils.isEmpty(timeString)) {
			return null;
		}
		try {
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			date = format.parse(timeString);
			return date;
		} catch (ParseException ex) {
			ex.printStackTrace();
			return new Date();
		}

	}

	public static void setCookie(String name, String value, int expiry) {

		FacesContext facesContext = FacesContext.getCurrentInstance();

		HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
		Cookie cookie = null;

		Cookie[] userCookies = request.getCookies();
		if (userCookies != null && userCookies.length > 0) {
			for (int i = 0; i < userCookies.length; i++) {
				if (userCookies[i].getName().equals(name)) {
					cookie = userCookies[i];
					break;
				}
			}
		}

		if (cookie != null) {
			cookie.setValue(value);
		} else {
			cookie = new Cookie(name, value);
			cookie.setPath("/");
		}

		cookie.setMaxAge(expiry);

		HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
		response.addCookie(cookie);
	}

	public static Cookie getCookie(String name) {

		FacesContext facesContext = FacesContext.getCurrentInstance();

		HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
		Cookie cookie = null;

		Cookie[] userCookies = request.getCookies();
		if (userCookies != null && userCookies.length > 0) {
			for (int i = 0; i < userCookies.length; i++) {
				if (userCookies[i].getName().equals(name)) {
					cookie = userCookies[i];
					return cookie;
				}
			}
		}
		return null;
	}

	public static String getMsg(String key) {
		if (key != null && !StringUtils.isEmpty(key.trim())) {
			FacesContext context = FacesContext.getCurrentInstance();
			String bundleKey = "#{msg['key']}".replace("key", key);
			String msg = context.getApplication().evaluateExpressionGet(context, bundleKey, String.class);
			if (msg != null) {
				return msg;
			}
		}
		return key;
	}

	public static SystemLanguage getCurrentLang() {
		SystemLanguage language = JsfUtils.getFromSessionMap("currentLanguage");
		if (language==null){
			language =Utils.getSystemLangsSorted().get(0);
		}

		return language;

	}

	public static Locale getLocale() {

		return FacesContext.getCurrentInstance().getViewRoot().getLocale();

	}

	public static boolean isArabic() {
		return getLocale().getLanguage().equals("ar");

	}

	public static String getHost() {
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		return req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort();
	}

	public static String getAppNampe() {
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		return req.getContextPath();
	}

	public static String getFullUrlApp() {
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		//return "https://ellepharma.com/store";//req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath();
		return GlobalConfigProperties.
				getInstance().getProperty(ConfigEnum.STORE_URL
						.name());//req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath();
	}

	public static Currency getCurrentCurrency() {
		Currency currency = JsfUtils.getFromSessionMap(Constants.CURRENT_CURRENCY);
		return currency;
	}

	public static Double convertPrice(Double price) {
		if (Objects.isNull(price)) {
			price = 0D;
		}
		DecimalFormat df = new DecimalFormat("#.###");
		df.setRoundingMode(RoundingMode.CEILING);
		String i = df.format(getCurrentCurrency().getValue());
		return new Double(i) * price;
	}

	public static String formatInvoice(Long invoiceNo) {
		return "INV" + String.format("%06d", invoiceNo);
	}

	public static String convertPriceToString(Double price) {
		DecimalFormat df = new DecimalFormat("#.###");
		if (Objects.isNull(price)) {
			price = 0D;
		}
		df.setRoundingMode(RoundingMode.CEILING);
		return df.format(price);
	}

	public static String encodeURIComponent(String component) {
		String result = null;

		try {
			result = URLEncoder.encode(component, "UTF-8")
							   .replaceAll("\\%28", "(")
							   .replaceAll("\\%29", ")")
							   .replaceAll("\\+", "%20")
							   .replaceAll("\\%27", "'")
							   .replaceAll("\\%21", "!")
							   .replaceAll("\\%7E", "~");
		} catch (UnsupportedEncodingException e) {
			result = component;
		}

		return result;
	}


	public Double ceil(Double val) {
		return Math.ceil(val);
	}
  /*  public static Double convertToTowDigitsOnly(Double val) {

        DecimalFormat df2 = new DecimalFormat("#.##");
        return new Double(df2.format(val));

    }*/

}
