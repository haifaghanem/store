package com.ellepharma.utils;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.jpa.EntityManagerFactoryInfo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.sql.Connection;

public class SpringUtil implements ApplicationContextAware {

	private static ApplicationContext CONTEXT;

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		CONTEXT = context;
	}

	public static Object getBean(String beanName) {
		return CONTEXT.getBean(beanName);
	}

    public static <T> T getBean(Class<T> beanClass) {
        return CONTEXT.getBean(beanClass);
    }

    public static ListableBeanFactory getListableBeanFactory() {
        return (ListableBeanFactory) CONTEXT.getAutowireCapableBeanFactory();
    }

	private static Connection connection;

	private static DataSource dataSource;

	@PersistenceContext
	EntityManager entityManager;

	public static void releaseConnection() {
		DataSourceUtils.releaseConnection(connection, dataSource);
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Connection getConnection() {
		if (this.connection == null) {
			EntityManagerFactoryInfo info = (EntityManagerFactoryInfo) entityManager.getEntityManagerFactory();
			dataSource = info.getDataSource();
			connection = DataSourceUtils.getConnection(dataSource);
		}

		return connection;
	}

}
