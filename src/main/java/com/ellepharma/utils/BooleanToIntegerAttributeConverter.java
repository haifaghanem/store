package com.ellepharma.utils;

import javax.persistence.AttributeConverter;

/**
 * BooleanToIntegerAttributeConverter.java, Used to convert entity attribute value from integer to boolean and vice versa
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */

public class BooleanToIntegerAttributeConverter implements AttributeConverter<Boolean, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Boolean attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute ? 1 : 0;
    }

    @Override
    public Boolean convertToEntityAttribute(Integer dbData) {
        if (dbData == null) {
            return null;
        }
        return dbData == 0 ? false : true;
    }

}
