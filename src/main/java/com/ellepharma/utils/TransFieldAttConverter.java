package com.ellepharma.utils;

import com.ellepharma.constant.TransField;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;

/**
 * TransFieldAttConverter.java is responsible to convert JSON to TransField and vis versa
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */

public class TransFieldAttConverter implements AttributeConverter<TransField, String> {

    @Override
    public String convertToDatabaseColumn(TransField attribute) {
        if (attribute == null) {
            return null;
        }

        return JSONUtil.convertTransFieldToJson(attribute);
    }

    @Override
    public TransField convertToEntityAttribute(String dbData) {
        if (StringUtils.isEmpty(dbData)) {
            return new TransField();
        }
        return JSONUtil.convertJsonToTransField(dbData);
    }

}
