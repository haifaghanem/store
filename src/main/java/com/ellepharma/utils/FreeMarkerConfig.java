package com.ellepharma.utils;

import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

//@Configuration
public class FreeMarkerConfig {
	/*
	 * FreeMarker configuration.
	 */
	//@Bean
	public FreeMarkerConfigurationFactoryBean getFreeMarkerConfiguration() {
		FreeMarkerConfigurationFactoryBean bean = new FreeMarkerConfigurationFactoryBean();
		bean.setDefaultEncoding("UTF-8");
		bean.setTemplateLoaderPath("classpath:/fmtemplates/");

		return bean;
	}
}
