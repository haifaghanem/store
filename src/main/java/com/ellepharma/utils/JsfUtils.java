package com.ellepharma.utils;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.sun.faces.component.visit.FullVisitContext;
import org.apache.commons.lang3.ArrayUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.component.dialog.Dialog;
import org.primefaces.component.selectonemenu.SelectOneMenu;

import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.FactoryFinder;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.event.PhaseId;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The JSFUtil class implements the logic of building binding expression
 * dynamically.
 */
@ManagedBean
public class JsfUtils {

    public static MethodExpression createMethodExpression(String expression, Class<?> returnType, Class<?>... parameterTypes) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getApplication().getExpressionFactory().createMethodExpression(
                facesContext.getELContext(), expression, returnType, parameterTypes);
    }

    public static MethodExpression createMethodExpression(FacesContext facesContext, String expression, Class<?> returnType,
                                                          Class<?>... parameterTypes) {
        return facesContext.getApplication().getExpressionFactory().createMethodExpression(
                facesContext.getELContext(), expression, returnType, parameterTypes);
    }

    public static ValueExpression createValueExpression(String valueExpression, Class<?> valueType) {
        FacesContext context = FacesContext.getCurrentInstance();
        return context.getApplication().getExpressionFactory()
                .createValueExpression(context.getELContext(), valueExpression, valueType);

    }

    public static <T> T findBean(String beanName) {
        FacesContext context = FacesContext.getCurrentInstance();
        return (T) context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", Object.class);
        //return (T) SpringUtil.getBean(beanName);
    }

    public static <T> T findBean(String beanName, FacesContext context) {
        return (T) context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", Object.class);
    }

    public static <T> T createCompositeComponent(String taglibURI, String tagName, Map<String, Object> attributes) {

        FacesContext context = FacesContext.getCurrentInstance();
        UIComponent composite = context.getApplication().getViewHandler()
                .getViewDeclarationLanguage(context, context.getViewRoot().getViewId())
                .createComponent(context, taglibURI, tagName, attributes);

        return (T) composite;
    }

    public static <T> List<T> getCompositeChildrens(UIComponent composite, Class<T> clazz) {
        return (List<T>) composite.getFacet(UIComponent.COMPOSITE_FACET_NAME).getChildren().stream().filter(c -> clazz.isInstance(c))
                .collect(Collectors
                        .toList()); //findFirst().orElseThrow(() -> new ChildComponentNotSupported("Composite does not has a requested child"));

    }

    public static <T> List<T> getCompositeChildrenLevelOnes(UIComponent composite, Class<T> clazz) {
        return (List<T>) composite.getFacet(UIComponent.COMPOSITE_FACET_NAME).getChildren().get(0).getChildren().stream()
                .filter(c -> clazz.isInstance(c)).collect(Collectors
                        .toList()); //findFirst().orElseThrow(() -> new ChildComponentNotSupported("Composite does not has a requested child"));

    }

    public static String formatEntityName(String entityName) {
        if (entityName == null) {
            return entityName;
        } else {
            return entityName.substring(entityName.lastIndexOf(".") + 1);
        }

    }

    public static <T> T findComponent(final String id) {

        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot root = context.getViewRoot();
        final UIComponent[] found = new UIComponent[1];

        root.visitTree(new FullVisitContext(context), new VisitCallback() {

            // TODO throw exception when duplicate found
            @Override
            public VisitResult visit(VisitContext context, UIComponent component) {
                if (component.getId() != null && component.getId().equals(id)) {
                    found[0] = component;
                    return VisitResult.COMPLETE;
                }
                return VisitResult.ACCEPT;
            }
        });

        return (T) found[0];

    }

    public static UIComponent findComponentByClientId(final String clientId) {

        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot root = context.getViewRoot();
        final UIComponent[] found = new UIComponent[1];
        root.visitTree(new FullVisitContext(context), new VisitCallback() {

            // TODO throw exception when duplicate found
            @Override
            public VisitResult visit(VisitContext context, UIComponent component) {
                if (component.getClientId() != null && component.getClientId().equals(clientId)) {
                    found[0] = component;
                    return VisitResult.COMPLETE;
                }
                return VisitResult.ACCEPT;
            }
        });

        return found[0];

    }

    public static List<UIComponent> findComponent(final Class clazz) {

        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot root = context.getViewRoot();
        final List<UIComponent> found = new ArrayList<>();

        root.visitTree(new FullVisitContext(context), new VisitCallback() {

            // TODO throw exception when duplicate found
            @Override
            public VisitResult visit(VisitContext context, UIComponent component) {
                if (component.getClass() != null && component.getClass().equals(clazz)) {
                    found.add(component);
                    return VisitResult.COMPLETE;
                }
                return VisitResult.ACCEPT;
            }
        });

        return found;

    }

    /**
     * @param request
     * @param response
     * @return
     */
    public static FacesContext getFacesContext(HttpServletRequest request, HttpServletResponse response) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext == null) {
            FacesContextFactory contextFactory = (FacesContextFactory) FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
            LifecycleFactory lifecycleFactory = (LifecycleFactory) FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
            Lifecycle lifecycle = lifecycleFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);
            facesContext = contextFactory.getFacesContext(request.getSession().getServletContext(), request, response, lifecycle);

            // set a new viewRoot, otherwise context.getViewRoot returns null
            UIViewRoot view = facesContext.getApplication().getViewHandler().createView(facesContext, "");
            facesContext.setViewRoot(view);
            //facesContext.getViewRoot().setLocale(new Locale("ar_JO")); // TODO

        }
        return facesContext;
    }

    /**
     * @param component
     * @return
     */
    public static boolean isComponentInDialog(UIComponent component) {
        boolean inDialog = false;
        UIComponent parent;

        if (component == null) {
            return false;
        }

        while (true) {

            parent = component.getParent();

            if (parent == null) {
                break;
            }
            if (parent.getClass().equals(Dialog.class)) {
                inDialog = true;
                break;
            }
            String style = (String) parent.getAttributes().get("style");
            if (style != null && style.contains("overflow-y")) {
                inDialog = true;
                break;
            }
            component = parent;
        }

        return inDialog;
    }

    public static boolean isValidationFailed() {
        return FacesContext.getCurrentInstance().isValidationFailed();
    }

    public static void update(@NotNull String componentId) {
        UIComponent uiComponent = findComponent(componentId);
        if (Objects.nonNull(uiComponent)) {
            PrimeFaces.current().ajax().update(uiComponent.getClientId());
        }

    }

    public static void update(String... componentIds) {
        for (String id : componentIds) {
            update(id);
        }
    }

    public static void update(UIComponent component) {
        PrimeFaces.current().ajax().update(component.getClientId());
    }

    public static void openDialog(String dialogVar) {
        PrimeFaces.current().executeScript("PF('" + dialogVar + "').show()");
    }

    public static void hideDialog(String dialogVar) {
        PrimeFaces.current().executeScript("PF('" + dialogVar + "').hide()");
    }

    public static void executeScript(String script) {
        PrimeFaces.current().executeScript(script);
    }

    public static String getRequestParameterMap(String key) {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
    }


    public static void redirect(String url, String... params) {

        if (ArrayUtils.isNotEmpty(params)) {
            for (String pa : params) {
                url = url + (url.contains("?") ? "&" : "?") + pa;
            }
        }
        String redirect = (url.contains("?") ? "&" : "?") + "faces-redirect=true";
        if (!url.contains(redirect)) {
            url += redirect;
        }
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, url);

    }

    public static void redirect(String url) {

        String redirect = (url.contains("?") ? "&" : "?") + "faces-redirect=true";
        if (!url.contains(redirect)) {
            url += redirect;
        }
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, url);

    }

    public static String getContextPath() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
    }

    /**
     * get SubmittedValue from AtsSelectOneMenu if  SubmittedValue == null return value
     *
     * @param idComponent
     * @param <T>
     * @return
     */
    public static <T> T getValueFromAtsSelectOneMenu(String idComponent) {
        SelectOneMenu selectOneMenu = JsfUtils.findComponent(idComponent);
        if (selectOneMenu.getSubmittedValue() instanceof String) {
            String submittedValue = selectOneMenu.getSubmittedValue().toString();
            return (T) selectOneMenu.getConverter().getAsObject(FacesContext.getCurrentInstance(), selectOneMenu, submittedValue);
        } else
            return (T) selectOneMenu.getValue();

    }


    public static void addSuccessMessage() {
        addSuccessMessage("common.operationSuccessful");
    }

    public static void addSuccessMessage(String key) {
        keepMessage();
        String successMsg = Utils.getMsg(key);
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, successMsg, null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growl", message);
        JsfUtils.update("form_messages");
    }

    public static void addErrorMessage(String key) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, Utils.getMsg(key), null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growl", message);
        JsfUtils.update("form_messages");
    }

    public static void addErrorMessageDirect(String messag) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, messag, null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growl", message);
        JsfUtils.update("form_messages");
    }

    public static void addWarningMessage(String key) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, Utils.getMsg(key), null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growl", message);
        JsfUtils.update("form_messages");
    }

    public static void keepMessage() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

    }

    public static void putToSessionMap(String key, Object value) {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        sessionMap.put(key, value);
    }

    public static <T> T getFromSessionMap(String key) {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        return (T) sessionMap.get(key);
    }

    public static boolean isRenderResponse() {
        return FacesContext.getCurrentInstance().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE;
    }
}
