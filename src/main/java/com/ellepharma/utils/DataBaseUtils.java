package com.ellepharma.utils;

import com.ellepharma.service.GlobalConfigProperties;
import com.smattme.MysqlExportService;

import java.io.File;
import java.util.Properties;

public class DataBaseUtils {

    public static File backupDataBase() {

        Properties properties = new Properties();
/*
        properties.setProperty(MysqlExportService.DB_NAME,GlobalConfigProperties.getInstance().getProperty("dbName"));
*/
        properties.setProperty(MysqlExportService.DB_USERNAME,GlobalConfigProperties.getInstance().getProperty("jdbcUsername"));
        properties.setProperty(MysqlExportService.DB_PASSWORD,GlobalConfigProperties.getInstance().getProperty("jdbcPassword"));
        properties.setProperty(MysqlExportService.JDBC_CONNECTION_STRING,GlobalConfigProperties.getInstance().getProperty("jdbcUrl"));
        properties.setProperty(MysqlExportService.JDBC_DRIVER_NAME,GlobalConfigProperties.getInstance().getProperty("jdbcDriver"));
        properties.setProperty(MysqlExportService.PRESERVE_GENERATED_ZIP, "true");
//properties relating to email config
/*        properties.setProperty(MysqlExportService.EMAIL_HOST,GlobalConfigProperties.getInstance().getProperty("mail.host"));
        properties.setProperty(MysqlExportService.EMAIL_PORT,GlobalConfigProperties.getInstance().getProperty("mail.port"));
        properties.setProperty(MysqlExportService.EMAIL_USERNAME,GlobalConfigProperties.getInstance().getProperty("mail.user"));
        properties.setProperty(MysqlExportService.EMAIL_PASSWORD,GlobalConfigProperties.getInstance().getProperty("mail.password"));
        properties.setProperty(MysqlExportService.EMAIL_FROM,GlobalConfigProperties.getInstance().getProperty("mail.user"));
        properties.setProperty(MysqlExportService.EMAIL_FROM,GlobalConfigProperties.getInstance().getProperty("mail.user"));
        properties.setProperty(MysqlExportService.EMAIL_TO,GlobalConfigProperties.getInstance().getProperty("dataBase.sendBackupTo"));
        properties.setProperty(MysqlExportService.EMAIL_SUBJECT, "DataBase Backup File >> "+DateUtil.today("MM/dd/yyyy hh:mm:ss a"));*/
//set the outputs temp dir
        try {
            properties.setProperty(MysqlExportService.TEMP_DIR, new File("dataBase").getPath());
            MysqlExportService mysqlExportService = new MysqlExportService(properties);
            mysqlExportService.export();
            return mysqlExportService.getGeneratedZipFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
