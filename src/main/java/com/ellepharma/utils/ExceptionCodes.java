package com.ellepharma.utils;

public enum ExceptionCodes {

    SUCCESS("10"),
    BR_COM_validateIsRecordUsed("00001"),
    BR_COM_inconsistentData("00002"),
    BR_COM_incompleteData("00003"),
    BR_COM_operationFailed("00004"),
    BR_COM_FixedLookup("00005"),
    BR_COM_DATE_GT_MAX("00006"),
    BR_COM_DATE_LT_MIN("00007"),
    BR_COM_DATE_IN_PAST("00008"),
    BR_COM_INVALID_DATE("00009"),
    BR_COM_INVALID_OR_INACTIVE_PROCESS("00010"),
    BR_COM_NO_PRIV_ON_PROCESS("00011"),
    BR_COM_INVALID_PHONE_NO("00012"),
    BR_COM_PHONE_NOT_FIXED_LINE("00013"),
    BR_COM_PHONE_NOT_MOBILE("00014"),
    BR_COM_PROCESS_HALTED("00015"),
    ;

    private String code;

    ExceptionCodes(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
