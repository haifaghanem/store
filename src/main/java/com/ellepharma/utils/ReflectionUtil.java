package com.ellepharma.utils;

import com.ellepharma.data.model.BaseEntity;
import com.google.gson.internal.Primitives;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.metamodel.internal.EntityTypeImpl;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.metamodel.Metamodel;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReflectionUtil {


	@SuppressWarnings("unchecked")
	public static Class<BaseEntity> getEntityClassByName(String entityName, EntityManager entityManage) {
		Metamodel metamodel = entityManage.getMetamodel();
		Set<?> ents = metamodel.getEntities();
		Iterator<?> it = ents.iterator();
		while (it.hasNext()) {
			EntityTypeImpl<?> i = (EntityTypeImpl<?>) it.next();

			if (i.getJavaType() != null) {
				if (entityName.equals(i.getJavaType().getSimpleName())) {
					return (Class<BaseEntity>) i.getJavaType();
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static Class<BaseEntity> getEntityClassByTableName(String tableName, EntityManager entityManage) {
		Metamodel metamodel = entityManage.getMetamodel();
		Set<?> ents = metamodel.getEntities();
		Iterator<?> it = ents.iterator();
		while (it.hasNext()) {
			EntityTypeImpl<?> i = (EntityTypeImpl<?>) it.next();

			if (i.getJavaType() != null) {
				if (tableName.equalsIgnoreCase(i.getJavaType().getAnnotation(Table.class).name())) {
					return (Class<BaseEntity>) i.getJavaType();
				}
			}
		}
		return null;
	}

	public static String getFieldNameFromColumn(String entityName, String columnName, EntityManager entityManager) {
		Class<?> entityClass = getEntityClassByName(entityName, entityManager);
		Field[] fields = entityClass.getDeclaredFields();
		Annotation[] annotations;
		for (Field field : fields) {
			annotations = field.getAnnotations();
			for (Annotation annotation : annotations) {
				if (annotation instanceof Column) {
					Column myAnnotation = (Column) annotation;
					if (myAnnotation.name().equals(columnName)) {
						return field.getName();
					}
				} else if (annotation instanceof JoinColumn) {
					JoinColumn myAnnotation = (JoinColumn) annotation;
					if (myAnnotation.name().equals(columnName)) {
						return field.getName();
					}
				}
			}
		}
		return columnName.toLowerCase();
	}

	/**
	 * Get the field type from a given entity using reflection
	 *
	 * @param entityName
	 * @param fieldName
	 * @return
	 */
	public static Class<?> getFieldType(String entityName, String fieldName, EntityManager entityManager) {
		Class<?> entityClass = getEntityClassByName(entityName, entityManager);
		Field field = null;
		try {
			field = entityClass.getDeclaredField(fieldName);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return field.getType();
	}

	/**
	 * @param clazz
	 * @param fieldName
	 * @return type of field in clazz
	 */
	public static Class<?> getFieldType(Class<?> clazz, String fieldName) {

		Field field = null;

		if (fieldName.contains(".")) {
			String[] fields = fieldName.split("\\.");
			String relationFieldName = fields[0];

			Field relationField = getFieldsWithInherited(clazz, relationFieldName);
			Class<?> relationClass = relationField.getType();

			// get type of field in the relation class
			String childFieldName = fields[1];
			Field childField = getFieldsWithInherited(relationClass, childFieldName);
			return childField.getType();
		}

		field = getFieldsWithInherited(clazz, fieldName);

		return field.getType();
	}

	private static Field getFieldsWithInherited(Class<?> entity, String fieldName) {
		List<Field> fields = new ArrayList<>();

		for (Class<?> c = entity; c != null; c = c.getSuperclass()) {
			fields.addAll(Arrays.asList(c.getDeclaredFields()));
		}

		Optional<Field> field = fields.stream()
									  .filter(f -> f.getName().equals(fieldName))
									  .findAny();

		return field.orElseThrow(
				() -> new NullPointerException(fieldName + " does not exsist in class " + entity.getName() + " or any of its ancestors"));
	}

	/**
	 * Search in joinEntity for a field of type mainEntity and return field name
	 *
	 * @param mainEntity
	 * @param joinEntity
	 * @param entityManager
	 * @return String
	 */
	public static String getJoinFieldName(String mainEntity, String joinEntity, EntityManager entityManager) {
		Class<?> clazz = getEntityClassByName(joinEntity, entityManager);
		Field[] fields = clazz.getDeclaredFields();
		String className;
		for (Field field : fields) {
			className = field.getType().getSimpleName();
			if (className.equals("List")) { // get the generic type
				ParameterizedType pType = ((ParameterizedType) field.getGenericType());
				Class<?> genClass = (Class<?>) pType.getActualTypeArguments()[0];
				className = genClass.getSimpleName();
			}
			if (className.equals(mainEntity)) {
				return field.getName();
			}
		}
		return "";
	}

	public static Class<?> getFieldDataType(Class<?> entityClass, String dbColumnName) {
		if (entityClass != null) {
			Class<?> tempClass = entityClass;
			try {
				Field field = tempClass.getDeclaredField(StringUtils.lowerCase(dbColumnName));
				Class<?> type = field.getType();
				if (type.isPrimitive()) {
					type = Primitives.wrap(type);
				}
				return type;
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
				// String clsName = tempClass.getPackage().getName() + ".extended." + tempClass.getSimpleName() + "EX";
				// try {
				// tempClass = Class.forName(clsName);
				// return getFieldDataType(tempClass, dbColumnName);
				// } catch (ClassNotFoundException ex) {
				// ex.printStackTrace();
				// }
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static boolean isNumericType(Class<?> dataType) {
		if (dataType == null) {
			return false;
		}

		return dataType.equals(Long.class) ||
				dataType.equals(Long.TYPE) ||

				dataType.equals(Integer.class) ||
				dataType.equals(Integer.TYPE) ||

				dataType.equals(Short.class) ||
				dataType.equals(Short.TYPE) ||

				dataType.equals(Double.class) ||
				dataType.equals(Double.TYPE) ||

				dataType.equals(BigDecimal.class);
	}

	/**
	 * Get all field names annotated with particular annotation
	 *
	 * @param annotationType Type of any annotation needed to look for.
	 * @param objectClass    the class to check its field
	 * @return list of string that contains field names
	 */
	public static List<String> getFieldNamesAnnotatedWith(Class<? extends Annotation> annotationType, Class<?> objectClass) {
		return Stream.of(objectClass.getDeclaredFields())
					 .filter(f -> f.getAnnotation(annotationType) != null)
					 .map(field -> ((Field) field).getName())
					 .collect(Collectors.toList());
	}

}
