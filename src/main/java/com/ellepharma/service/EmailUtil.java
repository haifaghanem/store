package com.ellepharma.service;

import com.ellepharma.constant.ConfigEnum;
import com.ellepharma.utils.FreeMarkerMailHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfig;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * EmailUtil.java
 *
 * @author Malek Yaseen <ma.yaseen@ats-ware.com>
 * @since Jan 12, 2020
 */

@Service
public class EmailUtil {

	/*@Autowired
	private FreeMarkerUtil freeMarkerUtil;*/

    @Autowired()
    @Qualifier("JavaMailSender")
    private JavaMailSender javaMailSender;

    @Bean("JavaMailSender")
    @DependsOn({"StoreProperties"})
    public JavaMailSender getJavaMailSender() {
        String host = GlobalConfigProperties.getInstance().getProperty("mail.host");
        String port = GlobalConfigProperties.getInstance().getProperty("mail.port");
        String userName = GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_USER.name());
        String password = GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_PASSWORD.name());

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setPort(Integer.parseInt(port));

        mailSender.setUsername(userName);
        mailSender.setPassword(password);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    public void sendMail(String to, String subject, String msg) {

        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_USER.name()));
            message.setTo(to);
            message.setSubject(subject);
            message.setText(msg);

            javaMailSender.send(message);
        } catch (MailException e) {
            e.printStackTrace();
        }
    }

    public void sendMailFreeMarker(String to, String subject, Object obj, String templateName) {
        AbstractApplicationContext context = new AnnotationConfigApplicationContext(FreeMarkerConfig.class);
        try {

            Map<String, Object> model = new HashMap<String, Object>();
            model.put("object", obj);

            MimeMessagePreparator preparator = getMessagePreparator(GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_USER.name()), to,
                    subject, model, templateName);

            javaMailSender.send(preparator);

        } catch (MailException e) {
            e.printStackTrace();
        }
    }

    public void sendMailFreeMarker(FreeMarkerMailHelper freeMarkerMailHelper, String subject, String templateName) {
        try {

            if (freeMarkerMailHelper.getFrom() == null)
                freeMarkerMailHelper.setFrom(GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_USER.name()));

            MimeMessagePreparator preparator = getMessagePreparator(subject, freeMarkerMailHelper, templateName);

            //String text = freeMarkerUtil.geFreeMarkerTemplateContent(model);
            javaMailSender.send(preparator);

        } catch (MailException e) {
            e.printStackTrace();
            System.out.println(
                    "############### Mail send Error: error send to " + GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_USER.name())
                            + " with subject " + subject);
        }
    }

    public void sendMailFreemarkerWithDefaultTemplate(FreeMarkerMailHelper freeMarkerMailHelper, String subject) {
        sendMailFreeMarker(freeMarkerMailHelper, subject, "" + ".ftl");
    }

    public boolean sendMailWithAttchmment(String to, String subject, String content, String fileName, byte[] fileBytes) {
        MimeMessage message = javaMailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");

            helper.setFrom(GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_USER.name()));
            helper.setTo(to.split(","));
            helper.setSubject(subject);
            helper.setText(content, true);
            if (StringUtils.isNotEmpty(fileName) && fileBytes != null
            ) {

                helper.addAttachment(fileName,
                        new ByteArrayResource(fileBytes));
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        try {
            javaMailSender.send(message);
        } catch (MailException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void sendMailWithAttchmment(String to, String subject, String content, String fileSystemPath) {

        try {
            FileSystemResource file = new FileSystemResource(fileSystemPath);
            InputStream inputStream = file.getInputStream();
            sendMailWithAttchmment(to, subject, content, file.getFilename(), IOUtils.toByteArray(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MailException me) {
            me.printStackTrace();
        }
    }

    public void sendMailCC(String to, String subject, String content) throws MessagingException {

        MimeMessage message = javaMailSender.createMimeMessage();


        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");

        helper.setFrom(GlobalConfigProperties.getInstance().getProperty(ConfigEnum.MAIL_USER.name()));
        helper.setCc(to.split(","));
        helper.setSubject(subject);
        helper.setText(content, true);


        javaMailSender.send(message);
    }

    public MimeMessagePreparator getMessagePreparator(String fromEmail, String to, String subject, Map<String, Object> model,
                                                      String templateName) {

        MimeMessagePreparator preparator = new MimeMessagePreparator() {

            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "utf-8");

                helper.setSubject(subject);
                helper.setFrom(fromEmail);
                helper.setTo(to);
                String text = "";//freeMarkerUtil.geFreeMarkerTemplateContent(model, templateName);
                System.out.println("Template content : " + text);

                // use the true flag to indicate you need a multipart message
                helper.setText(text, true);

                //				//Additionally, let's add a resource as an attachment as well.
                //				helper.addAttachment("cutie.png", new ClassPathResource("linux-icon.png"));

            }
        };
        return preparator;
    }

    public MimeMessagePreparator getMessagePreparator(String subject, FreeMarkerMailHelper freeMarkerMailHelper, String templateName) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("object", freeMarkerMailHelper);
        model.put("direction", "rtl");

        return getMessagePreparator(freeMarkerMailHelper.getFrom(), freeMarkerMailHelper.getTo(), subject, model, templateName);
    }

}