package com.ellepharma.service;

import com.ellepharma.exception.InternalSystemException;
import com.google.api.client.util.Lists;
import com.google.common.cache.CacheBuilder;
import io.minio.MinioClient;
import io.minio.ObjectStat;
import io.minio.Result;
import io.minio.errors.*;
import io.minio.messages.DeleteError;
import io.minio.messages.Item;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.xmlpull.v1.XmlPullParserException;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service("minioService")
public class MinioService implements ContentService, Serializable {

    private String endpoint;

    private String username;

    private String password;

    private static final Logger LOGGER = LoggerFactory.getLogger(MinioService.class);

    MinioClient minioClient;

    /**
     * @return MinioClient to access Minio server
     */
    public MinioClient getMinioClient() {
        if (minioClient != null) {
            return minioClient;
        }

        endpoint =GlobalConfigProperties.getInstance().getProperty("minio.endpoint");
        username =GlobalConfigProperties.getInstance().getProperty("minio.username");
        password =GlobalConfigProperties.getInstance().getProperty("minio.password");
        try {
            minioClient = new MinioClient(endpoint, username, password);
            LOGGER.info("Minio Client created");
        } catch (InvalidEndpointException | InvalidPortException e) {
            LOGGER.error("content storage connection error");
            throw new InternalSystemException("content storage connection error");
        }
        return minioClient;
    }

    /**
     * create Bucket (Folder) in Minio server, using provided name and policyType
     *
     * @param name the name of bucket (Folder)
     */
    @Override
    public void createStorageUnit(String name) {

        try {

            boolean found = getMinioClient().bucketExists(name);

            if (found) {
                LOGGER.error("'" + name + "' is already used for another storage unit name");
                return;
            }
            getMinioClient().makeBucket(name);
            getMinioClient().setBucketPolicy(name, "store");

            LOGGER.info("Minio Bucket created");

        } catch (InvalidKeyException | InvalidBucketNameException | RegionConflictException | NoSuchAlgorithmException
                | InsufficientDataException | NoResponseException | ErrorResponseException | InternalException | IOException
                | XmlPullParserException | InvalidObjectPrefixException e) {
            LOGGER.debug("Minio", e.getMessage());
            throw new InternalSystemException(e.getMessage());
        }

    }

    /**
     * remove Storage unit (Bucket) from Minio server, and recursively remove all the contents inside.
     *
     * @param name name of the storage unit bucket
     */
    @Override
    public void removeStorageUnit(String name) {

        boolean found;
        try {
            found = getMinioClient().bucketExists(name);

            if (!found) {
                LOGGER.info("'" + name + "'bucket is not available");
                throw new InternalSystemException("'" + name + "'Storage unit (Bucket) is not available, when delete");
            }

            Iterable<Result<Item>> bucketObjects = getMinioClient().listObjects(name);

            List<String> objectNames = Lists.newArrayList(bucketObjects).stream()
                    .map(item ->
                    {
                        try {
                            return item.get().objectName();
                        } catch (InvalidKeyException | InvalidBucketNameException | NoSuchAlgorithmException
                                | InsufficientDataException | IOException | InternalException
                                | NoResponseException | ErrorResponseException | XmlPullParserException e) {
                            LOGGER.info(e.getMessage());
                            return null;
                        }
                    })
                    .collect(Collectors.toList());

            Iterable<Result<DeleteError>> delErrors = getMinioClient().removeObjects(name, objectNames);
            delErrors.forEach(result ->
            {
                try {
                    DeleteError deleteError = result.get();
                    LOGGER.error("Content Name: " + deleteError.objectName() + ", delete error Message: " + deleteError.message());
                } catch (InvalidKeyException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException
                        | NoResponseException | ErrorResponseException | InternalException | IOException | XmlPullParserException e) {
                    LOGGER.error(e.getMessage());
                }
            });

            getMinioClient().removeBucket(name);

            LOGGER.info("Minio Bucket and its contents removed");

        } catch (InvalidKeyException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException
                | NoResponseException | ErrorResponseException | InternalException | IOException | XmlPullParserException e) {
            throw new InternalSystemException(e.getMessage());
        }

    }

    /**
     * Get content stream as {@link InputStream}
     *
     * @param storageUnit name of the StorageUnit (bucket) that contains the content
     * @param objectName  name of the content to get
     * @return {@link InputStream} of the Content
     */
    @Override
    public InputStream getContent(String storageUnit, String objectName) {
        try {
            return getMinioClient().getObject(storageUnit, objectName);
        } catch (InvalidKeyException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException
                | NoResponseException | ErrorResponseException | InternalException | InvalidArgumentException | IOException
                | XmlPullParserException e) {
            throw new InternalSystemException(e.getMessage());
        }
    }


   /* @Bean
    public Cache cacheUrl() {
        return new GuavaCache("cashUrl", CacheBuilder.newBuilder()
                .expireAfterWrite(30, TimeUnit.SECONDS)
                .build());
    }*/

    @Bean
    public CacheManager timeoutCacheManager() {
        GuavaCacheManager cacheManager = new GuavaCacheManager();
        CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder()
//604100
                .expireAfterWrite(4, TimeUnit.SECONDS);
        cacheManager.setCacheBuilder(cacheBuilder);
        return cacheManager;
    }


    public boolean bucketExists(String bucketName) {
        return true;
    }

    @Override
    @Cacheable(value = "cashUrl", key = "#objectName" /*cacheManager = "timeoutCacheManager"*/)
    public String getContentUrl(String storageUnit, String objectName) {
        try {
            boolean found = bucketExists(storageUnit);

            if (!found) {
                LOGGER.info("'" + objectName + "'Storage unit (Bucket) is not available; when get object URL");
                throw new InternalSystemException("'" + objectName + "'bucket is not available");
            }

            return getMinioClient().presignedGetObject(storageUnit, objectName, 604100);
        } catch (Exception e) {
            throw new InternalSystemException(e.getMessage());

        }
    }

    /**
     * Get Content URLs in Storage Unit (Bucket)
     *
     * @param storageUnit name of the StorageUnit (bucket) that contains the content
     * @return {@link List<String>} of Content URLs
     */
    @Override
    public List<String> listContentURLs(String storageUnit) {
        return listContentURLs(storageUnit, null);
    }

    /**
     * Get Content URLs in Storage Unit (Bucket) with prefix
     *
     * @param storageUnit name of the StorageUnit (bucket) that contains the content
     * @param prefix      of object name in Minio server
     * @return {@link List<String>} of Content URLs
     */
    @Override
    public List<String> listContentURLs(String storageUnit, String prefix) {
        try {

            boolean found = bucketExists(storageUnit);

            if (!found) {
                LOGGER.info("'" + storageUnit + "'bucket is not available");
                throw new InternalSystemException("'" + storageUnit + "'Storage unit (Bucket) is not available, when delete");
            }

            Iterable<Result<Item>> bucketObjects;
            if (StringUtils.isEmpty(prefix)) {
                bucketObjects = getMinioClient().listObjects(storageUnit);
            } else {
                bucketObjects = getMinioClient().listObjects(storageUnit, prefix);
            }

            List<String> objectNames = Lists.newArrayList(bucketObjects).stream()
                    .map(item ->
                    {
                        try {
                            return item.get().objectName();
                        } catch (InvalidKeyException | InvalidBucketNameException | NoSuchAlgorithmException
                                | InsufficientDataException | InternalException | IOException
                                | NoResponseException | ErrorResponseException | XmlPullParserException e) {
                            LOGGER.error(e.getMessage());
                            throw new InternalSystemException(e.getMessage());
                        }
                    })
                    .collect(Collectors.toList());

            return objectNames.stream()
                    .map(name ->
                    {
                        try {
                            return getMinioClient().getObjectUrl(storageUnit, name);
                        } catch (InvalidKeyException | InvalidBucketNameException
                                | ErrorResponseException | NoSuchAlgorithmException
                                | NoResponseException | InternalException | InsufficientDataException
                                | XmlPullParserException | IOException e) {
                            LOGGER.error(e.getMessage());
                            throw new InternalSystemException(e.getMessage());
                        }
                    })
                    .collect(Collectors.toList());

        } catch (XmlPullParserException e) {
            throw new InternalSystemException(e.getMessage());
        }

    }

    /**
     * upload content in the server
     *
     * @param storageUnit  name of the StorageUnit (bucket) that will contain the content
     * @param objectName   name of the content to upload
     * @param inputStream  {@link InputStream} of content to upload on server
     * @param size         of content (stream) in bytes
     * @param contentType; example: for PDF file (application/pdf)
     * @return content URL
     */
    @Override
    public String putContent(String storageUnit, String objectName, InputStream inputStream, long size, String contentType)
            throws InvalidBucketNameException {
        try {
            getMinioClient().putObject(storageUnit, objectName, inputStream, size, contentType);
            return objectName;
        } catch (InvalidKeyException | NoSuchAlgorithmException | InsufficientDataException
                | NoResponseException | ErrorResponseException | InternalException | InvalidArgumentException | XmlPullParserException e) {
            LOGGER.debug(e.getMessage());
            e.printStackTrace();
            throw new InternalSystemException(e.getMessage());
        } catch (IOException e1) {
            LOGGER.info(e1.getMessage());
        }

        return null;
    }

    /**
     * Delete old content provided by URL and put new Content in designated bucket
     *
     * @param oldContentURL    URL of old content (Object) needed to be replaced; you can pass full URL like:- (http://server:9000/bucket/content-name) or relative URL like:- (bucket/content-name).
     * @param newStorageUnit   new Storage unit (bucket) name that will be new content stored in.
     * @param newContentStream InputStream of the new file (Object).
     * @param newContentType   content type of the file (.doc, .jpg,...)
     * @return String contains URL of new content stored
     */
    @Override
    public String replaceContent(String oldContentURL, String newStorageUnit, String newContentName, InputStream newContentStream,
                                 long newContentSize, String newContentType) throws InvalidArgumentException {

        String newContentUrl = null;
        //URL handling
        oldContentURL = oldContentURL.endsWith("/") ? oldContentURL.substring(oldContentURL.length() - 2) : oldContentURL;
        String oldObjectName = oldContentURL.substring(oldContentURL.lastIndexOf("/") + 1);
        String oldBucketName = oldContentURL.subSequence(0, oldContentURL.lastIndexOf("/")).toString();
        oldBucketName = oldBucketName.contains("/") ?
                oldBucketName.substring(oldBucketName.lastIndexOf("/") + 1) :
                oldBucketName;

        try {
            getMinioClient().removeObject(oldBucketName, oldObjectName);
            newContentUrl = putContent(newStorageUnit, newContentName, newContentStream, newContentSize, newContentType);
        } catch (InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | InvalidKeyException |
                IOException | XmlPullParserException | NoResponseException | InternalException | ErrorResponseException e) {
            LOGGER.error(e.getMessage());
            throw new InternalSystemException(e.getMessage());
        }

        return newContentUrl;
    }

    /**
     * get Object metadata
     *
     * @param storageUnit bucket name
     * @param objectName  object name in Minio server
     * @return <p>Map that contains Object Metadata with the following:
     * <li>bucketName</li>
     * <li>contentType</li>
     * <li>name</li>
     * <li>length</li>
     * <li>createdTime</li>
     * <li>url</li>
     */
    @Override
    public Map<String, String> getContentMetadata(String storageUnit, String objectName) {
        Map<String, String> metadata = new HashMap<>();
        try {
            ObjectStat objectStat = getMinioClient().statObject(storageUnit, objectName);
            metadata.put("bucketName", objectStat.bucketName());
            metadata.put("contentType", objectStat.contentType());
            metadata.put("name", objectStat.name());
            metadata.put("length", String.valueOf(objectStat.length()));
            metadata.put("createdTime", String.valueOf(objectStat.createdTime()));
            metadata.put("url", objectStat.etag());
        } catch (InvalidBucketNameException e) {
            throw new InternalSystemException("Minio service: Invalid Bucket name in get content metadata");
        } catch (NoSuchAlgorithmException e) {
            throw new InternalSystemException(e.getMessage());
        } catch (InsufficientDataException | IOException | InvalidKeyException | NoResponseException | XmlPullParserException | ErrorResponseException | InternalException e) {
            e.printStackTrace();
        }
        return metadata;
    }

    /**
     * remove the content from the server
     *
     * @param storageUnit name of the StorageUnit (bucket) that contains the content
     * @param objectName  name of the content to remove
     */
    @Override
    public void removeContent(String storageUnit, @NotNull(message = "Object (Content) Name should not be null") String objectName) {
        try {
            getMinioClient().removeObject(storageUnit, objectName);
        } catch (InvalidKeyException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | NoResponseException | ErrorResponseException | InternalException | IOException | XmlPullParserException | InvalidArgumentException e) {
            LOGGER.debug(e.getMessage());
            throw new InternalSystemException(e.getMessage());
        }
    }

    /**
     * @param bucketName name of the StorageUnit (bucket) that contains the content
     */
    public void setBucketPolicy(String bucketName) {
        try {
            boolean found = bucketExists(bucketName);
            if (found) {
                getMinioClient().setBucketPolicy(bucketName, "store");
            } else {
                LOGGER.error("bucket policy set error, " + bucketName + " not exist to set policy");
                throw new InternalSystemException("bucket policy set error, " + bucketName + " not exist to set policy");
            }
        } catch (InvalidKeyException | InvalidBucketNameException | InvalidObjectPrefixException | NoSuchAlgorithmException
                | InsufficientDataException | NoResponseException | ErrorResponseException | InternalException | IOException
                | XmlPullParserException e) {
            throw new InternalSystemException(e.getMessage());
        }
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

}
