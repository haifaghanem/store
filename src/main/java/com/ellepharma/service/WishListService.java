package com.ellepharma.service;

import com.ellepharma.data.model.Account;
import com.ellepharma.data.model.WishList;
import com.ellepharma.data.repository.WishListRepo;
import com.ellepharma.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;


/**
 * WishListList.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since July 16, 2019
 */

@Service
@Transactional
public class WishListService extends BaseService<WishList> {

    @Autowired
    private WishListRepo wishListRepo;

    @Override
    public WishListRepo getRepo() {
        return wishListRepo;
    }

    @Transactional
    public WishList getForCurrentAccount() {


        Account account = SecurityUtils.getCurrentAccount();

        WishList wishList = wishListRepo.findByAccount_Id(account.getId());
        if (wishList == null)
            wishList = new WishList();
        wishList.setAccount(account);

        if (wishList.getItems() == null)
            wishList.setItems(new ArrayList<>());
        return wishList;
    }

}
