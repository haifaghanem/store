package com.ellepharma.service;

import com.ellepharma.data.model.StockIn;
import com.ellepharma.data.repository.OrderProductRepo;
import com.ellepharma.data.repository.OrderRepo;
import com.ellepharma.data.repository.StockInRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * StockInService.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 19, 2019
 */
@Service
public class StockInService extends BaseService<StockIn> {

	@Autowired
	private StockInRepo stockInRepo;

	@Autowired
	private OrderProductRepo orderProductRepo;

	@Autowired
	private OrderRepo orderRepo;

	@Override
	@Caching(evict = {
			@CacheEvict(cacheNames = "availableQuantityByItemID", allEntries = true),
	})
	public StockIn save(StockIn stockIn) {
		return super.save(stockIn);
	}

	@Override
	@Caching(evict = {
			@CacheEvict(cacheNames = "availableQuantityByItemID", allEntries = true),
	})
	public List<StockIn> save(List<StockIn> t) {
		return super.save(t);
	}

	@Override
	public StockInRepo getRepo() {
		return stockInRepo;
	}

	public Long getAvailableQuantityByItemCode(String itemCode) {
		return stockInRepo.getStockInQuantityByItemCode(itemCode) - orderRepo.getOrderProductQuantityByItemCode(itemCode);
	}

	@Caching(evict = {
			@CacheEvict(cacheNames = "availableQuantityByItemID", allEntries = true),
	})
	public List<StockIn> saveImportData(List<StockIn> stockInList) {

		return (List<StockIn>) stockInRepo.save(stockInList);
	}

	public StockIn findByVoucherNoAndItem_Code(String voucherNo, String codeItem) {
		StockIn stockIn = stockInRepo.findByVoucherNoAndItem_Code(voucherNo, codeItem);

		if (stockIn == null)
			return new StockIn();

		return stockIn;
	}

	public List<String> findAllVoucherNo() {
		return stockInRepo.findAllVoucherNo();
	}

	public List<StockIn> findByVoucherNo(String voucherNo) {
		return stockInRepo.findByVoucherNo(voucherNo);
	}

	@Cacheable(value = "availableQuantityByItemID", key = "#p0", condition = "#p0!=null")
	public Long getAvailableQuantityByItemID(Long itemId) {
		if (itemId != null) {

			return stockInRepo.getStockInQuantityByItemId(itemId) - orderRepo.getOrderProductQuantityByItemId(itemId);
		}
		return 0L;
	}

	public List<String> findInvoiceNos(Long id) {
		return stockInRepo.findAllVoucherNo(id);
	}
}
