package com.ellepharma.service;

import com.ellepharma.data.model.Newsletter;
import com.ellepharma.data.repository.NewsletterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value = "NewsletterService")
@Transactional
public class NewsletterService extends BaseService<Newsletter> {

    @Autowired
    private NewsletterRepo newsletterRepo;


    @Override
    public NewsletterRepo getRepo() {
        return newsletterRepo;
    }

    public NewsletterRepo getNewsletterRepo() {
        return newsletterRepo;
    }

    public void setNewsletterRepo(NewsletterRepo newsletterRepo) {
        this.newsletterRepo = newsletterRepo;
    }
}
