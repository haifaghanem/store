package com.ellepharma.service;

import com.ellepharma.data.model.Config;
import com.ellepharma.utils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

@Service
@Component
public class GlobalConfigProperties {

    private static Properties properties;


    @Autowired

    private ConfigService configService;


    @Bean("StoreProperties")
    @DependsOn({"configService"})
    public Properties GlobalConfigProperties() {

        properties= new Properties();
        String[] filePath = {getPath("/application.properties")};
        for (int i = 0; i < filePath.length; i++) {
            try {
                getFile(filePath[i]);
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
        List<Config> configList = configService.findAll();
        configList.forEach(config -> {
            properties.put(config.getName(), config.getValue().get("en"));
        });
        return properties;
    }

    public static Properties getInstance() {
        return properties;
    }


    private void getFile(String path) throws IOException {
        File file = new File(path);
        if (file.exists()) {
            properties.load(new FileInputStream(file));
        }
    }

    private static String getPath(String name) {
        if (null != GlobalConfigProperties.class.getResource(name)) {
            return GlobalConfigProperties.class.getResource(name).getPath();
        }
        return "";
    }

}