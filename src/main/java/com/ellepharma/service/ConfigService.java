package com.ellepharma.service;

import com.ellepharma.data.model.Config;
import com.ellepharma.data.repository.ConfigRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ConfigService extends BaseService<Config> {

    @Autowired
    private ConfigRepo configRepo;
    @Autowired
    CacheManager cacheManager;

    @Override
    public ConfigRepo getRepo() {
        return configRepo;
    }

    @Override
    @Cacheable(value = "configList")
    public List<Config> findAll() {
        return getRepo().findAll();
    }

    @Transactional
    public void evictAllCaches() {
        cacheManager.getCacheNames().stream()
                .forEach(cacheName -> cacheManager.getCache(cacheName).clear());
    }

    @Transactional
    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "config", key = "#config.name"),
            @CacheEvict(value = "configList", allEntries = true),
    })
    public Config save(Config config) {

        return getRepo().save(config);
    }

    @Cacheable(value = "config", key = "#name")
    @Transactional
    public Config findByName(String name) {
        return configRepo.findByName(name);
    }


    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "config", allEntries = true),
            @CacheEvict(cacheNames = "configList", allEntries = true)})

    public void delete(Config t) {
        getRepo().delete(t);
    }
}
