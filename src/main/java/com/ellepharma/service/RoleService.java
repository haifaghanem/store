package com.ellepharma.service;

import com.ellepharma.data.model.Role;
import com.ellepharma.data.repository.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * RoleService.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 28, 2019
 */
@Service
public class RoleService extends BaseService<Role> {

    @Autowired
    private RoleRepo roleRepo;

    @Override
    public RoleRepo getRepo() {
        return roleRepo;
    }


}
