package com.ellepharma.service;

import com.ellepharma.data.model.BaseEntity;
import com.ellepharma.data.repository.BaseRepo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class BaseService<T extends BaseEntity> {

    private BaseRepo<T> repo;

    public Specification<T> getFilterSpecification(Map<String, String> filterValues) {

        return (Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            return getOptionals(filterValues, root, query, builder);
        };
    }

    public Predicate getOptionals(Map<String, String> filterValues, Root<T> root, CriteriaQuery<?> query,
                                  CriteriaBuilder builder) {

        Optional<Predicate> predicate =

                filterValues.entrySet().stream()
                        .filter(v -> v.getValue() != null && v.getValue().length() > 0).map(entry ->
                {
                    Path<?> path = root;
                    String key = entry.getKey();
                    if (entry.getKey().contains(".")) {
                        String[] splitKey = entry.getKey().split("\\.");
                        if (splitKey.length > 2) {
                            path = root.join(splitKey[0]).join(splitKey[1]);
                            key = splitKey[2];
                        } else {
                            path = root.join(splitKey[0]);

                            key = splitKey[1];
                        }

                    }
                    return builder.like(builder.upper(path.get(key).as(String.class)),
                            "%" + entry.getValue().toUpperCase() + "%");
                })
                        .collect(Collectors.reducing((a, b) -> builder.and(a, b)));
        return predicate.orElseGet(() -> alwaysTrue(builder));

    }

    private Predicate alwaysTrue(CriteriaBuilder builder) {
        return builder.isTrue(builder.literal(true));
    }

    public Page<T> findAll(PageRequest pageRequest) {
        return getRepo().findAll(pageRequest);
    }

    @Transactional
    @Modifying
    public T save(T t) {
        return getRepo().save(t);
    }

    @Transactional
    @Modifying
    public List<T> save(List<T> t) {
        return (List<T>) getRepo().save(t);
    }


    public List<T> findAll() {
        return getRepo().findAll();
    }

    public abstract BaseRepo<T> getRepo();

    public Page<T> findAllLazy(Map<String, String> filters, Pageable pageable) {
        return getRepo().findAll(getFilterSpecification(filters), pageable);
    }

    public void delete(T t) {
        getRepo().delete(t);
    }

    public T findById(Long id) {
        return getRepo().findById(id);
    }

    public long count() {
        return getRepo().count();
    }
}
