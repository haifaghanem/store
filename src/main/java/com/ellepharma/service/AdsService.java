package com.ellepharma.service;

import com.ellepharma.data.model.Ads;
import com.ellepharma.data.model.Image;
import com.ellepharma.data.repository.AdsRepo;
import com.ellepharma.data.repository.ImageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AdsService extends BaseService<Ads> {

    @Autowired
    private AdsRepo adsRepo;

    @Autowired
    private ImageRepo imageRepo;

    @Override
    public AdsRepo getRepo() {
        return adsRepo;
    }

    @Transactional
    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "ads", allEntries = true),
            @CacheEvict(value = "countAds", allEntries = true)
    })
    public Ads save(Ads ads) {
        if (ads.getImage() != null) {

            Image savedImage = imageRepo.save(ads.getImage());
            ads.setImage(savedImage);
        }
        return getRepo().save(ads);
    }

    @Override
    @Cacheable(value = "ads")
    public List<Ads> findAll() {
        return super.findAll();
    }

    @Cacheable(value = "ads", key = "#id")
    public Ads findById(Long id) {
        return getRepo().findById(id);
    }

    @Cacheable(value = "ads", key = "#code")
    public Ads findByCode(String code) {
        return getRepo().findByCode(code);
    }

    @Transactional
    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "ads", allEntries = true),
            @CacheEvict(value = "countAds", allEntries = true)
    })
    public void deleteByCode(String code) {
        getRepo().deleteByCode(code);

    }

    @Cacheable(value = "countAds", key = "#code")
    public int countByCode(String code) {
        return getRepo().countByCode(code);
    }
}
