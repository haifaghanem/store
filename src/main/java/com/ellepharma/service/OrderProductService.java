package com.ellepharma.service;

import com.ellepharma.data.model.OrderProduct;
import com.ellepharma.data.repository.OrderProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * CouponService.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since July 12, 2019
 */
@Service
public class OrderProductService extends BaseService<OrderProduct> {

    @Autowired
    private OrderProductRepo orderProductRepo;

    @Override
    public OrderProductRepo getRepo() {
        return orderProductRepo;
    }

}
