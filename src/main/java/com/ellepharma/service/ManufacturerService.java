package com.ellepharma.service;

import com.ellepharma.data.model.Manufacturer;
import com.ellepharma.data.repository.ImageRepo;
import com.ellepharma.data.repository.ManufacturerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ManufacturerService extends BaseService<Manufacturer> {

    @Autowired
    private ManufacturerRepo manufacturerRepo;

    @Autowired
    private ImageRepo imageRepo;

    @Override
    public ManufacturerRepo getRepo() {
        return manufacturerRepo;
    }

    @Cacheable("allBrands")
    public List<Manufacturer> findAll() {
        return getRepo().findAll();
    }

   @Cacheable("allActiveBrands")
    public List<Manufacturer> findAllActive() {

        List<Manufacturer> result = getRepo().findAllByStatusIsTrue();

        List<Manufacturer> sortedList = result.stream()
                .sorted(Comparator.comparing(m -> m.getName().get(m.getCurrentLanguage())))
                .collect(Collectors.toList());
        return sortedList;
    }

    @Transactional
    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "allBrands", allEntries = true),
            @CacheEvict(value = "allActiveBrands", allEntries = true),
    })
    public Manufacturer save(Manufacturer manufacturer) {
        return getRepo().save(manufacturer);
    }

    @Caching(evict = {
            @CacheEvict(value = "allBrands", allEntries = true),
            @CacheEvict(value = "allActiveBrands", allEntries = true)
    })
    public List<Manufacturer> saveImportData(List<Manufacturer> brandList) {

        return (List<Manufacturer>) manufacturerRepo.save(brandList);


    }

    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "allBrands", allEntries = true),
            @CacheEvict(value = "allActiveBrands", allEntries = true)
    })

    public void delete(Manufacturer t) {
        getRepo().delete(t);
    }

    public Manufacturer getByCode(String code) {
        Manufacturer manufacturer = manufacturerRepo.findByCode(code);

        if (manufacturer == null)
            return new Manufacturer();

        return manufacturer;
    }
}
