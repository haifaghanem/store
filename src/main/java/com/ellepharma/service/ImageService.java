package com.ellepharma.service;

import com.ellepharma.data.model.Image;
import com.ellepharma.data.repository.ImageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ImageService extends BaseService<Image> {

    @Autowired
    private ImageRepo imageRepo;


    @Override
    public ImageRepo getRepo() {
        return imageRepo;
    }


    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "image", key = "#image.id"),
    })
    public Image save(Image image) {
        return imageRepo.save(image);

    }


    @Cacheable(value = "image", key = "#id")
    public Image findById(Long id) {
        return getRepo().findById(id);
    }
}
