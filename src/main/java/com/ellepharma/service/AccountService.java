package com.ellepharma.service;

import com.ellepharma.data.model.Account;
import com.ellepharma.data.model.AccountType;
import com.ellepharma.data.repository.AccountRepo;
import com.ellepharma.data.repository.RoleRepo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class AccountService extends BaseService<Account> {

	@Autowired
	private AccountRepo accountRepo;

	@Autowired
	private RoleRepo roleRepo;

	@Autowired
	private UserService.JwtTokenUtil jwtTokenUtil;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	public UserService.JwtTokenUtil getJwtTokenUtil() {
		return jwtTokenUtil;
	}

	@Override
	public AccountRepo getRepo() {
		return accountRepo;
	}

	public Account save(Account account) {

		account.setPassword(passwordEncoder().encode(account.getPassword()));
		account.setActive(true);

		if (StringUtils.isEmpty(account.getUsername())) {
			account.setUsername(account.getEmail());

		}
		if (CollectionUtils.isEmpty(account.getRoles())) {
			account.setRoles(Arrays.asList(roleRepo.findByName("CUSTOMER")));

		}
		return accountRepo.save(account);
	}

	public Account saveCelebrity(Account account) {

		account.setAccountType(AccountType.CELEBRATED);
		if (account.getState() == null) {
			account.setState(" ");
		}
		if (account.getPostcode() == null) {
			account.setPostcode(" ");
		}
		if (account.getAddress() == null) {
			account.setAddress(" ");
		}
		account.setRoles(Arrays.asList(roleRepo.findByName("CUSTOMER"), roleRepo.findByName("CELEBRITY")));

		account.setPassword(passwordEncoder().encode(account.getPassword()));
		account.setActive(true);

		if (StringUtils.isEmpty(account.getUsername())) {
			account.setUsername(account.getEmail());

		}
		if (CollectionUtils.isEmpty(account.getRoles())) {
			account.setRoles(Arrays.asList(roleRepo.findByName("CUSTOMER")));

		}
		return accountRepo.save(account);
	}

	public Account saveVendor(Account account) {

		account.setAccountType(AccountType.VENDOR);
		if (account.getState() == null) {
			account.setState(" ");
		}
		if (account.getPostcode() == null) {
			account.setPostcode(" ");
		}
		if (account.getAddress() == null) {
			account.setAddress(" ");
		}
		account.setRoles(Arrays.asList(roleRepo.findByName("CUSTOMER"), roleRepo.findByName("VENDOR")));

		if (StringUtils.isNotEmpty(account.getPassword())) {

			account.setPassword(passwordEncoder().encode(account.getPassword()));
		}
		account.setActive(true);

		if (StringUtils.isEmpty(account.getUsername())) {
			account.setUsername(account.getEmail());

		}
		if (CollectionUtils.isEmpty(account.getRoles())) {
			account.setRoles(Arrays.asList(roleRepo.findByName("CUSTOMER")));

		}
		return accountRepo.save(account);
	}

	public Account update(Account account) {

		//account.setPassword(passwordEncoder().encode(account.getPassword()));

		return accountRepo.save(account);
	}

	/**
	 * @param email
	 * @return
	 */
	public boolean existsAccountByEmail(String email, Long id) {
		return accountRepo.existsAccountByEmail(email, id);
	}

	/**
	 * @param email
	 * @return
	 */
	public Account findByEmailAndActiveIsTrue(String email) {
		return accountRepo.findByEmailAndActiveIsTrue(email);
	}

	public Account findByEmail(String email) {
		return accountRepo.findByEmail(email);
	}

	public Account findByUsername(String email) {
		return accountRepo.findByUsernameAndActiveIsTrue(email);
	}

	public Page<Account> findCelebrity(Map<String, String> filters, Pageable pageable) {
		if (filters == null)
			filters = new HashMap<>();
		filters.put("accountType", AccountType.CELEBRATED.name());
		return super.findAllLazy(filters, pageable);
	}

	public Page<Account> findVendors(Map<String, String> filters, Pageable pageable) {
		if (filters == null)
			filters = new HashMap<>();
		filters.put("accountType", AccountType.VENDOR.name());
		return super.findAllLazy(filters, pageable);
	}

	public Page<Account> findCustomer(Map<String, String> filters, Pageable pageable) {
		if (filters == null)
			filters = new HashMap<>();
		filters.put("accountType", AccountType.CUSTOMER.name());
		return super.findAllLazy(filters, pageable);
	}

	public long countUser() {
		return accountRepo.countAccountByAccountType(AccountType.CUSTOMER);
	}

	public long countCelebrity() {
		return accountRepo.countAccountByAccountType(AccountType.CELEBRATED);
	}

	public String generateToken(Account account) {
		return jwtTokenUtil.generateToken(account);
	}

	public List<Account> findByAccountTypeAndActiveIsTrue(AccountType celebrated) {
		return accountRepo.findByAccountTypeAndActiveIsTrue(celebrated);
	}

	public List<String> findAllEmail() {
		return accountRepo.findAllEmail();
	}

	public List<String> findAllActiveEmail() {
		return accountRepo.findAllActiveEmail();
	}
}
