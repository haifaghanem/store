package com.ellepharma.service;

import com.ellepharma.data.model.Currency;
import com.ellepharma.data.model.CurrencyResponse;
import com.ellepharma.data.repository.CurrencyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class CurrencyService extends BaseService<Currency> {

	@Autowired
	private CurrencyRepo currencyRepo;

	/**
	 * @return
	 */
	public List<Currency> findAllByStatusIsTrue() {
		return currencyRepo.findAllByStatusIsTrue();
	}

	@Override
	public CurrencyRepo getRepo() {
		return currencyRepo;
	}


	@Override

	@Caching(evict = {
			@CacheEvict(value = "CurrencyCode", key = "#currency.currencyCode"),
	})
	public Currency save(Currency currency) {
		return super.save(currency);
	}

	@Cacheable(value = "CurrencyCode", key = "#code")

	public Currency findByCurrencyCode(String code) {

		return currencyRepo.findByCurrencyCode(code);
	}

	@Transactional
	public void update(CurrencyResponse currencyResponse) {

		List<Currency> currencyList = new ArrayList<>();
		currencyResponse.getQuotes().forEach((key, value1) -> {
			String code = key.split("USD", 2)[1];
			String value = value1.toString();
			Double rate = new Double(value);
			Currency currency = currencyRepo.findByCurrencyCode(code);

			if (Objects.isNull(currency)) {
				currency = new Currency(code, rate);
			}
			currency.setValue(rate);
			currencyList.add(currency);
			currencyRepo.save(currency);
		});

	}
}
