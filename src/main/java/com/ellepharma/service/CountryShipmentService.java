package com.ellepharma.service;

import com.ellepharma.data.model.CountryShipment;
import com.ellepharma.data.model.ShipmentType;
import com.ellepharma.data.repository.BaseRepo;
import com.ellepharma.data.repository.CountryShipmentRepo;
import com.ellepharma.data.repository.ShipmentTypeRepo;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class CountryShipmentService extends BaseService<CountryShipment> {

    @Autowired
    private CountryShipmentRepo countryShipmentRepo;
    @Autowired
    private ShipmentTypeRepo shipmentTypeRepo;

    @Override
    public BaseRepo<CountryShipment> getRepo() {
        return countryShipmentRepo;
    }

    @Cacheable(value = "CountryShipment", key = "#id")
    public CountryShipment findById(Long id) {
        return countryShipmentRepo.findById(id);
    }

//    @Override

    /*   @Transactional
       @Modifying
       @Caching(evict = {
               @CacheEvict(value = "CountryShipment", allEntries = true),
               @CacheEvict(value = "CountryShipmentByCountryId", key = "#countryShipment.country.id"),
       })
       public CountryShipment save(CountryShipment countryShipment) {
           return countryShipmentRepo.save(countryShipment);
       }
   */
    @Cacheable(value = "CountryShipmentByCountryId", key = "#id")
    public CountryShipment findCountryShipmentByCountry_Id(Long id) {
        CountryShipment countryShipment = null;
        ShipmentType shipmentType = shipmentTypeRepo.findByStatusTrue();
        if (shipmentType != null && CollectionUtils.isNotEmpty(shipmentType.getCountryShipmentList())) {
            countryShipment = shipmentType.getCountryShipmentList().stream().filter(m -> m.getCountry().getId().equals(id)).findFirst().orElse(null);
        }
        return countryShipment;
    }
}
