package com.ellepharma.service;

import com.ellepharma.data.model.Image;
import com.ellepharma.data.model.Slider;
import com.ellepharma.data.repository.ImageRepo;
import com.ellepharma.data.repository.SliderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SliderService extends BaseService<Slider> {

    @Autowired
    private SliderRepo sliderRepo;

    @Autowired
    private ImageRepo imageRepo;

    @Override
    public SliderRepo getRepo() {
        return sliderRepo;
    }

    @Transactional
    @Modifying
	@Caching(evict = {
			@CacheEvict(value = "slider", allEntries = true)
	})
    public Slider save(Slider slider) {
        if (slider.getImage() != null) {
            Image savedImage = imageRepo.save(slider.getImage());
            slider.setImage(savedImage);
        }
        return getRepo().save(slider);
    }


    @Cacheable("slider")
    public List<Slider> findAllByStatusTrue() {

        return getRepo().findAllByStatusTrue();
    }
}
