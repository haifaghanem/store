package com.ellepharma.service;

import com.ellepharma.data.model.Account;
import com.ellepharma.data.model.Coupon;
import com.ellepharma.data.repository.CouponRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * CouponService.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 28, 2019
 */
@Service
public class CouponService extends BaseService<Coupon> {

	@Autowired
	private CouponRepo couponRepo;

	@Override
	public CouponRepo getRepo() {
		return couponRepo;
	}


	@Override
	public Page<Coupon> findAllLazy(Map<String, String> filters, Pageable pageable) {

		return super.findAllLazy(filters, pageable);
	}


	public Page<Coupon> findByCelebrity(Map<String, String> filters, Pageable pageable, Account celebrity) {

		filters.put("celebrity.id",celebrity.getId().toString());
		return getRepo().findAll(getFilterSpecification(filters), pageable);

	}


	public Coupon findActiceByCouponNumber(String couponNumber) {
		return couponRepo.findByCouponNumberAndStatusTrueAndValidToDateAfter(couponNumber, new Date());
	}

}
