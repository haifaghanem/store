package com.ellepharma.service;

import com.ellepharma.constant.ConfigEnum;
import com.ellepharma.data.dto.OrderReportFilterDTO;
import com.ellepharma.data.dto.ReportItemDto;
import com.ellepharma.data.dto.VerifyRequest;
import com.ellepharma.data.model.*;
import com.ellepharma.data.repository.OrderRepo;
import com.ellepharma.utils.Utils;
import com.ellepharma.web.MB.ReportUtils;
import com.ellepharma.web.api.PayTabs;
import com.ellepharma.web.aramex.dto.CreateShipmentsDto;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * CouponService.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 28, 2019
 */
@Service
@Transactional
public class OrderService extends BaseService<Order> {

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private PaymentTypeService paymentTypeService;

    @Autowired
    private StockInService stockInService;

    @Autowired
    private EmailUtil emailUtil;

    @Override
    public OrderRepo getRepo() {
        return orderRepo;
    }

    public List<Order> findByAccount(Account account) {
        return orderRepo.findByAccount(account);
    }

    public List<ReportItemDto> sellerReport(OrderReportFilterDTO orderReportFilterDTO) {
        OrderReportFilterDTO orderReportFilterDTO1 = new OrderReportFilterDTO();
        BeanUtils.copyProperties( orderReportFilterDTO,orderReportFilterDTO1);
        if (orderReportFilterDTO1 != null) {
            if (orderReportFilterDTO1.getFromDate() != null) {
                Calendar fromDateC = Calendar.getInstance();
                fromDateC.setTime(orderReportFilterDTO1.getFromDate());
                fromDateC.set(Calendar.HOUR, 0);
                fromDateC.set(Calendar.MINUTE, 0);
                fromDateC.set(Calendar.SECOND, 0);
                fromDateC.set(Calendar.HOUR_OF_DAY, 0);
                orderReportFilterDTO1.setFromDate(fromDateC.getTime());
            }
            if (orderReportFilterDTO1.getToDate() != null) {
                Calendar fromDateC = Calendar.getInstance();
                fromDateC.setTime(orderReportFilterDTO1.getToDate());
                fromDateC.set(Calendar.HOUR, 0);
                fromDateC.set(Calendar.MINUTE, 0);
                fromDateC.set(Calendar.SECOND, 0);
                fromDateC.set(Calendar.HOUR_OF_DAY, 0);
                fromDateC.add(Calendar.DAY_OF_MONTH, 1);
                orderReportFilterDTO1.setToDate(fromDateC.getTime());
            }
        }

        List<ReportItemDto> i = getRepo().sellerReport(orderReportFilterDTO1);

        i.forEach(reportItemDto -> {
            reportItemDto.setAvailable(stockInService.getAvailableQuantityByItemID(reportItemDto.getItem().getId()));
            reportItemDto.setOrderIds(orderRepo.findOrderIds(reportItemDto.getItem().getId()).stream()
                    .map(Objects::toString).collect(Collectors.joining(" , ")));
            reportItemDto.setInvoiceNo(stockInService.findInvoiceNos(reportItemDto.getItem().getId()).stream()
                    .map(Objects::toString).collect(Collectors.joining(" , ")));
        });
        return i;
    }

    @Override
    @Caching(evict = {
            @CacheEvict(cacheNames = "bestSeller", allEntries = true),
            @CacheEvict(value = "newItemTrue", allEntries = true),
            @CacheEvict(cacheNames = "items", allEntries = true),
            @CacheEvict(cacheNames = "bestPrice", allEntries = true),
            @CacheEvict(cacheNames = "bestSeller", allEntries = true),
            @CacheEvict(cacheNames = "item", allEntries = true),
            @CacheEvict(cacheNames = "countItems", allEntries = true),
            @CacheEvict(cacheNames = "countItemByStatusTrue", allEntries = true),
            @CacheEvict(cacheNames = "relatedProduct", allEntries = true),
            @CacheEvict(cacheNames = "findAllByCategories", allEntries = true),
            @CacheEvict(cacheNames = "availableQuantityByItemID", allEntries = true),
    })
    public Order save(Order order) {

        if (order.getId() != null) {
            order.setModifiedDate(new Date());
        } else {
            order.setCreationDate(new Date());

        }

        return super.save(order);
    }

    /*  public List<OrderReportResultDTO> findAllOrderReport(OrderReportFilterDTO filterParam) {
        return orderRepo.findAllOrderReport(filterParam);
    }*/

    public Page<Order> findAllOrderReport(Map<String, String> filters, Pageable pageable, OrderReportFilterDTO orderReportFilterDTO) {
        if (orderReportFilterDTO != null) {
            if (orderReportFilterDTO.getFromDate() != null) {
                Calendar fromDateC = Calendar.getInstance();
                fromDateC.setTime(orderReportFilterDTO.getFromDate());
                fromDateC.set(Calendar.HOUR, 0);
                fromDateC.set(Calendar.MINUTE, 0);
                fromDateC.set(Calendar.SECOND, 0);
                fromDateC.set(Calendar.HOUR_OF_DAY, 0);
                orderReportFilterDTO.setFromDate(fromDateC.getTime());
            }
            if (orderReportFilterDTO.getToDate() != null) {
                Calendar fromDateC = Calendar.getInstance();
                fromDateC.setTime(orderReportFilterDTO.getToDate());
                fromDateC.set(Calendar.HOUR, 0);
                fromDateC.set(Calendar.MINUTE, 0);
                fromDateC.set(Calendar.SECOND, 0);
                fromDateC.set(Calendar.HOUR_OF_DAY, 0);
                fromDateC.add(Calendar.DAY_OF_MONTH, 1);
                orderReportFilterDTO.setToDate(fromDateC.getTime());
            }
        }
        return getRepo().findAllOrderReport(orderReportFilterDTO, pageable);
    }

    public List<Order> findShipmentOrderReport(Map<String, String> filters, OrderReportFilterDTO orderReportFilterDTO) {
        OrderReportFilterDTO orderReportFilterDTO1 = new OrderReportFilterDTO();
        BeanUtils.copyProperties( orderReportFilterDTO,orderReportFilterDTO1);

        orderReportFilterDTO1.setPaymentStatus(PaymentStatusEnum.TNC_100);
        orderReportFilterDTO1.setAramexSent(true);
        if (orderReportFilterDTO1 != null) {
            if (orderReportFilterDTO1.getFromDate() != null) {
                Calendar fromDateC = Calendar.getInstance();
                fromDateC.setTime(orderReportFilterDTO1.getFromDate());
                fromDateC.set(Calendar.HOUR, 0);
                fromDateC.set(Calendar.MINUTE, 0);
                fromDateC.set(Calendar.SECOND, 0);
                fromDateC.set(Calendar.HOUR_OF_DAY, 0);
                orderReportFilterDTO1.setFromDate(fromDateC.getTime());
            }
            if (orderReportFilterDTO1.getToDate() != null) {
                Calendar fromDateC = Calendar.getInstance();
                fromDateC.setTime(orderReportFilterDTO1.getToDate());
                fromDateC.set(Calendar.HOUR, 0);
                fromDateC.set(Calendar.MINUTE, 0);
                fromDateC.set(Calendar.SECOND, 0);
                fromDateC.set(Calendar.HOUR_OF_DAY, 0);
                fromDateC.add(Calendar.DAY_OF_MONTH, 1);
                orderReportFilterDTO1.setToDate(fromDateC.getTime());
            }
        }
        return getRepo().findAllShipmentOrderReport(orderReportFilterDTO1);
    }

    public Double sumShipmentOrderReport(OrderReportFilterDTO orderReportFilterDTO) {
        OrderReportFilterDTO orderReportFilterDTO1 = new OrderReportFilterDTO();
        BeanUtils.copyProperties( orderReportFilterDTO,orderReportFilterDTO1);
        orderReportFilterDTO1.setPaymentStatus(PaymentStatusEnum.TNC_100);
        orderReportFilterDTO1.setAramexSent(true);
        return getRepo().sumShipmentOrderReport(orderReportFilterDTO1);
    }

    public Double sumLastShipmentOrderReport(OrderReportFilterDTO orderReportFilterDTO) {
        OrderReportFilterDTO orderReportFilterDTO1 = new OrderReportFilterDTO();
        BeanUtils.copyProperties( orderReportFilterDTO,orderReportFilterDTO1);

        Date from = Date.from(LocalDateTime.now().minusMonths(1).atZone(ZoneId.of(Utils.getDefaultZoneId())).toInstant());
        orderReportFilterDTO1.setFromDate(from);
        return sumShipmentOrderReport(orderReportFilterDTO1);
    }

    public Double sumAllOrder() {
        return getRepo().sumAllOrder();
    }

    public Page<Order> findByCoupon(Map<String, String> filters, Pageable pageable, Map<String, Object> map) {
        Date from = (Date) map.get("from");
        Date to = (Date) map.get("to");
        Calendar fromC = Calendar.getInstance();
        fromC.setTime(from);
        fromC.set(Calendar.MILLISECOND, 0);
        fromC.set(Calendar.SECOND, 0);
        fromC.set(Calendar.MINUTE, 0);
        fromC.set(Calendar.HOUR, 0);
        Calendar toC = Calendar.getInstance();
        toC.setTime(to);
        toC.set(Calendar.MILLISECOND, 250);
        toC.set(Calendar.SECOND, 59);
        toC.set(Calendar.MINUTE, 59);
        toC.set(Calendar.HOUR, 11);

        return getRepo().findByCoupon_CouponNumber((String) map.get("code"), fromC.getTime(), toC.getTime(), pageable);
    }

    public Page<Item> findItemsINOrderByCoupon(Map<String, String> filters, Pageable pageable, Map<String, Object> map) {
        Date from = (Date) map.get("from");
        Date to = (Date) map.get("to");
        Calendar fromC = Calendar.getInstance();
        fromC.setTime(from);
        fromC.set(Calendar.MILLISECOND, 0);
        fromC.set(Calendar.SECOND, 0);
        fromC.set(Calendar.MINUTE, 0);
        fromC.set(Calendar.HOUR, 0);
        Calendar toC = Calendar.getInstance();
        toC.setTime(to);
        toC.set(Calendar.MILLISECOND, 250);
        toC.set(Calendar.SECOND, 59);
        toC.set(Calendar.MINUTE, 59);
        toC.set(Calendar.HOUR, 11);

        return getRepo().findItemsInOrderByCoupon((String) map.get("code"), fromC.getTime(), toC.getTime(), pageable);
    }

    public Order findByIdAndAccount(Long id, Account account) {
        return orderRepo.findByIdAndAccount(id, account);
    }

    public Long findPendingAramexOrder() {
        return orderRepo.findPendingAramexOrder();
    }

    public Long findPendingEmailOrder() {
        return orderRepo.findPendingEmailOrder();
    }

    public Long findMaxInvoice() {
        return orderRepo.findMaxInvoice();
    }

	/*public Double sumOrdersByCouponCode(String code) {
		return orderRepo.sumOrdersByCouponCode(code);
	}*/

    public boolean generateInvoiceAndSendIt(Order order) {
        Map<String, Object> params = new HashMap<>();

        boolean sent = false;

        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            String currency = order.getCurrency().getCurrencyCode();
            params.put("logo", "https://ellepharma.com/logo.jpg");
            params.put("invoiceNumber", "INV" + String.format("%06d", order.getInvoiceNo()));
            params.put("orderNumber", " " + order.getId());
            params.put("invoiceDate", dateFormat.format(order.getCreationDate()));
            params.put("discount", Utils.convertPriceToString(order.getDiscount()) + " " + currency);
            params.put("subTotal", Utils.convertPriceToString(order.getTotal()) + " " + currency);
            params.put("shippingFee", Utils.convertPriceToString(order.getShippingFee()) + " " + currency);
            params.put("paymentFee", Utils.convertPriceToString(order.getPaymentFee()) + " " + currency);
            params.put("paymentType", order.getPaymentType().getId().toString());
            params.put("total",
                    Utils.convertPriceToString(order.getTotal() - order.getDiscount() + order.getPaymentFee() + order.getShippingFee())
                            + " " + currency);
            params.put("custEmail", order.getAccount().getEmail());
            params.put("custName", order.getAccount().getFullName());
            params.put("aramexTrNo", order.getTracking());

            params.put("custPhone", PhoneNumberUtil.getInstance()
                    .getCountryCodeForRegion(new Locale("ar",
                            order.getAccount().getPhoneCode())
                            .getCountry())
                    + " " + order.getAccount().getPhone());
            params.put("currency", order.getCurrency().getCurrencyCode());
            params.put("address", order.getAccount().getAddress());
            params.put("postcode", order.getAccount().getPostcode());
            params.put("state", order.getAccount().getState());
            params.put("city", order.getAccount().getCity());
            params.put("country", order.getAccount().getCountry().getCountryNameEn());
            params.put("custAddress", order.getAccount().getAddress() + " , " + order.getAccount().getState()
                    + " ," + order.getAccount().getCity() + " , " + order.getAccount().getPostcode() + " ," + order.getAccount()
                    .getCountry()
                    .getCountryNameEn());

            List<ReportItemDto> reportItemDtos = new ArrayList<>();
            int index = 1;
            if (CollectionUtils.isNotEmpty(order.getProductList())) {
                for (OrderProduct product : order.getProductList()) {
                    ReportItemDto n = new ReportItemDto();
                    n.setIndex(String.valueOf(index));
                    n.setCode(product.getItem().getCode());
                    n.setBarcode(product.getItem().getBarcodeString());
                    n.setPrice(product.getPrice());
                    n.setQty(product.getQty());
                    n.setTotal(product.getTotal());
                    n.setDesc(product.getItem().getDesc().get("en"));
                    n.setName(product.getItem().getName().get("en"));
                    reportItemDtos.add(n);
                    index++;
                }
            }

            params.put("list", new JRBeanCollectionDataSource(reportItemDtos));
            JasperPrint i = ReportUtils.doReport("invoce", params);

            String html = ReportUtils.export(i);
            File pdf = File.createTempFile("output.", ".html");
            //JasperExportManager.exportReportToHtmlFile(i,                    pdf.getPath());
            InputStream is = null;
            byte[] pdfbyte = null;
            String urlS = "";
            if (order.getAramexResponse() != null) {
                try {
                    Gson gson = new GsonBuilder().serializeNulls().create();

                    CreateShipmentsDto shipmentsDto = gson.fromJson(order.getAramexResponse(), CreateShipmentsDto.class);
                    is = new URL(shipmentsDto.getShipments().get(0).getShipmentLabel().getLabelURL()).openStream();
                    pdfbyte = IOUtils.toByteArray(is);
                } catch (Exception e) {
                    System.err.printf("Failed while reading bytes from %s: %s", urlS, e.getMessage());
                    e.printStackTrace();
                    // Perform any other exception handling that's appropriate.
                } finally {
                    if (is != null) {
                        is.close();
                    }
                }
            }
            sent = emailUtil.sendMailWithAttchmment(GlobalConfigProperties.getInstance().getProperty(ConfigEnum.SEND_ORDERS_TO.name()),
                    "new Order : " + order.getId(), html, "Aramex-label-order-" + order.getId() + ".pdf", pdfbyte);
            sent = emailUtil.sendMailWithAttchmment(order.getAccount().getEmail(), "new Order : " + order.getId(), html, "", null);

        } catch (Exception e) {
            sent = false;
            e.printStackTrace();
        }
        /*try {
        } catch (MessagingException e) {
            e.printStackTrace();
        }*/

        return sent;
    }

    public boolean generateInvoiceAndSendItToUser(Order order) {
        Map<String, Object> params = new HashMap<>();

        boolean sent = false;

        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            String currency = order.getCurrency().getCurrencyCode();
            params.put("logo", "https://ellepharma.com/logo.jpg");
            params.put("invoiceNumber", "INV" + String.format("%06d", order.getInvoiceNo()));
            params.put("orderNumber", " " + order.getId());
            params.put("invoiceDate", dateFormat.format(order.getCreationDate()));
            params.put("discount", Utils.convertPriceToString(order.getDiscount()) + " " + currency);
            params.put("subTotal", Utils.convertPriceToString(order.getTotal()) + " " + currency);
            params.put("shippingFee", Utils.convertPriceToString(order.getShippingFee()) + " " + currency);
            params.put("paymentFee", Utils.convertPriceToString(order.getPaymentFee()) + " " + currency);
            params.put("paymentType", order.getPaymentType().getId().toString());
            params.put("total",
                    Utils.convertPriceToString(order.getTotal() - order.getDiscount() + order.getPaymentFee() + order.getShippingFee())
                            + " " + currency);
            params.put("custEmail", order.getAccount().getEmail());
            params.put("custName", order.getAccount().getFullName());
            params.put("aramexTrNo", order.getTracking());

            params.put("custPhone", PhoneNumberUtil.getInstance()
                    .getCountryCodeForRegion(new Locale("ar",
                            order.getAccount().getPhoneCode())
                            .getCountry())
                    + " " + order.getAccount().getPhone());
            params.put("currency", order.getCurrency().getCurrencyCode());
            params.put("address", order.getAccount().getAddress());
            params.put("postcode", order.getAccount().getPostcode());
            params.put("state", order.getAccount().getState());
            params.put("city", order.getAccount().getCity());
            params.put("country", order.getAccount().getCountry().getCountryNameEn());
            params.put("custAddress", order.getAccount().getAddress() + " , " + order.getAccount().getState()
                    + " ," + order.getAccount().getCity() + " , " + order.getAccount().getPostcode() + " ," + order.getAccount()
                    .getCountry()
                    .getCountryNameEn());

            List<ReportItemDto> reportItemDtos = new ArrayList<>();
            int index = 1;
            if (CollectionUtils.isNotEmpty(order.getProductList())) {
                for (OrderProduct product : order.getProductList()) {
                    ReportItemDto n = new ReportItemDto();
                    n.setIndex(String.valueOf(index));
                    n.setCode(product.getItem().getCode());
                    n.setBarcode(product.getItem().getBarcodeString());
                    n.setPrice(product.getPrice());
                    n.setQty(product.getQty());
                    n.setTotal(product.getTotal());
                    n.setDesc(product.getItem().getDesc().get("en"));
                    n.setName(product.getItem().getName().get("en"));
                    reportItemDtos.add(n);
                    index++;
                }
            }

            params.put("list", new JRBeanCollectionDataSource(reportItemDtos));
            JasperPrint i = ReportUtils.doReport("invoce", params);

            String html = ReportUtils.export(i);
            File pdf = File.createTempFile("output.", ".html");
            //JasperExportManager.exportReportToHtmlFile(i,                    pdf.getPath());
            emailUtil.sendMail(order.getAccount().getEmail(), "new Order : " + order.getId(), html);
            sent = true;

        } catch (Exception e) {
            sent = false;
            e.printStackTrace();
        }
        /*try {
        } catch (MessagingException e) {
            e.printStackTrace();
        }*/

        return sent;
    }

    public List<Order> findByPaymentStatusAndPaymentType(PaymentStatusEnum paymentStatusEnum, PaymentTypeEnum paymentTypeEnum) {

        PaymentType paymentType = paymentTypeService.findById(paymentTypeEnum.getId());
        return orderRepo.findByPaymentStatusAndPaymentType(paymentStatusEnum, paymentType);
    }

    @Transactional
    @Modifying
    public void checkOrderByPaymentId(String paymentId) {

        if (StringUtils.isNotEmpty(paymentId)) {
            PayTabs payTabs = new PayTabs();
            VerifyRequest request = payTabs.verifyPayment(paymentId);
            if (request != null) {

                if (request.getResponse_code().equalsIgnoreCase("100")) {

                    orderRepo.updatePaymentStatusAndShipmentStatusByPaymentId(PaymentStatusEnum.TNC_100, ShipmentStatusEnum.Pending,
                            findMaxInvoice() + 1, paymentId);

                } else {
                    orderRepo.updatePaymentStatusAndShipmentStatusByPaymentId(PaymentStatusEnum.fomCode(request.getResponse_code()), null,
                            null, paymentId);

                }
            }
        }
    }

    @Modifying
    @Transactional
    public VerifyRequest  checkOrderById(Long orderId) {
        VerifyRequest request = null;
        if (orderId != null) {
            PayTabs payTabs = new PayTabs();
            String paymentId = orderRepo.findPaymentIdById(orderId);
            request = payTabs.verifyPayment(paymentId);
            if (request != null) {
                if (request.getResponse_code().equalsIgnoreCase("100")) {

                    orderRepo.updatePaymentStatusAndShipmentStatusById(PaymentStatusEnum.TNC_100, ShipmentStatusEnum.Pending,
                            findMaxInvoice() + 1, orderId);

                } else {
                    orderRepo.updatePaymentStatusAndShipmentStatusById(PaymentStatusEnum.fomCode(request.getResponse_code()), null, null,
                            orderId);

                }
            }
        }
        return request;
    }

    @Modifying
    @Transactional
    public void checkAllPaymentToday() {

        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.add(Calendar.DAY_OF_MONTH,-1);
        List<String> paymentIds = orderRepo.findPaymentIDByPaymentStatusNo100AndPaymentType(now.getTime());

        paymentIds.forEach(paymentId -> {
            if (StringUtils.isNotEmpty(paymentId)) {
                PayTabs payTabs = new PayTabs();
                VerifyRequest request = payTabs.verifyPayment(paymentId);
                if (request != null) {
                    if (request.getResponse_code().equalsIgnoreCase("100")) {
                        orderRepo.updatePaymentStatusAndShipmentStatusByPaymentId(PaymentStatusEnum.TNC_100, ShipmentStatusEnum.Pending,
                                findMaxInvoice() + 1, paymentId);
                    } else {
                        orderRepo.updatePaymentStatusAndShipmentStatusByPaymentId(PaymentStatusEnum.fomCode(request.getResponse_code()),
                                null, null, paymentId);

                    }
                }
            }
        });
    }

	/*public Double sumItemOrdersByCouponCode(String code) {
		return orderRepo.sumItemOrdersByCouponCode(code);
	}*/

    public Double totalCommissions(String code, Date from, Date to) {
        return orderRepo.totalCommissions(code);
    }

}
