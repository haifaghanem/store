package com.ellepharma.service;

import io.minio.errors.InvalidArgumentException;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface ContentService {

	public String getEndpoint();

	public void createStorageUnit(String name) throws Exception;

	public void removeStorageUnit(String name) throws Exception;

	public InputStream getContent(String storageUnit, String objectName);

	public String getContentUrl(String storageUnit, String objectName);

	public List<String> listContentURLs(String storageUnit);

	public List<String> listContentURLs(String storageUnit, String prefix);

	public String putContent(String StorageUnit, String ObjectName, InputStream inputStream, long size, String contentType)
			throws Exception;

    String replaceContent(String oldContentURL, String newStorageUnit, String newContentName, InputStream newContentStream,
			long newContentSize, String newContentType) throws InvalidArgumentException;

	public void removeContent(String storageUnit, String objectName);

    public Map<String, String> getContentMetadata(String storageUnit, String objectName);

}
