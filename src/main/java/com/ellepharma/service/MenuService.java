package com.ellepharma.service;

import com.ellepharma.data.model.Menu;
import com.ellepharma.data.repository.MenuRepo;
import com.ellepharma.utils.Utils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MenuService extends BaseService<Menu> {

    @Autowired
    private MenuRepo menuRepo;

    @Autowired
    private MenuService menuService;

    @Override
    public MenuRepo getRepo() {
        return menuRepo;
    }

    @Transactional
    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "menu", allEntries = true),
            @CacheEvict(value = "menuString", allEntries = true),
            @CacheEvict(value = "menuString2", allEntries = true),
    })
    public Menu save(Menu menu) {

        return getRepo().save(menu);
    }

    @Transactional
    @Cacheable("menu")
    public List<Menu> findAllByStatusTrueAndWitChild() {

        List<Menu> mainMenu = getRepo().findAllByStatusTrueAndParentIsNullOrderByPriorityOrderAsc();

        mainMenu.stream().forEach(menu -> {
            prepareChildren(menu);
        });

        return mainMenu;
    }

    private void prepareChildren(Menu menu) {

        List<Menu> child = menuRepo.findAllByStatusTrueAndParent_IdOrderByPriorityOrderAsc(menu.getId());
        if (child != null)
            child.stream().forEach(menu1 -> {
                prepareChildren(menu1);

            });
        menu.setChildrenList(child);

    }

    @Cacheable(value = "menuString", key = "#lang")
    public String   getMenuString(String lang) {
        List<Menu> menuList = findAllByStatusTrueAndWitChild();
        final String[] menuHtml = {""};
        if (CollectionUtils.isNotEmpty(menuList)) {
            menuList.stream().forEach(menu -> {
                String start = "";
                String center = "";
                String end = "";
                String icon = CollectionUtils.isNotEmpty(menu.getChildrenList()) ? "drop-icon" : "";
                start = "<li class='active'>" +
                        "<a class='" + icon + "' href='" + Utils.getAppNampe() +  menu.getUrl() + "'>" + menu
                        .getName().get(lang)
                        + "</a>";
                String centerStart = "<ul class='ht-dropdown '>";
                String centerEnd = "</ul>";
                String centerSub = "";
                if (CollectionUtils.isNotEmpty(menu.getChildrenList())) {

                    for (Menu menue2 : menu.getChildrenList()) {
                        boolean foundSub = false;
                        if (CollectionUtils.isNotEmpty(menue2.getChildrenList())) {
							centerStart = "<ul class='ht-dropdown megamenu megamenu-three custom-menue'>";
                            String mm = "<li> <ul><li class='menu-tile'> <a href='"+Utils.getAppNampe() +  menue2.getUrl() + "'>";
                            mm = mm + menue2.getName().get(lang);
                            mm = mm + "</a>";
                            for (Menu menue3 : menue2.getChildrenList()) {
                                mm = mm + "<li><a href='" + Utils.getAppNampe() +   menue3.getUrl() + "'>" + menue3.getName().get(lang)
                                        + "</a></li>";

                            }
                            mm = mm + "</ul></li>";
                            centerSub = centerSub + mm;
                        } else {
                            //centerStart = "<ul class='ht-dropdown'>";

                            centerSub += "<li><a href='" +Utils.getAppNampe() +   menue2.getUrl() + "'>" + menue2.getName().get(lang) + "</a></li> ";
                        }
                    }

                    center = centerStart + centerSub + centerEnd;

                }
                end = "</li>";

                menuHtml[0] += start + center + end;

            });
        }

        return menuHtml[0];
    }

    @Cacheable(value = "menuString2", key = "#lang")
    public String getMenuString2(String lang) {
        List<Menu> menuList = findAllByStatusTrueAndWitChild();
        final String[] menuHtml = {""};
        String start = "<nav class='mean-nav'><ul>";

        menuList.forEach(menu -> {
            menuHtml[0] += "<li><a href='" + Utils.getAppNampe() +   menu.getUrl() + "'>" + menu.getName().get(lang) + "</a>";
            if (CollectionUtils.isNotEmpty(menu.getChildrenList())) {
                menuHtml[0] += " <ul class='submobile-mega-dropdown' style='display: none;'>";
                menu.getChildrenList().forEach(menu1 -> {
                    menuHtml[0] += "<li><a href='"+ Utils.getAppNampe()  +   menu1.getUrl() + "'>" + menu1.getName().get(lang) + "</a>";
                    if (CollectionUtils.isNotEmpty(menu1.getChildrenList())) {
                        menuHtml[0] += " <ul class='submobile-mega-dropdown' style='display: none;'>";
                        menu1.getChildrenList().forEach(menu2 -> {
                            menuHtml[0] += "<li><a href='"+ Utils.getAppNampe()  +   menu2.getUrl() + "'>" + menu2.getName().get(lang) + "</a>";
                        });
                        menuHtml[0] += "</ul></li>";
                    }
                });
                menuHtml[0] += "</ul></li>";
            }

            menuHtml[0] += "</li>";

        });

        String end = "</ul></nav>";
        menuHtml[0] = start + menuHtml[0] + end;

        return menuHtml[0];
    }
}
