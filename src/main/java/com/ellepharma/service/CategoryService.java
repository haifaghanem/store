package com.ellepharma.service;

import com.ellepharma.data.model.Category;
import com.ellepharma.data.model.Image;
import com.ellepharma.data.repository.CategoryRepo;
import com.ellepharma.data.repository.ImageRepo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService extends BaseService<Category> {

    @Autowired
    private CategoryRepo categoryRepo;

    @Autowired
    private ImageRepo imageRepo;

    @Override
    public CategoryRepo getRepo() {
        return categoryRepo;
    }

    @Transactional
    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "category", key = "#category.id"),
            @CacheEvict(value = "slidCategory", allEntries = true),
            @CacheEvict(value = "categoryTreeNode", allEntries = true),

    })
    public Category save(Category category) {
        if (category.getImage() != null) {
            Image savedImage = imageRepo.save(category.getImage());
            category.setImage(savedImage);
        }
        return getRepo().save(category);
    }

    @Cacheable("category")
    public List<Category> findAllActive() {

        return getRepo().findAllByStatusTrue();
    }

    @Cacheable("slidCategory")
    public List<Category> findAllByStatusTrueAndShowInSliderTrue() {
        return getRepo().findAllByStatusTrueAndShowInSliderTrue();

    }

    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "slidCategory", allEntries = true),
            @CacheEvict(value = "category", allEntries = true),
            @CacheEvict(value = "categoryTreeNode", allEntries = true),
    })

    public void delete(Category t) {
        getRepo().delete(t);
    }

    /**
     * @param categoryList
     * @return
     */

    @Transactional
    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "category", allEntries = true),
            @CacheEvict(value = "slidCategory", allEntries = true),
            @CacheEvict(value = "categoryTreeNode", allEntries = true),

    })
    public List<Category> saveImportData(List<Category> categoryList) {
        List<Category> saved = new ArrayList<>();

        List<Category> parentList = categoryList.stream().filter(cat -> StringUtils.isEmpty(cat.getParentCode()))
                .collect(Collectors.toList());
        List<Category> childList = categoryList.stream().filter(cat -> StringUtils.isNotEmpty(cat.getParentCode()))
                .collect(Collectors.toList());

        parentList.stream().forEach(p -> {
            Category old = categoryRepo.findByCode(p.getCode());
            if (old == null) {
                old = new Category();
            }
            BeanUtils.copyProperties(p, old, "id", "parent", "image", "status", "showInSlider", "versionId", "");
            saved.add(categoryRepo.save(old));
        });

        childList.stream().forEach(c -> {
            Category old = categoryRepo.findByCode(c.getCode());
            if (old == null) {
                old = new Category();
            }
            if (StringUtils.isNotEmpty(c.getParentCode())) {
                Category parent = categoryRepo.findByCode(c.getParentCode());
                c.setParent(parent);
            }

            BeanUtils.copyProperties(c, old, "id", "parent", "image", "status", "showInSlider", "versionId", "");

            saved.add(categoryRepo.save(old));

        });

        return saved;
    }


    @Cacheable("categoryTreeNode")
    public TreeNode createCheckboxDocuments() {

        TreeNode root = new CheckboxTreeNode(new Category(), null);
        List<Category> categories = getRepo().findAllByParentIsNull();
        categories.forEach(category -> {
            TreeNode mainCat = new CheckboxTreeNode(category, root);
            mainCat.setPartialSelected(true);
            List<Category> i = categoryRepo.findAllByParent_Id(category.getId());
            if (CollectionUtils.isNotEmpty(i)) {
                prepare(category, mainCat, i);

            }
        });

        return root;
    }

    private void prepare(Category category, TreeNode mainCat, List<Category> categories) {

        categories.forEach(cate -> {
            TreeNode cat = new CheckboxTreeNode(cate, mainCat);
            List<Category> categories1 = categoryRepo.findAllByParent_Id(cate.getId());
            if (CollectionUtils.isNotEmpty(categories1)) {
                prepare(cate, cat, categories1);
            }

        });
    }

    public List<Category> getAllChildStrings(Long id) {
        Category c = categoryRepo.findById(id);
        List<Category> categories1 = categoryRepo.findAllByParent_Id(id);
        List<Category> categories2 = new ArrayList<>();
        categories2.add(c);
        categories2.addAll(categories1);
        categories1.forEach(category -> {
            categories2.addAll(getAllChildStrings(category.getId()));
        });

        return categories2;
    }
}
