package com.ellepharma.service;

import com.ellepharma.data.model.Country;
import com.ellepharma.data.model.Currency;
import com.ellepharma.data.repository.CountryRepo;
import com.ellepharma.web.api.AramexApi;
import com.ellepharma.web.aramex.dto.FetchCitiesDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.maxmind.db.Reader;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class CountryService extends BaseService<Country> {

    @Autowired
    private CountryRepo countryRepo;

    @Autowired
    private AramexApi aramexApi;

    DatabaseReader dbReader;
    String dbLocation = "GeoLite2-City.mmdb";


    public DatabaseReader getDbReader() {

        if (dbReader == null) {
            File database = new File(Objects.requireNonNull(getClass().getClassLoader().getResource(dbLocation)).getFile());
            try {
                dbReader = new DatabaseReader.Builder(database).fileMode(Reader.FileMode.MEMORY).build();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dbReader;
    }

    /**
     * @param countryName
     * @return
     */
    public Country getCountryByCountryName(String countryName) {
        return countryRepo.findCountryByCountryNameEnIgnoreCase(countryName);
    }

    @Cacheable(value = "cities", key = "#countryCode")
    public List<String> cities(String countryCode) {

        List<String> cities = new ArrayList<>();
        if (StringUtils.isNotEmpty(countryCode)) {
            ObjectMapper mapper = new ObjectMapper();
            String[] pp1 = mapper.convertValue(aramexApi.fetchCities(new FetchCitiesDto(countryCode)).get("Cities"), String[].class);
            cities = Arrays.asList(pp1);
        }
        return cities;
    }

    /**
     * @param code
     * @return
     */
    @Cacheable(value = "country", key = "#code")

    public Country findByIsoCode3(String code) {
        return countryRepo.findByIsoCode3IgnoreCase(code);
    }

    /**
     * @param code
     * @return
     */
    @Cacheable(value = "country", key = "#code")
    public Country findByIsoCode2(String code) {
        return countryRepo.findByIsoCode2IgnoreCase(code);
    }

    /**
     * @return
     */
    @Override
    public CountryRepo getRepo() {
        return countryRepo;
    }

    /**
     * @param ipAddress
     * @return
     */
    @Transactional
    @Cacheable(value = "countryByIp", key = "#ipAddress")
    public Country findByIP(String ipAddress) {
        String isoCode = "";
        Country country = null;
        Country defCountry = findByIsoCode3("SAU");

        try {
            InetAddress ip = InetAddress.getByName(ipAddress);
            CityResponse response = getDbReader().city(ip);
            isoCode = response.getCountry().getIsoCode();
            country = findByIsoCode2(isoCode);
        } catch (Exception e) {
            //	e.printStackTrace();
            return defCountry;
        }
        return country;
    }

    @Transactional(readOnly = false)
    public void updateCurrencyForAll(Currency currency2) {
        getRepo().updateCurrencyForAll(currency2);
    }
}
