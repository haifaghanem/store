package com.ellepharma.service;

import com.ellepharma.data.dto.ItemReportFilterDTO;
import com.ellepharma.data.dto.ItemReportResultDTO;
import com.ellepharma.data.repository.ItemRepo;
import com.ellepharma.data.repository.OrderProductRepo;
import com.ellepharma.data.repository.OrderRepo;
import com.ellepharma.data.repository.StockInRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * ItemReportService.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since August 16, 2019
 */
@Service
public class ItemReportService {

    @Autowired
    private ItemRepo itemRepo;

    @Autowired
    private OrderProductRepo orderProductRepo;

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private StockInRepo stockInRepo;

    public List<ItemReportResultDTO> findAllReportItem(ItemReportFilterDTO itemReportFilterDTO) {
        List<ItemReportResultDTO> itemReportResultDTOS = itemRepo.findAllReportItem(itemReportFilterDTO);
        itemReportResultDTOS.forEach(itemReportResultDTO -> {
             itemReportResultDTO.setOriginalItemQty(stockInRepo.getStockInQuantityByItemCode(itemReportResultDTO.getItem().getCode()));
             itemReportResultDTO.setSoldItemQty(orderRepo.getOrderProductQuantityByItemCode(itemReportResultDTO.getItem().getCode()));
         });

        return itemReportResultDTOS;

    }

    public void setItemRepo(ItemRepo itemRepo) {
        this.itemRepo = itemRepo;
    }

    public void setOrderProductRepo(OrderProductRepo orderProductRepo) {
        this.orderProductRepo = orderProductRepo;
    }

    public void setStockInRepo(StockInRepo stockInRepo) {
        this.stockInRepo = stockInRepo;
    }
}
