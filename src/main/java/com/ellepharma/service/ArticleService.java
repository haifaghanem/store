package com.ellepharma.service;

import com.ellepharma.data.model.Article;
import com.ellepharma.data.model.Image;
import com.ellepharma.data.repository.ArticleRepo;
import com.ellepharma.data.repository.ImageRepo;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ArticleService extends BaseService<Article> {

    @Autowired
    private ArticleRepo articleRepo;

    @Autowired
    private ImageRepo imageRepo;

    @Override
    public ArticleRepo getRepo() {
        return articleRepo;
    }


    @Override
    @Cacheable(value = "article", key = "#id")
    public Article findById(Long id) {
        return super.findById(id);
    }

    @Transactional
    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "article", key = "#article.id"),
    })
    public Article save(Article article) {
        if (article.getPublishDate() == null) {
            article.setPublishDate(new Date());
        }

        if (article.getCover() != null) {
            Image savedImage = imageRepo.save(article.getCover());
            article.setCover(savedImage);
        }

        if (CollectionUtils.isNotEmpty(article.getImages())) {
            List<Image> images = new ArrayList<>();
            article.getImages().stream().forEach(image -> {
                if (image.isMarkedForDeletion()) {
                    imageRepo.delete(image);
                } else {
                    Image savedImage = imageRepo.save(image);
                    images.add(savedImage);
                }
            });
            article.setImages(images);
        }
        return getRepo().save(article);
    }


}
