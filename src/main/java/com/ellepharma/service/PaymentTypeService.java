package com.ellepharma.service;

import com.ellepharma.data.model.Country;
import com.ellepharma.data.model.PaymentType;
import com.ellepharma.data.repository.CountryCashOnDeliveryRepo;
import com.ellepharma.data.repository.PaymentTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * PaymentTypeService.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 25, 2019
 */
@Service
public class PaymentTypeService extends BaseService<PaymentType> {

	@Autowired
	private PaymentTypeRepo paymentTypeRepo;

	@Autowired
	private CountryCashOnDeliveryRepo countryCashOnDeliveryRepo;

	@Override
	public PaymentTypeRepo getRepo() {
		return paymentTypeRepo;
	}

	@Override
	@Cacheable("paymentType")
	public PaymentType findById(Long id) {
		return super.findById(id);
	}

	@Override
	@Modifying
	@Caching(evict = {
			@CacheEvict(value = "paymentType", allEntries = true),
			@CacheEvict(value = "paymentTypes", allEntries = true),
	})
	public PaymentType save(PaymentType paymentType) {

		paymentType.getCountryCashOnDeliveries().forEach(countryShipment -> {
			countryShipment = countryCashOnDeliveryRepo.save(countryShipment);
		});

		return super.save(paymentType);
	}

	@Override
	@Modifying
	@Caching(evict = {
			@CacheEvict(value = "paymentType", allEntries = true),
			@CacheEvict(value = "paymentTypes", allEntries = true),
	})
	public void delete(PaymentType paymentType) {
		super.delete(paymentType);
	}

	public List<PaymentType> findAllActive() {

		return getRepo().findAllByStatusTrue();
	}

	public List<PaymentType> findAllByCountry(Country country) {

		return getRepo().findAllByCountry(country);
	}

}
