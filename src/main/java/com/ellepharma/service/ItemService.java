package com.ellepharma.service;

import com.ellepharma.data.dto.ItemDTO;
import com.ellepharma.data.dto.SortTypeEnum;
import com.ellepharma.data.model.Category;
import com.ellepharma.data.model.Image;
import com.ellepharma.data.model.Item;
import com.ellepharma.data.model.Manufacturer;
import com.ellepharma.data.repository.*;
import com.ellepharma.exception.BusinessException;
import com.ellepharma.utils.Utils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ItemService extends BaseService<Item> {

    @Autowired
    private ItemRepo itemRepo;

    @Autowired
    private CategoryRepo categoryRepo;

    @Autowired
    private ManufacturerRepo manufacturerRepo;

    @Autowired
    private ImageRepo imageRepo;

    @Autowired
    private OrderProductRepo orderProductRepo;

    @Override
    public ItemRepo getRepo() {
        return itemRepo;
    }

    @Transactional
    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "newItemTrue", allEntries = true),
            @CacheEvict(cacheNames = "items", allEntries = true),
            @CacheEvict(cacheNames = "bestPrice", allEntries = true),
            @CacheEvict(cacheNames = "bestSeller", allEntries = true),
            @CacheEvict(cacheNames = "item", key = "#item.id"),
            @CacheEvict(cacheNames = "countItems", allEntries = true),
            @CacheEvict(cacheNames = "countItemByStatusTrue", allEntries = true),
            @CacheEvict(cacheNames = "relatedProduct", allEntries = true),
            @CacheEvict(cacheNames = "findAllByCategories", allEntries = true),
            @CacheEvict(cacheNames = "availableQuantityByItemID", allEntries = true),
    })
    public Item save(Item item) {
        if (CollectionUtils.isNotEmpty(item.getImage())) {
            List<Image> images = new ArrayList<>();
            item.getImage().stream().forEach(image -> {
                if (image.isMarkedForDeletion()) {
                    imageRepo.delete(image);

                } else {
                    Image savedImage = imageRepo.save(image);
                    images.add(savedImage);
                }

            });
            item.setImage(images);
        }
        if (CollectionUtils.isNotEmpty(item.getBarcodeList())) {
            item.setBarcodeString(item.getBarcodeList().stream().map(String::trim).collect(Collectors.joining("#")));
        }

        return getRepo().save(item);
    }

    public List<Item> findAllActive() {
        return getRepo().findAllByStatusTrue();
    }

    public List<Item> findAllByStatus(Boolean status) {
        return getRepo().findAllByStatus(status);
    }

    /**
     * @param id`
     * @return
     */
    @Cacheable(value = "item", key = "#id")
    public Item findById(Long id) {
        Item item = itemRepo.findById(id);
        return item;
    }

    /**
     * @return
     */
    @Cacheable("newItemTrue")
    public List<Item> findAllByNewItemTrue() {
        return getRepo().findAllByNewItemTrueAndStatusTrue();
    }

    @Cacheable("bestPrice")
    public List<Item> findDistinctTop15ByStatusTrueOrderByPriceAsc() {
        return getRepo().findDistinctTop15ByStatusTrueOrderByPriceAsc();
    }


    @Cacheable("bestSeller")
    public List<Item> findTop15ByOrderBySeller() {

        Pageable pageable = new PageRequest(0, 15);

        List<Long> rids = orderProductRepo.getTids(pageable);
        return getRepo().findByIdIn(rids);
    }

    @Transactional
    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "newItemTrue", allEntries = true),
            @CacheEvict(cacheNames = "items", allEntries = true),
            @CacheEvict(cacheNames = "bestPrice", allEntries = true),
            @CacheEvict(cacheNames = "item", allEntries = true),
            @CacheEvict(cacheNames = "bestSeller", allEntries = true),
            @CacheEvict(cacheNames = "countItems", allEntries = true),
            @CacheEvict(cacheNames = "countItemByStatusTrue", allEntries = true),
            @CacheEvict(cacheNames = "relatedProduct", allEntries = true),
            @CacheEvict(cacheNames = "findAllByCategories", allEntries = true),
            @CacheEvict(cacheNames = "availableQuantityByItemID", allEntries = true),
    })
    public void saveNewProduct(List<Item> selectedItemList) {
        itemRepo.updateAllSetNotNew();

        selectedItemList.forEach(item -> item.setNewItem(true));
        itemRepo.save(selectedItemList);
    }

    /**
     * @param categories
     * @return
     */

    @Cacheable(value = "findAllByCategories", key = "{#categories}")
    public List<Item> findAllByCategories(List<Category> categories) {
        return getRepo().findDistinctByStatusTrueAndCategoriesIn(categories);
    }

    @Transactional
    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "newItemTrue", allEntries = true),
            @CacheEvict(cacheNames = "items", allEntries = true),
            @CacheEvict(cacheNames = "bestPrice", allEntries = true),
            @CacheEvict(cacheNames = "item", allEntries = true),
            @CacheEvict(cacheNames = "countItems", allEntries = true),
            @CacheEvict(cacheNames = "countItemByStatusTrue", allEntries = true),
            @CacheEvict(cacheNames = "relatedProduct", allEntries = true),
            @CacheEvict(cacheNames = "findAllByCategories", allEntries = true),
            @CacheEvict(cacheNames = "availableQuantityByItemID", allEntries = true),
    })
    public List<Item> saveImportData(List<Item> itemList) {
        List<Item> saved = new ArrayList<>();
        AtomicReference<String> codeC = new AtomicReference<>("");
        try {
            itemList.stream().filter(p -> p != null
            ).forEach(p -> {
                if (CollectionUtils.isNotEmpty(p.getBarcodeList())) {
                    p.setBarcodeString(p.getBarcodeList().stream().map(String::trim).collect(Collectors.joining(",")));
                }
                codeC.set(p.getCode());
                if (StringUtils.isNotEmpty(p.getCatCode())) {

                    List<String> codes = Stream.of(p.getCatCode().split(",")).collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(codes)) {
                        List<Category> categories = categoryRepo.findByCodeIn(codes);
                        p.setCategories(categories);
                    }
                }
                if (StringUtils.isNotEmpty(p.getBarcode())) {

                    Manufacturer brand = manufacturerRepo.findByCode(p.getBrandCode());
                    p.setManufacturer(brand);
                }

                saved.add(itemRepo.save(p));

            });
        } catch (Exception e) {
            throw new BusinessException("حصل خطأ عند حفظ المادة كود " + codeC + " >>>>>>" + e.getMessage());
        }

        return saved;

    }

    /**
     * @param code
     * @return
     */
    @Transactional
    public Item findByCode(String code) {
        Item item = itemRepo.findByCode(code);
        if (item == null)
            return new Item();
        else
            return item;
    }

    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "newItemTrue", allEntries = true),
            @CacheEvict(cacheNames = "items", allEntries = true),
            @CacheEvict(cacheNames = "bestPrice", allEntries = true),
            @CacheEvict(cacheNames = "item", allEntries = true),
            @CacheEvict(cacheNames = "countItems", allEntries = true),
            @CacheEvict(cacheNames = "countItemByStatusTrue", allEntries = true),
            @CacheEvict(cacheNames = "relatedProduct", allEntries = true),
            @CacheEvict(cacheNames = "findAllByCategories", allEntries = true),
            @CacheEvict(cacheNames = "availableQuantityByItemID", allEntries = true),
    })

    public void delete(Item t) {
        getRepo().delete(t);
    }

    public void setCategoryRepo(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }

    /*@Cacheable(value = "countItems", key = "{#pageable.pageNumber ,#pageable.offset,#catIds}")*/
    public List<Item> findAllByStatusTrueAndCategoriesIdIn(Pageable pageable, List<Long> catIds) {

        return itemRepo.findAllByStatusTrueAndCategoriesIdIn(pageable, catIds);
    }

    /*@Cacheable(value = "countItems", key = "{#catIds}")*/
    public int countAllByStatusTrueAndCategoriesIdIn(List<Long> catIds) {

        return itemRepo.countAllByStatusTrueAndCategoriesIdIn(catIds);
    }

    /*    @Cacheable(value = "items",
				key = "{#pageable.pageNumber ,#pageable.offset ,#pageable.pageSize,#pageable.sort, #itemCriteri.getStringToCash()}")*/
    public List<Item> findItems(final ItemDTO itemCriteri, Pageable pageable) {
        ItemDTO itemCriteria = new ItemDTO();
        itemCriteria.setSortWay(itemCriteri.getSortWay());
        itemCriteria.setBrandIds(itemCriteri.getBrandIds());
        itemCriteria.setCatIds(itemCriteri.getCatIds());
        itemCriteria.setSearchByName(itemCriteri.getSearchByName());

        try {
            itemCriteria.setFromPrice(itemCriteri.getFromPrice() / Utils.getCurrentCurrency().getValue());
            itemCriteria.setToPrice(itemCriteri.getToPrice() / Utils.getCurrentCurrency().getValue());
        } catch (Exception e) {

        }
        List<Item> items = itemRepo.findAllItemsWithFilters(pageable, itemCriteria);
        if (itemCriteria.getSortWay() != null) {
            Collections.sort(items, new Comparator<Item>() {

                public int compare(Item p1, Item p2) {
                    if (itemCriteria.getSortWay().equals(SortTypeEnum.ASC)) {

                        return p1.getName().get(p1.getCurrentLanguage()).compareTo(p2.getName().get(p2.getCurrentLanguage()));
                    } else if (itemCriteria.getSortWay().equals(SortTypeEnum.DESC)) {

                        return p2.getName().get(p2.getCurrentLanguage()).compareTo(p1.getName().get(p1.getCurrentLanguage()));
                    } else if (itemCriteria.getSortWay().equals(SortTypeEnum.PRICE_ASC)) {

                        return p1.getPrice().compareTo(p2.getPrice());
                    } else if (itemCriteria.getSortWay().equals(SortTypeEnum.PRICE_DESC)) {

                        return p2.getPrice().compareTo(p1.getPrice());
                    }

                    return 0;
                }
            });
        }

        return items;
    }

    /*@Cacheable(value = "countItems", key = "{#itemCriteri.getStringToCash()}")*/
    public Long findItemsCount(ItemDTO itemCriteri) {
        ItemDTO itemCriteria = new ItemDTO();
        itemCriteria.setSortWay(itemCriteri.getSortWay());
        itemCriteria.setBrandIds(itemCriteri.getBrandIds());
        itemCriteria.setCatIds(itemCriteri.getCatIds());
        itemCriteria.setSearchByName(itemCriteri.getSearchByName());

        try {
            itemCriteria.setFromPrice(itemCriteri.getFromPrice() / Utils.getCurrentCurrency().getValue());
            itemCriteria.setToPrice(itemCriteri.getToPrice() / Utils.getCurrentCurrency().getValue());
        } catch (Exception e) {

        }

        Long count = itemRepo.countAllItemsWithFilters(itemCriteria);
        return count == null ? 0 : count;
    }

    /*@Cacheable(value = "countItemByStatusTrue")*/
    public int countAllByStatusTrue() {
        return itemRepo.countAllByStatusTrue();
    }

    @Cacheable(value = "relatedProduct", key = "#item.id")
    public List<Item> findRelatedItems(Item item) {
        return findAllByCategories(item.getCategories());
    }

    /**
     * @param filters
     * @param pageable
     * @param r
     * @return
     */
    public Page<Item> findAllByBarcodeIn(Map<String, String> filters, Pageable pageable, Object... r) {
        return getRepo().findAllByBarcodeListLike(pageable, (String) r[0]);
    }
}
