package com.ellepharma.service;

import com.ellepharma.data.model.Account;
import com.ellepharma.data.model.Role;
import com.ellepharma.data.repository.AccountRepo;
import com.ellepharma.data.repository.RoleRepo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.internal.Function;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.*;

@Service
public class UserService extends BaseService<Account> implements UserDetailsService {


    @Autowired
    private RoleRepo roleRepo;
    @Autowired
    private AccountRepo accountRepo;

    @Override
    public AccountRepo getRepo() {
        return accountRepo;
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) {
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

        String userName = null;
        String password;
        if (isEmail(username)) {
            Account account = accountRepo.findByEmailAndActiveIsTrue(username);
            userName = account.getUsername();
            password = account.getPassword();
            if (account == null) throw new UsernameNotFoundException(username);
            for (Role role : account.getRoles()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
            }


        } else {
            Account account = accountRepo.findByUsernameAndActiveIsTrue(username);
            if (account == null) throw new UsernameNotFoundException(username);
            userName = account.getUsername();
            password = account.getPassword();
            for (Role role : account.getRoles()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
            }


        }
        return new org.springframework.security.core.userdetails.User(userName, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, grantedAuthorities);


    }

    static boolean isEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }


    /**
     * JwtTokenUtil.java
     *
     * @author Malek Yaseen <ma.yaseen@ats-ware.com>
     * @since Jul 21, 2019
     */
    @Component
    public static class JwtTokenUtil implements Serializable {

        //@See https://www.timecalculator.net/milliseconds-to-minutes
        public static final long JWT_TOKEN_VALIDITY = 86400000; // Milliseconds

        //@Value("${jwt.secret}")
        private final String secret = "5r&SnnQ7UBNs?$n$GLZC+Psss3=";// passwordsgenerator.net

        //retrieve username from jwt token
        public String getUsernameFromToken(String token) {
            return getClaimFromToken(token, Claims::getSubject);
        }

        //retrieve expiration date from jwt token
        public Date getExpirationDateFromToken(String token) {
            return getClaimFromToken(token, Claims::getExpiration);
        }

        public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
            final Claims claims = getAllClaimsFromToken(token);
            return claimsResolver.apply(claims);
        }

        //for retrieving any information from token we will need the secret key
        private Claims getAllClaimsFromToken(String token) {
            return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        }

        //check if the token has expired
        public Boolean isTokenExpired(String token) {
            final Date expiration = getExpirationDateFromToken(token);
            return expiration.before(new Date());
        }

        //generate token for user
        public String generateToken(Account account) {
            Map<String, Object> claims = new HashMap<>();
            return doGenerateToken(claims, account.getEmail());
        }

        //while creating the token -
        //1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
        //2. Sign the JWT using the HS512 algorithm and secret key.
        //3. According to JWS Compact Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
        //   compaction of the JWT to a URL-safe string
        private String doGenerateToken(Map<String, Object> claims, String subject) {
            return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                    .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                    .signWith(SignatureAlgorithm.HS512, secret).compact();
        }

        //validate token
        public Boolean validateToken(String token, Account account) {
            final String username = getUsernameFromToken(token);
            return (username.equals(account.getEmail()) && !isTokenExpired(token));
        }
    }
}
