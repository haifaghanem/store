package com.ellepharma.service;

import com.ellepharma.data.model.ShipmentType;
import com.ellepharma.data.repository.ShipmentTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ShipmentService.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 22, 2019
 */
@Service
@Transactional
public class ShipmentTypeService extends BaseService<ShipmentType> {

    @Autowired
    private ShipmentTypeRepo shipmentTypeRepo;



    @Cacheable(value = "shipmentType")

    public ShipmentType findByStatusTrue() {
        return shipmentTypeRepo.findByStatusTrue();
    }

    @Override
    public ShipmentTypeRepo getRepo() {
        return shipmentTypeRepo;
    }

    @Modifying
    @Caching(evict = {
            @CacheEvict(value = "shipmentType", allEntries = true)
    })
    public void saveDefaultShip(ShipmentType selectedDefaultShipmentType) {

        shipmentTypeRepo.updateAllFalse();
        if (selectedDefaultShipmentType != null) {
            shipmentTypeRepo.updateStatusTrueById(selectedDefaultShipmentType.getId());

        }


    }


}
