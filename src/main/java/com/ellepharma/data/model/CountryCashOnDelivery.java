package com.ellepharma.data.model;

import com.ellepharma.utils.BooleanToIntegerAttributeConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * CountryShipment.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since June 07, 2019
 */

@Entity
@Table(name = "COUNTRY_CASH_ON_DELIVERY")
public class CountryCashOnDelivery extends BaseEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	@Basic(optional = false)
	private Long id;

	@Basic(optional = false)
	@NotNull
	@Column(name = "IS_FREE")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private Boolean free = false;

	@Basic(optional = false)
	@NotNull
	@Column(name = "RATE", columnDefinition = "double(20,4) DEFAULT 0.0000")
	private Double rate;

	@Basic(optional = false)
	@NotNull
	@Column(name = "IS_PERCENTAGE")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private Boolean percentage = false;

	@OneToOne
	@JoinColumn(name = "COUNTRY_ID")
	private Country country;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		CountryCashOnDelivery that = (CountryCashOnDelivery) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "CountryShipment{" +
				"id=" + id +
				'}';
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Boolean getPercentage() {
		return percentage;
	}

	public void setPercentage(Boolean percentage) {
		this.percentage = percentage;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Boolean getFree() {
		return free;
	}

	public void setFree(Boolean free) {
		this.free = free;
	}
}
