package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * Manufacturer.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 20, 2019
 */

@Entity
@Table(name = "MANUFACTURER")
public class Manufacturer extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @NotNull
    @Column(name = "CODE", unique = true)
    @Basic(optional = false)
    private String code;

    @Basic(optional = false)
    @NotNull
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "MANUFACTURER_NAME", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField name = new TransField();

    @JoinColumn(name = "IMAGE_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Image image;

    @Column(name = "STATUS")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean status = true;


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Manufacturer manufacturer = (Manufacturer) o;
        return Objects.equals(id, manufacturer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return Manufacturer.class.getName() + "{" + "id=" + id + '}';
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransField getName() {
        return name;
    }

    public void setName(TransField name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
