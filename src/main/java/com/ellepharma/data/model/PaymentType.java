package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * ShipmentType.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 25, 2019
 */

@Entity
@Table(name = "PAYMENT_TYPE")
public class PaymentType extends BaseEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	@Basic(optional = false)
	private Long id;

	@Basic(optional = false)
	@NotNull
	@Size(max = 4000)
	@Convert(converter = TransFieldAttConverter.class)
	@Column(name = "ATTRIBUTE_NAME", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
	private TransField name = new TransField();

	@Basic(optional = false)
	@NotNull
	@Size(max = 4000)
	@Convert(converter = TransFieldAttConverter.class)
	@Column(name = "TYPE_DESC", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
	private TransField desc = new TransField();

	@Basic(optional = false)
	@NotNull
	@Column(name = "PAYMENT_RATE", columnDefinition = "double(20,4)  DEFAULT 0.0000")
	private Double paymentRate = 0D;

	@Basic(optional = false)
	@NotNull
	@Column(name = "STATUS")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private Boolean status = true;

	@Basic(optional = false)
	@NotNull
	@Column(name = "DEFAULTS")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private Boolean defaults = false;

	@OneToMany(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<CountryCashOnDelivery> countryCashOnDeliveries = new ArrayList<>();

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		PaymentType that = (PaymentType) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "PaymentType{" + "id=" + id + '}';
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TransField getName() {
		return name;
	}

	public void setName(TransField name) {
		this.name = name;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public TransField getDesc() {
		return desc;
	}

	public void setDesc(TransField desc) {
		this.desc = desc;
	}

	/*public Double getPaymentRate() {
		return paymentRate;
	}

	public void setPaymentRate(Double paymentRate) {
		this.paymentRate = paymentRate;
	}*/

	public Boolean getDefaults() {
		return defaults;
	}

	public void setDefaults(Boolean defaults) {
		this.defaults = defaults;
	}

	public List<CountryCashOnDelivery> getCountryCashOnDeliveries() {
		return countryCashOnDeliveries;
	}

	public void setCountryCashOnDeliveries(List<CountryCashOnDelivery> countryCashOnDeliveries) {
		this.countryCashOnDeliveries = countryCashOnDeliveries;
	}

}
