/*
 * Copyright 2009-2014 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ellepharma.data.model;

import com.ellepharma.utils.Constants;
import com.ellepharma.utils.JsfUtils;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Formula;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * Manufacturer.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Jun 26, 2019
 */
@Entity
@Table(name = "ACCOUNT")
public class Account extends BaseEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3847835937208411398L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	@Basic(optional = false)
	private Long id;

	@NotNull
	@Size(max = 1000)
	@Basic(optional = false)
	@Column(name = "FIRST_NAME")
	private String firstName;

	@NotNull
	@Size(max = 1000)
	@Basic(optional = false)
	@Column(name = "LAST_NAME")
	private String lastName;

	@NotNull
	@Size(max = 1000)
	@Basic(optional = false)
	@Column(name = "EMAIL")
	@Email
	private String email;

	@JoinColumn(name = "IMAGE_ID", referencedColumnName = "ID")
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonManagedReference
	private Image image;

	@NotNull
	@Size(max = 1000)
	@Basic(optional = false)
	@Column(name = "PASSWORD")
	private String password;

	@NotNull
	@JsonBackReference
	@JoinColumn(name = "COUNTRY_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Country country;

	@NotNull
	@Size(max = 1000)
	@Basic(optional = false)
	@Column(name = "ADDRESS")
	private String address;

	@Size(max = 500)
	@Column(name = "USERNAME")
	private String username;

	@NotNull
	@Basic(optional = false)
	@Column(name = "CITY")
	private String city;

	@NotNull
	@Basic(optional = false)
	@Column(name = "STATE")
	private String state;

	@NotNull
	@Basic(optional = false)
	@Column(name = "POSTCODE")
	private String Postcode;

	@NotNull
	@Basic(optional = false)
	@Column(name = "PHONE")
	private String phone;

	@NotNull
	@Basic(optional = false)
	@Column(name = "PHONE_CODE")
	private String phoneCode;

	@Column(name = "SOCIAL_TITLE")
	@Enumerated(EnumType.STRING)
	private SocialTitle socialTitle;

	@Column(name = "GENDER")
	@Enumerated(EnumType.STRING)
	private GenderEnum genderEnum;

	@Column(name = "ACCOUNT_TYPE")
	@Enumerated(EnumType.STRING)
	private AccountType accountType;

	@Column(name = "BIRTH_DATE", columnDefinition = "date DEFAULT NULL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date birthDate;

	@Basic(optional = false)
	@NotNull
	@Column(name = "RECEIVE_OFFERS")
	private Boolean receiveOffers = false;

	@Basic(optional = false)
	@NotNull
	@Column(name = "NEWSLETTER")
	private Boolean newsletter = false;

	@Column(name = "ACTIVE")
	private Boolean active = false;

	@ManyToMany
	private List<Role> roles;

	@JoinColumn(name = "ADDRESS_BILLING", referencedColumnName = "ID")
	@OneToOne(fetch = FetchType.EAGER)
	@JsonManagedReference
	private Address addressBilling;

	@JoinColumn(name = "ADDRESS_SHIPPING", referencedColumnName = "ID")
	@OneToOne(fetch = FetchType.EAGER)
	@JsonManagedReference
	private Address addressShipping;

	@Column(name = "CODE")
	private String code = "";

	@Transient
	private String passwordConfirm;

	@Transient
	private String phoneKey;

	@Formula("CONCAT(FIRST_NAME ,' ',LAST_NAME)")
	private String fullName;

	public Account() {
		super();
		try {

			Country country = JsfUtils.getFromSessionMap(Constants.CURRENT_COUNTRY);
			if (country != null) {
				this.setPhoneCode(country.getIsoCode2());

			}
		} catch (Exception e) {

		}

	}

	public String getFullName() {
/*
        String fullName = (StringUtils.isNotEmpty(firstName) ? firstName : "") + " " + (StringUtils.isNotEmpty(lastName) ? lastName : "");
*/

		return fullName;
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Boolean getReceiveOffers() {
		return receiveOffers;
	}

	public void setReceiveOffers(Boolean receiveOffers) {
		this.receiveOffers = receiveOffers;
	}

	public Boolean getNewsletter() {
		return newsletter;
	}

	public void setNewsletter(Boolean newsletter) {
		this.newsletter = newsletter;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public SocialTitle getSocialTitle() {
		return socialTitle;
	}

	public void setSocialTitle(SocialTitle socialTitle) {
		this.socialTitle = socialTitle;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Address getAddressBilling() {
		return addressBilling;
	}

	public void setAddressBilling(Address addressBilling) {
		this.addressBilling = addressBilling;
	}

	public Address getAddressShipping() {
		return addressShipping;
	}

	public void setAddressShipping(Address addressShipping) {
		this.addressShipping = addressShipping;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostcode() {
		return Postcode;
	}

	public void setPostcode(String postcode) {
		Postcode = postcode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Account account = (Account) o;
		return Objects.equals(id, account.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	public String toString() {
		return this.getClass().getName() + "[ id=" + id + " ]";
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public GenderEnum getGenderEnum() {
		return genderEnum;
	}

	public void setGenderEnum(GenderEnum genderEnum) {
		this.genderEnum = genderEnum;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhoneKey() {
		phoneKey = "";
		try {

			if (StringUtils.isNotEmpty(getPhoneCode())) {
				phoneKey = String
						.valueOf(PhoneNumberUtil.getInstance().getCountryCodeForRegion(new Locale("ar", getPhoneCode()).getCountry()));

			}
		} catch (Exception e) {

			phoneKey = "";
		}
		return phoneKey;
	}

	public void setPhoneKey(String phoneKey) {
		this.phoneKey = phoneKey;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
