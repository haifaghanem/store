package com.ellepharma.data.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * Cart.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 29, 2019
 */

@Entity
@Table(name = "ORDER_PRODUCT")
public class OrderProduct extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @JoinColumn(name = "ITEM_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Item item;

    @Basic(optional = false)
    @NotNull
    @Column(name = "QTY")
    private Long qty;

    @Basic(optional = false)
    @NotNull
    @Column(name = "PRICE")
    private Double price;

    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL")
    private Double total;

    @Basic(optional = false)
    @Column(name = "TAX")
    private Double tax ;


    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderProduct cart = (OrderProduct) o;
        return Objects.equals(id, cart.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "OrderProduct{" + "id=" + id + '}';
    }
}
