package com.ellepharma.data.model;

import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * Ads.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Jun19 24, 2019
 */

@Entity
@Table(name = "ADS")
public class Ads extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Size(max = 250)
    @Column(name = "URL")
    private String url;

    @Column(name = "CODE")
    private String code;


    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean status = true;

    @JoinColumn(name = "IMAGE_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Image image;


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Ads category = (Ads) o;
        return id.equals(category.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Ads{" + "id=" + id + '}';
    }

}

