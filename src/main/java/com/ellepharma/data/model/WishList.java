/*
 * Copyright 2009-2014 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ellepharma.data.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * WishList.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since July 16, 2019
 */
@Entity
@Table(name = "WISH_LIST")
public class WishList extends BaseEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3847835937208411398L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	@Basic(optional = false)
	private Long id;

	@ManyToMany(fetch = FetchType.EAGER)
	@JsonBackReference
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Item> items;

	@JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID")
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonManagedReference
	private Account account;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		WishList wishList = (WishList) o;
		return Objects.equals(id, wishList.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	public String toString() {
		return this.getClass().getName() + "[ id=" + id + " ]";
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
}
