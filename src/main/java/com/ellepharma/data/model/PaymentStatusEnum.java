package com.ellepharma.data.model;

import javax.annotation.Nullable;

/**
 * OrderStatusEnum.java
 *
 * @author Saeed Walweel <ma.yaseen@ats-ware.com>
 * @since July 21, 2019
 */
public enum PaymentStatusEnum {
    TNC_1("1", "The transaction Not Complete."),
    TNC_0("0", "The transaction is unsuccessful."),
    TNC_100("100", "The transaction is successful."),
    TNC_101("101", "The transaction is missing one or more required fields."),
    TNC_102("102", "The transaction contains one or more invalid data fields."),
    TNC_104("104", "The reference code sent with this authorization request matches the reference code of another authorization request that you sent in the last 15 minutes."),
    TNC_110("110", "The transaction was only approved for a partial amount."),
    TNC_111("111", "The transaction is authorized successfully"),
    TNC_112("112", "The authorization is partially captured"),
    TNC_113("113", "The authorization is fully captured"),
    TNC_114("114", "The authorization is expired"),
    TNC_115("115", "The authorization is partially captured and the remaining expired"),
    TNC_116("116", "The authorization is fully reversed"),
    TNC_150("150", "A technical error has occurred during the transaction. Please try again."),
    TNC_151("151", "The server has timed out. Please try again."),
    TNC_152("152", "The request has not been completed. Please try again."),
    TNC_200("200", "The authorization request was approved by the card issuing bank but declined by PayTabs because it did not pass the Address Verification Service (AVS) check."),
    TNC_201("201", "An error was encountered while processing your transaction. Please contact your bank for further clarification."),
    TNC_202("202", "An expired card has been used in this transaction."),
    TNC_203("203", "The card used in this transaction has been declined. Please contact your bank for further clarification."),
    TNC_204("204", "The funds are insufficient to cover this transaction."),
    TNC_205("205", "The card used in this transaction is potentially stolen or lost."),
    TNC_207("207", "The system was unable to establish a connection with the card issuing bank. Please contact your bank for further clarification."),
    TNC_208("208", "The card used in this transaction is inactive or not authorized for card-not-present transactions. Please contact your bank for further clarification."),
    TNC_209("209", "American Express Card Identification Digits (CID) did not match."),
    TNC_210("210", "The transaction amount exceeds the maximum withdrawal amount limit."),
    TNC_211("211", "The cardholder has entered an invalid card verification number."),
    TNC_220("220", "There may be a problem with your bank account. Please contact your bank for further clarification."),
    TNC_221("221", "An error was encountered during this transaction. Please contact your bank for further clarification."),
    TNC_222("222", "Your bank account may be frozen. Please contact your bank for further clarification."),
    TNC_230("230", "The authorization request was approved by the card issuing bank but declined by PayTabs because it did not pass the card verification (CV) check."),
    TNC_231("231", "The card number is invalid."),
    TNC_232("232", "The card type is not accepted by the payment processor."),
    TNC_233("233", "This transaction is declined by the processor."),
    TNC_234("234", "An error was encountered while processing the transaction. Kindly contact PayTabs customer service for further clarification."),
    TNC_235("235", "An error was encountered while processing the transaction. Kindly contact PayTabs customer service for further clarification."),
    TNC_236("236", "An error was encountered while processing the transaction. Kindly contact PayTabs customer service for further clarification."),
    TNC_237("237", "The transaction has been cancelled and the funds reversed."),
    TNC_238("238", "The transaction is successful."),
    TNC_240("240", "The card type is invalid or doesn't correlate with the credit card number."),
    TNC_242("242", "The transaction is unsuccessful."),
    TNC_244("244", "Merchant not active"),
    TNC_250("250", "An error was encountered while processing your transaction. Please try again."),
    TNC_301("301", "An error was encountered during this transaction. Please contact your bank for further clarification."),
    TNC_305("305", "The card holder has entered an incorrect PIN."),
    TNC_306("306", "The transaction amount exceeds the maximum withdrawal amount limit."),
    TNC_307("307", "The system was unable to establish a connection with the card issuing bank. Please contact your bank for further clarification."),
    TNC_308("308", "The card number has not yet been mapped to the related bank."),
    TNC_328("328", "Sadad account is not valid. Please contact your bank."),
    TNC_330("330", "The transaction amount exceeds the maximum per transaction limit."),
    TNC_331("331", "The transaction amount exceeds the maximum per day limit."),
    TNC_333("333", "Sadad account does not exist. Please enter valid Sadad account"),
    TNC_334("334", "The transaction is rejected."),
    TNC_336("336", "Transaction has been cancelled by the Consumer"),
    TNC_337("337", "Invalid input data. Please verify the input details provided."),
    TNC_338("338", "MerchantRefNum is already Used."),
    TNC_339("339", "Sorry, Unexpected System Error, Please try again"),
    TNC_340("340", "Transaction cannot be processed"),
    TNC_341("341", "Transaction cannot be processed at this moment"),
    TNC_342("342", "Transaction failed"),
    TNC_343("343", "Sadad account password entered is incorrect. Please provide the valid password."),
    TNC_344("344", "Second level authentication credentials incorrect. Please provide valid credentials."),
    TNC_345("345", "This transaction cannot be processed due to insufficient funds in your account."),
    TNC_346("346", "Transaction timed out"),
    TNC_347("347", "Cannot proceed with checkout process."),
    TNC_348("348", "SADAD Account status is not active/inactive"),
    TNC_349("349", "Merchant Name and Merchant Id does not match"),
    TNC_350("350", "Duplicate Transaction Found"),
    TNC_360("360", "Payment is pending for the transaction."),
    TNC_361("361", "Payment reference number is expired for this transaction."),
    TNC_362("362", "Invalid mobile number is used."),
    TNC_363("363", "Customer not found."),
    TNC_401("401", "The payment processing provider, or an intermediary, refused to authorize the payment despite the details being syntactically correct."),
    TNC_402("402", "The consumer is on the GoInterpay blacklist and GoInterpay has refused to process the order."),
    TNC_403("403", "Based on the parameters of the order, GoInterpay suspects that the order is fraudulent and/or represents too high a risk to proceed with processing."),
    TNC_475("475", "Authentication for this transaction is incomplete. Please enter your 3D Secure PIN in the pop-up authentication window."),
    TNC_476("476", "The cardholder has entered an invalid 3DSecure authentication PIN."),
    TNC_481("481", "This transaction is under review and will be reversed based on your card issuing bank's policy, if its not approved within 24 hours."),
    TNC_482("482", "This transaction is under review and will be reversed based on your card issuing bank's policy, if its not approved within 24 hours."),
    TNC_483("483", "This transaction is under review and will be reversed based on your card issuing bank's policy, if its not approved within 24 hours."),
    TNC_484("484", "Transaction is declined from UnderReview state."),
    TNC_485("485", "Device FingerPrint id was missing."),
    TNC_486("486", "Transaction restricted due to risk limitations or parameters outside merchant risk setting"),
    TNC_700("700", "There is a PayTabs System request error. Please try again."),
    TNC_800("800", "There is a PayTabs System request error. Please try again."),
    TNC_801("801", "American Express cards are not accepted by the Payment Processor."),
    TNC_802("802", "The total transaction amount has exceeded the maximum transaction limit set by PayTabs. Please contact your PayTabs account manager for further clarification."),
    TNC_803("803", "The total transaction amount does not meet the minimum allowable transaction limit set by PayTabs. Please contact your PayTabs account manager for further clarification."),
    TNC_804("804", "Transaction currency is not supported, please contact your merchant"),
    TNC_805("805", "Payment is not completed. Sadad Account password is not entered/invalid."),
    TNC_806("806", "This transaction is not Verified by Visa or MasterCard Secure. Please try other card."),
    TNC_807("807", "This token has been canceled by PayTabs Operations"),
    TNC_808("808", "There seems to be a problem connecting to the bank, Kindly try again in a few moments"),
    TNC_4003("4003", "There are no transactions available."),
    ;


    private String code;
    private String desc;

    PaymentStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;

    }
    @Nullable
    public static PaymentStatusEnum fomCode(String code) {
        for (PaymentStatusEnum at : PaymentStatusEnum.values()) {
            if (at.getCode().contentEquals(code)) {
                return at;
            }
        }
        return TNC_1;
    }


    public String getCode(String code) {
        return code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
