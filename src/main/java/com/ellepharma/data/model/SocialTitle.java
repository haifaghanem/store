package com.ellepharma.data.model;

/**
 * SocialTitle.java
 *
 * @author Malek Yaseen <ma.yaseen@ats-ware.com>
 * @since Jun 25, 2019
 */
public enum SocialTitle {
	Mr,
	Mrs

}
