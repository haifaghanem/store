package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Article.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 18, 2019
 */

@Entity
@Table(name = "ARTICLE")
public class Article extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Column(name = "CODE")
    @NotNull
    private String code;

    @Basic(optional = false)
    @NotNull
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "TITLE", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField title = new TransField();

    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    @Column(name = "ENABLE_COMMENT")
    private Boolean enableComment = false;

    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    @Column(name = "ENABLE_DELETE")
    private Boolean enableDelete = true;

    @Column(name = "PUBLISH_DATE")
    @Basic(optional = false)
    @NotNull
    private Date publishDate = new Date();

    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Account account;

    @Basic(optional = false)
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "ARTICLE_DESC", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField desc = new TransField();


    @JoinColumn(name = "COVER_IMG_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    private Image cover;

    @OneToMany(fetch = FetchType.EAGER)
    @JsonBackReference
    private List<Image> images = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransField getTitle() {
        return title;
    }

    public void setTitle(TransField title) {
        this.title = title;
    }

    public Boolean getEnableComment() {
        return enableComment;
    }

    public void setEnableComment(Boolean enableComment) {
        this.enableComment = enableComment;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public TransField getDesc() {
        return desc;
    }

    public void setDesc(TransField desc) {
        this.desc = desc;
    }

    public Image getCover() {
        return cover;
    }

    public void setCover(Image cover) {
        this.cover = cover;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Boolean getEnableDelete() {
        return enableDelete;
    }

    public void setEnableDelete(Boolean enableDelete) {
        this.enableDelete = enableDelete;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
