package com.ellepharma.data.model;

import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Celebrity.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 18, 2019
 */

@Entity
@Table(name = "AVAILABLE_CELEBRITY_ITEM")
public class CelebrityItem extends BaseEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	@Basic(optional = false)
	private Long id;

	@NotNull
	@JoinColumn(name = "ITEM_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JsonBackReference
	private Item item;

	@NotNull
	@JoinColumn(name = "CELEBRITY_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JsonBackReference
	private Celebrity celebrity;

	@Basic(optional = false)
	@NotNull
	@Column(name = "COMMISSION")
	private BigDecimal commission;

	@Basic(optional = false)
	@NotNull
	@Column(name = "USE_DEFAULT_COMMISSION")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private Boolean useDefaultCommission = true;

	@Basic(optional = false)
	@NotNull
	@Column(name = "ACCEPT_BY_CELEBRITY")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private Boolean acceptByCelebrity = false;

	@Basic(optional = false)
	@NotNull
	@Column(name = "STATUS")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private Boolean status = true;

	@Column(name = "DATE_ADDED")
	private Date dateAdded;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Celebrity getCelebrity() {
		return celebrity;
	}

	public void setCelebrity(Celebrity celebrity) {
		this.celebrity = celebrity;
	}

	public BigDecimal getCommission() {
		return commission;
	}

	public void setCommission(BigDecimal commission) {
		this.commission = commission;
	}

	public Boolean getUseDefaultCommission() {
		return useDefaultCommission;
	}

	public void setUseDefaultCommission(Boolean useDefaultCommission) {
		this.useDefaultCommission = useDefaultCommission;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Boolean getAcceptByCelebrity() {
		return acceptByCelebrity;
	}

	public void setAcceptByCelebrity(Boolean acceptByCelebrity) {
		this.acceptByCelebrity = acceptByCelebrity;
	}
}
