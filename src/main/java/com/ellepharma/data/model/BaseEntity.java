package com.ellepharma.data.model;

import com.ellepharma.constant.TransField;
import com.ellepharma.utils.Utils;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.persistence.Version;

/**
 * BaseEntity.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 18, 2019
 */
@MappedSuperclass
public abstract class BaseEntity {

    /**
     * get the entity ID (Primary key)
     *
     * @return Long
     */
    public abstract Long getId();

    @Transient
    private TransField discriminator;

    @Transient
    private boolean markedForDeletion = false;

    @Column(name = "VERSION_ID", columnDefinition = "bigint(20) default 1")
    @Version
    private Long versionId;

    public TransField getDiscriminator() {
        return new TransField();
    }

    public void setDiscriminator(TransField discriminator) {
        this.discriminator = discriminator;
    }

    public boolean isMarkedForDeletion() {
        return markedForDeletion;
    }

    public void setMarkedForDeletion(boolean markedForDeletion) {
        this.markedForDeletion = markedForDeletion;
    }

    public String getCurrentLanguage() {
        String currentLanguage = null;
		/*try {
			currentLanguage = SecurityUtil.getCurrentUser().getCurrentLanguage();
		} catch (Exception e) {
			try {
				//e.printStackTrace();
				currentLanguage = UserPreferenceUtil.getDefaultLanguage();
			} catch (Exception e1) {
			}
		}*/
        return Utils.getCurrentLang().getLocale();
    }

    public Long getVersionId() {

        return versionId;
    }

    public void setVersionId(Long versionId) {
        this.versionId = versionId;
    }
}
