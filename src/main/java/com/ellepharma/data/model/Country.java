package com.ellepharma.data.model;

import com.ellepharma.constant.TransField;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * Country.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 05, 2019
 */

@Entity
@Table(name = "COUNTRY")
public class Country extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "COUNTRY_NAME_AR")
    private String countryNameAr;

    @Basic(optional = false)
    @NotNull
    @Column(name = "COUNTRY_NAME_EN")
    private String countryNameEn;

    @OneToOne
    @JoinColumn(name = "CURRENCY_ID")
    private Currency currency;

    @Column(name = "ISO_CODE_2")
    private String isoCode2;

    @Column(name = "ISO_CODE_3")
    private String isoCode3;

    public Country() {
        super();
    }

    public Country(long id, String countryNameAr, String countryNameEn, String isoCode2, String isoCode3) {
        this.id = id;
        this.countryNameAr = countryNameAr;
        this.countryNameEn = countryNameEn;
        this.isoCode2 = isoCode2;
        this.isoCode3 = isoCode3;

    }

    public TransField getDiscriminator() {
        TransField transField = new TransField();
        transField.put("ar", countryNameAr);
        transField.put("en", countryNameAr);
        return transField;

    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCountryNameAr() {
        return countryNameAr;
    }

    public void setCountryNameAr(String countryNameAr) {
        this.countryNameAr = countryNameAr;
    }

    public String getCountryNameEn() {
        return countryNameEn;
    }

    public String getIsoCode2() {
        return isoCode2;
    }

    public void setIsoCode2(String isoCode2) {
        this.isoCode2 = isoCode2;
    }

    public String getIsoCode3() {
        return isoCode3;
    }

    public void setIsoCode3(String isoCode3) {
        this.isoCode3 = isoCode3;
    }

    public void setCountryNameEn(String countryNameEn) {
        this.countryNameEn = countryNameEn;
    }

    public String getDesc() {
        if (getCurrentLanguage().equalsIgnoreCase("ar")) {
            return countryNameAr;
        } else return countryNameEn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Country country = (Country) o;
        return id.equals(country.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Country{" + "id=" + id + '}';
    }
}
