package com.ellepharma.data.model;

import com.ellepharma.utils.BooleanToIntegerAttributeConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Newsletter.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Oct 03, 2019
 */
@Entity
@Table(name = "NEWSLETTER")
public class Newsletter extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Basic(optional = false)
    @Column(name = "EMAIL")
    private String email;

    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean status = true;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
