package com.ellepharma.data.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * StockIn.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 19, 2019
 */

@Entity
@Table(name = "STOCK_IN")
public class StockIn extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @JoinColumn(name = "ITEM_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Item item;

    @Column(name="QUANTITY")
    private Double quantity;

    @Column(name="VOUCHER_ID")
    private String voucherNo;

    @Column(name = "CREATION_DATE", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
