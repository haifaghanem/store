package com.ellepharma.data.model;

import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * CountryShipment.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since June 07, 2019
 */

@Entity
@Table(name = "COUNTRY_SHIPMENT")
public class CountryShipment extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "SHIPMENT_FREE")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean shipmentFree = false;

    @Basic(optional = false)
    @NotNull
    @Column(name = "SHIPMENT_RATE", columnDefinition = "double(20,4) DEFAULT 0.0000")
    private Double shipmentRate;


    @Column(name = "SHIPMENT_RATE_EXTRA", columnDefinition = "double(20,4) DEFAULT 0.0000")
    private double shipmentRateExtra;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IS_PERCENTAGE")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean percentage = false;

    @OneToOne
    @JoinColumn(name = "COUNTRY_ID")
    private Country country;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryShipment that = (CountryShipment) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "CountryShipment{" +
                "id=" + id +
                '}';
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getShipmentFree() {
        return shipmentFree;
    }

    public void setShipmentFree(Boolean shipmentFree) {
        this.shipmentFree = shipmentFree;
    }

    public Double getShipmentRate() {
        return shipmentRate;
    }

    public void setShipmentRate(Double shipmentRate) {
        this.shipmentRate = shipmentRate;
    }

    public Boolean getPercentage() {
        return percentage;
    }

    public void setPercentage(Boolean percentage) {
        this.percentage = percentage;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Double getShipmentRateExtra() {
        return shipmentRateExtra;
    }

    public void setShipmentRateExtra(Double shipmentRateExtra) {
        this.shipmentRateExtra = shipmentRateExtra;
    }
}
