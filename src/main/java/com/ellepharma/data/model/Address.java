/*
 * Copyright 2009-2014 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ellepharma.data.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.StringJoiner;

/**
 * Address.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Jun 26, 2019
 */
@Entity
@Table(name = "ADDRESS")
public class Address extends BaseEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3847835937208411398L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	@Basic(optional = false)
	private Long id;

/*	@Basic(optional = false)
	@NotNull
	@Size(max = 4000)
	@Convert(converter = TransFieldAttConverter.class)
	@Column(name = "FIRST_NAME", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
	private TransField firstName = new TransField();

	@Basic(optional = false)
	@NotNull
	@Size(max = 4000)
	@Convert(converter = TransFieldAttConverter.class)
	@Column(name = "LAST_NAME", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
	private TransField lastName = new TransField();*/

	@NotNull
	@Size(max = 1000)
	@Basic(optional = false)
	@Column(name = "FIRST_NAME")
	private String firstName;

	@NotNull
	@Size(max = 1000)
	@Basic(optional = false)
	@Column(name = "LAST_NAME")
	private String lastName;

	@NotNull
	@Size(max = 1000)
	@Basic(optional = false)
	@Column(name = "ADDRESS")
	private String address;

	@NotNull
	@Basic(optional = false)
	@Column(name = "CITY")
	private String city;

	@NotNull
	@Basic(optional = false)
	@Column(name = "STATE")
	private String state;

	@NotNull
	@Basic(optional = false)
	@Column(name = "POSTCODE")
	private String Postcode;

	@NotNull
	@Basic(optional = false)
	@Column(name = "EMAIL")
	private String email;

	@NotNull
	@JsonBackReference
	@JoinColumn(name = "COUNTRY_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Country country;

	@Column(name = "PHONE")
	private String phone;

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostcode() {
		return Postcode;
	}

	public void setPostcode(String postcode) {
		Postcode = postcode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Address account = (Address) o;

		return id.equals(account.id);

	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Address.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.toString();
	}
}
