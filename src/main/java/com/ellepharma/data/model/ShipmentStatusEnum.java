package com.ellepharma.data.model;

/**
 * OrderStatusEnum.java
 *
 * @author Saeed Walweel <ma.yaseen@ats-ware.com>
 * @since July 21, 2019
 */
public enum ShipmentStatusEnum {
    Processing,
    Expired,
    Processed,
    Voided,
    Pending,
    Charge_back,
    Reversed,
    Refunded,
    Failed,
    Canceled_Reversal,
    Denied,
    Complete,
    Canceled,
    Shipped
}
