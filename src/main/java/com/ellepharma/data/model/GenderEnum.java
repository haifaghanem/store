package com.ellepharma.data.model;

/**
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Aug 14, 2019
 */
public enum GenderEnum {
    MALE,
    FEMALE,
}
