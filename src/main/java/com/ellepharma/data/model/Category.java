package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * Category.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 18, 2019
 */

@Entity
@Table(name = "CATEGORY")
public class Category extends BaseEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	@Basic(optional = false)
	private Long id;

	@NotNull
	@Column(name = "CODE", unique = true)
	@Basic(optional = false)
	private String code;

	@Basic(optional = false)
	@NotNull
	@Size(max = 4000)
	@Convert(converter = TransFieldAttConverter.class)
	@Column(name = "CAT_NAME", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
	private TransField name = new TransField();

	@Size(max = 4000)
	@Convert(converter = TransFieldAttConverter.class)
	@Column(name = "CAT_DESC", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
	private TransField desc = new TransField();

	@JoinColumn(name = "PARENT_CAT_ID", referencedColumnName = "ID")
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonManagedReference
	private Category parent;

	@JoinColumn(name = "IMAGE_ID", referencedColumnName = "ID")
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonManagedReference
	private Image image;

	@Basic(optional = false)
	@NotNull
	@Column(name = "STATUS")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private Boolean status = true;

	@Basic(optional = false)
	@NotNull
	@Column(name = "SHOW_IS_SLIDER")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private Boolean showInSlider = false;

	@Transient
	private String parentCode;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TransField getName() {
		return name;
	}

	public void setName(TransField name) {
		this.name = name;
	}

	public TransField getDesc() {
		return desc;
	}

	public void setDesc(TransField desc) {
		this.desc = desc;
	}

	public Category getParent() {
		return parent;
	}

	public void setParent(Category parent) {
		this.parent = parent;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Category category = (Category) o;
		return id.equals(category.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "Category{" + "id=" + id + '}';
	}

	public Boolean getShowInSlider() {
		return showInSlider;
	}

	public void setShowInSlider(Boolean showInSlider) {
		this.showInSlider = showInSlider;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}
}
