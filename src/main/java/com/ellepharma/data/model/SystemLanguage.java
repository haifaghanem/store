package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * SystemLanguage.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 18, 2019
 */
@Entity
@Table(name = "SYSTEM_LANGUAGES")
public class SystemLanguage extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Basic(optional = false)
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 4000)
	@Column(name = "LANGUAGE_DESC", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)

	@Convert(converter = TransFieldAttConverter.class)
	private TransField languageDesc = new TransField();

	@Basic(optional = false)
	@NotNull
	@Column(name = "LANGUAGE_ORDER")
	@Digits(fraction = 0, integer = 19)
	@Min(value = 1)
	private long languageOrder;

	@Basic(optional = false)
	@NotNull
	@Column(name = "SHORTCUT")

	@Size(max = 10)
	private String shortcut;

	@Basic(optional = false)
	@NotNull
	@Column(name = "DIRECTION")
	@Size(max = 3)
	private String direction;

	@Basic(optional = false)
	@NotNull
	@Column(name = "LOCALE")
	@Size(max = 5)
	private String locale;

	@Basic(optional = false)
	@NotNull
	@Column(name = "IS_LOGIN")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private boolean isLogin = false;

	@Basic(optional = false)
	@NotNull
	@Column(name = "IS_ENTRY")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private boolean isEntry = false;

	@Basic(optional = false)
	@Column(name = "IS_MANDATORY")
	@NotNull
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private boolean isMandatory = false;

	@Basic(optional = false)
	@Column(name = "IS_RTL")
	@NotNull
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private boolean isRtl = false;

	@Basic(optional = false)
	@NotNull
	@Column(name = "IS_SHOW")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private boolean isShow = false;

	public SystemLanguage() {
	}

	public SystemLanguage(Long rid) {
		this.id = rid;
	}

	public SystemLanguage(Long id, long languageOrder, String shortcut, String locale, boolean isLogin, boolean isEntry, boolean isShow) {
		this.id = id;
		this.languageOrder = languageOrder;
		this.shortcut = shortcut;
		this.locale = locale;
		this.isLogin = isLogin;
		this.isEntry = isEntry;
		this.isShow = isShow;
	}

	@Override
	public String toString() {
		return getClass().getName() + "  [id=" + id + "]";
	}

	@Override
	public Long getId() {
		return id;
	}

	public SystemLanguage setId(Long id) {
		this.id = id;
		return this;
	}

	public TransField getLanguageDesc() {
		return languageDesc;
	}

	public SystemLanguage setLanguageDesc(TransField languageDesc) {
		this.languageDesc = languageDesc;
		return this;
	}

	public long getLanguageOrder() {
		return languageOrder;
	}

	public SystemLanguage setLanguageOrder(long languageOrder) {
		this.languageOrder = languageOrder;
		return this;
	}

	public String getShortcut() {
		return shortcut;
	}

	public SystemLanguage setShortcut(String shortcut) {
		this.shortcut = shortcut;
		return this;
	}

	public String getDirection() {
		return direction;
	}

	public SystemLanguage setDirection(String direction) {
		this.direction = direction;
		return this;
	}

	public String getLocale() {
		return locale;
	}

	public SystemLanguage setLocale(String locale) {
		this.locale = locale;
		return this;
	}

	public boolean getIsLogin() {
		return isLogin;
	}

	public void setIsLogin(boolean isLogin) {
		this.isLogin = isLogin;
	}

	public boolean getIsRtl() {
		return isRtl;
	}

	public void setIsRtl(boolean isRtl) {
		this.isRtl = isRtl;
	}

	public boolean getIsEntry() {
		return isEntry;
	}

	public void setIsEntry(boolean isEntry) {
		this.isEntry = isEntry;
	}

	public boolean getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public boolean getIsShow() {
		return isShow;
	}

	public void setIsShow(boolean isShow) {
		this.isShow = isShow;
	}

	@Override
	public TransField getDiscriminator() {
		return languageDesc;
	}

}
