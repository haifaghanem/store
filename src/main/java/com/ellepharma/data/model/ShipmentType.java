package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * ShipmentType.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 22, 2019
 */

@Entity
@Table(name = "SHIPMENT_TYPE")
public class ShipmentType extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "ATTRIBUTE_NAME", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField name = new TransField();

    @Basic(optional = false)
    @NotNull
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "TYPE_DESC", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField desc = new TransField();

    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean status = false;

    @Basic(optional = true)
    @Column(name = "SHIPMENT_RATE", columnDefinition = "double(20,4) NOT NULL  DEFAULT 0.000")
    private Double shipmentRate;

    @Basic(optional = true)
    @Column(name = "IS_PERCENTAGE")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean percentage = false;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<CountryShipment> countryShipmentList = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShipmentType that = (ShipmentType) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "ShipmentType{" +
                "id=" + id +
                '}';
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransField getName() {
        return name;
    }

    public void setName(TransField name) {
        this.name = name;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public TransField getDesc() {
        return desc;
    }

    public void setDesc(TransField desc) {
        this.desc = desc;
    }

    public Double getShipmentRate() {
        return shipmentRate;
    }

    public void setShipmentRate(Double shipmentRate) {
        this.shipmentRate = shipmentRate;
    }

    public Boolean getPercentage() {
        return percentage;
    }

    public void setPercentage(Boolean percentage) {
        this.percentage = percentage;
    }

    public List<CountryShipment> getCountryShipmentList() {
        return countryShipmentList;
    }

    public void setCountryShipmentList(List<CountryShipment> countryShipmentList) {
        this.countryShipmentList = countryShipmentList;
    }
}
