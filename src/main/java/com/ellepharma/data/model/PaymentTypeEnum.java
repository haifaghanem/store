package com.ellepharma.data.model;

/**
 * OrderStatusEnum.java
 *
 * @author Saeed Walweel <ma.yaseen@ats-ware.com>
 * @since July 21, 2019
 */
public enum PaymentTypeEnum {
    COD(1L),
    VISA(2L),
    ;

    private Long id;

    PaymentTypeEnum(long id) {

        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }}
