package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Item.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 20, 2019
 */

@Entity
@Table(name = "ITEM")
public class Item extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @NotNull
    @Column(name = "CODE", unique = true)
    @Basic(optional = false)
    private String code;

    @Basic(optional = false)
    @NotNull
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "ITEM_NAME", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField name = new TransField();

    @Basic(optional = false)
    @NotNull
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "ITEM_DESC", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField desc = new TransField();

    @Basic(optional = false)
    @NotNull
    @Column(name = "PRICE")
    private Double price;

    @Basic(optional = false)
    @NotNull
    @Column(name = "LIMIT_QTY")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean limitQty = true;

    @Basic(optional = false)
    @Column(name = "QTY", columnDefinition = "bigint(20) NOT NULL DEFAULT 0")
    private Long qty = 0L;

    @Basic(optional = false)
    @NotNull
    @Column(name = "HAVE_TAX")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean haveTax = true;

    @Column(name = "TAX", columnDefinition = "double(20,4) NOT NULL DEFAULT 0.0000")
    private double tax;

    @Transient
    private Long sumQtyItem;

    @Basic(optional = false)
    @NotNull
    @Column(name = "HAVE_DISCOUNT")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean haveDiscount = true;

    @Column(name = "DISCOUNT", columnDefinition = "double(20,4) NOT NULL DEFAULT 0.0000")
    private double discount;

    @Column(name = "WEIGHT", columnDefinition = "double(20,4) NOT NULL DEFAULT 0.0000")
    private double weight;

    @Column(name = "DEFAULT_COMMISSION", columnDefinition = "double(20,4) NOT NULL DEFAULT 0.0000")
    private Double defaultCommission=0D;

    @Column(name = "DATE_ADDED")
    private Date dateAdded;

    @Column(name = "DATE_AVAILABLE")
    private Date dateAvailable;

    @Column(name = "DATE_MODIFIED")
    private Date dateModified;

    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean status = true;

    @Basic(optional = false)
    @NotNull
    @Column(name = "NEW")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean newItem = false;

    @Column(name = "ENABLE_RETURN")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean enableReturn = false;

    @Column(name = "ENABLE_REPLACE")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean enableReplace = false;

    @JoinColumn(name = "MANUFACTURER_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Manufacturer manufacturer;

    @JoinColumn(name = "VENDOR_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Account vendor;

    @Column(name = "BARCODE_STRING")
    private String barcodeString;

    @Column(name = "BARCODE_S")
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> barcodeList = new ArrayList<String>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonBackReference
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Image> image;

    @OneToMany(fetch = FetchType.EAGER)
    @JsonBackReference
    @Fetch(value = FetchMode.SUBSELECT)
    private List<AttributeProduct> attributeProduct;

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonBackReference
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Category> categories;


    @Transient
    private String barcode;

    @Transient
    private String catCode;
    @Transient
    private String categoryString;
    @Transient
    private String categoryCodesString;

    @Transient
    private Double finalPrice;
    @Transient
    private String brandCode;

    public Item() {
        dateAdded = new Date();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public TransField getName() {
        return name;
    }

    public void setName(TransField name) {
        this.name = name;
    }

    public TransField getDesc() {
        return desc;
    }

    public void setDesc(TransField desc) {
        this.desc = desc;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getLimitQty() {
        return limitQty;
    }

    public void setLimitQty(Boolean limitQty) {
        this.limitQty = limitQty;
    }

    public Long getQty() {
        if(qty==null){
            qty=0L;
        }
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public Boolean getHaveTax() {

        return haveTax;
    }

    public void setHaveTax(Boolean haveTax) {
        this.haveTax = haveTax;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Boolean getHaveDiscount() {
        if (discount == 0) return false;
        return haveDiscount;
    }

    public void setHaveDiscount(Boolean haveDiscount) {
        this.haveDiscount = haveDiscount;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public List<String> getBarcodeList() {
        return barcodeList;
    }

    public void setBarcodeList(List<String> barcodeList) {

        this.barcodeList = barcodeList;
    }

    public Double getDefaultCommission() {
        return defaultCommission;
    }

    public void setDefaultCommission(Double defaultCommission) {
        this.defaultCommission = defaultCommission;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Date getDateAvailable() {
        return dateAvailable;
    }

    public void setDateAvailable(Date dateAvailable) {
        this.dateAvailable = dateAvailable;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getEnableReturn() {
        return enableReturn;
    }

    public void setEnableReturn(Boolean enableReturn) {
        this.enableReturn = enableReturn;
    }

    public Boolean getEnableReplace() {
        return enableReplace;
    }

    public void setEnableReplace(Boolean enableReplace) {
        this.enableReplace = enableReplace;
    }

    public List<Image> getImage() {
        return image;
    }

    public void setImage(List<Image> image) {
        this.image = image;
    }

    public List<AttributeProduct> getAttributeProduct() {
        return attributeProduct;
    }

    public void setAttributeProduct(List<AttributeProduct> attributeProduct) {
        this.attributeProduct = attributeProduct;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Item item = (Item) o;
        return Objects.equals(id, item.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return Item.class.getName() + "{" + "id=" + id + '}';
    }

    public String getBarcode() {
        if (CollectionUtils.isNotEmpty(barcodeList)) {

            return barcodeList.stream()
                    .map(n -> n)
                    .collect(Collectors.joining(" , "));
        }
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCategoryString() {

        if (CollectionUtils.isNotEmpty(categories)) {

            return categories.stream()
                    .map(cat -> cat.getName().get(getCurrentLanguage()))
                    .collect(Collectors.joining(" , "));
        }
        return categoryString;
    }

    public void setCategoryString(String categoryString) {
        this.categoryString = categoryString;
    }

    public Boolean getNewItem() {
        return newItem;
    }

    public void setNewItem(Boolean newItem) {
        this.newItem = newItem;
    }

    public Double getFinalPrice() {
        if (haveDiscount && discount > 0) {

            return price - (price * (discount) / (new Double(100)));
        }
        return price;
    }

    public String getCatCode() {
        return catCode;
    }

    public void setCatCode(String catCode) {
        this.catCode = catCode;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public void setBrandCode(String brandCode) {

        this.brandCode = brandCode;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getBarcodeString() {
        return barcodeString;
    }

    public void setBarcodeString(String barcodeString) {
        this.barcodeString = barcodeString;
    }

    public String getCategoryCodesString() {

        if (CollectionUtils.isNotEmpty(categories)) {

            return categories.stream()
                    .map(cat -> cat.getCode())
                    .collect(Collectors.joining(","));
        }
        return "";


    }

    public void setCategoryCodesString(String categoryCodesString) {
        this.categoryCodesString = categoryCodesString;
    }

    public Long getSumQtyItem() {
        return sumQtyItem;
    }

    public void setSumQtyItem(Long sumQtyItem) {
        this.sumQtyItem = sumQtyItem;
    }

    public Account getVendor() {
        return vendor;
    }

    public void setVendor(Account vendor) {
        this.vendor = vendor;
    }
}
