package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * Menu.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 24, 2019
 */

@Entity
@Table(name = "SLIDER")
public class Slider extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "SLIDER_TITLE", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField title = new TransField();

    @Size(max = 250)
    @Column(name = "URL")
    private String url;

    @Column(name = "PRIORITY_ORDER")
    @Digits(fraction = 0, integer = 19)
    @Min(value = 1)
    private Long priorityOrder = 0L;

    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_BUTTON")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean showButton = true;

    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean status = true;

    @JoinColumn(name = "IMAGE_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Image image;


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransField getTitle() {
        return title;
    }

    public void setTitle(TransField title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getShowButton() {
        return showButton;
    }

    public void setShowButton(Boolean showButton) {
        this.showButton = showButton;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Long getPriorityOrder() {
        return priorityOrder;
    }

    public void setPriorityOrder(Long priorityOrder) {
        this.priorityOrder = priorityOrder;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Slider category = (Slider) o;
        return id.equals(category.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Menu{" + "id=" + id + '}';
    }

}

