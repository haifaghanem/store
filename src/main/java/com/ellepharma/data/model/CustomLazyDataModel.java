package com.ellepharma.data.model;

import com.ellepharma.service.BaseService;
import org.apache.commons.collections4.CollectionUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * CustomLazyDataModel.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 23, 2019
 */
public class CustomLazyDataModel<T extends BaseEntity> extends LazyDataModel<T> {

	private transient BaseService service;

	private static final SortOrder DEFAULT_SORT_ORDER = SortOrder.DESCENDING;

	private static final String DEFAULT_SORT_FIELD = "id";

	private String nameMethod;
	private List<Object> csutomArgs;

	private final String defaultMethod = "findAllLazy";

	List<T> allLoadedData = new ArrayList<>();

	public CustomLazyDataModel(BaseService service) {
		nameMethod = defaultMethod;
		this.service = service;

	}

	public CustomLazyDataModel(BaseService service, String nameMethod,List<Object> csutomArgs) {
		this.nameMethod = nameMethod;
		this.service = service;
		this.csutomArgs = csutomArgs;
	}

	@Override
	public List<T> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		Sort sort = new Sort(getDirection(sortOrder != null ? sortOrder : DEFAULT_SORT_ORDER),
				sortField != null ? sortField : DEFAULT_SORT_FIELD);
		return filterAndSort(first, pageSize, filters, sort);
	}

	private List<T> filterAndSort(int first, int pageSize, Map<String, Object> filters, Sort sort) {

		Page<T> page = getAllLazy(first, pageSize, sort, filters);
		if (page != null) {
			this.setRowCount(((Number) page.getTotalElements()).intValue());
			allLoadedData.addAll(page.getContent());
			this.setWrappedData(allLoadedData);
			return page.getContent();
		} else {
			this.setRowCount(0);
			allLoadedData.clear();
			this.setWrappedData(allLoadedData);
			return allLoadedData;
		}
	}

	private Page<T> getAllLazy(int first, int pageSize, Sort sort, Map<String, Object> filtersMap) {
	//	List<Object> csutomArgs = filtersMap.entrySet().stream()
	//										.filter(stringObjectEntry -> stringObjectEntry.getKey().contentEquals("$$"))
	//										.map(s -> s.getValue()).collect(Collectors.toList());
		try {
			if (CollectionUtils.isNotEmpty(csutomArgs)) {

				Method main = Arrays.stream(service.getClass().getMethods()).filter(method -> method.getName().equals(nameMethod))
									.findFirst()
									.get();

				return (Page<T>) main.invoke(service, filtersMap, new PageRequest(first / pageSize, pageSize, sort), csutomArgs.toArray()[0]);
			}

			Method main = Arrays.stream(service.getClass().getMethods()).filter(method -> method.getName().equals(nameMethod))
								.findFirst()
								.get();
			return (Page<T>) main.invoke(service, filtersMap, new PageRequest(first / pageSize, pageSize, sort));
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public List<T> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
		return null;
		//TODO Implement this

	}

	@Override
	public T getRowData(String rowKey) {
		Long rid = Long.valueOf(rowKey);

		for (T res : allLoadedData) {
			if (res.getId().equals(rid)) {
				return res;
			}
		}
		return null;
	}

	private static Sort.Direction getDirection(SortOrder order) {
		switch (order) {
			case ASCENDING:
				return Sort.Direction.ASC;
			case DESCENDING:
				return Sort.Direction.DESC;
			case UNSORTED:
			default:
				return null;
		}
	}

	public String getNameMethod() {
		return nameMethod;
	}

	public void setNameMethod(String nameMethod) {
		this.nameMethod = nameMethod;
	}

}