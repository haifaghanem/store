package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Celebrity.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 18, 2019
 */

@Entity
@Table(name = "CELEBRITY")
public class Celebrity extends BaseEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	@Basic(optional = false)
	private Long id;

	@Basic(optional = false)
	@NotNull
	@Size(max = 4000)
	@Convert(converter = TransFieldAttConverter.class)
	@Column(name = "CELEBRITY_NAME", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
	private TransField name = new TransField();

	@JoinColumn(name = "IMAGE_ID", referencedColumnName = "ID")
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonManagedReference
	private Image image;

	@Basic(optional = false)
	@NotNull
	@Column(name = "STATUS")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private Boolean status = true;



	@OneToMany
	@JsonBackReference
	private List<CelebrityItem> items = new ArrayList<>();

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TransField getName() {
		return name;
	}

	public void setName(TransField name) {
		this.name = name;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public List<CelebrityItem> getItems() {
		return items;
	}

	public void setItems(List<CelebrityItem> items) {
		this.items = items;
	}


}
