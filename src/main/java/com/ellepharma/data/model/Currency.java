package com.ellepharma.data.model;

import com.ellepharma.utils.BooleanToIntegerAttributeConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * Currency.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 05, 2019
 */

@Entity
@Table(name = "CURRENCY")
public class Currency extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE", unique = true)
    private String currencyCode;

    @NotNull
    @Basic(optional = false)
    @Column(name = "VALUE", precision = 19, scale = 3, columnDefinition = "double(20,3) NOT NULL  DEFAULT 0.0  ")
    private Double value;

    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean status = true;

    public Currency() {

    }

    public Currency(String currencyCode, Object value) {
        this.currencyCode = currencyCode;
        if (value instanceof Integer) {
            this.value = Double.parseDouble(String.valueOf(value));
        } else if (value instanceof Double) {
            this.value = (Double) value;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Currency currency = (Currency) o;
        return id.equals(currency.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Currency{" + "id=" + id + '}';
    }
}
