package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Menu.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 18, 2019
 */

@Entity
@Table(name = "MENU")
public class Menu extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "MENU_NAME", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField name = new TransField();

    @JoinColumn(name = "PARENT_MENU_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Menu parent;

    @Size(max = 250)
    @Column(name = "URL")
    private String url = "";

    @Column(name = "PRIORITY_ORDER")
    @Digits(fraction = 0, integer = 19)
    @Min(value = 1)
    private Long priorityOrder;

    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean status = true;

    @Transient
    private List<Menu> childrenList;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransField getName() {
        return name;
    }

    public void setName(TransField name) {
        this.name = name;
    }

    public Menu getParent() {
        return parent;
    }

    public void setParent(Menu parent) {
        this.parent = parent;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Menu category = (Menu) o;
        return id.equals(category.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Menu{" + "id=" + id + '}';
    }

    public List<Menu> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<Menu> childrenList) {
        this.childrenList = childrenList;
    }

    public Long getPriorityOrder() {
        return priorityOrder;
    }

    public void setPriorityOrder(Long priorityOrder) {
        this.priorityOrder = priorityOrder;
    }
}
