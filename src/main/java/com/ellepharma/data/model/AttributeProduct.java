package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.TransFieldAttConverter;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Category.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 20, 2019
 */

@Entity
@Table(name = "ATTRIBUTE_PRODUCT")
public class AttributeProduct extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @NotNull
    @JoinColumn(name = "ATTRIBUTE_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JsonManagedReference
    private Attribute attribute;


    @Basic(optional = false)
    @NotNull
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "ATTRIBUTE_VALUE", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField value = new TransField();

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public TransField getValue() {
        return value;
    }

    public void setValue(TransField value) {
        this.value = value;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
