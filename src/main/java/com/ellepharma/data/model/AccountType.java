package com.ellepharma.data.model;

public enum AccountType {

	CUSTOMER,
	CELEBRATED,
	ADMIN,
	VENDOR

}
