package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Coupon.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 28, 2019
 */

@Entity
@Table(name = "COUPON")
public class Coupon extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "COUPON_NUMBER")
    private String couponNumber;

    @Column(name = "VALID_TO_DATE")
    @Basic(optional = false)
    @NotNull
    private Date validToDate;

    @Basic(optional = false)
    @NotNull
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "ATTRIBUTE_NAME", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField name = new TransField();

    @Basic(optional = false)
    @NotNull
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "TYPE_DESC", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField desc = new TransField();


    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean status = true;

    @Basic(optional = false)
    @NotNull
    @Column(name = "DISCOUNT")
    private Double discount;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IS_PERCENTAGE")
    @Convert(converter = BooleanToIntegerAttributeConverter.class)
    private Boolean percentage = false;


    @JoinColumn(name = "CELEBRITY_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    @JsonBackReference
    private Account celebrity;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransField getName() {
        return name;
    }

    public void setName(TransField name) {
        this.name = name;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public TransField getDesc() {
        return desc;
    }

    public void setDesc(TransField desc) {
        this.desc = desc;
    }

    public String getCouponNumber() {
        return couponNumber;
    }

    public void setCouponNumber(String couponNumber) {
        this.couponNumber = couponNumber;
    }

    public Date getValidToDate() {
        return validToDate;
    }

    public void setValidToDate(Date validToDate) {
        this.validToDate = validToDate;
    }


    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Boolean getPercentage() {
        return percentage;
    }

    public void setPercentage(Boolean percentage) {
        this.percentage = percentage;
    }

    public Account getCelebrity() {
        return celebrity;
    }

    public void setCelebrity(Account celebrity) {
        this.celebrity = celebrity;
    }
}
