package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.TransFieldAttConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * OrderStatus.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 29, 2019
 */

@Entity
@Table(name = "ORDER_STATUS")
public class OrderStatus extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(max = 4000)
    @Convert(converter = TransFieldAttConverter.class)
    @Column(name = "NAME", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private TransField name = new TransField();


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransField getName() {
        return name;
    }

    public void setName(TransField name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderStatus status = (OrderStatus) o;
        return Objects.equals(id, status.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "OrderStatus{" +
                "id=" + id +
                '}';
    }
}
