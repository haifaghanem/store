package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Cart.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 29, 2019
 */

@Entity
@Table(name = "ORDERS")
public class Order extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Basic(optional = false)
    private Long id;

    @Column(name = "INVOICE_NO")
    private Long invoiceNo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL")
    private Double total;

    @NotNull
    @Column(name = "IP")
    @Basic(optional = false)
    private String ip;

    @Column(name = "COMMENTS")
    private String comment;

    @Column(name = "CREATION_DATE", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate = new Date();

    @Column(name = "DATE_MODIFIED", columnDefinition = "TIMESTAMP", insertable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate = new Date();

    @OneToMany(fetch = FetchType.EAGER)
    @JsonBackReference
    @Fetch(value = FetchMode.JOIN)
    private List<OrderProduct> productList;

    @JoinColumn(name = "CURRENCY_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Currency currency;

    @JoinColumn(name = "COUPON_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Coupon coupon;

    @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Account account;

    @JoinColumn(name = "PAYMENT_TYPE_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private PaymentType paymentType;

    @Basic(optional = false)
    @NotNull
    @Column(name = "CURRENCY_VALUE")
    private Double currencyValue;

    @NotNull
    @Column(name = "LANGUAGE_ID")
    @Basic(optional = false)
    private String languageCode;

    @Column(name = "TRACKING")
    private String tracking;

    @Size(max = 4000)
    @Column(name = "PAY_TAB_DTO_JSON", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
    private String payTabDTOJson;

    @Column(name = "PAYMENT_ID")
    private String paymentId;

    @Column(name = "DISCOUNT", columnDefinition = "double(20,4) NOT NULL  DEFAULT 0.000")
    private double discount;

    @Column(name = "SHIPPING_FEE", columnDefinition = "double(20,4) NOT NULL  DEFAULT 0.000")
    private double shippingFee;

    @Column(name = "PAYMENT_FEE", columnDefinition = "double(20,4) NOT NULL  DEFAULT 0.000")
    private double paymentFee;

    @Column(name = "SHIPMENT_STATUS")
    @Enumerated(EnumType.STRING)
    private ShipmentStatusEnum shipmentStatus;

    @Column(name = "PAYMENT_STATUS")
    @Enumerated(EnumType.STRING)
    private PaymentStatusEnum paymentStatus;

    @Column(name = "EMAIL_SENT", columnDefinition = "bit NOT NULL  DEFAULT 0")
    private boolean emailSent;

    @Column(name = "ARAMEX_SENT", columnDefinition = "bit NOT NULL  DEFAULT 0")
    private boolean aramexSent;

    @Column(name = "EXCEPTION", columnDefinition = "TEXT")
    private String exception;

    @Column(name = "ARAMEXR_ESPONSE", columnDefinition = "TEXT")
    private String aramexResponse;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Order cart = (Order) o;
        return Objects.equals(id, cart.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Cart{" +
                "id=" + id +
                '}';
    }

    public Long getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(Long invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<OrderProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<OrderProduct> productList) {
        this.productList = productList;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Double getCurrencyValue() {
        return currencyValue;
    }

    public void setCurrencyValue(Double currencyValue) {
        this.currencyValue = currencyValue;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getTracking() {
        return tracking;
    }

    public void setTracking(String tracking) {
        this.tracking = tracking;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public void setPayTabDTOJson(String payTabDTOJson) {

        this.payTabDTOJson = payTabDTOJson;
    }

    public String getPayTabDTOJson() {
        return payTabDTOJson;
    }

    public void setPaymentId(String paymentId) {

        this.paymentId = paymentId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setDiscount(double discount) {

        this.discount = discount;
    }

    public double getDiscount() {
        return discount;
    }

    public ShipmentStatusEnum getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(ShipmentStatusEnum shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    public PaymentStatusEnum getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatusEnum paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public double getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(double shippingFee) {
        this.shippingFee = shippingFee;
    }

    public boolean isEmailSent() {
        return emailSent;
    }

    public void setEmailSent(boolean emailSent) {
        this.emailSent = emailSent;
    }

    public boolean isAramexSent() {
        return aramexSent;
    }

    public void setAramexSent(boolean aramexSent) {
        this.aramexSent = aramexSent;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public double getPaymentFee() {
        return paymentFee;
    }

    public void setPaymentFee(double paymentFee) {
        this.paymentFee = paymentFee;
    }

    public void setAramexResponse(String aramexResponse) {

        this.aramexResponse = aramexResponse;
    }

    public String getAramexResponse() {
        return aramexResponse;
    }
}
