package com.ellepharma.data.model;

import com.ellepharma.constant.GeneralConstants;
import com.ellepharma.constant.TransField;
import com.ellepharma.utils.BooleanToIntegerAttributeConverter;
import com.ellepharma.utils.TransFieldAttConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Config.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Mey 08, 2019
 */

@Entity
@Table(name = "CONFIG")
public class Config extends BaseEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	@Basic(optional = false)
	private Long id;

	@NotNull
	@Column(name = "NAME", unique = true)
	@Basic(optional = false)
	private String name;

	@Basic(optional = false)
	@NotNull
	@Size(max = 4000)
	@Convert(converter = TransFieldAttConverter.class)
	@Column(name = "VALUE", length = GeneralConstants.TRANS_FIELD_COLUMN_LENGTH)
	private TransField value = new TransField();

	@Column(name = "ORIGINAL")
	@Convert(converter = BooleanToIntegerAttributeConverter.class)
	private Boolean original = true;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TransField getValue() {
		return value;
	}

	public void setValue(TransField value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Config{" +
				"id=" + id +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Config config = (Config) o;

		return id != null ? id.equals(config.id) : config.id == null;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	public Boolean getOriginal() {
		return original;
	}

	public void setOriginal(Boolean original) {
		this.original = original;
	}
}
