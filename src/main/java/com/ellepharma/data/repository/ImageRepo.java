package com.ellepharma.data.repository;

import com.ellepharma.data.model.Image;

/**
 * ImageRepo.java
 *
 * @author Malek Yaseen <ma.yaseen@ats-ware.com>
 * @since Apr 24, 2019
 */
public interface ImageRepo extends BaseRepo<Image> {

    Image findByName(String name);


}
