package com.ellepharma.data.repository;

import com.ellepharma.data.model.Country;
import com.ellepharma.data.model.PaymentType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PaymentTypeRepo.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 25, 2019
 */

@Repository
public interface PaymentTypeRepo extends BaseRepo<PaymentType> {

	List<PaymentType> findAllByStatusTrue();

	@Query("SELECT type from PaymentType type left join type.countryCashOnDeliveries cc  where cc.country =:country or type.countryCashOnDeliveries is empty  and type.status=true")
	List<PaymentType> findAllByCountry(@Param("country") Country country);
}