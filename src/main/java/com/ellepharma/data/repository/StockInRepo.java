package com.ellepharma.data.repository;

import com.ellepharma.data.model.StockIn;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * StockInRepo.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 19, 2019
 */

@Repository
public interface StockInRepo extends BaseRepo<StockIn> {

	@Query(value = "select  COALESCE(sum(s.quantity) ,0) as stockQty from StockIn s where s.item.code = :code")
	Long getStockInQuantityByItemCode(@Param("code") String code);

	@Query(value = "select COALESCE(sum(s.quantity) ,0) as stockQty from StockIn s where s.item.id = :id")
	Long getStockInQuantityByItemId(@Param("id") Long itemId);

	StockIn findByVoucherNoAndItem_Code(String voucherNo, String CodeNumber);

	List<StockIn> findByVoucherNo(String voucherNo);

	@Query(value = "select distinct s.voucherNo  from StockIn s")
	List<String> findAllVoucherNo();

	@Query(value = "select distinct s.voucherNo  from StockIn s where s.item.id = :id")
	List<String> findAllVoucherNo(@Param("id") Long id );

	//    List<StockIn> findAllByStatusTrueAndCategoriesIdIn(Pageable pageable, @Param("code") String code);

}