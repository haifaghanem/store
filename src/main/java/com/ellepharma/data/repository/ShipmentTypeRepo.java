package com.ellepharma.data.repository;

import com.ellepharma.data.model.ShipmentType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * ShipmentTypeRepo.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 22, 2019
 */

@Repository
@Transactional
public interface ShipmentTypeRepo extends BaseRepo<ShipmentType> {


    ShipmentType findByStatusTrue();

    @Query("update ShipmentType s set  s.status = false")
    @Modifying
    void updateAllFalse();

    @Query("update ShipmentType s set s.status=true where s.id=:id")
    @Modifying
    void updateStatusTrueById(@Param("id") Long id);


}