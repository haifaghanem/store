package com.ellepharma.data.repository;

import com.ellepharma.data.dto.OrderReportFilterDTO;
import com.ellepharma.data.dto.ReportItemDto;
import com.ellepharma.data.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * CouponRepo.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 28, 2019
 */

@Repository
public interface OrderRepo extends BaseRepo<Order> {

	List<Order> findByAccount(Account account);

	@Transactional
	List<Order> findByPaymentStatusAndPaymentType(PaymentStatusEnum paymentStatusEnum, PaymentType paymentType);

	@Transactional
	@Query("SELECT ord.paymentId FROM Order ord left join ord.paymentType c where  ord.paymentStatus='TNC_1' and c.id=2 and ord.paymentId IS NOT NULL and ord.creationDate <:date")
	List<String> findPaymentIDByPaymentStatusAndPaymentType(@Param("date") Date date);

	@Transactional
	@Query("SELECT ord.paymentId FROM Order ord left join ord.paymentType c where  ord.paymentStatus<>'TNC_100' and c.id=2 and ord.paymentId IS NOT NULL and ord.creationDate >:date")
	List<String> findPaymentIDByPaymentStatusNo100AndPaymentType(@Param("date") Date date);

	@Transactional
	Order findByPaymentId(String id);

	@Query("SELECT ord.paymentId FROM Order ord where ord.id=:id")
	String findPaymentIdById(@Param("id") Long id);

	@Transactional
	@Modifying
	@Query("UPDATE Order SET  paymentStatus  = :paymentStatus, shipmentStatus = :shipmentStatus ,invoiceNo =:invoiceNo  WHERE paymentId = :paymentId")
	void updatePaymentStatusAndShipmentStatusByPaymentId(@Param("paymentStatus") PaymentStatusEnum paymentStatusEnum,
			@Param("shipmentStatus") ShipmentStatusEnum shipmentStatusEnum,
			@Param("invoiceNo") Long invoiceNo,
			@Param("paymentId") String paymentId);

	@Transactional
	@Modifying
	@Query("UPDATE Order SET  paymentStatus  = :paymentStatus, shipmentStatus = :shipmentStatus ,invoiceNo =:invoiceNo  WHERE id = :orderId")
	void updatePaymentStatusAndShipmentStatusById(@Param("paymentStatus") PaymentStatusEnum paymentStatusEnum,
			@Param("shipmentStatus") ShipmentStatusEnum shipmentStatusEnum,
			@Param("invoiceNo") Long invoiceNo,
			@Param("orderId") Long orderId);

	@Query(value = "select count(ord) from Order ord where  ord.paymentStatus='TNC_100' and ord.aramexSent=false")
	Long findPendingAramexOrder();

	@Query(value = "select count(ord) from Order ord where  ord.paymentStatus='TNC_100' and ord.emailSent=false")
	Long findPendingEmailOrder();

	Order findByIdAndAccount(Long id, Account account);

	@Query("select new com.ellepharma.data.dto.ReportItemDto(i,SUM(os.qty) ) from Order  o left join o.productList os left join os.item i "
				   +
				   "where o.paymentStatus='TNC_100'" +
				   " AND (:#{#filterParam.fromDate} is null or o.creationDate >= :#{#filterParam.fromDate} )" +
				   " AND (:#{#filterParam.status} is null or i.status= :#{#filterParam.status} )" +
				   " AND (:#{#filterParam.toDate} is null or o.creationDate <= :#{#filterParam.toDate} )" +
				   " group by  i.id ")
	List<ReportItemDto> sellerReport(@Param("filterParam") OrderReportFilterDTO orderReportFilterDTO);

	@Query(value = "select o from Order o   " +
			" left join o.productList product " +
			" left join o.account account " +
			" left join account.country country " +
			" left join product.item.categories category" +
			" left join category.parent pc " +
			" WHERE (:#{#filterParam.orderNumber} is null or o.id = :#{#filterParam.orderNumber} )" +
			" AND (:#{#filterParam.fromTotal} is null or o.total >= :#{#filterParam.fromTotal} )" +
			" AND (:#{#filterParam.toTotal} is null or o.total <= :#{#filterParam.toTotal} )" +
			" AND (:#{#filterParam.fromDate} is null or o.creationDate >= :#{#filterParam.fromDate} )" +
			" AND (:#{#filterParam.toDate} is null or o.creationDate <= :#{#filterParam.toDate} )" +
			" AND (:#{#filterParam.gender} is null or o.account.genderEnum = :#{#filterParam.gender} )" +
			" AND (:#{#filterParam.paymentStatus} is null or o.paymentStatus = :#{#filterParam.paymentStatus} )" +
			" AND (:#{#filterParam.shipmentStatus} is null or o.shipmentStatus = :#{#filterParam.shipmentStatus} )" +
			" AND (:#{#filterParam.paymentType} is null or o.paymentType = :#{#filterParam.paymentType} )" +
			" AND (:#{#filterParam.coupon} is null or o.coupon = :#{#filterParam.coupon} )" +
			" AND (:#{#filterParam.countryId} is null or country.id = :#{#filterParam.countryId} )" +
			" AND (:#{#filterParam.aramexSent} is null or o.aramexSent = :#{#filterParam.aramexSent} )" +
			" AND (:#{#filterParam.emailSent} is null or o.emailSent = :#{#filterParam.emailSent} )" +
			" group by o.id")
	Page<Order> findAllOrderReport(@Param("filterParam") OrderReportFilterDTO filterParam, Pageable pageable);

	@Query(value = "select o from Order o   " +
			" left join o.productList product " +
			" WHERE (:#{#filterParam.orderNumber} is null or o.id = :#{#filterParam.orderNumber} )" +
			" AND (:#{#filterParam.fromDate} is null or o.creationDate >= :#{#filterParam.fromDate} )" +
			" AND (:#{#filterParam.toDate} is null or o.creationDate <= :#{#filterParam.toDate} )" +
			" AND (:#{#filterParam.paymentStatus} is null or o.paymentStatus = :#{#filterParam.paymentStatus} )" +
			" AND (:#{#filterParam.shipmentStatus} is null or o.shipmentStatus = :#{#filterParam.shipmentStatus} )" +
			" AND (:#{#filterParam.aramexSent} is null or o.aramexSent = :#{#filterParam.aramexSent} )" +
			" group by o.id")
	List<Order> findAllShipmentOrderReport(@Param("filterParam") OrderReportFilterDTO filterParam);

	@Query(value = "select sum (o.shippingFee/o.currencyValue) from Order o   " +
			" WHERE (:#{#filterParam.fromDate} is null or o.creationDate >= :#{#filterParam.fromDate} )" +
			" AND (:#{#filterParam.toDate} is null or o.creationDate <= :#{#filterParam.toDate} )" +
			" AND (:#{#filterParam.shipmentStatus} is null or o.shipmentStatus = :#{#filterParam.shipmentStatus} )" +
			" AND (:#{#filterParam.aramexSent} is null or o.aramexSent = :#{#filterParam.aramexSent} )" +
			" AND (:#{#filterParam.emailSent} is null or o.emailSent = :#{#filterParam.emailSent} )")
	Double sumShipmentOrderReport(@Param("filterParam") OrderReportFilterDTO filterParam);

	@Query(value = "select  COALESCE(sum(o.qty)  ,0) as orderQty from Order ord left join ord.productList o where o.item.code = :code and ord.paymentStatus='TNC_100'")
	Long getOrderProductQuantityByItemCode(@Param("code") String code);

	@Query(value = "select COALESCE(sum(o.qty)  ,0) as orderQty from Order ord left join ord.productList o where o.item.id = :id and ord.paymentStatus='TNC_100'")
	Long getOrderProductQuantityByItemId(@Param("id") Long itemId);

	@Query("SELECT coalesce(max(ord.invoiceNo), 0) FROM Order ord where  ord.paymentStatus='TNC_100'")
	Long findMaxInvoice();

	@Query("SELECT ord FROM Order ord left join  ord.coupon c where  ord.paymentStatus='TNC_100' and c.couponNumber=:code")
	Page<Order> findByCoupon_CouponNumber(@Param("code") String code, Pageable pageable);

	@Query("select ord.id from Order ord left join ord.productList o where o.item.id = :itemId and ord.paymentStatus='TNC_100'")
	List<Long> findOrderIds(@Param("itemId") Long itemId);


/*    @Query(value = "SELECT COALESCE(sum((ord.total - ord.discount + ord.shippingFee + ord.paymentFee)/ord.currencyValue) ,0) FROM Order ord left join  ord.coupon c where  ord.paymentStatus='TNC_100' and c.couponNumber=:code")
    Double sumOrdersByCouponCode(@Param("code") String code);*/

/*    @Query(value = "SELECT COALESCE(sum((ord.total - ord.discount)/ord.currencyValue) ,0) FROM Order ord left join  ord.coupon c where  ord.paymentStatus='TNC_100' and c.couponNumber=:code")
    Double sumItemOrdersByCouponCode(@Param("code") String code);*/

	@Query("SELECT ord FROM Order ord left join  ord.coupon c where  ord.paymentStatus='TNC_100' and c.couponNumber=:code and ord.creationDate >=:from and ord.creationDate <=:to")
	Page<Order> findByCoupon_CouponNumber(@Param("code") String code, @Param("from") Date from, @Param("to") Date to, Pageable pageable);

	@Query("SELECT it FROM Order ord left join ord.productList pr left join pr.item it left join  ord.coupon c where  ord.paymentStatus='TNC_100' and c.couponNumber=:code and ord.creationDate >=:from and ord.creationDate <=:to")
	Page<Item> findItemsInOrderByCoupon(@Param("code") String code, @Param("from") Date from, @Param("to") Date to, Pageable pageable);

	@Query(value = "SELECT COALESCE(sum( ( ( (pr.total /ord.currencyValue) -( ( (pr.total/ord.currencyValue) * c.discount)/100) ) * (it.defaultCommission) /100)) ,0) FROM Order ord  left join ord.productList pr left join pr.item it left join   ord.coupon c where  ord.paymentStatus='TNC_100' and c.couponNumber=:code")
	Double totalCommissions(@Param("code") String code);

	@Query(value = "SELECT COALESCE(sum( (ord.total-ord.discount +ord.shippingFee +ord.paymentFee) /ord.currencyValue) ,0) FROM Order ord  where  ord.paymentStatus='TNC_100'")
	Double sumAllOrder();

}
