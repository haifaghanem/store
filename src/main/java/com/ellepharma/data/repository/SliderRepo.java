package com.ellepharma.data.repository;

import com.ellepharma.data.model.Slider;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * SliderRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 24, 2019
 */

@Repository
public interface SliderRepo extends BaseRepo<Slider> {

    List<Slider> findAllByStatusTrue();

}