package com.ellepharma.data.repository;

import com.ellepharma.data.model.CountryShipment;
import org.springframework.stereotype.Repository;

/**
 * CountryShipmentRepo.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since June 07, 2019
 */

@Repository
public interface CountryShipmentRepo extends BaseRepo<CountryShipment> {

    CountryShipment findCountryShipmentByCountry_Id(Long id);
}