package com.ellepharma.data.repository;

import com.ellepharma.data.model.Menu;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CategoryRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 24, 2019
 */

@Repository
public interface MenuRepo extends BaseRepo<Menu> {


    List<Menu> findAllByStatusTrueAndParentIsNullOrderByPriorityOrderAsc();

    List<Menu> findAllByStatusTrueAndParent_IdOrderByPriorityOrderAsc(Long id);

}