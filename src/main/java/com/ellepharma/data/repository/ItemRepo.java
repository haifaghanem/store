package com.ellepharma.data.repository;

import com.ellepharma.data.dto.ItemDTO;
import com.ellepharma.data.dto.ItemReportFilterDTO;
import com.ellepharma.data.dto.ItemReportResultDTO;
import com.ellepharma.data.model.Category;
import com.ellepharma.data.model.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ItemRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */

@Repository
public interface ItemRepo extends BaseRepo<Item> {

    List<Item> findAllByStatusTrue();

    List<Item> findAllByStatus(Boolean status);

    List<Item> findAllByStatusTrue(Pageable pageable);

    @Query(value = "select i from Item i left join i.categories c where i.status=true and c.id in :ids")
    List<Item> findAllByStatusTrueAndCategoriesIdIn(Pageable pageable, @Param("ids") List<Long> ids);

    @Query(value = "select count(i) from Item i left join i.categories c where i.status=true and c.id in :ids")
    int countAllByStatusTrueAndCategoriesIdIn(@Param("ids") List<Long> ids);


    int countAllByStatusTrue();

    Item findById(Long id);

    List<Item> findAllByNewItemTrueAndStatusTrue();


    List<Item> findDistinctTop15ByStatusTrueOrderByPriceAsc();

    List<Item> findAllByStatusTrueAndCategoriesIn(List<Category> categories);

    List<Item> findDistinctByStatusTrueAndCategoriesIn(List<Category> categories);


    Page<Item> findAllByBarcodeListLike(Specification<Item> filterSpecification, Pageable pageable, String code);


    Page<Item> findAllByBarcodeListLike(Pageable pageable, String code);

    List<Item> findAllByBarcodeList(String code);

    @Modifying
    @Query(value = "update Item  i set i.newItem =false")
    void updateAllSetNotNew();

    Item findByCode(String code);

	/*@Query("SELECT DISTINCT item FROM Item item left join Category category  on 1 = 1 " +
				   " WHERE (:#{#itemCriteria.catIds eq null ? null:1} is null or category.id IN (:#{#itemCriteria.catIds}))" +
				   " AND (:#{#itemCriteria.brandIds eq null ? null:1} is null or item.manufacturer.id IN (:#{#itemCriteria.brandIds}))" +
				   " AND (:#{#itemCriteria.fromPrice} is null or (item.price >= :#{#itemCriteria.fromPrice}))" +
				   " AND (:#{#itemCriteria.toPrice} is null or (item.price <= :#{#itemCriteria.toPrice}))" +
				   " AND item.status = true " +
				   " GROUP BY item.id")*/


    List<Item> findByIdIn(List<Long> rids);


    @Query("select distinct item from Item item left join item.categories category left join category.parent pc " +
            " WHERE (:#{#itemCriteria.catIds eq null ? null:1} is null or (category.id IN (:#{#itemCriteria.catIds}) or pc.id IN (:#{#itemCriteria.catIds}) ) )" +
            " AND (:#{#itemCriteria.brandIds eq null ? null:1} is null or item.manufacturer.id IN (:#{#itemCriteria.brandIds}))" +
            " AND (:#{#itemCriteria.fromPrice} is null or (item.price >= :#{#itemCriteria.fromPrice}))" +
            " AND (:#{#itemCriteria.toPrice} is null or (item.price <= :#{#itemCriteria.toPrice}))" +
            " AND (:#{#itemCriteria.searchByName} is null or (item.name like CONCAT('%',:#{#itemCriteria.searchByName},'%') or (item.code like CONCAT(:#{#itemCriteria.searchByName},'')) ))" +
            " AND item.status = true  GROUP BY item.id order by item.name")
    List<Item> findAllItemsWithFilters(Pageable pageable, @Param("itemCriteria") ItemDTO itemCriteria);


    @Query("select COALESCE(count (distinct item.id),0) from Item item left join item.categories category  left join category.parent pc " +
            " WHERE (:#{#itemCriteria.catIds eq null ? null:1} is null or (category.id IN (:#{#itemCriteria.catIds}) or pc.id IN (:#{#itemCriteria.catIds}) ) )" +
            " AND (:#{#itemCriteria.brandIds eq null ? null:1} is null or item.manufacturer.id IN (:#{#itemCriteria.brandIds}))" +
            " AND (:#{#itemCriteria.fromPrice} is null or (item.price >= :#{#itemCriteria.fromPrice}))" +
            " AND (:#{#itemCriteria.toPrice} is null or (item.price <= :#{#itemCriteria.toPrice}))" +
            " AND (:#{#itemCriteria.searchByName} is null or (item.name like CONCAT('%',:#{#itemCriteria.searchByName},'%') or (item.code like CONCAT(:#{#itemCriteria.searchByName},'')) ))" +
            " AND item.status = true")
    Long countAllItemsWithFilters(@Param("itemCriteria") ItemDTO itemCriteria);

//    @Query(value = "select new com.ellepharma.data.dto.ItemReportResultDTO(i, sum(s.quantity)) from Item i " +
//            " left join StockIn s on i.id = s.item " +
//            " left join i.categories category  left join category.parent pc " +
//            " WHERE (:#{#filterParam.categoryId} is null or (category.id IN (:#{#filterParam.categoryId}) or pc.id IN (:#{#filterParam.categoryId}) ) )" +
//            " AND (:#{#filterParam.brandId} is null or i.manufacturer.id = (:#{#filterParam.brandId}))" +
//            " GROUP BY i.id")

    @Query(value = "select new com.ellepharma.data.dto.ItemReportResultDTO(i) from Item i left join i.categories category  left join category.parent pc " +
            " WHERE (:#{#filterParam.categoryId eq null ? null:1} is null or (category.id IN (:#{#filterParam.categoryId}) or pc.id IN (:#{#filterParam.categoryId}) ) )" +
            " AND (:#{#filterParam.brandId eq null ? null:1} is null or ( i.manufacturer.id = (:#{#filterParam.brandId})) )" +
            " AND (:#{#filterParam.fromDate} is null or i.dateAdded >= :#{#filterParam.fromDate} )" +
            " AND (:#{#filterParam.status} is null or i.status = :#{#filterParam.status} )" +
            " AND (:#{#filterParam.toDate} is null or i.dateAdded <= :#{#filterParam.toDate} )  GROUP BY i.id")
    List<ItemReportResultDTO> findAllReportItem(@Param("filterParam") ItemReportFilterDTO filterParam);
}