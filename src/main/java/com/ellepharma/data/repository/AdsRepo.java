package com.ellepharma.data.repository;

import com.ellepharma.data.model.Ads;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

/**
 * AdsRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 24, 2019
 */

@Repository
public interface AdsRepo extends BaseRepo<Ads> {


    Ads findByCode(String code);

    @Modifying
    void deleteByCode(String code);

    int countByCode(String code);
}