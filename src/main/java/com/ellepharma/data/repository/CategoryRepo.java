package com.ellepharma.data.repository;

import com.ellepharma.data.model.Category;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CategoryRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */

@Repository
public interface CategoryRepo extends BaseRepo<Category> {

    List<Category> findAllByStatusTrue();

    List<Category> findAllByParentIsNull();

    List<Category> findAllByParent_Id(Long id);

    List<Category> findAllByStatusTrueAndShowInSliderTrue();

    List<Category> findAllByStatusTrueAndShowInSliderTrue(Pageable pageable);

    List<Category> findAllByStatusTrue(Pageable pageable);

    Category findById(Long id);

    Category findByCode(String code);

    List<Category> findByCodeIn(List<String> codes);
}