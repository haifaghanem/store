package com.ellepharma.data.repository;

import com.ellepharma.data.model.Config;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CategoryRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 08, 2019
 */

@Repository
public interface ConfigRepo extends BaseRepo<Config> {

	Config findByName(String name);

	List<Config> findAll();

}