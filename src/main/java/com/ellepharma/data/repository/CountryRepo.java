package com.ellepharma.data.repository;

import com.ellepharma.data.model.Country;
import com.ellepharma.data.model.Currency;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * CountryRepo.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 05, 2019
 */

@Repository
public interface CountryRepo extends BaseRepo<Country> {

    Country findCountryByCountryNameEnIgnoreCase(String countryName);

    Country findByIsoCode2IgnoreCase(String code);

    Country findByIsoCode3IgnoreCase(String code);

    @Modifying
    @Query("update Country  set currency=:currency")
    void updateCurrencyForAll(@Param("currency") Currency currency);
}