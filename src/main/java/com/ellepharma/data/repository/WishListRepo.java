package com.ellepharma.data.repository;

import com.ellepharma.data.model.WishList;

/**
 * WishListRepo.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since July 16, 2019
 */
public interface WishListRepo extends BaseRepo<WishList> {

    WishList findByAccount_Id(Long id);

}
