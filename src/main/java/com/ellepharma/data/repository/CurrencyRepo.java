package com.ellepharma.data.repository;

import com.ellepharma.data.model.Currency;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CurencyRepo.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 05, 2019
 */

@Repository
public interface CurrencyRepo extends BaseRepo<Currency> {

    Currency findByCurrencyCode(String code);

    List<Currency> findAllByStatusIsTrue();
}