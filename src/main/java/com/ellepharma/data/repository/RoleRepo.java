package com.ellepharma.data.repository;

import com.ellepharma.data.model.Role;
import org.springframework.stereotype.Repository;

/**
 * CategoryRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */

@Repository
public interface RoleRepo extends BaseRepo<Role> {

    Role findByName(String name);


}