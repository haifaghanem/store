package com.ellepharma.data.repository;

import com.ellepharma.data.model.BaseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * BaseRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 23, 2019
 */
@NoRepositoryBean
public interface BaseRepo<T extends BaseEntity> extends PagingAndSortingRepository<T, Long> {

	List<T> findAll();

	Page<T> findAll(Specification<T> filterSpecification, Pageable pageable);

	T findById(Long id);
}
