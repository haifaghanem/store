package com.ellepharma.data.repository;

import com.ellepharma.data.model.Category;
import com.ellepharma.data.model.Manufacturer;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * SliderRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 01, 2019
 */

@Repository
public interface ManufacturerRepo extends BaseRepo<Manufacturer> {

	List<Manufacturer> findAll();
	List<Manufacturer> findAllByStatusIsTrue();

    Manufacturer findByCode(String code);
}