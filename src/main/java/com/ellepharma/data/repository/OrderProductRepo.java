package com.ellepharma.data.repository;

import com.ellepharma.data.model.OrderProduct;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * OrderProductRepo.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since July 12, 2019
 */

@Repository
public interface OrderProductRepo extends BaseRepo<OrderProduct> {


    @Query(value = "select  i.id from OrderProduct o left join  o.item i  group by i.id ORDER BY COUNT(i.id)  DESC")
    List<Long> getTids(Pageable pageable);

}