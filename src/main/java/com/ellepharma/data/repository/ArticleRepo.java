package com.ellepharma.data.repository;

import com.ellepharma.data.model.Article;
import org.springframework.stereotype.Repository;

/**
 * ArticleRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 21, 2019
 */

@Repository
public interface ArticleRepo extends BaseRepo<Article> {

}