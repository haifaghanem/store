package com.ellepharma.data.repository;

import com.ellepharma.data.model.Account;
import com.ellepharma.data.model.Coupon;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * CouponRepo.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since May 28, 2019
 */

@Repository
public interface CouponRepo extends BaseRepo<Coupon> {

    Coupon findByCouponNumberAndStatusTrueAndValidToDateAfter(String couponNumber, Date date);

}