package com.ellepharma.data.repository;

import com.ellepharma.data.model.CountryCashOnDelivery;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PaymentTypeRepo.java
 *
 * @author Saeed Walweel <saeed.walweel@gmail.com>
 * @since Feb 25, 2019
 */

@Repository
public interface CountryCashOnDeliveryRepo extends BaseRepo<CountryCashOnDelivery> {

	List<CountryCashOnDelivery> findAll();

	void deleteBy(CountryCashOnDelivery entity);
}