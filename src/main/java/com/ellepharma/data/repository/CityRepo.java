package com.ellepharma.data.repository;

import com.ellepharma.data.model.City;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CategoryRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since May 08, 2019
 */

@Repository
public interface CityRepo extends BaseRepo<City> {

    City findByName(String name);

    List<City> findByCountryCode(String name);


}