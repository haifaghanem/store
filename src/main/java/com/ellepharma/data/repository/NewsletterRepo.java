package com.ellepharma.data.repository;

import com.ellepharma.data.model.Newsletter;
import org.springframework.stereotype.Repository;

/**
 * NewsletterRepo.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Oct 03, 2019
 */

@Repository
public interface NewsletterRepo extends BaseRepo<Newsletter> {

}