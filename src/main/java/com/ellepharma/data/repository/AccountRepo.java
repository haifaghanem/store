package com.ellepharma.data.repository;

import com.ellepharma.data.model.Account;
import com.ellepharma.data.model.AccountType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * ImageRepo.java
 *
 * @author Malek Yaseen <ma.yaseen@ats-ware.com>
 * @since Apr 24, 2019
 */
public interface AccountRepo extends BaseRepo<Account> {


    @Query("select case when count(c)> 0 then true else false end from Account c where lower(c.email) like lower(:email) and c.id <> :id ")
    boolean existsAccountByEmail(@Param("email") String email, @Param("id") Long id);

    @Query("select case when count(c)> 0 then true else false end from Account c where lower(c.username) like lower(:username)")
    boolean existsAccountByUsername(@Param("username") String username);


    @Query("select c.email from Account c")
    List<String> findAllEmail();

    @Query("select  c.email from Account c where c.newsletter=true")
    List<String> findAllActiveEmail();

    Long countAccountByAccountType(AccountType accountType);

    Account findByEmailAndActiveIsTrue(String email);

    Account findByEmail(String email);

    Account findByUsernameAndActiveIsTrue(String userName);

    List<Account> findByAccountTypeAndActiveIsTrue(AccountType accountType);

}
