package com.ellepharma.data.common;

import com.ellepharma.data.model.BaseEntity;
import com.ellepharma.utils.ReflectionUtil;
import org.apache.commons.collections4.CollectionUtils;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * /**
 * GenericDAO.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 18, 2019
 */

public class GenericDAO<T extends BaseEntity> {

	public static final String RID_PARAM = "id";

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * The entity that the DAO will work with
	 */
	private Class<T> entityClass;

	/**
	 * Constructor
	 *
	 * @param entityClass
	 */
	public GenericDAO(Class<T> entityClass) {
		this.setEntityClass(entityClass);
	}

	/**
	 * Save new entity
	 *
	 * @param entity
	 * @return the saved entity
	 */
	public T add(T entity) {
		T addedEntity = getEntityManager().merge(entity);
		getEntityManager().flush();// FIXME
		return addedEntity;
	}

	/**
	 * Update entity
	 *
	 * @param entity
	 * @return the updated entity
	 */
	public T update(T entity) {
		T updatedEntity = getEntityManager().merge(entity);
		getEntityManager().flush();// FIXME
		return updatedEntity;
	}

	/**
	 * Delete entity
	 *
	 * @param entity
	 */
	public void delete(T entity) {

		@SuppressWarnings("unchecked")
		T deletedEntity = (T) getEntityManager().find(entity.getClass(), entity.getId());
		getEntityManager().remove(deletedEntity);
		getEntityManager().flush();// FIXME
	}

	/**
	 * Discard all changes made to the entity and get the data from the database
	 *
	 * @param entity
	 * @return the entity as in the DB
	 */
	public T refresh(T entity) {
		getEntityManager().refresh(entity);
		return entity;
	}

	/**
	 * Send pending SQL DML operation to database (without commit)
	 */
	public void flush() {
		getEntityManager().flush();
	}

	/**
	 * Find entity by ID
	 *
	 * @param id
	 * @return Entity
	 */
	public T find(Object id) {
		return getEntityManager().find(getEntityClass(), id);
	}

	/**
	 * Execute UPDATE or DELETE using predefined NamedQuery
	 *
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	protected int executeUpdateDelete(String namedQuery, Map<String, Object> parameters) {

		Query query = getEntityManager().createNamedQuery(namedQuery);

		/* Parameters will be set in named query if parameters Map is not empty */

		if (parameters != null && !parameters.isEmpty()) {
			populateQueryParameters(query, parameters);
		}

		return query.executeUpdate();
	}

	/**
	 * check if the generic entity has detail records
	 *
	 * @param rid
	 * @return
	 */
	public boolean isParentRecord(Long rid) {
		return isParentRecord(getEntityClass().getSimpleName(), rid);
	}

	/**
	 * Check if the given entity has detail records
	 *
	 * @param entityName
	 * @param rid
	 * @return
	 */
	public boolean isParentRecord(String entityName, Long rid) {
		// FIXME
		return false;

	}

	/**
	 * check if the generic entity has detail records excluding some
	 *
	 * @param rid
	 * @return
	 */
	public boolean isParentRecordExclude(Long rid, String... excludeEntities) {
		return isParentRecord(getEntityClass().getSimpleName(), rid, excludeEntities);
	}

	/**
	 * Check if the given entity has detail records excluding some
	 *
	 * @param entityName
	 * @param rid
	 * @return
	 */
	public boolean isParentRecord(String entityName, Long rid, String... excludeEntities) {
		// FIXME
		// String excludeEntitiesStr = CollectionUtils.convertListToCommaSeparatedStr(excludeEntities);

		return false;

	}

	/**
	 * Get all data
	 *
	 * @return List of entities
	 */
	public List<T> findAll() {

		CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(getEntityClass());
		cq.select(cq.from(getEntityClass()));

		TypedQuery<T> findAllQuery = getEntityManager().createQuery(cq);

		return findAllQuery.getResultList();
	}

	public T findLast() {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

		CriteriaQuery<T> cq = cb.createQuery(getEntityClass());
		Root<T> c = cq.from(getEntityClass());
		cq.select(c);
		cq.orderBy(cb.desc(c.get("id")));

		// CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(getEntityClass());
		// cq.select(cq.from(getEntityClass())).orderBy(arg0);

		TypedQuery<T> findAllQuery = getEntityManager().createQuery(cq);

		List<T> list = findAllQuery.setMaxResults(1).getResultList();

		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;

	}

	public BaseEntity findLast(String entityName) {

		Class<BaseEntity> clazz = ReflectionUtil.getEntityClassByName(entityName, entityManager);
		System.out.println("######################" + clazz);
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

		CriteriaQuery<BaseEntity> cq = cb.createQuery(clazz);
		Root<BaseEntity> c = cq.from(clazz);
		cq.select(c);
		cq.orderBy(cb.desc(c.get("id")));

		// CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(getEntityClass());
		// cq.select(cq.from(getEntityClass())).orderBy(arg0);

		TypedQuery<BaseEntity> findAllQuery = getEntityManager().createQuery(cq);

		List<BaseEntity> list = findAllQuery.setMaxResults(1).getResultList();

		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;

	}

	public BaseEntity findPrevLast(String entityName) {

		Class<BaseEntity> clazz = ReflectionUtil.getEntityClassByName(entityName, entityManager);
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

		CriteriaQuery<BaseEntity> cq = cb.createQuery(clazz);
		Root<BaseEntity> c = cq.from(clazz);
		cq.select(c);
		cq.orderBy(cb.desc(c.get("id")));

		// CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(getEntityClass());
		// cq.select(cq.from(getEntityClass())).orderBy(arg0);

		TypedQuery<BaseEntity> findAllQuery = getEntityManager().createQuery(cq);

		List<BaseEntity> list = findAllQuery.setMaxResults(2).getResultList();

		if (CollectionUtils.isNotEmpty(list)) {
			if (CollectionUtils.isNotEmpty(list)) {
				if (list.size() > 1) {
					return list.get(1);
				}
			}
			return list.get(0);
		}
		return null;

	}

	public T findPrevLast() {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

		CriteriaQuery<T> cq = cb.createQuery(getEntityClass());
		Root<T> c = cq.from(getEntityClass());
		cq.select(c);
		cq.orderBy(cb.desc(c.get("id")));

		// CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(getEntityClass());
		// cq.select(cq.from(getEntityClass())).orderBy(arg0);

		TypedQuery<T> findAllQuery = getEntityManager().createQuery(cq);

		List<T> list = findAllQuery.setMaxResults(2).getResultList();

		if (CollectionUtils.isNotEmpty(list)) {
			if (CollectionUtils.isNotEmpty(list)) {
				if (list.size() > 1) {
					return list.get(1);
				}
			}
			return list.get(0);
		}
		return null;

	}

	public long count() {
		return count(null, null);
	}

	/**
	 * Return records count of a given named query with the given parameters
	 *
	 * @param parameters
	 * @param execludeParameters
	 * @return long, records count
	 */
	protected long count(Map<String, Object> parameters, Map<String, Object> execludeParameters) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = criteriaBuilder.createQuery(Long.class);
		Root<T> root = cq.from(getEntityClass());
		cq.select(criteriaBuilder.count(root));

		Predicate predicate = criteriaBuilder.conjunction();
		if (parameters != null) {
			for (Entry<String, Object> entry : parameters.entrySet()) {
				Predicate newPredicate;
				if (entry.getValue() == null) {
					newPredicate = criteriaBuilder.isNull(root.get(entry.getKey()));
				} else {
					newPredicate = criteriaBuilder.equal(root.get(entry.getKey()), entry.getValue());
				}
				predicate = criteriaBuilder.and(predicate, newPredicate);
			}
		}

		if (execludeParameters != null) {
			for (Entry<String, Object> entry : execludeParameters.entrySet()) {
				Predicate newPredicate;
				if (entry.getValue() == null) {
					newPredicate = criteriaBuilder.isNotNull(root.get(entry.getKey()));
				} else {
					newPredicate = criteriaBuilder.notEqual(root.get(entry.getKey()), entry.getValue());
				}
				predicate = criteriaBuilder.and(predicate, newPredicate);
			}
		}

		cq.where(predicate);

		return entityManager.createQuery(cq).getSingleResult();

	}

	protected long count(Map<String, Object> parameters) {
		return count(parameters, null);
	}

	/**
	 * Return true if the given named query return data
	 *
	 * @param parameters
	 * @return boolean
	 */
	protected boolean hasRecords(Map<String, Object> parameters, Map<String, Object> execludeParameters) {

		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();

		CriteriaQuery<Integer> criteriaQuery = criteriaBuilder.createQuery(Integer.class);
		Root<T> queryRoot = criteriaQuery.from(getEntityClass());
		criteriaQuery.select(criteriaBuilder.literal(1));

		Predicate predicate = criteriaBuilder.conjunction();
		if (parameters != null) {
			for (Entry<String, Object> entry : parameters.entrySet()) {
				Predicate newPredicate;
				if (entry.getValue() == null) {
					newPredicate = criteriaBuilder.isNull(queryRoot.get(entry.getKey()));
				} else {
					newPredicate = criteriaBuilder.equal(queryRoot.get(entry.getKey()), entry.getValue());
				}
				predicate = criteriaBuilder.and(predicate, newPredicate);
			}
		}

		if (execludeParameters != null) {
			for (Entry<String, Object> entry : execludeParameters.entrySet()) {
				Predicate newPredicate;
				if (entry.getValue() == null) {
					newPredicate = criteriaBuilder.isNotNull(queryRoot.get(entry.getKey()));
				} else {
					newPredicate = criteriaBuilder.notEqual(queryRoot.get(entry.getKey()), entry.getValue());
				}
				predicate = criteriaBuilder.and(predicate, newPredicate);
			}
		}

		criteriaQuery.where(predicate);

		TypedQuery<Integer> query = getEntityManager().createQuery(criteriaQuery);

		return query.getResultList().size() > 0;

	}

	protected boolean hasRecords(Map<String, Object> parameters) {

		return hasRecords(parameters, null);
	}

	/**
	 * Check if the given id exists in table corresponding to entityName
	 *
	 * @param entityName
	 * @param recordId
	 * @return
	 */
	public boolean isRecordExists(String entityName, long recordId) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();

		CriteriaQuery<Integer> criteriaQuery = criteriaBuilder.createQuery(Integer.class);
		Root<BaseEntity> queryRoot = criteriaQuery.from(ReflectionUtil.getEntityClassByName(entityName, getEntityManager()));
		criteriaQuery.select(criteriaBuilder.literal(1));

		Predicate predicate = criteriaBuilder.conjunction();
		Predicate newPredicate = criteriaBuilder.equal(queryRoot.get(RID_PARAM), recordId);
		predicate = criteriaBuilder.and(predicate, newPredicate);
		criteriaQuery.where(predicate);
		TypedQuery<Integer> query = getEntityManager().createQuery(criteriaQuery);
		return query.getResultList().size() > 0;
	}

	// /**
	// * Get active recored (where is_active=1)
	// *
	// * @return List of active entities
	// */
	// public List<T> findActive() {
	// CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
	//
	// CriteriaQuery<T> q = cb.createQuery(getEntityClass());
	// Root<T> c = q.from(getEntityClass());
	//
	// ParameterExpression<Short> p = cb.parameter(Short.class);
	// q.select(c).where(cb.equal(c.get("isActive"), p));
	//
	// TypedQuery<T> query = getEntityManager().createQuery(q);
	// query.setParameter(p, (short) 1);
	// return query.getResultList();
	// }

	/**
	 * Get chunk of data
	 *
	 * @param first    (start index)
	 * @param pageSize (number of records)
	 * @return List of entities
	 */
	public List<T> findAll(int first, int pageSize) {
		CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(getEntityClass());
		cq.select(cq.from(getEntityClass()));

		return getEntityManager().createQuery(cq).setFirstResult(first).setMaxResults(pageSize).getResultList();
	}

	/**
	 * Find one entity using predefined NamedQuery, and the result will be cached
	 *
	 * @param namedQuery
	 * @return Entity
	 */
	protected T getOneResult(String namedQuery) {
		return getOneResult(namedQuery, null);
	}

	/**
	 * Execute a named query a return a given type
	 *
	 * @param returnTypeClass, named query return types
	 * @param namedQuery
	 * @param parameters
	 * @return List, list of return type class
	 */
	protected <X> List<X> executeNamedQueryList(Class<X> returnTypeClass, String namedQuery, Map<String, Object> parameters) {

		TypedQuery<X> query = getEntityManager().createNamedQuery(namedQuery, returnTypeClass);

		if (parameters != null && !parameters.isEmpty()) {
			populateQueryParameters(query, parameters);
		}

		return query.getResultList();
	}

	public List<T> find(Map<String, Object> parameters) {
		return find(parameters, null);
	}

	public List<T> find(Map<String, Object> parameters, Map<String, Object> execludeParameters) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();

		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
		Root<T> queryRoot = criteriaQuery.from(getEntityClass());

		Predicate predicate = criteriaBuilder.conjunction();
		if (parameters != null) {
			for (Entry<String, Object> entry : parameters.entrySet()) {
				Predicate newPredicate;
				if (entry.getValue() == null) {
					newPredicate = criteriaBuilder.isNull(queryRoot.get(entry.getKey()));
				} else {
					newPredicate = criteriaBuilder.equal(queryRoot.get(entry.getKey()), entry.getValue());
				}
				predicate = criteriaBuilder.and(predicate, newPredicate);
			}
		}

		if (execludeParameters != null) {
			for (Entry<String, Object> entry : execludeParameters.entrySet()) {
				Predicate newPredicate;
				if (entry.getValue() == null) {
					newPredicate = criteriaBuilder.isNotNull(queryRoot.get(entry.getKey()));
				} else {
					newPredicate = criteriaBuilder.notEqual(queryRoot.get(entry.getKey()), entry.getValue());
				}
				predicate = criteriaBuilder.and(predicate, newPredicate);
			}
		}

		criteriaQuery.where(predicate);

		TypedQuery<T> query = getEntityManager().createQuery(criteriaQuery);

		return query.getResultList();
	}

	/**
	 * Execute a named query a return a given type
	 *
	 * @param returnTypeClass, named query return types
	 * @param namedQuery
	 * @param parameters
	 * @return Object
	 */
	protected <X> Object executeNamedQuerySingleResult(Class<X> returnTypeClass, String namedQuery, Map<String, Object> parameters)
			throws NonUniqueResultException, NoResultException {

		List<X> resultList = executeNamedQueryList(returnTypeClass, namedQuery, parameters);

		if (resultList != null) {
			if (!resultList.isEmpty()) {
				if (resultList.size() == 1) {
					return resultList.get(0);
				} else {
					throw new NonUniqueResultException();
				}
			} else {
				throw new NoResultException();
			}
		} else {
			throw new NoResultException();
		}
	}

	/**
	 * Find one entity using predefined NamedQuery
	 *
	 * @param namedQuery
	 * @param parameters
	 * @param cache
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected T getOneResult(String namedQuery, Map<String, Object> parameters) {

		T result = null;

		Query query = getEntityManager().createNamedQuery(namedQuery);

		// Method that will populate parameters if they are passed not null
		// and empty

		if (parameters != null && !parameters.isEmpty()) {
			populateQueryParameters(query, parameters);
		}

		result = (T) query.getSingleResult();

		return result;
	}

	/**
	 * Find list of entities using predefined NamedQuery, and the result will be cached by default
	 *
	 * @param namedQuery
	 * @param parameters
	 * @return
	 */
	protected List<T> findListResult(String namedQuery) {
		return findListResult(namedQuery, null);
	}

	/**
	 * Find list of entities using predefined NamedQuery
	 *
	 * @param namedQuery
	 * @param parameters
	 * @param cache
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected List<T> findListResult(String namedQuery, Map<String, Object> parameters) {

		List<T> result = new ArrayList<T>();

		Query query = getEntityManager().createNamedQuery(namedQuery);

		// Method that will populate parameters if they are passed not null
		// and empty

		if (parameters != null && !parameters.isEmpty()) {
			populateQueryParameters(query, parameters);
		}

		result = query.getResultList();

		return result;
	}

	private void populateQueryParameters(Query query, Map<String, Object> parameters) {

		for (Entry<String, Object> entry : parameters.entrySet()) {

			query.setParameter(entry.getKey(), entry.getValue());

		}

	}

	/**
	 * @return the entityManager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * @return the entityClass
	 */
	public Class<T> getEntityClass() {
		return entityClass;
	}

	/**
	 * @param entityClass the entityClass to set
	 */
	public final void setEntityClass(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

}
