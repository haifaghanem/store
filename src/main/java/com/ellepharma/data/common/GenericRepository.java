package com.ellepharma.data.common;

import com.ellepharma.data.model.BaseEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * /**
 * GenericRepository.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 18, 2019
 */

@NoRepositoryBean
public interface GenericRepository<T extends BaseEntity> {

	List<T> findAll();

	List<T> findAll(Pageable pageable);

	T findById(Long id);

	@Modifying
	T save(T t);
}
