package com.ellepharma.data.dto;

public class ImportAsnDateLines {

    private String externLineNo;
    private String linePO;
    private Integer qty;
    private String sku;
    private Double unitCost;

    public String getExternLineNo() {
        return externLineNo;
    }

    public void setExternLineNo(String externLineNo) {
        this.externLineNo = externLineNo;
    }

    public String getLinePO() {
        return linePO;
    }

    public void setLinePO(String linePO) {
        this.linePO = linePO;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(Double unitCost) {
        this.unitCost = unitCost;
    }
}
