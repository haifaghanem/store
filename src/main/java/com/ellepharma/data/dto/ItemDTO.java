package com.ellepharma.data.dto;

import org.apache.commons.collections4.CollectionUtils;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

public class ItemDTO implements Serializable {

    private double fromPrice;
    private double toPrice;
    private String searchByName;
    private List<Long> catIds;
    private List<Long> brandIds;
    private SortTypeEnum sortWay;

    public String getStringToCash() {

        String s = catIds != null ? catIds.stream().sorted().map(aLong -> aLong.toString()).collect(Collectors.joining(",")) : "";
        String b = brandIds != null ? brandIds.stream().sorted().map(aLong -> aLong.toString()).collect(Collectors.joining(",")) : "";
        return s + "," + b + "," + searchByName + "," + sortWay.getCode() + "," + fromPrice + "," + toPrice;

    }

    public double getFromPrice() {
        return fromPrice;
    }

    public void setFromPrice(double fromPrice) {
        this.fromPrice = fromPrice;
    }

    public double getToPrice() {
        return toPrice;
    }

    public void setToPrice(double toPrice) {
        this.toPrice = toPrice;
    }

    public SortTypeEnum getSortWay() {
        return sortWay;
    }

    public void setSortWay(SortTypeEnum sortWay) {
        this.sortWay = sortWay;
    }

    public List<Long> getCatIds() {
        return CollectionUtils.isNotEmpty(catIds) ? catIds : null;
    }

    public void setCatIds(List<Long> catIds) {
        this.catIds = catIds;
    }

    public List<Long> getBrandIds() {
        return CollectionUtils.isNotEmpty(brandIds) ? brandIds : null;
    }

    public void setBrandIds(List<Long> brandIds) {
        this.brandIds = brandIds;
    }

    public String getSearchByName() {
        return searchByName;
    }

    public void setSearchByName(String searchByName) {
        this.searchByName = searchByName;
    }
}
