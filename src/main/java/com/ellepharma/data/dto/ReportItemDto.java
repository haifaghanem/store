package com.ellepharma.data.dto;

import com.ellepharma.data.model.BaseEntity;
import com.ellepharma.data.model.Item;

import java.io.Serializable;

public class ReportItemDto extends BaseEntity implements Serializable {
    private Double total;
    private Long qty;
    private Item item;
    private String desc;
    private String name;
    private String code;
    private String barcode;
    private String index;
    private Double price;
    private Long available;

    private Double finalPrice;

    private String orderIds;
    private String invoiceNo;

    public ReportItemDto(Item item, Long qty) {
        this.qty = qty;
        this.item = item;
    }

    public ReportItemDto() {
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public Long getId() {
        if (item != null) {

            return item.getId();
        }
        return null;
    }

    public void setAvailable(Long available) {

        this.available = available;
    }

    public Long getAvailable() {
        return available;
    }

	public void setFinalPrice(Double finalPrice) {


		this.finalPrice = finalPrice;
	}

	public Double getFinalPrice() {
		return finalPrice;
	}

    public String getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(String orderIds) {
        this.orderIds = orderIds;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
}
