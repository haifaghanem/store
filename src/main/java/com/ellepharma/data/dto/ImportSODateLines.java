package com.ellepharma.data.dto;

public class ImportSODateLines {

    private String externLineNo;
    private Integer qty;
    private String sku;
    private Double unitPrice;

    public String getExternLineNo() {
        return externLineNo;
    }

    public void setExternLineNo(String externLineNo) {
        this.externLineNo = externLineNo;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }
}
