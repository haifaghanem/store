package com.ellepharma.data.dto;

import com.ellepharma.data.model.Item;

public class ItemReportResultDTO {
    private Long originalItemQty;
    private Long soldItemQty;
    private Item item;



    public ItemReportResultDTO() {
    }

    public ItemReportResultDTO(Item item) {
        this.item = item;

    }

    public Long getOriginalItemQty() {
        return null != originalItemQty ? originalItemQty : 0;
    }

    public void setOriginalItemQty(Long originalItemQty) {
        this.originalItemQty = originalItemQty;
    }

    public Long getSoldItemQty() {
        return null != soldItemQty ? soldItemQty : 0;
    }

    public void setSoldItemQty(Long soldItemQty) {
        this.soldItemQty = soldItemQty;
    }

    public Long getRemainingItemQty() {
        return (null != originalItemQty ? (originalItemQty - (null != soldItemQty ? soldItemQty : 0)) : (0 - (null != soldItemQty ? soldItemQty : 0)));
    }

    public Double getSellPrice() {
        return (null != item.getFinalPrice() ?
                (item.getFinalPrice() * (null != item.getHaveDiscount() && item.getHaveDiscount() ?
                        (item.getDiscount() > 0 ? item.getDiscount() / 100 : 1) : 1)) : 0);
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }


}
