package com.ellepharma.data.dto;

public class InquirySODTO {

    private String clinetSystemRef;
    private String facility;
    private Boolean latestOnly;
    private String storerKey;
    private String ssaLogin;
    private String ssaPassword;

    public InquirySODTO() {
    /*   setClinetSystemRef("Order Ref");
       setFacility("WMWHSE1");
       setLatestOnly(false);
       setStorerKey("DEMO");
       setSsaLogin("wptest");
       setSsaPassword("pass");*/
    }

    public String getClinetSystemRef() {
        return clinetSystemRef;
    }

    public void setClinetSystemRef(String clinetSystemRef) {
        this.clinetSystemRef = clinetSystemRef;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public Boolean getLatestOnly() {
        return latestOnly;
    }

    public void setLatestOnly(Boolean latestOnly) {
        this.latestOnly = latestOnly;
    }

    public String getStorerKey() {
        return storerKey;
    }

    public void setStorerKey(String storerKey) {
        this.storerKey = storerKey;
    }

    public String getSsaLogin() {
        return ssaLogin;
    }

    public void setSsaLogin(String ssaLogin) {
        this.ssaLogin = ssaLogin;
    }

    public String getSsaPassword() {
        return ssaPassword;
    }

    public void setSsaPassword(String ssaPassword) {
        this.ssaPassword = ssaPassword;
    }
}
