package com.ellepharma.data.dto;

public class ImportSKUsDateHeaderDTO {

    private String description;
    private String facility;
    private String hsCode;
    private String sku;
    private Integer serialCount;
    private String storerKey;
    private String upc;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Integer getSerialCount() {
        return serialCount;
    }

    public void setSerialCount(Integer serialCount) {
        this.serialCount = serialCount;
    }

    public String getStorerKey() {
        return storerKey;
    }

    public void setStorerKey(String storerKey) {
        this.storerKey = storerKey;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }
}
