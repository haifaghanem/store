package com.ellepharma.data.dto;

public class ImportSKUDTO {

    private String requestedData;
    private String transactionID;
    private String description;
    private String facility;
    private String hsCode;
    private String sku;
    private Integer serialCount;
    private String storerKey;
    private String upc;
    private String ssaLogin;
    private String ssaPassword;

    public ImportSKUDTO() {
        //For test
       /* setRequestedData("Test");
        setTransactionID("Test123456789");
        setDescription("Description goes here");
        setFacility("WMWHSE1");
        setHsCode("10255215");
        setSku("Item001");
        setSerialCount(0);
        setStorerKey("DEMO");
        setUpc("BarCode111");
        setSsaLogin("wptest");
        setSsaPassword("pass");*/
    }

    public String getRequestedDate() {
        return requestedData;
    }

    public void setRequestedData(String requestedData) {
        this.requestedData = requestedData;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Integer getSerialCount() {
        return serialCount;
    }

    public void setSerialCount(Integer serialCount) {
        this.serialCount = serialCount;
    }

    public String getStorerKey() {
        return storerKey;
    }

    public void setStorerKey(String storerKey) {
        this.storerKey = storerKey;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getSsaLogin() {
        return ssaLogin;
    }

    public void setSsaLogin(String ssaLogin) {
        this.ssaLogin = ssaLogin;
    }

    public String getSsaPassword() {
        return ssaPassword;
    }

    public void setSsaPassword(String ssaPassword) {
        this.ssaPassword = ssaPassword;
    }
}
