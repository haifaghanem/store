package com.ellepharma.data.dto;

public enum SortTypeEnum {
    ASC("ASC"),DESC("DESC"),PRICE_ASC("PRICE_ASC"),PRICE_DESC("PRICE_DESC");
    private String desc;

    public String getCode() {
        return desc;
    }

    SortTypeEnum(String code) {
        this.desc = code;
    }
}
