package com.ellepharma.data.dto;

import java.util.ArrayList;
import java.util.List;

public class ImportSKUsDTO {

    private String transactionID;
    private String ssaLogin;
    private String ssaPassword;
    private List<ImportSKUsDateHeaderDTO> dataHeader;

    public ImportSKUsDTO() {
        //For test
       /* setTransactionID("Test System");
        setSsaLogin("wptest");
        setSsaPassword("pass");

        List<ImportSKUsDateHeaderDTO> dataHeader = new ArrayList<>();
        ImportSKUsDateHeaderDTO obj = new ImportSKUsDateHeaderDTO();
        obj.setDescription("Description goes here");
        obj.setFacility("WMWHSE1");
        obj.setHsCode("10255215");
        obj.setSku("Item001");
        obj.setSerialCount(0);
        obj.setStorerKey("DEMO");
        obj.setUpc("BarCode111");
        dataHeader.add(obj);

        ImportSKUsDateHeaderDTO obj2 = new ImportSKUsDateHeaderDTO();
        obj2.setDescription("Description2 goes here");
        obj2.setFacility("WH1");
        obj2.setHsCode("10255216");
        obj2.setSku("Item002");
        obj2.setSerialCount(0);
        obj2.setStorerKey("TEST");
        obj2.setUpc("BarCode112");
        dataHeader.add(obj2);

        setDataHeader(dataHeader);*/
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getSsaLogin() {
        return ssaLogin;
    }

    public void setSsaLogin(String ssaLogin) {
        this.ssaLogin = ssaLogin;
    }

    public String getSsaPassword() {
        return ssaPassword;
    }

    public void setSsaPassword(String ssaPassword) {
        this.ssaPassword = ssaPassword;
    }

    public List<ImportSKUsDateHeaderDTO> getDataHeader() {
        return dataHeader;
    }

    public void setDataHeader(List<ImportSKUsDateHeaderDTO> dataHeader) {
        this.dataHeader = dataHeader;
    }
}
