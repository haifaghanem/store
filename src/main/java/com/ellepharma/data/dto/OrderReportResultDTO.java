package com.ellepharma.data.dto;

import com.ellepharma.data.model.Order;

public class OrderReportResultDTO {
    private Order order;

    public OrderReportResultDTO() {
    }

    public OrderReportResultDTO(Order order) {
        this.order = order;

    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
