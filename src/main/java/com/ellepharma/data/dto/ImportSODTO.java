package com.ellepharma.data.dto;

import java.util.ArrayList;
import java.util.List;

public class ImportSODTO {

    private ImportSOAddressDTO bAddress;
    private ImportSOAddressDTO cAddress;
    private String clinetSystemRef;
    private String currency;
    private ImportSOAddressDTO dAddress;
    private String facility;
    private String forwarder;
    private String storerKey;
    private String transportationMode;
    private String ssaLogin;
    private String ssaPassword;

    private List<ImportSODateLines> dataLines;

    public ImportSODTO() {
        //for test
       /* bAddress = new ImportSOAddressDTO();
        cAddress = new ImportSOAddressDTO();
        dAddress = new ImportSOAddressDTO();
        bAddress.setAddress1("Address1");
        bAddress.setAddress1("Address2");
        bAddress.setAddress1("Address3");
        bAddress.setCity("Dubai");
        bAddress.setCompany("Test Company");
        bAddress.setContact("Contact 1");
        bAddress.setContact("ARE");
        bAddress.setEmail("Email@Domain.com");
        bAddress.setFax("00970412365489");
        bAddress.setPhone1("00970505245510");
        bAddress.setPhone2("00970505842620");

        cAddress = bAddress;
        dAddress = bAddress;

        setbAddress(bAddress);
        setcAddress(cAddress);
        setdAddress(dAddress);

        setClinetSystemRef("Order Ref1");
        setCurrency("AED");
        setFacility("WMWHSE1");
        setForwarder("aramex");
        setStorerKey("DEMO");


        List<ImportSODateLines> dataLines =new ArrayList<>();
        ImportSODateLines importSoDateLines = new ImportSODateLines();
        importSoDateLines.setExternLineNo("00001");
        importSoDateLines.setQty(5);
        importSoDateLines.setSku("Test");
        importSoDateLines.setUnitPrice(1.32);
        dataLines.add(importSoDateLines);

        setDataLines(dataLines);
        setSsaLogin("wptest");
        setSsaPassword("pass");*/

    }

    public ImportSOAddressDTO getbAddress() {
        return bAddress;
    }

    public void setbAddress(ImportSOAddressDTO bAddress) {
        this.bAddress = bAddress;
    }

    public ImportSOAddressDTO getcAddress() {
        return cAddress;
    }

    public void setcAddress(ImportSOAddressDTO cAddress) {
        this.cAddress = cAddress;
    }

    public String getClinetSystemRef() {
        return clinetSystemRef;
    }

    public void setClinetSystemRef(String clinetSystemRef) {
        this.clinetSystemRef = clinetSystemRef;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public ImportSOAddressDTO getdAddress() {
        return dAddress;
    }

    public void setdAddress(ImportSOAddressDTO dAddress) {
        this.dAddress = dAddress;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getForwarder() {
        return forwarder;
    }

    public void setForwarder(String forwarder) {
        this.forwarder = forwarder;
    }

    public String getStorerKey() {
        return storerKey;
    }

    public void setStorerKey(String storerKey) {
        this.storerKey = storerKey;
    }

    public String getTransportationMode() {
        return transportationMode;
    }

    public void setTransportationMode(String transportationMode) {
        this.transportationMode = transportationMode;
    }

    public String getSsaLogin() {
        return ssaLogin;
    }

    public void setSsaLogin(String ssaLogin) {
        this.ssaLogin = ssaLogin;
    }

    public String getSsaPassword() {
        return ssaPassword;
    }

    public void setSsaPassword(String ssaPassword) {
        this.ssaPassword = ssaPassword;
    }

    public List<ImportSODateLines> getDataLines() {
        return dataLines;
    }

    public void setDataLines(List<ImportSODateLines> dataLines) {
        this.dataLines = dataLines;
    }
}
