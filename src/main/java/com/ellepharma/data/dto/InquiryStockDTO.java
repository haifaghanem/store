package com.ellepharma.data.dto;

public class InquiryStockDTO {

    private String facility;
    private boolean groupBySKU;
    private String storerKey;
    private String ssaLogin;
    private String ssaPassword;

    public InquiryStockDTO() {
        //For test
      /*  setFacility("WMWHSE1");
        setGroupBySKU(true);
        setStorerKey("DEMO");
        setSsaLogin("wptest");
        setSsaPassword("pass");*/
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public boolean isGroupBySKU() {
        return groupBySKU;
    }

    public void setGroupBySKU(boolean groupBySKU) {
        this.groupBySKU = groupBySKU;
    }

    public String getStorerKey() {
        return storerKey;
    }

    public void setStorerKey(String storerKey) {
        this.storerKey = storerKey;
    }

    public String getSsaLogin() {
        return ssaLogin;
    }

    public void setSsaLogin(String ssaLogin) {
        this.ssaLogin = ssaLogin;
    }

    public String getSsaPassword() {
        return ssaPassword;
    }

    public void setSsaPassword(String ssaPassword) {
        this.ssaPassword = ssaPassword;
    }
}
