package com.ellepharma.data.dto;

import java.util.ArrayList;
import java.util.List;

public class ImportASNDTO {

    private String clinetSystemRef;
    private String currency;
    private String facility;
    private String storerKey;
    private String ssaLogin;
    private String ssaPassword;

    private List<ImportAsnDateLines> dataLines;

    public ImportASNDTO() {
        //For test
        /*setClinetSystemRef("ClientRef");
        setCurrency("AED");
        setFacility("WMWHSE1");
        setStorerKey("DEMO");
        setSsaLogin("wptest");
        setSsaPassword("pass");

        List<ImportAsnDateLines> dataLines =new ArrayList<>();
        ImportAsnDateLines importAsnDateLines = new ImportAsnDateLines();
        importAsnDateLines.setExternLineNo("1RANDOM");
        importAsnDateLines.setLinePO("NOPO");
        importAsnDateLines.setQty(2);
        importAsnDateLines.setSku("sku123");
        importAsnDateLines.setUnitCost(20d);
        dataLines.add(importAsnDateLines);

        importAsnDateLines = new ImportAsnDateLines();
        importAsnDateLines.setExternLineNo("2RANDOM");
        importAsnDateLines.setLinePO("NOPOs");
        importAsnDateLines.setQty(3);
        importAsnDateLines.setSku("sku1234");
        importAsnDateLines.setUnitCost(22d);
        dataLines.add(importAsnDateLines);

        setDataLines(dataLines);*/
    }

    public String getClinetSystemRef() {
        return clinetSystemRef;
    }

    public void setClinetSystemRef(String clinetSystemRef) {
        this.clinetSystemRef = clinetSystemRef;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getStorerKey() {
        return storerKey;
    }

    public void setStorerKey(String storerKey) {
        this.storerKey = storerKey;
    }

    public String getSsaLogin() {
        return ssaLogin;
    }

    public void setSsaLogin(String ssaLogin) {
        this.ssaLogin = ssaLogin;
    }

    public String getSsaPassword() {
        return ssaPassword;
    }

    public void setSsaPassword(String ssaPassword) {
        this.ssaPassword = ssaPassword;
    }

    public List<ImportAsnDateLines> getDataLines() {
        return dataLines;
    }

    public void setDataLines(List<ImportAsnDateLines> dataLines) {
        this.dataLines = dataLines;
    }
}
