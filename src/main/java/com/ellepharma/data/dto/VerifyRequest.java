package com.ellepharma.data.dto;

public class VerifyRequest {
    private String result;

    private String transaction_id;

    private String response_code;

    private String amount;

    private String currency;

    private String pt_invoice_id;

    private String reference_no;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getResponse_code() {
        return response_code;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPt_invoice_id() {
        return pt_invoice_id;
    }

    public void setPt_invoice_id(String pt_invoice_id) {
        this.pt_invoice_id = pt_invoice_id;
    }

    public String getReference_no() {
        return reference_no;
    }

    public void setReference_no(String reference_no) {
        this.reference_no = reference_no;
    }

    @Override
    public String toString() {
        return "VerifyRequest [result = " + result + ", transaction_id = " + transaction_id + ", response_code = " + response_code + ", amount = " + amount + ", currency = " + currency + ", pt_invoice_id = " + pt_invoice_id + ", reference_no = " + reference_no + "]";
    }
}

