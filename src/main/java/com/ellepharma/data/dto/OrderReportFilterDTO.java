package com.ellepharma.data.dto;

import com.ellepharma.data.model.*;

import java.util.Date;

public class OrderReportFilterDTO {

	private Long orderNumber;

	private Date fromDate;

	private Date toDate;

	private Long countryId;

	private GenderEnum gender;

	private PaymentType paymentType;

	private Coupon coupon;

	private PaymentStatusEnum paymentStatus;

	private ShipmentStatusEnum shipmentStatus;

	private Double fromTotal;

	private Double toTotal;

	private Boolean aramexSent;

	private Boolean emailSent;
	private Boolean status;

	public Long getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Long orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}

	public Double getFromTotal() {
		return fromTotal;
	}

	public void setFromTotal(Double fromTotal) {
		this.fromTotal = fromTotal;
	}

	public Double getToTotal() {
		return toTotal;
	}

	public void setToTotal(Double toTotal) {
		this.toTotal = toTotal;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public PaymentStatusEnum getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatusEnum paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public ShipmentStatusEnum getShipmentStatus() {
		return shipmentStatus;
	}

	public void setShipmentStatus(ShipmentStatusEnum shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}

	public Boolean getAramexSent() {

		return aramexSent;
	}

	public void setAramexSent(Boolean aramexSent) {
		this.aramexSent = aramexSent;
	}

	public Boolean getEmailSent() {
		return emailSent;
	}

	public void setEmailSent(Boolean emailSent) {
		this.emailSent = emailSent;
	}

	public Coupon getCoupon() {
		return coupon;
	}

	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
}
