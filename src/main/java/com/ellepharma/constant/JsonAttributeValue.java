package com.ellepharma.constant;

/**
 /**
 * JsonAttributeValue.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Apr 18, 2019
 */

public class JsonAttributeValue {

    ComEntityAttributeEnum entityAttributeEnum;

    Long value;

    public JsonAttributeValue(ComEntityAttributeEnum entityAttributeEnum, Long value) {
        this.entityAttributeEnum = entityAttributeEnum;
        this.value = value;
    }

    public ComEntityAttributeEnum getEntityAttributeEnum() {
        return entityAttributeEnum;
    }

    public void setEntityAttributeEnum(ComEntityAttributeEnum entityAttributeEnum) {
        this.entityAttributeEnum = entityAttributeEnum;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
