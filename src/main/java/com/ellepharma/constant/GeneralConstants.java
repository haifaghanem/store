package com.ellepharma.constant;

public class GeneralConstants {

    public static final int TRANS_FIELD_COLUMN_LENGTH = 4000;

    public static final int PASSWORD_LEVEL_COMPINATION_NO_MIN = 1;

    public static final int PASSWORD_LEVEL_COMPINATION_NO_MAX = 4;

}
