package com.ellepharma.constant;

public enum ConfigEnum {
    EMAIL(1L),
    MOBILE(2L),
    FACEBOOK(3L),
    TWITTER(4L),
    INSTAGRAM(5L),
    ADDRESS(6L),
    WORKING_TIME(7L),
    LOGO(8L),
    NAME(9L),
    MERCHANT_EMAIL(10L),
    MERCHANT_SECRET_KEY(11L),
    SITE_URL(12L),
    STORE_URL(13L),
    MAIL_USER(14L),
    MAIL_PASSWORD(15L),
    SEND_ORDERS_TO(16L),
    DEFAULT_ZONE_ID(16L),
    SYSTEM_FACES_RESOURCE_VERSION(18L),
    FACEBOOK_PAGE_ID(19L),
    ENABLE_SUPPORT(20L),
    ENABLE_PACKAGING_COMPANY(21L),
    ;

    private Long code;

    public Long getCode() {
        return code;
    }

    ConfigEnum(Long code) {
        this.code = code;
    }
}
