package com.ellepharma.constant;

public enum ComEntityAttributeEnum {
    CAMPUS_CAMPUSES(1L);

    private Long code;

    public Long getCode() {
        return code;
    }

    ComEntityAttributeEnum(Long code) {
        this.code = code;
    }
}
