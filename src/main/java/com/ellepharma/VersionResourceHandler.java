package com.ellepharma;

import com.ellepharma.constant.ConfigEnum;
import com.ellepharma.service.GlobalConfigProperties;
import org.apache.commons.lang3.StringUtils;

import javax.faces.application.Resource;
import javax.faces.application.ResourceHandler;
import javax.faces.application.ResourceHandlerWrapper;
import javax.faces.application.ResourceWrapper;

/**
 * VersionResourceHandler.java
 *
 * @author Malek Yaseen <malek.m.yaseen@gmail.com>
 * @since Jul 03, 2019
 */
public class VersionResourceHandler extends ResourceHandlerWrapper {

    private ResourceHandler wrapped;

    public VersionResourceHandler(ResourceHandler wrapped) {
        this.wrapped = wrapped;
    }


    @Override
    public Resource createResource(String resourceName, String libraryName, String contentType) {
        final Resource resource = super.createResource(resourceName, libraryName, contentType);

        if (resource == null) {
            return null;
        }

        return prepare(resource);
    }

    @Override
    public Resource createResource(String resourceName, String libraryName) {
        final Resource resource = super.createResource(resourceName, libraryName);

        if (resource == null) {
            return null;
        }

        return prepare(resource);
    }

    private Resource prepare(Resource resource) {
        return new ResourceWrapper() {

           /* @Override
            public String getRequestPath() {
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                String mapping = externalContext.getRequestServletPath();

                if (externalContext.getRequestPathInfo() == null) {
                    mapping = mapping.substring(mapping.lastIndexOf('.'));
                }

                String path = super.getRequestPath();

                if (mapping.charAt(0) == '/') {
                    return path.replaceFirst(mapping, "");
                } else if (path.contains("?")) {
                    return path.replace(mapping + "?", "?");
                } else {
                    return path.substring(0, path.length() - mapping.length());
                }
            }*/

            @Override
            public String getRequestPath() {
				String requestPath = super.getRequestPath();
				requestPath = requestPath.replace(".xhtml", "");
				if (!requestPath.contains("/faces/")) {
					requestPath = requestPath.replace("/javax.faces.resource", "/faces/javax.faces.resource");

				}
				String v = GlobalConfigProperties
						.getInstance().getProperty(ConfigEnum.SYSTEM_FACES_RESOURCE_VERSION.name());
				if (StringUtils.isEmpty(v)) {
					v = "1.0.0";
				}

				return requestPath + (requestPath.contains("?") ? "&" : "?") + "v=" + v;
			}

            @Override // Necessary because this is missing in ResourceWrapper (will be fixed in JSF 2.2).
            public String getResourceName() {
                return resource.getResourceName();
            }

            @Override // Necessary because this is missing in ResourceWrapper (will be fixed in JSF 2.2).
            public String getLibraryName() {
                return resource.getLibraryName();
            }

            @Override // Necessary because this is missing in ResourceWrapper (will be fixed in JSF 2.2).
            public String getContentType() {
                return resource.getContentType();
            }

            @Override
            public Resource getWrapped() {
                return resource;
            }
        };
    }




    @Override
    public ResourceHandler getWrapped() {
        return wrapped;
    }
}
